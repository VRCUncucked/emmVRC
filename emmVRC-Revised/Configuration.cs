﻿
using emmVRC.Libraries;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using emmVRCLoader;
using System;
using System.IO;
using UnityEngine;

namespace emmVRC
{
    public class Configuration
    {
        public static Config JSONConfig;

        public static void Initialize()
        {
            Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC"));
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json")))
            {
                try
                {
                    Configuration.JSONConfig = Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"))).Make<Config>();
                    if (Configuration.JSONConfig != null)
                        return;
                    emmVRCLoader.Logger.LogError("An error occured while parsing the config file. It has been moved to config.old.json, and a new one has been created.");
                    emmVRCLoader.Logger.LogError("Error: The parsed config was null...");
                    File.Move(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"), Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json"));
                    Configuration.JSONConfig = new Config();
                }
                catch (Exception ex)
                {
                    emmVRCLoader.Logger.LogError("An error occured while parsing the config file. It has been moved to config.old.json, and a new one has been created.");
                    emmVRCLoader.Logger.LogError("Error: " + ex.ToString());
                    if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json")))
                        File.Delete(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json"));
                    File.Move(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"), Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.old.json"));
                    Configuration.JSONConfig = new Config();
                }
            }
            else
                Configuration.JSONConfig = new Config();
        }

        public static void SaveConfig() => File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/config.json"), Encoder.Encode((object)Configuration.JSONConfig, EncodeOptions.PrettyPrint));

        public static Color menuColor() => ColorConversion.HexToColor(Configuration.JSONConfig.UIColorHex);

        public static Color defaultMenuColor() => new Color(0.05f, 0.65f, 0.68f);
    }
}
