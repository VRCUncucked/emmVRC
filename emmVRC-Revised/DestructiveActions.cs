﻿
using emmVRC.Network;
using System;
using System.Diagnostics;
using System.Threading;
using UnityEngine;

namespace emmVRC
{
    public class DestructiveActions
    {
        public static void ForceQuit()
        {
            if (NetworkClient.webToken != null)
                HTTPRequest.get(NetworkClient.baseURL + "/api/authentication/logout").NoAwait("Logout");
            new Thread(new ThreadStart(DestructiveActions.QuitAfterQuit))
            {
                IsBackground = true,
                Name = "emmVRC Quit Thread"
            }.Start();
        }

        public static void ForceRestart()
        {
            if (NetworkClient.webToken != null)
                HTTPRequest.get(NetworkClient.baseURL + "/api/authentication/logout").NoAwait("Logout");
            new Thread(new ThreadStart(DestructiveActions.RestartAfterQuit))
            {
                IsBackground = true,
                Name = "emmVRC Restart Thread"
            }.Start();
        }

        public static void RestartAfterQuit()
        {
            Application.Quit();
            Thread.Sleep(2500);
            try
            {
                Process.Start(Environment.CurrentDirectory + "\\VRChat.exe", Environment.CommandLine.ToString());
            }
            catch (Exception ex)
            {
                Exception exception = new Exception();
            }
            Process.GetCurrentProcess().Kill();
        }

        public static void QuitAfterQuit()
        {
            Application.Quit();
            Thread.Sleep(2500);
            Process.GetCurrentProcess().Kill();
        }
    }
}
