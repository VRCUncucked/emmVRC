﻿
using System;
using System.Threading.Tasks;
using VRC.Core;

namespace emmVRC
{
    public static class Extensions
    {
        public static bool HasCustomName(this APIUser user) => !string.IsNullOrWhiteSpace(user.displayName);

        public static string GetName(this APIUser user) => user.HasCustomName() ? user.displayName : user.username;

        public static void NoAwait(this Task task, string taskDescription = null) => task.ContinueWith((Action<Task>)(tsk =>
       {
           if (!tsk.IsFaulted)
               return;
           emmVRCLoader.Logger.LogError(string.Format("Free-floating Task {0}}} failed with exception: {1}", taskDescription == null ? (object)"" : (object)("(" + taskDescription + ")"), (object)tsk.Exception));
       }));
    }
}
