﻿
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Menus;
using emmVRCLoader;
using Il2CppSystem.Collections.Generic;
using MelonLoader;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class AlarmClock
    {
        public static bool AlarmEnabled = false;
        public static string AlarmTimeString = "00:00";
        public static System.TimeSpan alarmTimeActual;
        public static bool InstanceAlarmEnabled = false;
        public static string InstanceAlarmTimeString = "00:00";
        public static System.TimeSpan instanceAlarmTimeActual;
        public static bool AlarmTriggered = false;
        public static bool InstanceAlarmTriggered = false;
        private static System.Collections.Generic.List<AudioClip> alarmClips = new();
        private static AudioSource referenceSource;
        private static AudioSource alarmSource;
        public static bool Hour24 = false;
        public static QMNestedButton baseMenu;
        private static QMNestedButton alarmMenu;
        private static QMToggleButton alarmEnabled;
        private static QMToggleButton alarmPersistentEnabled;
        private static QMSingleButton alarmTime;
        private static QMNestedButton instanceAlarmMenu;
        private static QMToggleButton instanceAlarmEnabled;
        private static QMToggleButton instanceAlarmPersistentEnabled;
        private static QMSingleButton instanceAlarmTime;

        public static IEnumerator Initialize()
        {
            while ((UnityEngine.Object)Resources.onlineSprite == (UnityEngine.Object)null)
                yield return (object)new WaitForSeconds(1f);
            if (!Directory.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/AlarmSounds")))
                Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/AlarmSounds"));
            if (Directory.GetFiles(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/AlarmSounds")).Length != 0)
            {
                string[] strArray = Directory.GetFiles(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/AlarmSounds"));
                for (int index = 0; index < strArray.Length; ++index)
                {
                    string str = strArray[index];
                    if (str.Contains(".ogg") || str.Contains(".wav"))
                    {
                        emmVRCLoader.Logger.LogDebug("Processing alarm clip " + str);
                        UnityWebRequest CustomLoadingMusicRequest = UnityWebRequest.Get(string.Format("file://{0}", (object)str).Replace("\\", "/"));
                        CustomLoadingMusicRequest.SendWebRequest();
                        while (!CustomLoadingMusicRequest.isDone)
                            yield return (object)null;
                        AudioClip audioClip = (AudioClip)null;
                        if (CustomLoadingMusicRequest.isHttpError)
                            emmVRCLoader.Logger.LogError("Could not load music file: " + CustomLoadingMusicRequest.error);
                        else
                            audioClip = WebRequestWWW.InternalCreateAudioClipUsingDH(CustomLoadingMusicRequest.downloadHandler, CustomLoadingMusicRequest.url, false, false, AudioType.UNKNOWN);
                        audioClip.hideFlags |= HideFlags.DontUnloadUnusedAsset;
                        AlarmClock.alarmClips.Add(audioClip);
                        CustomLoadingMusicRequest = (UnityWebRequest)null;
                    }
                }
                strArray = (string[])null;
            }
            else
                AlarmClock.alarmClips.Add(Resources.alarmTone);
            while ((UnityEngine.Object)GameObject.Find("UserInterface/MenuContent/Popups/LoadingPopup") == (UnityEngine.Object)null)
                yield return (object)new WaitForEndOfFrame();
            AlarmClock.referenceSource = GameObject.Find("UserInterface/MenuContent/Popups/LoadingPopup").GetComponentInChildren<AudioSource>(true);
            GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(AlarmClock.referenceSource.gameObject);
            gameObject.transform.parent = (Transform)null;
            gameObject.name = "AlarmSound";
            UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object)gameObject);
            AlarmClock.alarmSource = gameObject.GetComponent<AudioSource>();
            AlarmClock.alarmSource.clip = (AudioClip)null;
            AlarmClock.Hour24 = !DateTimeFormatInfo.CurrentInfo.ShortTimePattern.ToLower().Contains("h");
            AlarmClock.alarmTimeActual = System.TimeSpan.FromSeconds((double)Configuration.JSONConfig.AlarmTime);
            System.DateTime dateTime1;
            string str1;
            if (!AlarmClock.Hour24)
            {
                dateTime1 = new System.DateTime() + AlarmClock.alarmTimeActual;
                str1 = dateTime1.ToString("hh:mm tt", (System.IFormatProvider)CultureInfo.InvariantCulture);
            }
            else
            {
                dateTime1 = new System.DateTime() + AlarmClock.alarmTimeActual;
                str1 = dateTime1.ToString("HH:mm");
            }
            AlarmClock.AlarmTimeString = str1;
            AlarmClock.AlarmEnabled = Configuration.JSONConfig.PersistentAlarm;
            AlarmClock.InstanceAlarmEnabled = Configuration.JSONConfig.PersistentInstanceAlarm;
            AlarmClock.instanceAlarmTimeActual = System.TimeSpan.FromSeconds((double)Configuration.JSONConfig.InstanceAlarmTime);
            dateTime1 = new System.DateTime();
            dateTime1 += AlarmClock.instanceAlarmTimeActual;
            AlarmClock.InstanceAlarmTimeString = dateTime1.ToString("HH:mm:ss");
            AlarmClock.baseMenu = new QMNestedButton(FunctionsMenu.baseMenu.menuBase, 19283, 10293, "", "");
            AlarmClock.baseMenu.getMainButton().DestroyMe();
            AlarmClock.alarmMenu = new QMNestedButton(AlarmClock.baseMenu, 1, 0, "Basic\nAlarm", "Configure the basic alarm clock, which uses system time");
            AlarmClock.alarmEnabled = new QMToggleButton(AlarmClock.alarmMenu, 1, 0, "Alarm\nEnabled", (System.Action)(() =>
           {
               AlarmClock.AlarmEnabled = true;
               AlarmClock.AlarmTriggered = false;
           }), "Disabled", (System.Action)(() =>
     {
         AlarmClock.AlarmEnabled = false;
         AlarmClock.AlarmTriggered = false;
     }), "TOGGLE: Enables the alarm clock for the time provided", defaultPosition: AlarmClock.AlarmEnabled);
            AlarmClock.alarmPersistentEnabled = new QMToggleButton(AlarmClock.alarmMenu, 2, 0, "Keep Alarm", (System.Action)(() =>
           {
               Configuration.JSONConfig.PersistentAlarm = true;
               Configuration.JSONConfig.AlarmTime = (uint)AlarmClock.alarmTimeActual.TotalSeconds;
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.PersistentAlarm = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Allow this alarm to stay on, even across restarts", defaultPosition: Configuration.JSONConfig.PersistentAlarm);
            AlarmClock.alarmTime = new QMSingleButton(AlarmClock.alarmMenu, 3, 0, "Time:\n" + AlarmClock.AlarmTimeString, (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter the alarm time", AlarmClock.AlarmTimeString, InputField.InputType.Standard, false, "Accept", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((time, keycodes, txt) =>
          {
              System.DateTime dateTime2 = System.DateTime.Parse(time);
              int hour = dateTime2.Hour;
              dateTime2 = System.DateTime.Parse(time);
              int minute = dateTime2.Minute;
              dateTime2 = System.DateTime.Parse(time);
              int second = dateTime2.Second;
              AlarmClock.alarmTimeActual = new System.TimeSpan(hour, minute, second);
              AlarmClock.AlarmTimeString = AlarmClock.Hour24 ? (new System.DateTime() + AlarmClock.alarmTimeActual).ToString("HH:mm") : (new System.DateTime() + AlarmClock.alarmTimeActual).ToString("hh:mm tt", (System.IFormatProvider)CultureInfo.InvariantCulture);
              if (Configuration.JSONConfig.PersistentAlarm)
              {
                  Configuration.JSONConfig.AlarmTime = (uint)AlarmClock.alarmTimeActual.TotalSeconds;
                  Configuration.SaveConfig();
              }
              AlarmClock.alarmTime.setButtonText("Time:\n" + AlarmClock.AlarmTimeString);
          }), (Il2CppSystem.Action)null, AlarmClock.Hour24 ? "00:00" : "00:00 PM")), "The time the alarm will go off. Select to change.");
            string str2;
            if (!AlarmClock.Hour24)
            {
                dateTime1 = new System.DateTime();
                dateTime1 += AlarmClock.alarmTimeActual;
                str2 = dateTime1.ToString("hh:mm tt");
            }
            else
            {
                dateTime1 = new System.DateTime();
                dateTime1 += AlarmClock.alarmTimeActual;
                str2 = dateTime1.ToString("HH:mm");
            }
            AlarmClock.AlarmTimeString = str2;
            AlarmClock.alarmTime.setButtonText("Time:\n" + AlarmClock.AlarmTimeString);
            AlarmClock.instanceAlarmMenu = new QMNestedButton(AlarmClock.baseMenu, 2, 0, "Instance\nAlarm", "Configure the instance alarm clock, which uses the time you've been in the current instance");
            AlarmClock.instanceAlarmEnabled = new QMToggleButton(AlarmClock.instanceAlarmMenu, 1, 0, "Instance\nAlarm Enabled", (System.Action)(() =>
           {
               AlarmClock.InstanceAlarmEnabled = true;
               AlarmClock.InstanceAlarmTriggered = false;
           }), "Disabled", (System.Action)(() =>
     {
         AlarmClock.InstanceAlarmEnabled = false;
         AlarmClock.InstanceAlarmTriggered = false;
     }), "TOGGLE: Enables the instance alarm clock for the time provided");
            AlarmClock.instanceAlarmPersistentEnabled = new QMToggleButton(AlarmClock.instanceAlarmMenu, 2, 0, "Keep Instance\nAlarm", (System.Action)(() =>
           {
               Configuration.JSONConfig.PersistentInstanceAlarm = true;
               Configuration.JSONConfig.InstanceAlarmTime = (uint)AlarmClock.instanceAlarmTimeActual.TotalSeconds;
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.PersistentInstanceAlarm = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Allow this alarm to stay on, even across restarts");
            AlarmClock.instanceAlarmTime = new QMSingleButton(AlarmClock.instanceAlarmMenu, 3, 0, "Instance\nTime:\n" + AlarmClock.InstanceAlarmTimeString, (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter the instance alarm time", AlarmClock.InstanceAlarmTimeString, InputField.InputType.Standard, false, "Accept", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((time, keycodes, txt) =>
          {
              System.DateTime dateTime2 = System.DateTime.Parse(time);
              int hour = dateTime2.Hour;
              dateTime2 = System.DateTime.Parse(time);
              int minute = dateTime2.Minute;
              dateTime2 = System.DateTime.Parse(time);
              int second = dateTime2.Second;
              AlarmClock.instanceAlarmTimeActual = new System.TimeSpan(hour, minute, second);
              AlarmClock.InstanceAlarmTimeString = (new System.DateTime() + AlarmClock.instanceAlarmTimeActual).ToString("HH:mm:ss");
              if (Configuration.JSONConfig.PersistentInstanceAlarm)
              {
                  Configuration.JSONConfig.InstanceAlarmTime = (uint)AlarmClock.instanceAlarmTimeActual.TotalSeconds;
                  Configuration.SaveConfig();
              }
              AlarmClock.instanceAlarmTime.setButtonText("Time:\n" + AlarmClock.InstanceAlarmTimeString);
          }), (Il2CppSystem.Action)null, "00:00")), "The time the instance alarm will go off. Select to change.");
            MelonCoroutines.Start(AlarmClock.Loop());
        }

        public static IEnumerator Loop()
        {
            while (true)
            {
                do
                {
                    yield return (object)new WaitForFixedUpdate();
                    if ((UnityEngine.Object)AlarmClock.alarmSource != (UnityEngine.Object)null && (UnityEngine.Object)AlarmClock.referenceSource != (UnityEngine.Object)null)
                    {
                        AlarmClock.alarmSource.volume = AlarmClock.referenceSource.volume;
                        AlarmClock.alarmSource.outputAudioMixerGroup = AlarmClock.referenceSource.outputAudioMixerGroup;
                    }
                    System.DateTime dateTime;
                    if (AlarmClock.AlarmEnabled && !AlarmClock.AlarmTriggered)
                    {
                        dateTime = System.DateTime.Now;
                        if (dateTime.TimeOfDay.Hours == AlarmClock.alarmTimeActual.Hours)
                        {
                            dateTime = System.DateTime.Now;
                            if (dateTime.TimeOfDay.Minutes == AlarmClock.alarmTimeActual.Minutes)
                            {
                                dateTime = System.DateTime.Now;
                                if (dateTime.TimeOfDay.Seconds == AlarmClock.alarmTimeActual.Seconds && !AlarmClock.AlarmTriggered)
                                {
                                    AlarmClock.AlarmTriggered = true;
                                    emmVRCLoader.Logger.LogDebug("Alarm triggered");
                                    AlarmClock.alarmSource.clip = AlarmClock.alarmClips.Count > 1 ? AlarmClock.alarmClips[new System.Random().Next(AlarmClock.alarmClips.Count)] : AlarmClock.alarmClips.First<AudioClip>();
                                    if ((UnityEngine.Object)AlarmClock.alarmSource.clip == (UnityEngine.Object)null)
                                        AlarmClock.alarmSource.clip = Resources.alarmTone;
                                    AlarmClock.alarmSource.Play();
                                    string str;
                                    if (!AlarmClock.Hour24)
                                    {
                                        dateTime = new System.DateTime();
                                        dateTime += AlarmClock.alarmTimeActual;
                                        str = dateTime.ToString("HH:mm tt");
                                    }
                                    else
                                    {
                                        dateTime = new System.DateTime();
                                        dateTime += AlarmClock.alarmTimeActual;
                                        str = dateTime.ToString("hh:mm");
                                    }
                                    Managers.NotificationManager.AddNotification("Your alarm for " + str + " is ringing.", "Snooze\n(15 min)", (System.Action)(() =>
                                   {
                                       AlarmClock.alarmTimeActual += new System.TimeSpan(0, 15, 0);
                                       AlarmClock.AlarmTimeString = AlarmClock.Hour24 ? (new System.DateTime() + AlarmClock.alarmTimeActual).ToString("HH:mm") : (new System.DateTime() + AlarmClock.alarmTimeActual).ToString("hh:mm tt", (System.IFormatProvider)CultureInfo.InvariantCulture);
                                       AlarmClock.AlarmTriggered = false;
                                       AlarmClock.alarmTime.setButtonText("Time:\n" + AlarmClock.AlarmTimeString);
                                       Managers.NotificationManager.DismissCurrentNotification();
                                   }), "Dismiss", (System.Action)(() =>
                 {
                     AlarmClock.AlarmTriggered = false;
                     if (!Configuration.JSONConfig.PersistentAlarm)
                     {
                         AlarmClock.AlarmEnabled = false;
                         AlarmClock.alarmEnabled.setToggleState(false);
                     }
                     Managers.NotificationManager.DismissCurrentNotification();
                 }), Resources.alarmSprite);
                                }
                            }
                        }
                    }
                    if (AlarmClock.InstanceAlarmEnabled && !AlarmClock.InstanceAlarmTriggered && System.TimeSpan.FromSeconds((double)InfoBarClock.instanceTime) == AlarmClock.instanceAlarmTimeActual)
                    {
                        AlarmClock.InstanceAlarmTriggered = true;
                        emmVRCLoader.Logger.LogDebug("Instance Alarm triggered");
                        AlarmClock.alarmSource.clip = AlarmClock.alarmClips.Count > 1 ? AlarmClock.alarmClips[new System.Random().Next(AlarmClock.alarmClips.Count)] : AlarmClock.alarmClips.First<AudioClip>();
                        if ((UnityEngine.Object)AlarmClock.alarmSource.clip == (UnityEngine.Object)null)
                            AlarmClock.alarmSource.clip = Resources.alarmTone;
                        AlarmClock.alarmSource.Play();
                        dateTime = new System.DateTime();
                        dateTime += AlarmClock.instanceAlarmTimeActual;
                        Managers.NotificationManager.AddNotification("Your instance alarm for " + dateTime.ToString("HH:mm:ss") + " is ringing.", "Snooze\n(15 min)", (System.Action)(() =>
                       {
                           AlarmClock.instanceAlarmTimeActual += new System.TimeSpan(0, 15, 0);
                           AlarmClock.InstanceAlarmTriggered = false;
                           AlarmClock.InstanceAlarmTimeString = (new System.DateTime() + AlarmClock.instanceAlarmTimeActual).ToString("HH:mm:ss");
                           AlarmClock.instanceAlarmTime.setButtonText("Time:\n" + AlarmClock.InstanceAlarmTimeString);
                           Managers.NotificationManager.DismissCurrentNotification();
                       }), "Dismiss", (System.Action)(() =>
           {
               AlarmClock.InstanceAlarmTriggered = false;
               if (!Configuration.JSONConfig.PersistentInstanceAlarm)
               {
                   AlarmClock.InstanceAlarmEnabled = false;
                   AlarmClock.instanceAlarmEnabled.setToggleState(false);
               }
               Managers.NotificationManager.DismissCurrentNotification();
           }), Resources.alarmSprite);
                    }
                }
                while (!AlarmClock.alarmSource.isPlaying || (AlarmClock.AlarmEnabled || AlarmClock.InstanceAlarmEnabled) && (AlarmClock.AlarmTriggered || AlarmClock.InstanceAlarmTriggered));
                emmVRCLoader.Logger.LogDebug("Alarm stopped");
                AlarmClock.alarmSource.Stop();
            }
        }
    }
}
