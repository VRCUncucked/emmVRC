﻿
using emmVRC.Libraries;
using MelonLoader;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class AvatarMenu
    {
        private static GameObject avatarScreen;
        private static GameObject hotWorldsViewPort;
        private static GameObject hotWorldsButton;
        private static GameObject randomWorldsViewPort;
        private static GameObject randomWorldsButton;
        private static GameObject personalList;
        private static GameObject legacyList;
        private static GameObject publicList;
        private static GameObject otherList;
        private static UiAvatarList otherListList;

        public static void Initialize()
        {
            AvatarMenu.avatarScreen = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar").gameObject;
            AvatarMenu.hotWorldsViewPort = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (What's Hot)/ViewPort").gameObject;
            AvatarMenu.hotWorldsButton = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (What's Hot)/Button").gameObject;
            AvatarMenu.randomWorldsViewPort = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (Random)/ViewPort").gameObject;
            AvatarMenu.randomWorldsButton = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Avatar Worlds (Random)/Button").gameObject;
            AvatarMenu.personalList = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Personal Avatar List").gameObject;
            AvatarMenu.legacyList = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Legacy Avatar List").gameObject;
            AvatarMenu.publicList = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Public Avatar List").gameObject;
            AvatarMenu.otherList = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Licensed Avatar List (1)").gameObject;
            if ((UnityEngine.Object)AvatarMenu.otherList == (UnityEngine.Object)null)
                AvatarMenu.otherList = AvatarMenu.avatarScreen.transform.Find("Vertical Scroll View/Viewport/Content/Licensed Avatar List").gameObject;
            if ((UnityEngine.Object)AvatarMenu.otherList != (UnityEngine.Object)null)
                AvatarMenu.otherListList = AvatarMenu.otherList.GetComponent<UiAvatarList>();
            MelonCoroutines.Start(AvatarMenu.Loop());
        }

        private static IEnumerator Loop()
        {
            while (true)
            {
                if (Configuration.JSONConfig.DisableAvatarHotWorlds)
                {
                    AvatarMenu.hotWorldsViewPort.SetActive(false);
                    AvatarMenu.hotWorldsButton.SetActive(false);
                }
                else
                {
                    AvatarMenu.hotWorldsViewPort.SetActive(true);
                    AvatarMenu.hotWorldsButton.SetActive(true);
                }
                if (Configuration.JSONConfig.DisableAvatarRandomWorlds)
                {
                    AvatarMenu.randomWorldsViewPort.SetActive(false);
                    AvatarMenu.randomWorldsButton.SetActive(false);
                }
                else
                {
                    AvatarMenu.randomWorldsViewPort.SetActive(true);
                    AvatarMenu.randomWorldsButton.SetActive(true);
                }
                if (Configuration.JSONConfig.DisableAvatarPersonal)
                    AvatarMenu.personalList.SetActive(false);
                else
                    AvatarMenu.personalList.SetActive(true);
                if (Configuration.JSONConfig.DisableAvatarLegacy)
                    AvatarMenu.legacyList.SetActive(false);
                else
                    AvatarMenu.legacyList.SetActive(true);
                if (Configuration.JSONConfig.DisableAvatarPublic)
                    AvatarMenu.publicList.SetActive(false);
                else
                    AvatarMenu.publicList.SetActive(true);
                if (Configuration.JSONConfig.DisableAvatarOther && (UnityEngine.Object)AvatarMenu.otherListList != (UnityEngine.Object)null)
                    AvatarMenu.otherList.SetActive(false);
                else if (!Configuration.JSONConfig.DisableAvatarOther && (UnityEngine.Object)AvatarMenu.otherListList != (UnityEngine.Object)null && AvatarMenu.otherListList.pickers.ToArray().Any<VRCUiContentButton>((Func<VRCUiContentButton, bool>)(a => a.isActiveAndEnabled)))
                    AvatarMenu.otherList.SetActive(true);
                yield return (object)new WaitForSeconds(1f);
            }
        }
    }
}
