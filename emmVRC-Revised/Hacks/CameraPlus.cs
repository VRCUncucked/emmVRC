﻿
using emmVRC.Objects;
using System;
using System.Collections;
using UnityEngine;
using VRC.UserCamera;

namespace emmVRC.Hacks
{
    public class CameraPlus
    {
        public static GameObject zoomInButton;
        public static GameObject zoomOutButton;
        public static GameObject toggleCameraIndicatorButton;
        private static Transform camera_body;

        public static IEnumerator Initialize()
        {
            while ((UnityEngine.Object)Resources.zoomIn == (UnityEngine.Object)null || (UnityEngine.Object)Resources.zoomOut == (UnityEngine.Object)null || ((UnityEngine.Object)Resources.lensOff == (UnityEngine.Object)null || (UnityEngine.Object)Resources.lensOn == (UnityEngine.Object)null))
                yield return (object)new WaitForEndOfFrame();
            UserCameraController userCameraController = UnityEngine.Resources.FindObjectsOfTypeAll<UserCameraController>()[0];
            CameraPlus.zoomInButton = UnityEngine.Object.Instantiate<GameObject>(((Component)userCameraController).transform.Find("ViewFinder/PhotoControls/Right_Filters").gameObject, ((Component)userCameraController).transform.Find("ViewFinder"));
            VRC_UdonTrigger.Instantiate(CameraPlus.zoomInButton, "Zoom-In", (Action)(() =>
           {
               Camera component1 = ((Component)userCameraController).transform.Find("PhotoCamera").GetComponent<Camera>();
               if ((double)component1.fieldOfView - 10.0 > 0.0)
                   component1.fieldOfView -= 10f;
               Camera component2 = ((Component)userCameraController).transform.Find("PhotoCamera/VideoCamera").GetComponent<Camera>();
               if ((double)component2.fieldOfView - 10.0 > 0.0)
                   component2.fieldOfView -= 10f;
               userCameraController.field_Public_AudioSource_0.PlayOneShot(userCameraController.field_Public_AudioClip_0);
           }));
            CameraPlus.SetButtonSprite(CameraPlus.zoomInButton, Resources.zoomIn);
            CameraPlus.SetButtonIconScale(CameraPlus.zoomInButton);
            CameraPlus.SetButtonOffset(CameraPlus.zoomInButton);
            CameraPlus.zoomOutButton = UnityEngine.Object.Instantiate<GameObject>(((Component)userCameraController).transform.Find("ViewFinder/PhotoControls/Right_Lock").gameObject, ((Component)userCameraController).transform.Find("ViewFinder"));
            VRC_UdonTrigger.Instantiate(CameraPlus.zoomOutButton, "Zoom-Out", (Action)(() =>
           {
               Camera component1 = ((Component)userCameraController).transform.Find("PhotoCamera").GetComponent<Camera>();
               if ((double)component1.fieldOfView + 10.0 < 120.0)
                   component1.fieldOfView += 10f;
               Camera component2 = ((Component)userCameraController).transform.Find("PhotoCamera/VideoCamera").GetComponent<Camera>();
               if ((double)component2.fieldOfView + 10.0 < 120.0)
                   component2.fieldOfView += 10f;
               userCameraController.field_Public_AudioSource_0.PlayOneShot(userCameraController.field_Public_AudioClip_0);
           }));
            CameraPlus.SetButtonSprite(CameraPlus.zoomOutButton, Resources.zoomOut);
            CameraPlus.SetButtonIconScale(CameraPlus.zoomOutButton);
            CameraPlus.SetButtonOffset(CameraPlus.zoomOutButton);
            GameObject cameraHelper = ((Component)userCameraController).transform.Find("PhotoCamera/camera_lens_mesh").gameObject;
            CameraPlus.toggleCameraIndicatorButton = UnityEngine.Object.Instantiate<GameObject>(((Component)userCameraController).transform.Find("ViewFinder/PhotoControls/Right_Timer").gameObject, ((Component)userCameraController).transform.Find("ViewFinder"));
            VRC_UdonTrigger.Instantiate(CameraPlus.toggleCameraIndicatorButton, "Camera Indicator", (Action)(() =>
           {
               cameraHelper.SetActive(!cameraHelper.activeSelf);
               if (cameraHelper.activeSelf)
                   CameraPlus.SetButtonSprite(CameraPlus.toggleCameraIndicatorButton, Resources.lensOn);
               else
                   CameraPlus.SetButtonSprite(CameraPlus.toggleCameraIndicatorButton, Resources.lensOff);
               userCameraController.field_Public_AudioSource_0.PlayOneShot(userCameraController.field_Public_AudioClip_0);
           }));
            CameraPlus.SetButtonSprite(CameraPlus.toggleCameraIndicatorButton, Resources.lensOn);
            CameraPlus.SetButtonIconScale(CameraPlus.toggleCameraIndicatorButton);
            CameraPlus.SetButtonOffset(CameraPlus.toggleCameraIndicatorButton);
            CameraPlus.camera_body = ((Component)userCameraController).transform.Find("ViewFinder/camera_mesh/body");
            CameraPlus.camera_body.localPosition += new Vector3(-0.025f, 0.0f, 0.0f);
            CameraPlus.camera_body.localScale += new Vector3(0.8f, 0.0f, 0.0f);
            CameraPlus.SetCameraPlus();
        }

        private static void SetButtonOffset(GameObject button) => button.transform.localPosition += new Vector3(-0.05f, 0.0f, 0.0f);

        private static void SetButtonIconScale(GameObject button)
        {
            foreach (Transform componentsInChild in button.GetComponentsInChildren<Transform>(true))
            {
                if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null && componentsInChild.gameObject.name.StartsWith("Icon"))
                    componentsInChild.localScale *= 1.25f;
            }
        }

        private static void SetButtonSprite(GameObject button, Sprite sp)
        {
            foreach (Transform componentsInChild in button.GetComponentsInChildren<Transform>(true))
            {
                if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null && componentsInChild.gameObject.name.StartsWith("Icon"))
                    componentsInChild.GetComponent<SpriteRenderer>().sprite = sp;
            }
        }

        public static void SetCameraPlus()
        {
            if (Configuration.JSONConfig.CameraPlus)
            {
                CameraPlus.camera_body.localPosition = new Vector3(-0.025f, 0.0f, 0.0f);
                CameraPlus.camera_body.localScale = new Vector3(6.3317f, CameraPlus.camera_body.localScale.y, CameraPlus.camera_body.localScale.z);
                CameraPlus.zoomInButton.SetActive(true);
                CameraPlus.zoomOutButton.SetActive(true);
                CameraPlus.toggleCameraIndicatorButton.SetActive(true);
            }
            else
            {
                CameraPlus.camera_body.localPosition = Vector3.zero;
                CameraPlus.camera_body.localScale = new Vector3(5.5317f, CameraPlus.camera_body.localScale.y, CameraPlus.camera_body.localScale.z);
                CameraPlus.zoomInButton.SetActive(false);
                CameraPlus.zoomOutButton.SetActive(false);
                CameraPlus.toggleCameraIndicatorButton.SetActive(false);
            }
        }
    }
}
