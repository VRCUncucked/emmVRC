﻿
using emmVRC.Libraries;
using emmVRC.Menus;
using emmVRCLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    internal static class ColorChanger
    {
        private static List<Image> normalColorImage;
        private static List<Image> dimmerColorImage;
        private static List<Image> darkerColorImage;
        private static List<Text> normalColorText;
        private static bool setupSkybox;
        private static GameObject loadingBackground;
        private static GameObject initialLoadingBackground;

        public static void ApplyIfApplicable()
        {
            Color color1 = Configuration.JSONConfig.UIColorChangingEnabled ? Configuration.menuColor() : Configuration.defaultMenuColor();
            Color color2 = new Color(color1.r, color1.g, color1.b, 0.7f);
            Color color3 = new Color(color1.r / 0.75f, color1.g / 0.75f, color1.b / 0.75f);
            Color color4 = new Color(color1.r / 0.75f, color1.g / 0.75f, color1.b / 0.75f, 0.9f);
            Color color5 = new Color(color1.r / 2.5f, color1.g / 2.5f, color1.b / 2.5f);
            Color color6 = new Color(color1.r / 2.5f, color1.g / 2.5f, color1.b / 2.5f, 0.9f);
            if (ColorChanger.normalColorImage == null || ColorChanger.normalColorImage.Count == 0)
            {
                emmVRCLoader.Logger.LogDebug("Gathering elements to color normally...");
                ColorChanger.normalColorImage = new List<Image>();
                GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent();
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Description_SafetyLevel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Custom/ON").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_None/ON").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Normal/ON").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Maxiumum/ON").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/InputKeypadPopup/Rectangle/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/InputKeypadPopup/InputField").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/StandardPopupV2/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/StandardPopup/InnerDashRing").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/StandardPopup/RingGlow").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/UpdateStatusPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/InputPopup/InputField").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/UpdateStatusPopup/Popup/InputFieldStatus").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/AdvancedSettingsPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/AddToPlaylistPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/BookmarkFriendPopup/Popup/Panel (2)").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/EditPlaylistPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/PerformanceSettingsPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/AlertPopup/Lighter").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/RoomInstancePopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/ReportWorldPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/ReportUserPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/AddToAvatarFavoritesPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/SearchOptionsPopup/Popup/Panel (1)").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/SendInvitePopup/SendInviteMenu/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/RequestInvitePopup/RequestInviteMenu/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/ControllerBindingsPopup/Popup/Panel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/ChangeProfilePicPopup/Popup/PanelBackground").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/ChangeProfilePicPopup/Popup/TitlePanel").GetComponent<Image>());
                ColorChanger.normalColorImage.Add(gameObject.transform.Find("Screens/UserInfo/User Panel/PanelHeaderBackground").GetComponent<Image>());
                foreach (Component component in gameObject.GetComponentsInChildren<Transform>(true).Where<Transform>((Func<Transform, bool>)(x => x.name.Contains("Panel_Header"))))
                {
                    foreach (Image componentsInChild in component.GetComponentsInChildren<Image>())
                        ColorChanger.normalColorImage.Add(componentsInChild);
                }
                foreach (Component component in gameObject.GetComponentsInChildren<Transform>(true).Where<Transform>((Func<Transform, bool>)(x => x.name.Contains("Handle"))))
                {
                    foreach (Image componentsInChild in component.GetComponentsInChildren<Image>())
                        ColorChanger.normalColorImage.Add(componentsInChild);
                }
                try
                {
                    ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Panel_Backdrop").GetComponent<Image>());
                    ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Decoration_Left").GetComponent<Image>());
                    ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/ProgressPanel/Parent_Loading_Progress/Decoration_Right").GetComponent<Image>());
                    ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Panel_Backdrop").GetComponent<Image>());
                    ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Decoration_Left").GetComponent<Image>());
                    ColorChanger.normalColorImage.Add(gameObject.transform.Find("Popups/LoadingPopup/MirroredElements/ProgressPanel (1)/Parent_Loading_Progress/Decoration_Right").GetComponent<Image>());
                }
                catch (Exception ex)
                {
                    Exception exception = new Exception();
                }
            }
            if (ColorChanger.dimmerColorImage == null || ColorChanger.dimmerColorImage.Count == 0)
            {
                emmVRCLoader.Logger.LogDebug("Gathering elements to color lighter...");
                ColorChanger.dimmerColorImage = new List<Image>();
                GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent();
                ColorChanger.dimmerColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Custom/ON/TopPanel_SafetyLevel").GetComponent<Image>());
                ColorChanger.dimmerColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_None/ON/TopPanel_SafetyLevel").GetComponent<Image>());
                ColorChanger.dimmerColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Normal/ON/TopPanel_SafetyLevel").GetComponent<Image>());
                ColorChanger.dimmerColorImage.Add(gameObject.transform.Find("Screens/Settings_Safety/_Buttons_SafetyLevel/Button_Maxiumum/ON/TopPanel_SafetyLevel").GetComponent<Image>());
                ColorChanger.dimmerColorImage.Add(gameObject.transform.Find("Popups/ChangeProfilePicPopup/Popup/BorderImage").GetComponent<Image>());
                foreach (Component component in gameObject.GetComponentsInChildren<Transform>(true).Where<Transform>((Func<Transform, bool>)(x => x.name.Contains("Fill"))))
                {
                    foreach (Image componentsInChild in component.GetComponentsInChildren<Image>())
                        ColorChanger.dimmerColorImage.Add(componentsInChild);
                }
            }
            if (ColorChanger.darkerColorImage == null || ColorChanger.darkerColorImage.Count == 0)
            {
                emmVRCLoader.Logger.LogDebug("Gathering elements to color darker...");
                ColorChanger.darkerColorImage = new List<Image>();
                GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent();
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/InputKeypadPopup/Rectangle").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/StandardPopupV2/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/StandardPopup/Rectangle").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/StandardPopup/MidRing").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/UpdateStatusPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/AdvancedSettingsPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/AddToPlaylistPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/BookmarkFriendPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/EditPlaylistPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/PerformanceSettingsPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/RoomInstancePopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/RoomInstancePopup/Popup/BorderImage (1)").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/ReportWorldPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/ReportUserPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/AddToAvatarFavoritesPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/SearchOptionsPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/SendInvitePopup/SendInviteMenu/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/RequestInvitePopup/RequestInviteMenu/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Popups/ControllerBindingsPopup/Popup/BorderImage").GetComponent<Image>());
                ColorChanger.darkerColorImage.Add(gameObject.transform.Find("Screens/UserInfo/ModerateDialog/Panel/BorderImage").GetComponent<Image>());
                foreach (Component component in gameObject.GetComponentsInChildren<Transform>(true).Where<Transform>((Func<Transform, bool>)(x => x.name.Contains("Background") && x.name != "PanelHeaderBackground" && !x.transform.parent.name.Contains("UserIcon"))))
                {
                    foreach (Image componentsInChild in component.GetComponentsInChildren<Image>())
                        ColorChanger.darkerColorImage.Add(componentsInChild);
                }
            }
            if (ColorChanger.normalColorText == null || ColorChanger.normalColorText.Count == 0)
            {
                emmVRCLoader.Logger.LogDebug("Gathering text elements to color...");
                ColorChanger.normalColorText = new List<Text>();
                GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent();
                foreach (Text componentsInChild in gameObject.transform.Find("Popups/InputPopup/Keyboard/Keys").GetComponentsInChildren<Text>(true))
                    ColorChanger.normalColorText.Add(componentsInChild);
                foreach (Text componentsInChild in gameObject.transform.Find("Popups/InputKeypadPopup/Keyboard/Keys").GetComponentsInChildren<Text>(true))
                    ColorChanger.normalColorText.Add(componentsInChild);
            }
            emmVRCLoader.Logger.LogDebug("Coloring normal elements...");
            foreach (Graphic graphic in ColorChanger.normalColorImage)
                graphic.color = color2;
            emmVRCLoader.Logger.LogDebug("Coloring lighter elements...");
            foreach (Graphic graphic in ColorChanger.dimmerColorImage)
                graphic.color = color4;
            emmVRCLoader.Logger.LogDebug("Coloring darker elements...");
            foreach (Graphic graphic in ColorChanger.darkerColorImage)
                graphic.color = color6;
            emmVRCLoader.Logger.LogDebug("Coloring text elements...");
            foreach (Graphic graphic in ColorChanger.normalColorText)
                graphic.color = color1;
            if (!ColorChanger.setupSkybox)
            {
                if (!ModCompatibility.BetterLoadingScreen)
                {
                    try
                    {
                        emmVRCLoader.Logger.LogDebug("Setting up skybox coloring...");
                        ColorChanger.loadingBackground = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup/3DElements/LoadingBackground_TealGradient/SkyCube_Baked").gameObject;
                        ColorChanger.loadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", (Texture)Resources.blankGradient);
                        ColorChanger.loadingBackground.GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(color1.r / 2f, color1.g / 2f, color1.b / 2f, color1.a));
                        ColorChanger.loadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", (Texture)Resources.blankGradient);
                        ColorChanger.setupSkybox = true;
                        ColorChanger.initialLoadingBackground = GameObject.Find("LoadingBackground_TealGradient_Music/SkyCube_Baked");
                        ColorChanger.initialLoadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", (Texture)Resources.blankGradient);
                        ColorChanger.initialLoadingBackground.GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(color1.r / 2f, color1.g / 2f, color1.b / 2f, color1.a));
                        ColorChanger.initialLoadingBackground.GetComponent<MeshRenderer>().material.SetTexture("_Tex", (Texture)Resources.blankGradient);
                    }
                    catch (Exception ex)
                    {
                        emmVRCLoader.Logger.LogError(ex.ToString());
                    }
                }
            }
            if (ColorChanger.setupSkybox && (UnityEngine.Object)ColorChanger.loadingBackground != (UnityEngine.Object)null && !ModCompatibility.BetterLoadingScreen)
            {
                emmVRCLoader.Logger.LogDebug("Coloring skybox...");
                ColorChanger.loadingBackground.GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(color1.r / 2f, color1.g / 2f, color1.b / 2f, color1.a));
            }
            ColorBlock colorBlock1 = new ColorBlock()
            {
                colorMultiplier = 1f,
                disabledColor = Color.grey,
                highlightedColor = color1 * 1.5f,
                normalColor = color1 / 1.5f,
                pressedColor = new Color(1f, 1f, 1f, 1f),
                fadeDuration = 0.1f,
                selectedColor = color1 / 1.5f
            };
            color1.a = 0.9f;
            if (UnityEngine.Resources.FindObjectsOfTypeAll<HighlightsFXStandalone>().Count != 0)
                UnityEngine.Resources.FindObjectsOfTypeAll<HighlightsFXStandalone>().FirstOrDefault<HighlightsFXStandalone>().highlightColor = color1;
            try
            {
                if (Configuration.JSONConfig.UIMicIconColorChangingEnabled && Configuration.JSONConfig.UIColorChangingEnabled)
                {
                    emmVRCLoader.Logger.LogDebug("Coloring Push To Talk icons...");
                    foreach (Image componentsInChild in ((Component)UnityEngine.Object.FindObjectOfType<HudVoiceIndicator>()).transform.GetComponentsInChildren<Image>())
                    {
                        if (componentsInChild.gameObject.name != "PushToTalkKeybd" && componentsInChild.gameObject.name != "PushToTalkXbox")
                            componentsInChild.color = color1;
                    }
                }
                else
                {
                    emmVRCLoader.Logger.LogDebug("Decoloring Push To Talk icons...");
                    foreach (Image componentsInChild in ((Component)UnityEngine.Object.FindObjectOfType<HudVoiceIndicator>()).transform.GetComponentsInChildren<Image>())
                    {
                        if (componentsInChild.gameObject.name != "PushToTalkKeybd" && componentsInChild.gameObject.name != "PushToTalkXbox")
                            componentsInChild.color = Color.red;
                    }
                }
                foreach (Component component in UnityEngine.Object.FindObjectsOfType<HudVoiceIndicator>())
                    component.transform.Find("VoiceDotDisabled").GetComponent<FadeCycleEffect>().enabled = Configuration.JSONConfig.UIMicIconPulsingEnabled;
            }
            catch (Exception ex)
            {
                Exception exception = new Exception();
            }
            if (!((UnityEngine.Object)QuickMenuUtils.GetVRCUiMInstance().menuContent() != (UnityEngine.Object)null))
                return;
            GameObject gameObject1 = QuickMenuUtils.GetVRCUiMInstance().menuContent();
            emmVRCLoader.Logger.LogDebug("Coloring input popup...");
            try
            {
                Transform transform1 = gameObject1.transform.Find("Popups/InputPopup");
                color5.a = 0.8f;
                transform1.Find("Rectangle").GetComponent<Image>().color = color5;
                color5.a = 0.5f;
                color1.a = 0.8f;
                transform1.Find("Rectangle/Panel").GetComponent<Image>().color = color1;
                color1.a = 0.5f;
                Transform transform2 = gameObject1.transform.Find("Backdrop/Header/Tabs/ViewPort/Content/Search");
                transform2.Find("SearchTitle").GetComponent<Text>().color = color1;
                transform2.Find("InputField").GetComponent<Image>().color = color1;
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError(ex.ToString());
            }
            emmVRCLoader.Logger.LogDebug("Coloring QM buttons...");
            try
            {
                ColorBlock colorBlock2 = new ColorBlock()
                {
                    colorMultiplier = 1f,
                    disabledColor = Color.grey,
                    highlightedColor = color5,
                    normalColor = color1,
                    pressedColor = Color.gray,
                    fadeDuration = 0.1f
                };
                gameObject1.GetComponentsInChildren<Transform>(true).FirstOrDefault<Transform>((Func<Transform, bool>)(x => x.name == "Row:4 Column:0")).GetComponent<UnityEngine.UI.Button>().colors = colorBlock1;
                color1.a = 0.5f;
                color5.a = 1f;
                colorBlock2.normalColor = color5;
                foreach (Selectable componentsInChild in gameObject1.GetComponentsInChildren<Slider>(true))
                    componentsInChild.colors = colorBlock2;
                color5.a = 0.5f;
                colorBlock2.normalColor = color1;
                foreach (UnityEngine.UI.Button componentsInChild in gameObject1.GetComponentsInChildren<UnityEngine.UI.Button>(true))
                {
                    if (componentsInChild.gameObject.name != "SubscribeToAddPhotosButton" && componentsInChild.gameObject.name != "SupporterButton" && componentsInChild.gameObject.name != "ModerateButton" && (componentsInChild.gameObject.name != "ReportButton" || componentsInChild.transform.parent.name.Contains("WorldInfo")))
                        componentsInChild.colors = colorBlock1;
                }
                GameObject gameObject2 = GameObject.Find("QuickMenu");
                foreach (UnityEngine.UI.Button componentsInChild in gameObject2.GetComponentsInChildren<UnityEngine.UI.Button>(true))
                {
                    if (componentsInChild.gameObject.name != "rColorButton" && componentsInChild.gameObject.name != "gColorButton" && (componentsInChild.gameObject.name != "bColorButton" && componentsInChild.gameObject.name != "ColorPickPreviewButton") && (componentsInChild.transform.parent.name != "SocialNotifications") && (componentsInChild.gameObject.name != (VRHUD.ToggleHUDButton == null ? "" : VRHUD.ToggleHUDButton.getGameObject().name) && componentsInChild.transform.parent.parent.name != "EmojiMenu" && !componentsInChild.transform.parent.name.Contains("NotificationUiPrefab")))
                        componentsInChild.colors = colorBlock1;
                }
                foreach (Component componentsInChild1 in gameObject2.GetComponentsInChildren<UiToggleButton>(true))
                {
                    foreach (Graphic componentsInChild2 in componentsInChild1.GetComponentsInChildren<Image>(true))
                        componentsInChild2.color = color1;
                }
                foreach (Slider componentsInChild1 in gameObject2.GetComponentsInChildren<Slider>(true))
                {
                    componentsInChild1.colors = colorBlock2;
                    foreach (Graphic componentsInChild2 in componentsInChild1.GetComponentsInChildren<Image>(true))
                        componentsInChild2.color = color1;
                }
                foreach (Toggle componentsInChild1 in gameObject2.GetComponentsInChildren<Toggle>(true))
                {
                    componentsInChild1.colors = colorBlock2;
                    foreach (Graphic componentsInChild2 in componentsInChild1.GetComponentsInChildren<Image>(true))
                        componentsInChild2.color = color1;
                }
                foreach (Image componentsInChild in GameObject.Find("UserInterface/QuickMenu/QuickModeMenus/QuickModeNotificationsMenu/ScrollRect").GetComponentsInChildren<Image>(true))
                {
                    if (componentsInChild.transform.name == "Background")
                        componentsInChild.color = color1;
                }
                foreach (MonoBehaviourPublicObCoGaCoObCoObCoUnique componentsInChild in GameObject.Find("UserInterface/QuickMenu/QuickModeTabs").GetComponentsInChildren<MonoBehaviourPublicObCoGaCoObCoObCoUnique>())
                    componentsInChild.field_Public_Color32_0 = (Color32)new Color(color1.r / 2.25f, color1.g / 2.25f, color1.b / 2.25f);
            }
            catch (Exception ex)
            {
                Exception exception = new Exception();
            }
            emmVRCLoader.Logger.LogDebug("Coloring Quick Menu header text...");
            foreach (Text componentsInChild in ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_CONTEXT").GetComponentsInChildren<Text>(true))
            {
                if (componentsInChild.transform.name == "Text" && componentsInChild.transform.parent.name != "AvatarImage")
                    componentsInChild.color = new Color(color1.r * 1.25f, color1.g * 1.25f, color1.b * 1.25f);
            }
            if (!Configuration.JSONConfig.UIActionMenuColorChangingEnabled)
                return;
            try
            {
                emmVRCLoader.Logger.LogDebug("Coloring Action Menu...");
                Color color7 = Configuration.JSONConfig.UIColorChangingEnabled ? Configuration.menuColor() : new Color(Configuration.defaultMenuColor().r * 1.5f, Configuration.defaultMenuColor().g * 1.5f, Configuration.defaultMenuColor().b * 1.5f);
                Color color8 = new Color(color7.r, color7.g, color7.b, color7.a / 1.25f);
                foreach (Graphic graphic in UnityEngine.Resources.FindObjectsOfTypeAll<PedalGraphic>())
                    graphic.color = color7;
                foreach (ActionMenu actionMenu in UnityEngine.Resources.FindObjectsOfTypeAll<ActionMenu>())
                    ;
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError(ex.ToString());
            }
        }
    }
}
