﻿
using RenderHeads.Media.AVProVideo;
using UnityEngine;
using UnityEngine.Video;
using VRC.SDK3.Components;
using VRCSDK2;

namespace emmVRC.Hacks
{
    public class ComponentToggle
    {
        internal static VRC_SyncVideoPlayer[] Video_stored_sdk2;
        internal static SyncVideoPlayer[] Video_stored_sdk3;
        internal static MediaPlayer[] Video_stored_sdk3_2;
        internal static VideoPlayer[] Video_stored_sdk3_3;
        public static bool videoplayers = true;
        internal static VRC.SDKBase.VRC_Pickup[] Pickup_stored;
        public static bool pickupable = true;
        public static bool pickup_object = true;
        internal static VRC.SDKBase.VRC_Trigger[] Trigger_stored;

        public static void OnLevelLoad() => ComponentToggle.Store();

        private static void Store()
        {
            if (RoomManager.field_Internal_Static_ApiWorld_0 != null && UnityEngine.Resources.FindObjectsOfTypeAll<VRCSceneDescriptor>().Count > 0)
            {
                ComponentToggle.Video_stored_sdk3 = (SyncVideoPlayer[])UnityEngine.Resources.FindObjectsOfTypeAll<SyncVideoPlayer>();
                ComponentToggle.Video_stored_sdk3_2 = (MediaPlayer[])UnityEngine.Resources.FindObjectsOfTypeAll<MediaPlayer>();
                ComponentToggle.Video_stored_sdk3_3 = (VideoPlayer[])UnityEngine.Resources.FindObjectsOfTypeAll<VideoPlayer>();
            }
            else
                ComponentToggle.Video_stored_sdk2 = (VRC_SyncVideoPlayer[])UnityEngine.Resources.FindObjectsOfTypeAll<VRC_SyncVideoPlayer>();
            ComponentToggle.Pickup_stored = (VRC.SDKBase.VRC_Pickup[])Object.FindObjectsOfType<VRC.SDKBase.VRC_Pickup>();
            ComponentToggle.Trigger_stored = (VRC.SDKBase.VRC_Trigger[])Object.FindObjectsOfType<VRC.SDKBase.VRC_Trigger>();
        }

        internal static void Toggle(bool tempOn = false)
        {
            if (RoomManager.field_Internal_Static_ApiWorld_0 != null && UnityEngine.Resources.FindObjectsOfTypeAll<VRCSceneDescriptor>().Count > 0)
            {
                if (ComponentToggle.Video_stored_sdk3 == null || ComponentToggle.Video_stored_sdk3_2 == null || ComponentToggle.Video_stored_sdk3_3 == null)
                    ComponentToggle.Store();
                foreach (SyncVideoPlayer syncVideoPlayer in ComponentToggle.Video_stored_sdk3)
                {
                    if (tempOn)
                    {
                        syncVideoPlayer.GetComponent<SyncVideoPlayer>().enabled = true;
                        syncVideoPlayer.gameObject.SetActive(true);
                    }
                    else
                    {
                        syncVideoPlayer.GetComponent<SyncVideoPlayer>().enabled = ComponentToggle.videoplayers;
                        syncVideoPlayer.gameObject.SetActive(ComponentToggle.videoplayers);
                    }
                }
                foreach (MediaPlayer mediaPlayer in ComponentToggle.Video_stored_sdk3_2)
                {
                    if (tempOn)
                    {
                        mediaPlayer.GetComponent<MediaPlayer>().enabled = true;
                        mediaPlayer.gameObject.SetActive(true);
                    }
                    else
                    {
                        mediaPlayer.GetComponent<MediaPlayer>().enabled = ComponentToggle.videoplayers;
                        mediaPlayer.gameObject.SetActive(ComponentToggle.videoplayers);
                    }
                }
                foreach (VideoPlayer videoPlayer in ComponentToggle.Video_stored_sdk3_3)
                {
                    if (tempOn)
                    {
                        videoPlayer.GetComponent<VideoPlayer>().enabled = true;
                        videoPlayer.gameObject.SetActive(true);
                    }
                    else
                    {
                        videoPlayer.GetComponent<VideoPlayer>().enabled = ComponentToggle.videoplayers;
                        videoPlayer.gameObject.SetActive(ComponentToggle.videoplayers);
                    }
                }
            }
            else
            {
                if (ComponentToggle.Video_stored_sdk2 == null)
                    ComponentToggle.Store();
                foreach (VRC_SyncVideoPlayer vrcSyncVideoPlayer in ComponentToggle.Video_stored_sdk2)
                {
                    vrcSyncVideoPlayer.GetComponent<VRC_SyncVideoPlayer>().enabled = ComponentToggle.videoplayers;
                    vrcSyncVideoPlayer.gameObject.SetActive(ComponentToggle.videoplayers);
                }
            }
            if (ComponentToggle.Pickup_stored == null)
                ComponentToggle.Store();
            foreach (VRC.SDKBase.VRC_Pickup vrcPickup in ComponentToggle.Pickup_stored)
            {
                if (tempOn)
                {
                    vrcPickup.GetComponent<VRC.SDKBase.VRC_Pickup>().pickupable = true;
                    vrcPickup.gameObject.SetActive(true);
                }
                else
                {
                    vrcPickup.GetComponent<VRC.SDKBase.VRC_Pickup>().pickupable = ComponentToggle.pickupable;
                    vrcPickup.gameObject.SetActive(ComponentToggle.pickup_object);
                }
            }
        }
    }
}
