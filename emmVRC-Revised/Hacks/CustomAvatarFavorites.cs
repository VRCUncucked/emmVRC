﻿
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using VRC.Core;
using VRC.UI;

namespace emmVRC.Hacks
{
    public static class CustomAvatarFavorites
    {
        internal static GameObject PublicAvatarList;
        internal static UiAvatarList NewAvatarList;
        private static GameObject avText;
        private static Text avTextText;
        private static GameObject ChangeButton;
        public static UnityEngine.UI.Button.ButtonClickedEvent baseChooseEvent;
        private static GameObject FavoriteButton;
        private static GameObject FavoriteButtonNew;
        private static UnityEngine.UI.Button FavoriteButtonNewButton;
        private static Text FavoriteButtonNewText;
        private static GameObject ShowAuthorButton;
        public static GameObject pageAvatar;
        private static PageAvatar currPageAvatar;
        private static bool error;
        private static bool errorWarned;
        private static bool Searching;
        public static Il2CppSystem.Collections.Generic.List<ApiAvatar> LoadedAvatars;
        private static Il2CppSystem.Collections.Generic.List<ApiAvatar> SearchedAvatars;
        private static bool menuJustActivated;
        private static UiInputField searchBox;
        private static UnityAction<string> searchBoxAction;
        private static GameObject refreshButton;
        private static GameObject backButton;
        private static GameObject forwardButton;
        private static GameObject pageTicker;
        private static GameObject sortButton;
        private static bool waitingForSearch;
        public static int currentPage;
        private static CustomAvatarFavorites.SortingMode currentSortingMode;
        private static bool sortingInverse;
        private static MethodInfo renderElementMethod;

        internal static void RenderElement(this UiVRCList uivrclist, Il2CppSystem.Collections.Generic.List<ApiAvatar> AvatarList)
        {
            if (!uivrclist.gameObject.activeInHierarchy || !uivrclist.isActiveAndEnabled || (uivrclist.isOffScreen || !uivrclist.enabled))
                return;
            if (CustomAvatarFavorites.renderElementMethod == (MethodInfo)null)
                CustomAvatarFavorites.renderElementMethod = ((IEnumerable<MethodInfo>)typeof(UiVRCList).GetMethods()).FirstOrDefault<MethodInfo>((Func<MethodInfo, bool>)(a => a.Name.Contains("Method_Protected_Void_List_1_T_Int32_Boolean"))).MakeGenericMethod(typeof(ApiAvatar));
            CustomAvatarFavorites.renderElementMethod.Invoke((object)uivrclist, new object[4]
            {
        (object) AvatarList,
        (object) 0,
        (object) true,
        null
            });
        }

        internal static void RenderElement(this UiVRCList uivrclist, List<string> idList)
        {
            if (!uivrclist.gameObject.activeInHierarchy || !uivrclist.isActiveAndEnabled || (uivrclist.isOffScreen || !uivrclist.enabled))
                return;
            if (CustomAvatarFavorites.renderElementMethod == (MethodInfo)null)
                CustomAvatarFavorites.renderElementMethod = ((IEnumerable<MethodInfo>)typeof(UiVRCList).GetMethods()).FirstOrDefault<MethodInfo>((Func<MethodInfo, bool>)(a => a.Name.Contains("Method_Protected_Void_List_1_T_Int32_Boolean"))).MakeGenericMethod(typeof(ApiAvatar));
            CustomAvatarFavorites.renderElementMethod.Invoke((object)uivrclist, new object[4]
            {
        (object) idList,
        (object) 0,
        (object) true,
        null
            });
        }

        internal static void Initialize()
        {
            CustomAvatarFavorites.currentSortingMode = Configuration.JSONConfig.SortingMode > 2 ? CustomAvatarFavorites.SortingMode.DateAdded : (CustomAvatarFavorites.SortingMode)Configuration.JSONConfig.SortingMode;
            CustomAvatarFavorites.sortingInverse = Configuration.JSONConfig.SortingInverse;
            CustomAvatarFavorites.pageAvatar = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar").gameObject;
            CustomAvatarFavorites.FavoriteButton = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Favorite Button").gameObject;
            CustomAvatarFavorites.FavoriteButtonNew = UnityEngine.Object.Instantiate<GameObject>(CustomAvatarFavorites.FavoriteButton, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/"));
            CustomAvatarFavorites.FavoriteButtonNewButton = CustomAvatarFavorites.FavoriteButtonNew.GetComponent<UnityEngine.UI.Button>();
            CustomAvatarFavorites.FavoriteButtonNewButton.onClick.RemoveAllListeners();
            CustomAvatarFavorites.FavoriteButtonNewButton.onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              ApiAvatar apiAvatar = CustomAvatarFavorites.pageAvatar.GetComponent<PageAvatar>().field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0;
              bool flag = false;
              foreach (ApiModel loadedAvatar in CustomAvatarFavorites.LoadedAvatars)
              {
                  if (loadedAvatar.id == apiAvatar.id)
                      flag = true;
              }
              if (!flag)
              {
                  if ((apiAvatar.releaseStatus == "public" || apiAvatar.authorId == APIUser.CurrentUser.id) && apiAvatar.releaseStatus != null)
                      CustomAvatarFavorites.FavoriteAvatar(apiAvatar).NoAwait("FavoriteAvatar");
                  else
                      VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot favorite this avatar (it is private!)", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
              }
              else
                  VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Are you sure you want to unfavorite the avatar \"" + apiAvatar.name + "\"?", "Yes", (System.Action)(() =>
             {
                 CustomAvatarFavorites.UnfavoriteAvatar(apiAvatar).NoAwait("UnfavoriteAvatar");
                 VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
             }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
          }));
            RectTransform componentInChildren = CustomAvatarFavorites.FavoriteButtonNew.GetComponentInChildren<RectTransform>();
            componentInChildren.localPosition = componentInChildren.localPosition + new Vector3(0.0f, 165f);
            CustomAvatarFavorites.FavoriteButtonNewText = CustomAvatarFavorites.FavoriteButtonNew.GetComponentInChildren<Text>();
            CustomAvatarFavorites.FavoriteButtonNewText.supportRichText = true;
            try
            {
                CustomAvatarFavorites.FavoriteButtonNew.transform.Find("Horizontal/FavoritesCountSpacingText").gameObject.SetActive(false);
                CustomAvatarFavorites.FavoriteButtonNew.transform.Find("Horizontal/FavoritesCurrentCountText").gameObject.SetActive(false);
                CustomAvatarFavorites.FavoriteButtonNew.transform.Find("Horizontal/FavoritesCountDividerText").gameObject.SetActive(false);
                CustomAvatarFavorites.FavoriteButtonNew.transform.Find("Horizontal/FavoritesMaxAvailableText").gameObject.SetActive(false);
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("GameObject toggling failed. VRChat must have moved something in an update. Sorry!");
            }
            CustomAvatarFavorites.ShowAuthorButton = UnityEngine.Object.Instantiate<GameObject>(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Fallback Hide Button").gameObject, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/"));
            CustomAvatarFavorites.ShowAuthorButton.GetComponentInChildren<Text>().text = "";
            CustomAvatarFavorites.ShowAuthorButton.GetComponent<RectTransform>().sizeDelta = new Vector2(82f, 82f);
            CustomAvatarFavorites.ShowAuthorButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(250f, -25f);
            CustomAvatarFavorites.ShowAuthorButton.transform.Find("PlatformIcon").gameObject.SetActive(true);
            CustomAvatarFavorites.ShowAuthorButton.transform.Find("PlatformIcon").GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            MelonCoroutines.Start(CustomAvatarFavorites.ShowAuthorIconUpdate());
            UnityEngine.UI.Button component = CustomAvatarFavorites.ShowAuthorButton.GetComponent<UnityEngine.UI.Button>();
            component.onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            component.onClick.AddListener((UnityAction)(System.Action)(() => QuickMenu.prop_QuickMenu_0.field_Public_MenuController_0.ViewUserInfo(CustomAvatarFavorites.pageAvatar.GetComponent<PageAvatar>().field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0.authorId)));
            GameObject gameObject = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Vertical Scroll View/Viewport/Content/Legacy Avatar List").gameObject;
            CustomAvatarFavorites.PublicAvatarList = UnityEngine.Object.Instantiate<GameObject>(gameObject, gameObject.transform.parent);
            CustomAvatarFavorites.PublicAvatarList.transform.SetAsFirstSibling();
            CustomAvatarFavorites.ChangeButton = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Avatar/Change Button").gameObject;
            CustomAvatarFavorites.baseChooseEvent = CustomAvatarFavorites.ChangeButton.GetComponent<UnityEngine.UI.Button>().onClick;
            CustomAvatarFavorites.ChangeButton.GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            CustomAvatarFavorites.ChangeButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              ApiAvatar selectedAvatar = CustomAvatarFavorites.pageAvatar.GetComponent<PageAvatar>().field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0;
              if (CustomAvatarFavorites.LoadedAvatars.ToArray().Any<ApiAvatar>((Func<ApiAvatar, bool>)(a => a.id == selectedAvatar.id)) || CustomAvatarFavorites.SearchedAvatars.ToArray().Any<ApiAvatar>((Func<ApiAvatar, bool>)(a => a.id == selectedAvatar.id)))
              {
                  if (NetworkConfig.Instance.APICallsAllowed && !selectedAvatar.id.Contains("local"))
                      API.Fetch<ApiAvatar>(selectedAvatar.id, (Il2CppSystem.Action<ApiContainer>)(System.Action<ApiContainer>)(cont =>
                {
                    ApiAvatar fetchedAvatar = ((Il2CppObjectBase)cont.Model).Cast<ApiAvatar>();
                    if (fetchedAvatar.releaseStatus == "private" && fetchedAvatar.authorId != APIUser.CurrentUser.id)
                    {
                        if (CustomAvatarFavorites.LoadedAvatars.ToArray().Any<ApiAvatar>((Func<ApiAvatar, bool>)(a => a.id == fetchedAvatar.id)))
                            VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (it is private).\nDo you want to unfavorite it?", "Yes", (System.Action)(() =>
                       {
                           CustomAvatarFavorites.UnfavoriteAvatar(selectedAvatar).NoAwait("UnfavoriteAvatar");
                           VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
                       }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                        else
                            VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (it is private).", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                    }
                    else
                        CustomAvatarFavorites.baseChooseEvent.Invoke();
                }), (Il2CppSystem.Action<ApiContainer>)(System.Action<ApiContainer>)(cont =>
          {
              if (CustomAvatarFavorites.LoadedAvatars.ToArray().Any<ApiAvatar>((Func<ApiAvatar, bool>)(a => a.id == selectedAvatar.id)))
                  VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (no longer available).\nDo you want to unfavorite it?", "Yes", (System.Action)(() =>
             {
                 CustomAvatarFavorites.UnfavoriteAvatar(selectedAvatar).NoAwait("UnfavoriteAvatar");
                 VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
             }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
              else
                  VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (no longer available).", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
          }), false);
                  else if (selectedAvatar.releaseStatus == "private" && selectedAvatar.authorId != APIUser.CurrentUser.id)
                  {
                      if (CustomAvatarFavorites.LoadedAvatars.ToArray().Any<ApiAvatar>((Func<ApiAvatar, bool>)(a => a.id == selectedAvatar.id)))
                          VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (it is private).\nDo you want to unfavorite it?", "Yes", (System.Action)(() =>
                     {
                         CustomAvatarFavorites.UnfavoriteAvatar(selectedAvatar).NoAwait("UnfavoriteAvatar");
                         VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
                     }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                      else
                          VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (it is private).", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                  }
                  else if (selectedAvatar.releaseStatus == "unavailable")
                  {
                      if (CustomAvatarFavorites.LoadedAvatars.ToArray().Any<ApiAvatar>((Func<ApiAvatar, bool>)(a => a.id == selectedAvatar.id)))
                          VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (no longer available).\nDo you want to unfavorite it?", "Yes", (System.Action)(() =>
                     {
                         CustomAvatarFavorites.UnfavoriteAvatar(selectedAvatar).NoAwait("UnfavoriteAvatar");
                         VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
                     }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                      else
                          VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Cannot switch into this avatar (no longer available).", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                  }
                  else
                      CustomAvatarFavorites.baseChooseEvent.Invoke();
              }
              else
                  CustomAvatarFavorites.baseChooseEvent.Invoke();
          }));
            CustomAvatarFavorites.avText = CustomAvatarFavorites.PublicAvatarList.transform.Find("Button").gameObject;
            CustomAvatarFavorites.avTextText = CustomAvatarFavorites.avText.GetComponentInChildren<Text>();
            CustomAvatarFavorites.avTextText.text = "(0) emmVRC Favorites";
            CustomAvatarFavorites.currPageAvatar = CustomAvatarFavorites.pageAvatar.GetComponent<PageAvatar>();
            CustomAvatarFavorites.NewAvatarList = CustomAvatarFavorites.PublicAvatarList.GetComponent<UiAvatarList>();
            CustomAvatarFavorites.NewAvatarList.clearUnseenListOnCollapse = false;
            CustomAvatarFavorites.NewAvatarList.field_Public_EnumNPublicSealedvaInPuMiFaSpClPuLi11Unique_0 = UiAvatarList.EnumNPublicSealedvaInPuMiFaSpClPuLi11Unique.SpecificList;
            CustomAvatarFavorites.currPageAvatar.field_Public_SimpleAvatarPedestal_0.field_Public_Single_0 *= 0.85f;
            CustomAvatarFavorites.refreshButton = UnityEngine.Object.Instantiate<GameObject>(CustomAvatarFavorites.ChangeButton, CustomAvatarFavorites.avText.transform.parent);
            CustomAvatarFavorites.refreshButton.GetComponentInChildren<Text>().text = "↻";
            CustomAvatarFavorites.refreshButton.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
            CustomAvatarFavorites.refreshButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              CustomAvatarFavorites.Searching = false;
              MelonCoroutines.Start(CustomAvatarFavorites.JumpToStart());
              MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.5f));
          }));
            CustomAvatarFavorites.refreshButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
            CustomAvatarFavorites.refreshButton.transform.SetParent(CustomAvatarFavorites.avText.transform, true);
            CustomAvatarFavorites.refreshButton.GetComponent<RectTransform>().anchoredPosition = CustomAvatarFavorites.avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(980f, 0.0f);
            CustomAvatarFavorites.backButton = UnityEngine.Object.Instantiate<GameObject>(CustomAvatarFavorites.ChangeButton, CustomAvatarFavorites.avText.transform.parent);
            CustomAvatarFavorites.backButton.GetComponentInChildren<Text>().text = "←";
            CustomAvatarFavorites.backButton.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
            CustomAvatarFavorites.backButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              --CustomAvatarFavorites.currentPage;
              MelonCoroutines.Start(CustomAvatarFavorites.JumpToStart());
              MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.5f));
          }));
            CustomAvatarFavorites.backButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
            CustomAvatarFavorites.backButton.transform.SetParent(CustomAvatarFavorites.avText.transform, true);
            CustomAvatarFavorites.backButton.GetComponent<RectTransform>().anchoredPosition = CustomAvatarFavorites.avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(750f, 0.0f);
            CustomAvatarFavorites.forwardButton = UnityEngine.Object.Instantiate<GameObject>(CustomAvatarFavorites.ChangeButton, CustomAvatarFavorites.avText.transform.parent);
            CustomAvatarFavorites.forwardButton.GetComponentInChildren<Text>().text = "→";
            CustomAvatarFavorites.forwardButton.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
            CustomAvatarFavorites.forwardButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              ++CustomAvatarFavorites.currentPage;
              MelonCoroutines.Start(CustomAvatarFavorites.JumpToStart());
              MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.5f));
          }));
            CustomAvatarFavorites.forwardButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
            CustomAvatarFavorites.forwardButton.transform.SetParent(CustomAvatarFavorites.avText.transform, true);
            CustomAvatarFavorites.forwardButton.GetComponent<RectTransform>().anchoredPosition = CustomAvatarFavorites.avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(900f, 0.0f);
            CustomAvatarFavorites.pageTicker = UnityEngine.Object.Instantiate<GameObject>(CustomAvatarFavorites.ChangeButton, CustomAvatarFavorites.avText.transform.parent);
            CustomAvatarFavorites.pageTicker.GetComponentInChildren<Text>().text = "0 / 0";
            UnityEngine.Object.Destroy((UnityEngine.Object)CustomAvatarFavorites.pageTicker.GetComponent<UnityEngine.UI.Button>());
            UnityEngine.Object.Destroy((UnityEngine.Object)CustomAvatarFavorites.pageTicker.GetComponent<Image>());
            CustomAvatarFavorites.pageTicker.GetComponent<RectTransform>().sizeDelta /= new Vector2(4f, 1f);
            CustomAvatarFavorites.pageTicker.transform.SetParent(CustomAvatarFavorites.avText.transform, true);
            CustomAvatarFavorites.pageTicker.GetComponent<RectTransform>().anchoredPosition = CustomAvatarFavorites.avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(825f, 0.0f);
            CustomAvatarFavorites.sortButton = UnityEngine.Object.Instantiate<GameObject>(CustomAvatarFavorites.ChangeButton, CustomAvatarFavorites.avText.transform.parent);
            switch (CustomAvatarFavorites.currentSortingMode)
            {
                case CustomAvatarFavorites.SortingMode.DateAdded:
                    CustomAvatarFavorites.sortButton.GetComponentInChildren<Text>().text = "Date " + (CustomAvatarFavorites.sortingInverse ? "↑" : "↓");
                    break;
                case CustomAvatarFavorites.SortingMode.Alphabetical:
                    CustomAvatarFavorites.sortButton.GetComponentInChildren<Text>().text = "ABC " + (CustomAvatarFavorites.sortingInverse ? "↑" : "↓");
                    break;
                case CustomAvatarFavorites.SortingMode.Creator:
                    CustomAvatarFavorites.sortButton.GetComponentInChildren<Text>().text = "Creator " + (CustomAvatarFavorites.sortingInverse ? "↑" : "↓");
                    break;
            }
            CustomAvatarFavorites.sortButton.GetComponent<UnityEngine.UI.Button>().onClick.RemoveAllListeners();
            CustomAvatarFavorites.sortButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              if (!CustomAvatarFavorites.sortingInverse)
              {
                  CustomAvatarFavorites.sortingInverse = true;
              }
              else
              {
                  if (CustomAvatarFavorites.currentSortingMode != CustomAvatarFavorites.SortingMode.Creator)
                      ++CustomAvatarFavorites.currentSortingMode;
                  else
                      CustomAvatarFavorites.currentSortingMode = CustomAvatarFavorites.SortingMode.DateAdded;
                  CustomAvatarFavorites.sortingInverse = false;
              }
              switch (CustomAvatarFavorites.currentSortingMode)
              {
                  case CustomAvatarFavorites.SortingMode.DateAdded:
                      CustomAvatarFavorites.sortButton.GetComponentInChildren<Text>().text = "Date " + (CustomAvatarFavorites.sortingInverse ? "↑" : "↓");
                      break;
                  case CustomAvatarFavorites.SortingMode.Alphabetical:
                      CustomAvatarFavorites.sortButton.GetComponentInChildren<Text>().text = "ABC " + (CustomAvatarFavorites.sortingInverse ? "↑" : "↓");
                      break;
                  case CustomAvatarFavorites.SortingMode.Creator:
                      CustomAvatarFavorites.sortButton.GetComponentInChildren<Text>().text = "Creator " + (CustomAvatarFavorites.sortingInverse ? "↑" : "↓");
                      break;
              }
              CustomAvatarFavorites.currentPage = 0;
              Configuration.JSONConfig.SortingMode = (int)CustomAvatarFavorites.currentSortingMode;
              Configuration.JSONConfig.SortingInverse = CustomAvatarFavorites.sortingInverse;
              Configuration.SaveConfig();
              MelonCoroutines.Start(CustomAvatarFavorites.JumpToStart());
              MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.5f));
          }));
            CustomAvatarFavorites.sortButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(2f, 1f);
            CustomAvatarFavorites.sortButton.transform.SetParent(CustomAvatarFavorites.avText.transform, true);
            CustomAvatarFavorites.sortButton.GetComponent<RectTransform>().anchoredPosition = CustomAvatarFavorites.avText.transform.Find("ToggleIcon").GetComponent<RectTransform>().anchoredPosition + new Vector2(635f, 0.0f);
            CustomAvatarFavorites.pageAvatar.transform.Find("AvatarPreviewBase").transform.localPosition += new Vector3(0.0f, 60f, 0.0f);
            CustomAvatarFavorites.pageAvatar.transform.Find("AvatarPreviewBase").transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
            foreach (PropertyInfo propertyInfo in ((IEnumerable<PropertyInfo>)typeof(PageAvatar).GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>)(a => a.PropertyType == typeof(Vector3) && (double)((Vector3)a.GetValue((object)CustomAvatarFavorites.currPageAvatar)).x <= -461.0 && (int)((Vector3)a.GetValue((object)CustomAvatarFavorites.currPageAvatar)).y == -200)))
            {
                Vector3 vector3 = (Vector3)propertyInfo.GetValue((object)CustomAvatarFavorites.currPageAvatar);
                propertyInfo.SetValue((object)CustomAvatarFavorites.currPageAvatar, (object)new Vector3(vector3.x, vector3.y + 80f, vector3.z));
            }
            foreach (PropertyInfo propertyInfo in ((IEnumerable<PropertyInfo>)typeof(PageAvatar).GetProperties()).Where<PropertyInfo>((Func<PropertyInfo, bool>)(a => a.PropertyType == typeof(Vector3) && (int)((Vector3)a.GetValue((object)CustomAvatarFavorites.currPageAvatar)).x == -91)))
            {
                Vector3 vector3 = (Vector3)propertyInfo.GetValue((object)CustomAvatarFavorites.currPageAvatar);
                propertyInfo.SetValue((object)CustomAvatarFavorites.currPageAvatar, (object)new Vector3(vector3.x, vector3.y + 80f, vector3.z));
            }
            CustomAvatarFavorites.LoadedAvatars = new Il2CppSystem.Collections.Generic.List<ApiAvatar>();
            CustomAvatarFavorites.SearchedAvatars = new Il2CppSystem.Collections.Generic.List<ApiAvatar>();
        }

        public static IEnumerator ShowAuthorIconUpdate()
        {
            while ((UnityEngine.Object)Resources.authorSprite == (UnityEngine.Object)null)
                yield return (object)new WaitForEndOfFrame();
            CustomAvatarFavorites.ShowAuthorButton.transform.Find("PlatformIcon").GetComponent<Image>().sprite = Resources.authorSprite;
            CustomAvatarFavorites.ShowAuthorButton.transform.Find("PlatformIcon").GetComponent<RectTransform>().sizeDelta /= new Vector2(1.5f, 1.5f);
        }

        public static async Task FavoriteAvatar(ApiAvatar avtr)
        {
            if (CustomAvatarFavorites.LoadedAvatars.ToArray().ToList<ApiAvatar>().FindIndex((Predicate<ApiAvatar>)(a => a.id == avtr.id)) == -1)
            {
                CustomAvatarFavorites.LoadedAvatars.Insert(0, avtr);
                Network.Objects.Avatar avatar = new Network.Objects.Avatar(avtr);
                try
                {
                    string str = await HTTPRequest.post(NetworkClient.baseURL + "/api/avatar", (object)avatar);
                    if (!CustomAvatarFavorites.Searching)
                    {
                        CustomAvatarFavorites.currentPage = 0;
                        MelonCoroutines.Start(CustomAvatarFavorites.JumpToStart());
                        MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.1f));
                    }
                }
                catch (Exception ex)
                {
                    await emmVRC.AwaitUpdate.Yield();
                    VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Error occured while updating avatar list.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                    throw;
                }
            }
            else
                emmVRCLoader.Logger.LogDebug("Tried to add an avatar that already exists...");
        }

        public static async Task UnfavoriteAvatar(ApiAvatar avtr)
        {
            if (CustomAvatarFavorites.LoadedAvatars.Contains(avtr))
                CustomAvatarFavorites.LoadedAvatars.Remove(avtr);
            try
            {
                string str = await HTTPRequest.delete(NetworkClient.baseURL + "/api/avatar", (object)new Network.Objects.Avatar(avtr));
                if (!CustomAvatarFavorites.Searching)
                {
                    await emmVRC.AwaitUpdate.Yield();
                    MelonCoroutines.Start(CustomAvatarFavorites.JumpToStart());
                    MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.1f));
                }
            }
            catch (Exception ex)
            {
                await emmVRC.AwaitUpdate.Yield();
                VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Error occured while updating avatar list.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                throw;
            }
        }

        public static async Task PopulateList()
        {
            CustomAvatarFavorites.LoadedAvatars = new Il2CppSystem.Collections.Generic.List<ApiAvatar>();
            Network.Objects.Avatar[] avatarArray = (Network.Objects.Avatar[])null;
            try
            {
                avatarArray = Decoder.Decode(await HTTPRequest.get(NetworkClient.baseURL + "/api/avatar")).Make<Network.Objects.Avatar[]>();
                await emmVRC.AwaitUpdate.Yield();
                if (avatarArray != null)
                {
                    foreach (Network.Objects.Avatar avatar in avatarArray)
                        CustomAvatarFavorites.LoadedAvatars.Add(avatar.apiAvatar());
                    MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.1f));
                }
            }
            catch (Exception ex)
            {
                await emmVRC.AwaitUpdate.Yield();
                Managers.NotificationManager.AddNotification("emmVRC Avatar Favorites list failed to load. Please check your internet connection.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
                CustomAvatarFavorites.error = true;
                CustomAvatarFavorites.errorWarned = true;
                throw;
            }
            avatarArray = (Network.Objects.Avatar[])null;
        }

        public static IEnumerator RefreshMenu(float delay)
        {
            if ((UnityEngine.Object)CustomAvatarFavorites.NewAvatarList.scrollRect != (UnityEngine.Object)null)
            {
                yield return (object)new WaitForSeconds(delay);
                if (CustomAvatarFavorites.Searching)
                {
                    if (CustomAvatarFavorites.currentPage > CustomAvatarFavorites.SearchedAvatars.Count / Configuration.JSONConfig.SearchRenderLimit)
                        CustomAvatarFavorites.currentPage = CustomAvatarFavorites.SearchedAvatars.Count / Configuration.JSONConfig.SearchRenderLimit;
                    if (CustomAvatarFavorites.currentPage < 0)
                        CustomAvatarFavorites.currentPage = 0;
                    var list = new Il2CppSystem.Collections.Generic.List<ApiAvatar>();
                    switch (CustomAvatarFavorites.currentSortingMode)
                    {
                        case CustomAvatarFavorites.SortingMode.DateAdded:
                            foreach (ApiAvatar searchedAvatar in CustomAvatarFavorites.SearchedAvatars)
                                list.Add(searchedAvatar);
                            break;
                        case CustomAvatarFavorites.SortingMode.Alphabetical:
                            using (IEnumerator<ApiAvatar> enumerator = CustomAvatarFavorites.SearchedAvatars.ToArray().OrderBy<ApiAvatar, string>((Func<ApiAvatar, string>)(x => x.name)).GetEnumerator())
                            {
                                while (enumerator.MoveNext())
                                {
                                    ApiAvatar current = enumerator.Current;
                                    list.Add(current);
                                }
                                break;
                            }
                        case CustomAvatarFavorites.SortingMode.Creator:
                            using (IEnumerator<ApiAvatar> enumerator = CustomAvatarFavorites.SearchedAvatars.ToArray().OrderBy<ApiAvatar, string>((Func<ApiAvatar, string>)(x => x.authorName)).GetEnumerator())
                            {
                                while (enumerator.MoveNext())
                                {
                                    ApiAvatar current = enumerator.Current;
                                    list.Add(current);
                                }
                                break;
                            }
                    }
                    if (CustomAvatarFavorites.sortingInverse)
                        list.Reverse();
                    Text componentInChildren1 = CustomAvatarFavorites.pageTicker.GetComponentInChildren<Text>();
                    string str1 = (CustomAvatarFavorites.currentPage + 1).ToString();
                    int num = list.Count / Configuration.JSONConfig.SearchRenderLimit + 1;
                    string str2 = num.ToString();
                    string str3 = str1 + " / " + str2;
                    componentInChildren1.text = str3;
                    var range = list.GetRange(CustomAvatarFavorites.currentPage * Configuration.JSONConfig.SearchRenderLimit, System.Math.Abs(CustomAvatarFavorites.currentPage * Configuration.JSONConfig.SearchRenderLimit - list.Count));
                    if (range.Count > Configuration.JSONConfig.SearchRenderLimit)
                        range.RemoveRange(Configuration.JSONConfig.SearchRenderLimit, range.Count - Configuration.JSONConfig.SearchRenderLimit);
                    CustomAvatarFavorites.NewAvatarList.RenderElement(new Il2CppSystem.Collections.Generic.List<ApiAvatar>());
                    CustomAvatarFavorites.NewAvatarList.RenderElement(range);
                    Text componentInChildren2 = CustomAvatarFavorites.avText.GetComponentInChildren<Text>();
                    num = list.Count;
                    string str4 = "(" + num.ToString() + ") Search Results";
                    componentInChildren2.text = str4;
                    if (CustomAvatarFavorites.currentPage == 0)
                        CustomAvatarFavorites.backButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
                    else
                        CustomAvatarFavorites.backButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
                    if (CustomAvatarFavorites.currentPage >= list.Count / Configuration.JSONConfig.SearchRenderLimit)
                        CustomAvatarFavorites.forwardButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
                    else
                        CustomAvatarFavorites.forwardButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
                }
                else
                {
                    if (CustomAvatarFavorites.currentPage > CustomAvatarFavorites.LoadedAvatars.Count / Configuration.JSONConfig.FavoriteRenderLimit)
                        CustomAvatarFavorites.currentPage = CustomAvatarFavorites.LoadedAvatars.Count / Configuration.JSONConfig.FavoriteRenderLimit;
                    if (CustomAvatarFavorites.currentPage < 0)
                        CustomAvatarFavorites.currentPage = 0;
                    Il2CppSystem.Collections.Generic.List<ApiAvatar> list = new Il2CppSystem.Collections.Generic.List<ApiAvatar>();
                    switch (CustomAvatarFavorites.currentSortingMode)
                    {
                        case CustomAvatarFavorites.SortingMode.DateAdded:
                            foreach (ApiAvatar loadedAvatar in CustomAvatarFavorites.LoadedAvatars)
                                list.Add(loadedAvatar);
                            break;
                        case CustomAvatarFavorites.SortingMode.Alphabetical:
                            using (IEnumerator<ApiAvatar> enumerator = CustomAvatarFavorites.LoadedAvatars.ToArray().OrderBy<ApiAvatar, string>((Func<ApiAvatar, string>)(x => x.name)).GetEnumerator())
                            {
                                while (enumerator.MoveNext())
                                {
                                    ApiAvatar current = enumerator.Current;
                                    list.Add(current);
                                }
                                break;
                            }
                        case CustomAvatarFavorites.SortingMode.Creator:
                            using (IEnumerator<ApiAvatar> enumerator = CustomAvatarFavorites.LoadedAvatars.ToArray().OrderBy<ApiAvatar, string>((Func<ApiAvatar, string>)(x => x.authorName)).GetEnumerator())
                            {
                                while (enumerator.MoveNext())
                                {
                                    ApiAvatar current = enumerator.Current;
                                    list.Add(current);
                                }
                                break;
                            }
                    }
                    if (CustomAvatarFavorites.sortingInverse)
                        list.Reverse();
                    Text componentInChildren1 = CustomAvatarFavorites.pageTicker.GetComponentInChildren<Text>();
                    string str1 = (CustomAvatarFavorites.currentPage + 1).ToString();
                    int num = list.Count / Configuration.JSONConfig.FavoriteRenderLimit + 1;
                    string str2 = num.ToString();
                    string str3 = str1 + " / " + str2;
                    componentInChildren1.text = str3;
                    Il2CppSystem.Collections.Generic.List<ApiAvatar> range = list.GetRange(CustomAvatarFavorites.currentPage * Configuration.JSONConfig.FavoriteRenderLimit, System.Math.Abs(CustomAvatarFavorites.currentPage * Configuration.JSONConfig.FavoriteRenderLimit - list.Count));
                    if (range.Count > Configuration.JSONConfig.FavoriteRenderLimit)
                        range.RemoveRange(Configuration.JSONConfig.FavoriteRenderLimit, range.Count - Configuration.JSONConfig.FavoriteRenderLimit);
                    CustomAvatarFavorites.NewAvatarList.RenderElement(new Il2CppSystem.Collections.Generic.List<ApiAvatar>());
                    CustomAvatarFavorites.NewAvatarList.RenderElement(range);
                    Text componentInChildren2 = CustomAvatarFavorites.avText.GetComponentInChildren<Text>();
                    num = list.Count;
                    string str4 = "(" + num.ToString() + ") emmVRC Favorites";
                    componentInChildren2.text = str4;
                    if (CustomAvatarFavorites.currentPage == 0)
                        CustomAvatarFavorites.backButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
                    else
                        CustomAvatarFavorites.backButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
                    if (CustomAvatarFavorites.currentPage >= list.Count / Configuration.JSONConfig.FavoriteRenderLimit)
                        CustomAvatarFavorites.forwardButton.GetComponent<UnityEngine.UI.Button>().interactable = false;
                    else
                        CustomAvatarFavorites.forwardButton.GetComponent<UnityEngine.UI.Button>().interactable = true;
                }
            }
        }

        public static IEnumerator JumpToStart()
        {
            if (Configuration.JSONConfig.AvatarFavoritesJumpToStart)
            {
                while ((double)CustomAvatarFavorites.NewAvatarList.scrollRect.normalizedPosition.x > 0.0)
                {
                    CustomAvatarFavorites.NewAvatarList.scrollRect.normalizedPosition = new Vector2(CustomAvatarFavorites.NewAvatarList.scrollRect.normalizedPosition.x - 0.1f, 0.0f);
                    yield return (object)new WaitForEndOfFrame();
                }
            }
        }

        public static IEnumerator SearchAvatarsAfterDelay(string query)
        {
            yield return (object)new WaitForSecondsRealtime(1f);
            if (Configuration.JSONConfig.AvatarFavoritesJumpToStart)
            {
                while ((double)CustomAvatarFavorites.NewAvatarList.scrollRect.normalizedPosition.x > 0.0)
                {
                    CustomAvatarFavorites.NewAvatarList.scrollRect.normalizedPosition = new Vector2(CustomAvatarFavorites.NewAvatarList.scrollRect.normalizedPosition.x - 0.1f, 0.0f);
                    yield return (object)new WaitForEndOfFrame();
                }
            }
            CustomAvatarFavorites.SearchAvatars(query).NoAwait("SearchAvatars");
        }

        public static async Task SearchAvatars(string query)
        {
            if (!Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled || NetworkClient.webToken == null)
                await emmVRC.AwaitUpdate.Yield();
            if (CustomAvatarFavorites.waitingForSearch)
            {
                VRCUiPopupManager.prop_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Please wait for your current search\nto finish before starting a new one.", "Okay", (System.Action)(() => VRCUiPopupManager.prop_VRCUiPopupManager_0.HideCurrentPopup()));
            }
            else
            {
                CustomAvatarFavorites.avText.GetComponentInChildren<Text>().text = "Searching. Please wait...";
                CustomAvatarFavorites.SearchedAvatars.Clear();
                Network.Objects.Avatar[] avatarArray = (Network.Objects.Avatar[])null;
                CustomAvatarFavorites.waitingForSearch = true;
                try
                {
                    string jsonString = await HTTPRequest.post(NetworkClient.baseURL + "/api/avatar/search", (object)new Dictionary<string, string>()
                    {
                        [nameof(query)] = query
                    });
                    if (jsonString.Contains("Bad Request"))
                    {
                        await emmVRC.AwaitUpdate.Yield();
                        VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Your search could not be processed.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                    }
                    else
                    {
                        avatarArray = Decoder.Decode(jsonString).Make<Network.Objects.Avatar[]>();
                        await emmVRC.AwaitUpdate.Yield();
                        if (avatarArray != null)
                        {
                            foreach (Network.Objects.Avatar avatar in avatarArray)
                                CustomAvatarFavorites.SearchedAvatars.Add(avatar.apiAvatar());
                        }
                        CustomAvatarFavorites.currentPage = 0;
                        CustomAvatarFavorites.Searching = true;
                        MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(0.1f));
                    }
                }
                catch (Exception ex)
                {
                    await emmVRC.AwaitUpdate.Yield();
                    VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Your search could not be processed.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                    throw;
                }
                finally
                {
                    CustomAvatarFavorites.waitingForSearch = false;
                    await emmVRC.AwaitUpdate.Yield();
                    if ((UnityEngine.Object)CustomAvatarFavorites.NewAvatarList.expandButton.gameObject.transform.Find("ToggleIcon").GetComponentInChildren<Image>().sprite == (UnityEngine.Object)CustomAvatarFavorites.NewAvatarList.expandSprite)
                        CustomAvatarFavorites.NewAvatarList.ToggleExtend();
                }
                avatarArray = (Network.Objects.Avatar[])null;
            }
        }

        internal static void OnUpdate()
        {
            if (!Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled)
            {
                CustomAvatarFavorites.PublicAvatarList.SetActive(false);
                CustomAvatarFavorites.FavoriteButtonNew.SetActive(false);
            }
            if (!Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled || (NetworkClient.webToken == null || (UnityEngine.Object)CustomAvatarFavorites.PublicAvatarList == (UnityEngine.Object)null) || ((UnityEngine.Object)CustomAvatarFavorites.FavoriteButtonNew == (UnityEngine.Object)null || RoomManager.field_Internal_Static_ApiWorld_0 == null || !CustomAvatarFavorites.PublicAvatarList.activeInHierarchy))
                return;
            if ((UnityEngine.Object)CustomAvatarFavorites.searchBox == (UnityEngine.Object)null && CustomAvatarFavorites.NewAvatarList.gameObject.activeInHierarchy)
            {
                VRCUiPageHeader componentInChildren = QuickMenuUtils.GetVRCUiMInstance().GetComponentInChildren<VRCUiPageHeader>(true);
                if ((UnityEngine.Object)componentInChildren != (UnityEngine.Object)null)
                    CustomAvatarFavorites.searchBox = componentInChildren.field_Public_UiInputField_0;
            }
            if ((Il2CppSystem.Delegate)CustomAvatarFavorites.searchBoxAction == (Il2CppSystem.Delegate)null)
                CustomAvatarFavorites.searchBoxAction = DelegateSupport.ConvertDelegate<UnityAction<string>>((System.Action<string>)(searchTerm =>
               {
                   if (string.IsNullOrWhiteSpace(searchTerm) || searchTerm.Length < 2)
                       return;
                   CustomAvatarFavorites.SearchAvatars(searchTerm).NoAwait("SearchAvatars");
               }));
            if ((UnityEngine.Object)CustomAvatarFavorites.searchBox != (UnityEngine.Object)null && (UnityEngine.Object)CustomAvatarFavorites.searchBox.field_Public_Button_0 != (UnityEngine.Object)null && (!CustomAvatarFavorites.searchBox.field_Public_Button_0.interactable && RoomManager.field_Internal_Static_ApiWorld_0 != null))
            {
                CustomAvatarFavorites.searchBox.field_Public_Button_0.interactable = true;
                CustomAvatarFavorites.searchBox.field_Public_UnityAction_1_String_0 = CustomAvatarFavorites.searchBoxAction;
            }
            if (Configuration.JSONConfig.AvatarFavoritesEnabled && Configuration.JSONConfig.emmVRCNetworkEnabled && NetworkClient.webToken != null)
            {
                CustomAvatarFavorites.NewAvatarList.collapsedCount = Configuration.JSONConfig.FavoriteRenderLimit + Configuration.JSONConfig.SearchRenderLimit;
                CustomAvatarFavorites.NewAvatarList.expandedCount = Configuration.JSONConfig.FavoriteRenderLimit + Configuration.JSONConfig.SearchRenderLimit;
                if (!CustomAvatarFavorites.menuJustActivated)
                {
                    MelonCoroutines.Start(CustomAvatarFavorites.RefreshMenu(1f));
                    CustomAvatarFavorites.menuJustActivated = true;
                }
                if (CustomAvatarFavorites.menuJustActivated && (CustomAvatarFavorites.NewAvatarList.pickers.Count < CustomAvatarFavorites.LoadedAvatars.Count || CustomAvatarFavorites.NewAvatarList.isOffScreen))
                    CustomAvatarFavorites.menuJustActivated = false;
                if ((UnityEngine.Object)CustomAvatarFavorites.currPageAvatar != (UnityEngine.Object)null && (UnityEngine.Object)CustomAvatarFavorites.currPageAvatar.field_Public_SimpleAvatarPedestal_0 != (UnityEngine.Object)null && (CustomAvatarFavorites.currPageAvatar.field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0 != null && CustomAvatarFavorites.LoadedAvatars != null) && (UnityEngine.Object)CustomAvatarFavorites.FavoriteButtonNew != (UnityEngine.Object)null)
                {
                    bool flag = false;
                    foreach (ApiModel loadedAvatar in CustomAvatarFavorites.LoadedAvatars)
                    {
                        if (loadedAvatar.id == CustomAvatarFavorites.currPageAvatar.field_Public_SimpleAvatarPedestal_0.field_Internal_ApiAvatar_0.id)
                            flag = true;
                    }
                    CustomAvatarFavorites.FavoriteButtonNewText.text = flag ? "<color=#FF69B4>emmVRC</color> Unfavorite" : "<color=#FF69B4>emmVRC</color> Favorite";
                }
            }
            if ((!Configuration.JSONConfig.AvatarFavoritesEnabled || !Configuration.JSONConfig.emmVRCNetworkEnabled || NetworkClient.webToken == null) && (CustomAvatarFavorites.PublicAvatarList.activeSelf || CustomAvatarFavorites.FavoriteButtonNew.activeSelf))
            {
                CustomAvatarFavorites.PublicAvatarList.SetActive(false);
                CustomAvatarFavorites.FavoriteButtonNew.SetActive(false);
            }
            else if ((!CustomAvatarFavorites.PublicAvatarList.activeSelf || !CustomAvatarFavorites.FavoriteButtonNew.activeSelf) && (Configuration.JSONConfig.AvatarFavoritesEnabled && Configuration.JSONConfig.emmVRCNetworkEnabled) && NetworkClient.webToken != null)
            {
                CustomAvatarFavorites.PublicAvatarList.SetActive(true);
                CustomAvatarFavorites.FavoriteButtonNew.SetActive(true);
            }
            if (!CustomAvatarFavorites.error || CustomAvatarFavorites.errorWarned)
                return;
            Managers.NotificationManager.AddNotification("Your emmVRC avatars could not be loaded. Please ask in the Discord to resolve this.", "Dismiss", (System.Action)(() => Managers.NotificationManager.DismissCurrentNotification()), "", (System.Action)null, Resources.errorSprite);
            CustomAvatarFavorites.errorWarned = true;
        }

        internal static void Hide()
        {
            CustomAvatarFavorites.PublicAvatarList.SetActive(false);
            CustomAvatarFavorites.FavoriteButtonNew.SetActive(false);
        }

        internal static void Show()
        {
            if (CustomAvatarFavorites.error || !Configuration.JSONConfig.AvatarFavoritesEnabled)
                return;
            CustomAvatarFavorites.PublicAvatarList.SetActive(true);
            CustomAvatarFavorites.FavoriteButtonNew.SetActive(true);
        }

        internal static void Destroy()
        {
            UnityEngine.Object.Destroy((UnityEngine.Object)CustomAvatarFavorites.PublicAvatarList);
            UnityEngine.Object.Destroy((UnityEngine.Object)CustomAvatarFavorites.FavoriteButtonNew);
        }

        public static async Task CheckForAvatarPedestals()
        {
            if (NetworkClient.webToken == null || APIUser.CurrentUser == null || (!Configuration.JSONConfig.SubmitAvatarPedestals || !NetworkConfig.Instance.AvatarPedestalScansAllowed))
                return;
            while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                await emmVRC.AwaitUpdate.Yield();
            if (!RoomManager.field_Internal_Static_ApiWorld_0.IsPublicPublishedWorld)
                return;
            foreach (AvatarPedestal avatarPedestal in UnityEngine.Resources.FindObjectsOfTypeAll<AvatarPedestal>())
            {
                AvatarPedestal pedestal = avatarPedestal;
                if ((UnityEngine.Object)pedestal != (UnityEngine.Object)null && pedestal.field_Private_ApiAvatar_0.releaseStatus == "public")
                {
                    await emmVRC.AwaitUpdate.Yield();
                    Network.Objects.Avatar avatar = new Network.Objects.Avatar(pedestal.field_Private_ApiAvatar_0);
                    emmVRCLoader.Logger.LogDebug("Found pedestal " + pedestal.field_Private_ApiAvatar_0.name + ", putting...");
                    try
                    {
                        string str = await HTTPRequest.put(NetworkClient.baseURL + "/api/avatar", (object)avatar);
                    }
                    catch (Exception ex)
                    {
                        await emmVRC.AwaitUpdate.Yield();
                        emmVRCLoader.Logger.LogDebug("Could not put avatar");
                        throw;
                    }
                    await Task.Delay(500);
                }
                pedestal = (AvatarPedestal)null;
            }
        }

        public enum SortingMode
        {
            DateAdded,
            Alphabetical,
            Creator,
        }
    }
}
