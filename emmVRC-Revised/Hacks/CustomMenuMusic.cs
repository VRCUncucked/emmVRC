﻿
using emmVRC.Libraries;
using emmVRCLoader;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace emmVRC.Hacks
{
    public class CustomMenuMusic
    {
        public static IEnumerator Initialize()
        {
            if (!ModCompatibility.BetterLoadingScreen)
            {
                if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic")))
                    Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic"));
                if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/custommenu.ogg")))
                {
                    try
                    {
                        File.Move(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/custommenu.ogg"), Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic/custommenu.ogg"));
                    }
                    catch (Exception ex)
                    {
                        Exception exception = new Exception();
                        emmVRCLoader.Logger.LogError("A custommenu.ogg was detected, but you already have one in the CustomMenuMusic folder. Please put custom menu music in the Ogg Vorbis format into UserData/emmVRC/CustomMenuMusic instead.");
                    }
                }
                string[] files = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/CustomMenuMusic"));
                if (files.Length >= 1)
                {
                    emmVRCLoader.Logger.Log("Processing custom menu music...");
                    int index = new System.Random().Next(files.Length);
                    emmVRCLoader.Logger.LogDebug("Picked track: " + files[index]);
                    GameObject loadingMusic1 = GameObject.Find("LoadingBackground_TealGradient_Music/LoadingSound");
                    GameObject loadingMusic2 = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup/LoadingSound").gameObject;
                    if ((UnityEngine.Object)loadingMusic1 != (UnityEngine.Object)null)
                        loadingMusic1.GetComponent<AudioSource>().Stop();
                    if ((UnityEngine.Object)loadingMusic2 != (UnityEngine.Object)null)
                        loadingMusic2.GetComponent<AudioSource>().Stop();
                    UnityWebRequest CustomLoadingMusicRequest = UnityWebRequest.Get(string.Format("file://{0}", (object)files[index]).Replace("\\", "/"));
                    CustomLoadingMusicRequest.SendWebRequest();
                    while (!CustomLoadingMusicRequest.isDone)
                        yield return (object)null;
                    emmVRCLoader.Logger.LogDebug("Request sent and returned");
                    AudioClip audioClip = (AudioClip)null;
                    if (CustomLoadingMusicRequest.isHttpError)
                        emmVRCLoader.Logger.LogError("Could not load music file: " + CustomLoadingMusicRequest.error);
                    else
                        audioClip = WebRequestWWW.InternalCreateAudioClipUsingDH(CustomLoadingMusicRequest.downloadHandler, CustomLoadingMusicRequest.url, false, false, AudioType.UNKNOWN);
                    if ((UnityEngine.Object)audioClip != (UnityEngine.Object)null)
                    {
                        if ((UnityEngine.Object)loadingMusic1 != (UnityEngine.Object)null)
                        {
                            loadingMusic1.GetComponent<AudioSource>().clip = audioClip;
                            loadingMusic1.GetComponent<AudioSource>().Play();
                        }
                        if ((UnityEngine.Object)loadingMusic2 != (UnityEngine.Object)null)
                        {
                            loadingMusic2.GetComponent<AudioSource>().clip = audioClip;
                            loadingMusic2.GetComponent<AudioSource>().Play();
                        }
                    }
                    loadingMusic1 = (GameObject)null;
                    loadingMusic2 = (GameObject)null;
                    CustomLoadingMusicRequest = (UnityWebRequest)null;
                }
            }
            yield return (object)null;
        }
    }
}
