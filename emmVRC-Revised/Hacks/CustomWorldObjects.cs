﻿
using emmVRC.Objects;
using System.Collections;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class CustomWorldObjects
    {
        public static void Initialize()
        {
        }

        public static IEnumerator OnRoomEnter()
        {
            while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                yield return (object)new WaitForEndOfFrame();
            foreach (GameObject parent in (GameObject[])UnityEngine.Resources.FindObjectsOfTypeAll<GameObject>())
            {
                if (parent.name == "eVRCDisable")
                    parent.SetActive(false);
                else if (parent.name == "eVRCEnable")
                    parent.SetActive(true);
                else if (parent.name == "eVRCPanel")
                    emmVRCPanel.Instantiate(parent);
            }
        }
    }
}
