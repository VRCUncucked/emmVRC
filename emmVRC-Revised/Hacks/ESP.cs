﻿
using MelonLoader;
using System.Collections;
using UnhollowerBaseLib;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class ESP
    {
        public static bool ESPEnabled;
        private static bool wasEnabled;

        public static void Initialize() => MelonCoroutines.Start(ESP.Loop());

        private static IEnumerator Loop()
        {
            while (true)
            {
                do
                {
                    yield return (object)new WaitForEndOfFrame();
                    if (ESP.ESPEnabled)
                    {
                        foreach (GameObject gameObject in (Il2CppArrayBase<GameObject>)GameObject.FindGameObjectsWithTag("Player"))
                        {
                            if ((bool)(Object)gameObject.transform.Find("SelectRegion"))
                                HighlightsFX.prop_HighlightsFX_0.Method_Public_Void_Renderer_Boolean_0(gameObject.transform.Find("SelectRegion").GetComponent<Renderer>(), true);
                        }
                        ESP.wasEnabled = true;
                    }
                }
                while (ESP.ESPEnabled || !ESP.wasEnabled);
                foreach (GameObject gameObject in (Il2CppArrayBase<GameObject>)GameObject.FindGameObjectsWithTag("Player"))
                {
                    if ((bool)(Object)gameObject.transform.Find("SelectRegion"))
                        HighlightsFX.prop_HighlightsFX_0.Method_Public_Void_Renderer_Boolean_0(gameObject.transform.Find("SelectRegion").GetComponent<Renderer>(), false);
                }
                ESP.wasEnabled = false;
            }
        }
    }
}
