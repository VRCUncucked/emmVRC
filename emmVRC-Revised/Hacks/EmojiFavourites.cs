﻿
using emmVRC.Libraries;
using Il2CppSystem.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class EmojiFavourites
    {
        public static QMNestedButton baseMenu;
        public static PaginatedMenu AvailableEmojiMenu;
        public static PaginatedMenu CurrentEmojiMenu;
        private static QMSingleButton availableEmojiMenuButton;
        private static QMSingleButton currentEmojiMenuButton;
        public static List<GameObject> AvailableEmojis;
        private static FieldInfo emojiGenField;
        private static FieldInfo emojisField;

        public static IEnumerator Initialize()
        {
            EmojiFavourites.baseMenu = new QMNestedButton("ShortcutMenu", 13213, 29481, "", "");
            EmojiFavourites.baseMenu.getMainButton().DestroyMe();
            EmojiFavourites.AvailableEmojiMenu = new PaginatedMenu(EmojiFavourites.baseMenu, 1, 0, "", "", new Color?());
            EmojiFavourites.AvailableEmojiMenu.menuEntryButton.DestroyMe();
            EmojiFavourites.CurrentEmojiMenu = new PaginatedMenu(EmojiFavourites.baseMenu, 2, 0, "", "", new Color?());
            EmojiFavourites.CurrentEmojiMenu.menuEntryButton.DestroyMe();
            EmojiFavourites.availableEmojiMenuButton = new QMSingleButton(EmojiFavourites.baseMenu, 1, 0, "Add\nEmoji", new System.Action(EmojiFavourites.OpenAvailableEmojiMenu), "Add an Emoji to your favorites");
            EmojiFavourites.currentEmojiMenuButton = new QMSingleButton(EmojiFavourites.baseMenu, 2, 0, "Remove\nEmoji", new System.Action(EmojiFavourites.OpenDeleteFavouriteEmojiMenu), "Delete an Emoji from your favorites");
            while ((UnityEngine.Object)VRCPlayer.field_Internal_Static_VRCPlayer_0 == (UnityEngine.Object)null)
                yield return (object)new WaitForSeconds(1f);
            EmojiFavourites.AvailableEmojis = EmojiFavourites.emojiObjects();
        }

        public static List<GameObject> emojiObjects()
        {
            Il2CppReferenceArray<FieldInfo> fields1 = Il2CppType.Of<VRCPlayer>().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            if (EmojiFavourites.emojiGenField == (FieldInfo)null || EmojiFavourites.emojisField == (FieldInfo)null)
            {
                foreach (FieldInfo fieldInfo1 in (Il2CppArrayBase<FieldInfo>)fields1)
                {
                    Il2CppReferenceArray<FieldInfo> fields2 = fieldInfo1.FieldType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    if (fields2.Length <= 1)
                    {
                        foreach (FieldInfo fieldInfo2 in (Il2CppArrayBase<FieldInfo>)fields2)
                        {
                            if (fieldInfo2.FieldType.IsArray && fieldInfo2.FieldType.GetElementType() == Il2CppType.Of<GameObject>())
                            {
                                EmojiFavourites.emojiGenField = fieldInfo1;
                                EmojiFavourites.emojisField = fieldInfo2;
                                break;
                            }
                        }
                    }
                }
            }
            Il2CppSystem.Object @object = EmojiFavourites.emojiGenField.GetValue((Il2CppSystem.Object)VRCPlayer.field_Internal_Static_VRCPlayer_0);
            return EmojiFavourites.emojisField.GetValue(@object).Cast<Il2CppReferenceArray<GameObject>>().ToList<GameObject>();
        }

        public static void OpenAvailableEmojiMenu()
        {
            if (Configuration.JSONConfig.FavouritedEmojis.Count >= 8)
            {
                VRCUiPopupManager.prop_VRCUiPopupManager_0.ShowAlert("emmVRC", "You have reached the maximum amount of Emoji Favorites.", 0.0f);
            }
            else
            {
                EmojiFavourites.AvailableEmojiMenu.pageItems.Clear();
                for (int index1 = 0; index1 < EmojiFavourites.AvailableEmojis.Count; ++index1)
                {
                    if (!Configuration.JSONConfig.FavouritedEmojis.Contains(index1))
                    {
                        int emojiValue = index1;
                        PageItem pageItem = new PageItem("", (System.Action)(() =>
                       {
                           Configuration.JSONConfig.FavouritedEmojis.Add(emojiValue);
                           Configuration.SaveConfig();
                           QuickMenuUtils.ShowQuickmenuPage(EmojiFavourites.baseMenu.getMenuName());
                       }), EmojiFavourites.AvailableEmojis[index1].name.Replace("Emoji", "").Replace("_", " "));
                        Texture2D texture = EmojiFavourites.AvailableEmojis[index1].GetComponent<ParticleSystemRenderer>().material.mainTexture.Cast<Texture2D>();
                        pageItem.buttonSprite = Sprite.CreateSprite(texture, new Rect(0.0f, 0.0f, (float)texture.width, (float)texture.height), new Vector2(0.5f, 0.5f), 25f, 0U, SpriteMeshType.FullRect, new Vector4(), false);
                        EmojiFavourites.AvailableEmojiMenu.pageItems.Add(pageItem);
                        if (index1 == 30)
                        {
                            for (int index2 = 0; index2 < 5; ++index2)
                                EmojiFavourites.AvailableEmojiMenu.pageItems.Add(PageItem.Space);
                        }
                        if (index1 == 38)
                            EmojiFavourites.AvailableEmojiMenu.pageItems.Add(PageItem.Space);
                    }
                    else
                        EmojiFavourites.AvailableEmojiMenu.pageItems.Add(PageItem.Space);
                }
                EmojiFavourites.AvailableEmojiMenu.OpenMenu();
            }
        }

        public static void OpenDeleteFavouriteEmojiMenu()
        {
            if (Configuration.JSONConfig.FavouritedEmojis.Count == 0)
                return;
            EmojiFavourites.CurrentEmojiMenu.pageItems.Clear();
            foreach (int favouritedEmoji in Configuration.JSONConfig.FavouritedEmojis)
            {
                int delEmojiValue = favouritedEmoji;
                PageItem pageItem = new PageItem("", (System.Action)(() =>
               {
                   Configuration.JSONConfig.FavouritedEmojis.Remove(delEmojiValue);
                   Configuration.SaveConfig();
                   QuickMenuUtils.ShowQuickmenuPage(EmojiFavourites.baseMenu.getMenuName());
               }), EmojiFavourites.AvailableEmojis[favouritedEmoji].name.Replace("Emoji", "").Replace("_", " "));
                Texture2D texture = EmojiFavourites.AvailableEmojis[favouritedEmoji].GetComponent<ParticleSystemRenderer>().material.mainTexture.Cast<Texture2D>();
                pageItem.buttonSprite = Sprite.CreateSprite(texture, new Rect(0.0f, 0.0f, (float)texture.width, (float)texture.height), new Vector2(0.5f, 0.5f), 25f, 0U, SpriteMeshType.FullRect, new Vector4(), false);
                EmojiFavourites.CurrentEmojiMenu.pageItems.Add(pageItem);
            }
            EmojiFavourites.CurrentEmojiMenu.OpenMenu();
        }
    }
}
