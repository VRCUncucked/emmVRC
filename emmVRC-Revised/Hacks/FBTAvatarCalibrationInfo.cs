﻿
using UnityEngine;

namespace emmVRC.Hacks
{
    public class FBTAvatarCalibrationInfo
    {
        public string AvatarID;
        public Vector3 LeftFootTrackerPosition;
        public Vector3 RightFootTrackerPosition;
        public Vector3 HipTrackerPosition;
        public Quaternion LeftFootTrackerRotation;
        public Quaternion RightFootTrackerRotation;
        public Quaternion HipTrackerRotation;
        public float PlayerHeight;
    }
}
