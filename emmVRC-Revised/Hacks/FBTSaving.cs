﻿
using emmVRC.Libraries;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using VRC.Core;

namespace emmVRC.Hacks
{
    public class FBTSaving
    {
        public static List<FBTAvatarCalibrationInfo> calibratedAvatars;
        public static UnityEngine.UI.Button.ButtonClickedEvent originalCalibrateButton;
        public static PropertyInfo leftFootInf;
        public static PropertyInfo rightFootInf;
        public static PropertyInfo hipInf;

        public static void Initialize()
        {
            FBTSaving.calibratedAvatars = new List<FBTAvatarCalibrationInfo>();
            FBTSaving.originalCalibrateButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/CalibrateButton").GetComponent<UnityEngine.UI.Button>().onClick;
            VRCTrackingSteam steam = UnityEngine.Resources.FindObjectsOfTypeAll<VRCTrackingSteam>().First<VRCTrackingSteam>();
            FBTSaving.leftFootInf = ((IEnumerable<PropertyInfo>)typeof(VRCTrackingSteam).GetProperties()).First<PropertyInfo>((Func<PropertyInfo, bool>)(a => a.PropertyType == typeof(Transform) && ((Transform)a.GetValue((object)steam)).parent.name == "Puck1"));
            FBTSaving.rightFootInf = ((IEnumerable<PropertyInfo>)typeof(VRCTrackingSteam).GetProperties()).First<PropertyInfo>((Func<PropertyInfo, bool>)(a => a.PropertyType == typeof(Transform) && ((Transform)a.GetValue((object)steam)).parent.name == "Puck2"));
            FBTSaving.hipInf = ((IEnumerable<PropertyInfo>)typeof(VRCTrackingSteam).GetProperties()).First<PropertyInfo>((Func<PropertyInfo, bool>)(a => a.PropertyType == typeof(Transform) && ((Transform)a.GetValue((object)steam)).parent.name == "Puck3"));
            ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/CalibrateButton").GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/CalibrateButton").GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
         {
             if (Configuration.JSONConfig.TrackingSaving)
             {
                 if ((UnityEngine.Object)VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0 != (UnityEngine.Object)null)
                 {
                     try
                     {
                         ApiAvatar targetAvtr = VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_1 == null ? VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_0 : VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_1;
                         if (FBTSaving.calibratedAvatars.FindIndex((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == targetAvtr.id)) != -1)
                             FBTSaving.calibratedAvatars.RemoveAll((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == targetAvtr.id));
                     }
                     catch (Exception ex)
                     {
                         FBTSaving.originalCalibrateButton.Invoke();
                         emmVRCLoader.Logger.LogError("An error occured with FBT Saving. Invoking original calibration button. Error: " + ex.ToString());
                     }
                 }
             }
             FBTSaving.originalCalibrateButton.Invoke();
         }));
        }

        public static IEnumerator SaveCalibrationInfo(
          VRCTrackingSteam trackingSteam,
          string avatarID)
        {
            yield return (object)new WaitForSeconds(1f);
            if (FBTSaving.calibratedAvatars.FindIndex((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == avatarID)) != -1)
                FBTSaving.calibratedAvatars.RemoveAll((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == avatarID));
            FBTAvatarCalibrationInfo avatarCalibrationInfo = new FBTAvatarCalibrationInfo()
            {
                AvatarID = avatarID,
                LeftFootTrackerPosition = ((Transform)FBTSaving.leftFootInf.GetValue((object)trackingSteam)).localPosition,
                LeftFootTrackerRotation = ((Transform)FBTSaving.leftFootInf.GetValue((object)trackingSteam)).localRotation,
                RightFootTrackerPosition = ((Transform)FBTSaving.rightFootInf.GetValue((object)trackingSteam)).localPosition,
                RightFootTrackerRotation = ((Transform)FBTSaving.rightFootInf.GetValue((object)trackingSteam)).localRotation,
                HipTrackerPosition = ((Transform)FBTSaving.hipInf.GetValue((object)trackingSteam)).localPosition,
                HipTrackerRotation = ((Transform)FBTSaving.hipInf.GetValue((object)trackingSteam)).localRotation,
                PlayerHeight = VRCTrackingManager.field_Private_Static_VRCTrackingManager_0.GetPlayerHeight()
            };
            FBTSaving.calibratedAvatars.Add(avatarCalibrationInfo);
            emmVRCLoader.Logger.LogDebug("Saved calibration info");
        }

        public static void LoadCalibrationInfo(VRCTrackingSteam trackingSteam, string avatarID)
        {
            if (FBTSaving.calibratedAvatars.FindIndex((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == avatarID)) != -1)
            {
                FBTAvatarCalibrationInfo avatarCalibrationInfo = FBTSaving.calibratedAvatars.Find((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == avatarID));
                ((Transform)FBTSaving.leftFootInf.GetValue((object)trackingSteam)).localPosition = avatarCalibrationInfo.LeftFootTrackerPosition;
                ((Transform)FBTSaving.leftFootInf.GetValue((object)trackingSteam)).localRotation = avatarCalibrationInfo.LeftFootTrackerRotation;
                ((Transform)FBTSaving.rightFootInf.GetValue((object)trackingSteam)).localPosition = avatarCalibrationInfo.RightFootTrackerPosition;
                ((Transform)FBTSaving.rightFootInf.GetValue((object)trackingSteam)).localRotation = avatarCalibrationInfo.RightFootTrackerRotation;
                ((Transform)FBTSaving.hipInf.GetValue((object)trackingSteam)).localPosition = avatarCalibrationInfo.HipTrackerPosition;
                ((Transform)FBTSaving.hipInf.GetValue((object)trackingSteam)).localRotation = avatarCalibrationInfo.HipTrackerRotation;
                VRCTrackingManager.field_Private_Static_VRCTrackingManager_0.SetPlayerHeight(avatarCalibrationInfo.PlayerHeight);
            }
            emmVRCLoader.Logger.LogDebug("Loaded calibration info");
        }

        public static bool IsPreviouslyCalibrated(string avatarID) => FBTSaving.calibratedAvatars.FindIndex((Predicate<FBTAvatarCalibrationInfo>)(a => a.AvatarID == avatarID)) != -1;
    }
}
