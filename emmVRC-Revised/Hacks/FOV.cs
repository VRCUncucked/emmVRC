﻿
using UnityEngine;

namespace emmVRC.Hacks
{
    public class FOV
    {
        public static void Initialize()
        {
            if (Configuration.JSONConfig.CustomFOV == 60)
                return;
            GameObject gameObject = GameObject.Find("Camera (eye)");
            if ((Object)gameObject == (Object)null)
                gameObject = GameObject.Find("CenterEyeAnchor");
            if (!((Object)gameObject != (Object)null))
                return;
            Camera component = gameObject.GetComponent<Camera>();
            if (!((Object)component != (Object)null))
                return;
            component.fieldOfView = (float)Configuration.JSONConfig.CustomFOV;
        }
    }
}
