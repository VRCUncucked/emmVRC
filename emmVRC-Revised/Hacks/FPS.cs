﻿
using UnityEngine;

namespace emmVRC.Hacks
{
    public class FPS
    {
        public static void Initialize()
        {
            if (!Configuration.JSONConfig.UnlimitedFPSEnabled)
                return;
            Application.targetFrameRate = Configuration.JSONConfig.FPSLimit;
        }
    }
}
