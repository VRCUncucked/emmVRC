﻿
using emmVRC.Libraries;
using MelonLoader;
using System.Collections;
using UnityEngine;
using UnityEngine.XR;
using VRC;
using VRC.Animation;

namespace emmVRC.Hacks
{
    public class Flight
    {
        public static bool FlightEnabled;
        public static bool NoclipEnabled;
        private static GameObject localPlayer;
        private static Player player;
        private static VRCMotionState motionState;
        private static InputStateController stateController;
        private static Vector3 originalGravity;

        public static void Initialize() => MelonCoroutines.Start(Flight.Loop());

        public static IEnumerator Loop()
        {
            while (true)
            {
                if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
                {
                    if ((Object)Flight.localPlayer == (Object)null && (Object)VRCPlayer.field_Internal_Static_VRCPlayer_0 != (Object)null && (Object)VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject != (Object)null)
                        Flight.localPlayer = VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject;
                    if ((Object)Flight.motionState == (Object)null && (Object)Flight.localPlayer != (Object)null)
                        Flight.motionState = Flight.localPlayer.GetComponent<VRCMotionState>();
                    if ((Object)Flight.stateController == (Object)null && (Object)Flight.localPlayer != (Object)null)
                        Flight.stateController = Flight.localPlayer.GetComponent<InputStateController>();
                    if ((Object)Flight.player == (Object)null && (Object)VRCPlayer.field_Internal_Static_VRCPlayer_0 != (Object)null)
                    {
                        Flight.player = VRCPlayer.field_Internal_Static_VRCPlayer_0._player;
                    }
                    else
                    {
                        if (Flight.FlightEnabled && Flight.originalGravity == Vector3.zero)
                        {
                            Flight.originalGravity = Physics.gravity;
                            Physics.gravity = Vector3.zero;
                        }
                        if (!Flight.FlightEnabled && Flight.originalGravity != Vector3.zero)
                        {
                            Physics.gravity = Flight.originalGravity;
                            Flight.originalGravity = Vector3.zero;
                        }
                        if (!Flight.FlightEnabled && Flight.NoclipEnabled)
                            Flight.NoclipEnabled = false;
                        if (Flight.FlightEnabled)
                        {
                            Transform transform = Camera.main.transform;
                            if (XRDevice.isPresent && Configuration.JSONConfig.VRFlightControls)
                            {
                                if ((double)Input.GetAxis("Vertical") != 0.0)
                                    Flight.localPlayer.transform.position += Flight.localPlayer.transform.forward * Time.deltaTime * Input.GetAxis("Vertical") * (Speed.SpeedModified ? Speed.Modifier : 1f) * 2f;
                                if ((double)Input.GetAxis("Horizontal") != 0.0)
                                    Flight.localPlayer.transform.position += Flight.localPlayer.transform.right * Time.deltaTime * Input.GetAxis("Horizontal") * (Speed.SpeedModified ? Speed.Modifier : 1f) * 2f;
                                if ((double)Input.GetAxis("Oculus_CrossPlatform_SecondaryThumbstickVertical") != 0.0)
                                    Flight.localPlayer.transform.position += new Vector3(0.0f, (float)((double)Time.deltaTime * (double)Input.GetAxis("Oculus_CrossPlatform_SecondaryThumbstickVertical") * (Speed.SpeedModified ? (double)Speed.Modifier : 1.0) * 2.0));
                            }
                            else
                            {
                                if ((double)Input.GetAxis("Vertical") != 0.0)
                                    Flight.localPlayer.transform.position += transform.transform.forward * Time.deltaTime * Input.GetAxis("Vertical") * (Speed.SpeedModified ? Speed.Modifier : 1f) * (Input.GetKey(KeyCode.LeftShift) ? 4f : 2f);
                                if ((double)Input.GetAxis("Horizontal") != 0.0)
                                    Flight.localPlayer.transform.position += transform.transform.right * Time.deltaTime * Input.GetAxis("Horizontal") * (Speed.SpeedModified ? Speed.Modifier : 1f) * (Input.GetKey(KeyCode.LeftShift) ? 4f : 2f);
                                if (Input.GetKey(KeyCode.Q))
                                    Flight.localPlayer.transform.position -= new Vector3(0.0f, (float)((double)Time.deltaTime * (Input.GetKey(KeyCode.LeftShift) ? 4.0 : 2.0) * (Speed.SpeedModified ? (double)Speed.Modifier : 1.0)), 0.0f);
                                if (Input.GetKey(KeyCode.E))
                                    Flight.localPlayer.transform.position += new Vector3(0.0f, (float)((double)Time.deltaTime * (Input.GetKey(KeyCode.LeftShift) ? 4.0 : 2.0) * (Speed.SpeedModified ? (double)Speed.Modifier : 1.0)), 0.0f);
                            }
                            if ((Object)Flight.motionState != (Object)null)
                            {
                                Flight.motionState.Reset();
                                if ((Object)Flight.motionState.field_Private_CharacterController_0 != (Object)null)
                                    Flight.motionState.field_Private_CharacterController_0.enabled = !Flight.NoclipEnabled;
                            }
                            if ((Object)Flight.stateController != (Object)null && !Flight.NoclipEnabled)
                                Flight.stateController.ResetLastPosition();
                        }
                    }
                }
                yield return (object)new WaitForEndOfFrame();
            }
        }
    }
}
