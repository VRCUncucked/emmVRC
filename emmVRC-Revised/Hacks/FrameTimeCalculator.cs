﻿
using MelonLoader;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class FrameTimeCalculator
    {
        public static int[] frameTimes = new int[30];
        public static int frameTimeAvg = 0;
        public static int iterator = 0;
        private static Stopwatch watch = new Stopwatch();

        public static void Update()
        {
            FrameTimeCalculator.watch.Start();
            MelonCoroutines.Start(FrameTimeCalculator.EndOfFrame());
        }

        public static IEnumerator EndOfFrame()
        {
            yield return (object)new WaitForEndOfFrame();
            FrameTimeCalculator.frameTimes[FrameTimeCalculator.iterator] = (int)FrameTimeCalculator.watch.ElapsedMilliseconds;
            if (FrameTimeCalculator.iterator < FrameTimeCalculator.frameTimes.Length - 1)
                ++FrameTimeCalculator.iterator;
            else
                FrameTimeCalculator.iterator = 0;
            FrameTimeCalculator.watch.Reset();
            int num = 0;
            foreach (int frameTime in FrameTimeCalculator.frameTimes)
                num += frameTime;
            FrameTimeCalculator.frameTimeAvg = Mathf.CeilToInt((float)(num / FrameTimeCalculator.frameTimes.Length));
        }
    }
}
