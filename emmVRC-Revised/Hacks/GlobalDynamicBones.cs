﻿
using emmVRC.Managers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRC.Core;
using VRC.SDKBase;

namespace emmVRC.Hacks
{
    public class GlobalDynamicBones
    {
        private static List<DynamicBone> currentWorldDynamicBones = new List<DynamicBone>();
        private static List<DynamicBoneCollider> currentWorldDynamicBoneColliders = new List<DynamicBoneCollider>();

        public static void ProcessDynamicBones(
          GameObject avatarObject,
          VRC_AvatarDescriptor avatarDescriptor)
        {
            if (Configuration.JSONConfig.GlobalDynamicBonesEnabled)
            {
                if ((Object)avatarObject == (Object)null)
                    emmVRCLoader.Logger.LogDebug("Avatar object is null");
                if ((Object)avatarDescriptor == (Object)null)
                    emmVRCLoader.Logger.LogDebug("Avatar descriptor is null");
                else if ((Object)avatarDescriptor.GetComponent<PipelineManager>() == (Object)null)
                    emmVRCLoader.Logger.LogDebug("Avatar pipeline is null");
                if ((Object)avatarObject == (Object)null)
                    emmVRCLoader.Logger.LogDebug("Avatar VRCPlayer is null");
                if ((Object)avatarObject == (Object)null || (Object)avatarDescriptor == (Object)null || ((Object)avatarDescriptor.GetComponent<PipelineManager>() == (Object)null || (Object)avatarDescriptor.GetComponentInParent<VRCPlayer>() == (Object)null))
                    return;
                AvatarPermissions avatarPermissions = AvatarPermissions.GetAvatarPermissions(avatarDescriptor.GetComponent<PipelineManager>().blueprintId);
                if (UserPermissions.GetUserPermissions(avatarDescriptor.GetComponentInParent<VRCPlayer>().prop_Player_0.prop_APIUser_0.id).GlobalDynamicBonesEnabled || avatarDescriptor.GetComponentInParent<VRCPlayer>().prop_Player_0.prop_APIUser_0.id == APIUser.CurrentUser.id || Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled || Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled && APIUser.IsFriendsWith(avatarDescriptor.GetComponentInParent<VRCPlayer>().prop_Player_0.prop_APIUser_0.id))
                {
                    emmVRCLoader.Logger.LogDebug("Global Dynamic Bones is allowed for this user, processing...");
                    if (!avatarPermissions.HandColliders && !avatarPermissions.FeetColliders)
                    {
                        emmVRCLoader.Logger.LogDebug("Hand and Feet colliders are disabled, so we're fetching all colliders.");
                        foreach (DynamicBoneCollider componentsInChild in avatarObject.GetComponentsInChildren<DynamicBoneCollider>())
                            GlobalDynamicBones.currentWorldDynamicBoneColliders.Add(componentsInChild);
                        emmVRCLoader.Logger.LogDebug("There are " + GlobalDynamicBones.currentWorldDynamicBoneColliders.Count.ToString() + " processed colliders in this instance.");
                    }
                    else
                    {
                        emmVRCLoader.Logger.LogDebug("Hand colliders are " + (avatarPermissions.HandColliders ? "enabled" : "disabled") + ", foot colliders are " + (avatarPermissions.FeetColliders ? "enabled" : "disabled"));
                        if (avatarPermissions.HandColliders && (Object)avatarObject.GetComponentInChildren<Animator>() != (Object)null && ((Object)avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftHand) != (Object)null && (Object)avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand) != (Object)null))
                        {
                            emmVRCLoader.Logger.LogDebug("This avatar has valid hands, fetching colliders...");
                            foreach (DynamicBoneCollider componentsInChild in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).GetComponentsInChildren<DynamicBoneCollider>())
                            {
                                if (componentsInChild.m_Bound != DynamicBoneCollider.EnumNPublicSealedvaOuIn3vUnique.Inside)
                                    GlobalDynamicBones.currentWorldDynamicBoneColliders.Add(componentsInChild);
                            }
                            foreach (DynamicBoneCollider componentsInChild in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightHand).GetComponentsInChildren<DynamicBoneCollider>())
                            {
                                if (componentsInChild.m_Bound != DynamicBoneCollider.EnumNPublicSealedvaOuIn3vUnique.Inside)
                                    GlobalDynamicBones.currentWorldDynamicBoneColliders.Add(componentsInChild);
                            }
                            emmVRCLoader.Logger.LogDebug("There are " + GlobalDynamicBones.currentWorldDynamicBoneColliders.Count.ToString() + " processed colliders in this instance.");
                        }
                        if (avatarPermissions.FeetColliders && (Object)avatarObject.GetComponentInChildren<Animator>() != (Object)null && ((Object)avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot) != (Object)null && (Object)avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightFoot) != (Object)null))
                        {
                            emmVRCLoader.Logger.LogDebug("This avatar has valid feet, fetching colliders...");
                            foreach (DynamicBoneCollider componentsInChild in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).GetComponentsInChildren<DynamicBoneCollider>())
                            {
                                if (componentsInChild.m_Bound != DynamicBoneCollider.EnumNPublicSealedvaOuIn3vUnique.Inside)
                                    GlobalDynamicBones.currentWorldDynamicBoneColliders.Add(componentsInChild);
                            }
                            foreach (DynamicBoneCollider componentsInChild in avatarObject.GetComponentInChildren<Animator>().GetBoneTransform(HumanBodyBones.RightFoot).GetComponentsInChildren<DynamicBoneCollider>())
                            {
                                if (componentsInChild.m_Bound != DynamicBoneCollider.EnumNPublicSealedvaOuIn3vUnique.Inside)
                                    GlobalDynamicBones.currentWorldDynamicBoneColliders.Add(componentsInChild);
                            }
                            emmVRCLoader.Logger.LogDebug("There are " + GlobalDynamicBones.currentWorldDynamicBoneColliders.Count.ToString() + " processed colliders in this instance.");
                        }
                    }
                    if (!avatarPermissions.HeadBones && !avatarPermissions.ChestBones && !avatarPermissions.HipBones)
                    {
                        emmVRCLoader.Logger.LogDebug("Global Dynamic Bones are allowed for all parts of the avatar. Processing bones...");
                        foreach (DynamicBone componentsInChild in avatarObject.GetComponentsInChildren<DynamicBone>())
                            GlobalDynamicBones.currentWorldDynamicBones.Add(componentsInChild);
                        emmVRCLoader.Logger.LogDebug("There are " + GlobalDynamicBones.currentWorldDynamicBones.Count.ToString() + " processed bones in this instance.");
                    }
                    emmVRCLoader.Logger.LogDebug("Now processing available dynamic bones, and adding colliders...");
                    foreach (DynamicBone dynamicBone in GlobalDynamicBones.currentWorldDynamicBones.ToList<DynamicBone>())
                    {
                        if ((Object)dynamicBone == (Object)null)
                        {
                            GlobalDynamicBones.currentWorldDynamicBones.Remove(dynamicBone);
                        }
                        else
                        {
                            foreach (DynamicBoneCollider dynamicBoneCollider in GlobalDynamicBones.currentWorldDynamicBoneColliders.ToList<DynamicBoneCollider>())
                            {
                                if ((Object)dynamicBoneCollider == (Object)null)
                                    GlobalDynamicBones.currentWorldDynamicBoneColliders.Remove(dynamicBoneCollider);
                                else if (dynamicBone.m_Colliders.IndexOf(dynamicBoneCollider) == -1)
                                    dynamicBone.m_Colliders.Add(dynamicBoneCollider);
                            }
                        }
                    }
                }
            }
            emmVRCLoader.Logger.LogDebug("Done processing dynamic bones for avatar.");
        }
    }
}
