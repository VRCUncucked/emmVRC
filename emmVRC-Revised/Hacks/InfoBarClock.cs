﻿
using emmVRC.Libraries;
using emmVRCLoader;
using MelonLoader;
using Microsoft.Win32;
using System.Collections;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class InfoBarClock
    {
        private static bool Enabled = true;
        public static Text clockText;
        public static uint instanceTime = 0;
        private static bool amPm = false;

        public static void Initialize()
        {
            InfoBarClock.amPm = Registry.GetValue("HKEY_CURRENT_USER\\Control Panel\\International", "sShortTime", (object)"x").ToString().Contains("h");
            Transform transform1 = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/PingText");
            if ((UnityEngine.Object)transform1 != (UnityEngine.Object)null)
            {
                Transform transform2 = new GameObject("emmVRCClock", (Il2CppReferenceArray<Il2CppSystem.Type>)new Il2CppSystem.Type[2]
                {
          Il2CppType.Of<RectTransform>(),
          Il2CppType.Of<Text>()
                }).transform;
                transform2.SetParent(transform1.parent, false);
                transform2.SetSiblingIndex(transform1.GetSiblingIndex() + 1);
                InfoBarClock.clockText = transform2.GetComponent<Text>();
                RectTransform component = transform2.GetComponent<RectTransform>();
                component.localScale = transform1.localScale;
                component.anchorMin = transform1.GetComponent<RectTransform>().anchorMin;
                component.anchorMax = transform1.GetComponent<RectTransform>().anchorMax;
                component.anchoredPosition = transform1.GetComponent<RectTransform>().anchoredPosition;
                component.sizeDelta = new Vector2(2000f, 92f);
                component.pivot = transform1.GetComponent<RectTransform>().pivot;
                Vector3 localPosition = transform1.localPosition;
                localPosition.x -= transform1.GetComponent<RectTransform>().sizeDelta.x * 0.5f;
                localPosition.x += 687.5f;
                localPosition.y += -90f;
                component.localPosition = localPosition;
                InfoBarClock.clockText.text = "(00:00:00) NA:NA PM";
                InfoBarClock.clockText.color = transform1.GetComponent<Text>().color;
                InfoBarClock.clockText.font = transform1.GetComponent<Text>().font;
                InfoBarClock.clockText.fontSize = transform1.GetComponent<Text>().fontSize - 8;
                InfoBarClock.clockText.fontStyle = transform1.GetComponent<Text>().fontStyle;
                InfoBarClock.clockText.gameObject.SetActive(Configuration.JSONConfig.ClockEnabled);
                MelonCoroutines.Start(InfoBarClock.Loop());
            }
            else
                emmVRCLoader.Logger.LogError("QuickMenu/ShortcutMenu/PingText is null");
        }

        private static IEnumerator Loop()
        {
            while (InfoBarClock.Enabled)
            {
                yield return (object)new WaitForSecondsRealtime(1f);
                ++InfoBarClock.instanceTime;
                if (Configuration.JSONConfig.ClockEnabled)
                {
                    InfoBarClock.clockText.gameObject.SetActive(true);
                    string str1 = System.DateTime.Now.ToString(InfoBarClock.amPm ? "hh:mm tt" : "HH:mm");
                    string str2 = "00:00:00";
                    if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
                    {
                        System.TimeSpan timeSpan = System.TimeSpan.FromSeconds((double)InfoBarClock.instanceTime);
                        str2 = string.Format("{0:D2}:{1:D2}:{2:D2}", (object)timeSpan.Hours, (object)timeSpan.Minutes, (object)timeSpan.Seconds);
                    }
                    InfoBarClock.clockText.text = "(" + str2 + ") " + str1;
                }
                else
                    InfoBarClock.clockText.gameObject.SetActive(false);
            }
        }
    }
}
