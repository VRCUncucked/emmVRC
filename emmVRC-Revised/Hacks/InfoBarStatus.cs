﻿
using emmVRC.Libraries;
using emmVRC.Network;
using emmVRC.Objects;
using emmVRCLoader;
using MelonLoader;
using System.Collections;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class InfoBarStatus
    {
        private static bool Enabled = true;
        internal static Text emmVRCStatusText;

        public static void Initialize()
        {
            Transform transform1 = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/BuildNumText");
            if ((UnityEngine.Object)transform1 != (UnityEngine.Object)null)
            {
                Transform transform2 = new GameObject("emmVRCStatus", (Il2CppReferenceArray<Il2CppSystem.Type>)new Il2CppSystem.Type[2]
                {
          Il2CppType.Of<RectTransform>(),
          Il2CppType.Of<Text>()
                }).transform;
                transform2.SetParent(transform1.parent, false);
                transform2.SetSiblingIndex(transform1.GetSiblingIndex() + 1);
                InfoBarStatus.emmVRCStatusText = transform2.GetComponent<Text>();
                RectTransform component1 = transform2.GetComponent<RectTransform>();
                component1.localScale = transform1.localScale;
                component1.anchorMin = transform1.GetComponent<RectTransform>().anchorMin;
                component1.anchorMax = transform1.GetComponent<RectTransform>().anchorMax;
                component1.anchoredPosition = transform1.GetComponent<RectTransform>().anchoredPosition;
                component1.sizeDelta = new Vector2(2000f, 92f);
                component1.pivot = transform1.GetComponent<RectTransform>().pivot;
                Vector3 localPosition = transform1.localPosition;
                localPosition.x -= transform1.GetComponent<RectTransform>().sizeDelta.x * 0.5f;
                localPosition.x += 1000f;
                localPosition.y += -85f;
                component1.localPosition = localPosition;
                InfoBarStatus.emmVRCStatusText.text = "emmVRC v" + Attributes.Version;
                InfoBarStatus.emmVRCStatusText.color = transform1.GetComponent<Text>().color;
                InfoBarStatus.emmVRCStatusText.font = transform1.GetComponent<Text>().font;
                InfoBarStatus.emmVRCStatusText.fontSize = 67;
                InfoBarStatus.emmVRCStatusText.fontStyle = transform1.GetComponent<Text>().fontStyle;
                RectTransform component2 = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/Panel").GetComponent<RectTransform>();
                component2.sizeDelta += new Vector2(0.0f, 80f);
                component2.anchoredPosition -= new Vector2(0.0f, 40f);
                RectTransform component3 = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/StreamerModeEnabled/Outline").GetComponent<RectTransform>();
                component3.sizeDelta += new Vector2(0.0f, 80f);
                component3.anchoredPosition -= new Vector2(0.0f, 40f);
                ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/StreamerModeEnabled/StreamerBackground").GetComponent<RectTransform>().anchoredPosition -= new Vector2(0.0f, 160f);
                ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/StreamerModeEnabled/StreamerModeText").GetComponent<RectTransform>().anchoredPosition -= new Vector2(0.0f, 160f);
                if (!Configuration.JSONConfig.InfoBarDisplayEnabled)
                {
                    InfoBarStatus.emmVRCStatusText.gameObject.SetActive(false);
                    component2.sizeDelta = new Vector2(component2.sizeDelta.x, component2.sizeDelta.y - 80f);
                    component2.anchoredPosition = new Vector2(component2.anchoredPosition.x, component2.anchoredPosition.y + 40f);
                }
                MelonCoroutines.Start(InfoBarStatus.Loop());
            }
            else
                emmVRCLoader.Logger.LogError("QuickMenu/ShortcutMenu/PingText is null");
        }

        private static IEnumerator Loop()
        {
            while (InfoBarStatus.Enabled)
            {
                yield return (object)new WaitForSeconds(5f);
                if (Configuration.JSONConfig.InfoBarDisplayEnabled)
                {
                    InfoBarStatus.emmVRCStatusText.gameObject.SetActive(true);
                    InfoBarStatus.emmVRCStatusText.text = !Configuration.JSONConfig.emmVRCNetworkEnabled ? "<color=#FF69B4>emmVRC</color> v" + Attributes.Version : (NetworkClient.webToken == null ? "<color=#FF69B4>emmVRC</color> v" + Attributes.Version + "           Network Status: <color=red> Disconnected</color>" : "<color=#FF69B4>emmVRC</color> v" + Attributes.Version + "                Network Status: <color=lime>Connected</color>");
                }
                else
                    InfoBarStatus.emmVRCStatusText.gameObject.SetActive(false);
            }
        }
    }
}
