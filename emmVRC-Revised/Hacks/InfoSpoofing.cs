﻿
using emmVRC.Libraries;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;

namespace emmVRC.Hacks
{
    public class InfoSpoofing
    {
        private static bool Enabled = true;
        private static bool wasEnabled1 = false;
        private static bool wasEnabled2 = false;
        private static List<Transform> objectRoots;

        public static void Initialize()
        {
            NameSpoofGenerator.GenerateNewName();
            MelonCoroutines.Start(InfoSpoofing.Loop());
        }

        private static IEnumerator Loop()
        {
            while (InfoSpoofing.Enabled)
            {
                yield return (object)new WaitForEndOfFrame();
                if (InfoSpoofing.objectRoots == null && (UnityEngine.Object)QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Social") != (UnityEngine.Object)null && ((UnityEngine.Object)QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/WorldInfo") != (UnityEngine.Object)null && (UnityEngine.Object)QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/UserInfo") != (UnityEngine.Object)null))
                {
                    InfoSpoofing.objectRoots = new List<Transform>();
                    InfoSpoofing.objectRoots.Add(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Social"));
                    InfoSpoofing.objectRoots.Add(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/WorldInfo"));
                    InfoSpoofing.objectRoots.Add(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/UserInfo"));
                    InfoSpoofing.objectRoots.Add(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings"));
                }
                if (Configuration.JSONConfig.InfoSpoofingEnabled)
                {
                    if (InfoSpoofing.objectRoots != null)
                    {
                        try
                        {
                            if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
                            {
                                foreach (Transform objectRoot in InfoSpoofing.objectRoots)
                                {
                                    if (objectRoot.gameObject.activeInHierarchy)
                                    {
                                        foreach (Text componentsInChild in objectRoot.GetComponentsInChildren<Text>())
                                        {
                                            if (componentsInChild.text.Contains(APIUser.CurrentUser.GetName()))
                                                componentsInChild.text = componentsInChild.text.Replace(APIUser.CurrentUser.GetName(), NameSpoofGenerator.spoofedName);
                                        }
                                        InfoSpoofing.wasEnabled1 = true;
                                    }
                                }
                                if (((Component)QuickMenuUtils.GetQuickMenuInstance()).gameObject.activeInHierarchy)
                                {
                                    foreach (Text componentsInChild in ((Component)QuickMenuUtils.GetQuickMenuInstance()).gameObject.GetComponentsInChildren<Text>())
                                    {
                                        if (componentsInChild.text.Contains(APIUser.CurrentUser.GetName()))
                                            componentsInChild.text = componentsInChild.text.Replace(APIUser.CurrentUser.GetName(), NameSpoofGenerator.spoofedName);
                                    }
                                    InfoSpoofing.wasEnabled2 = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exception exception = new Exception();
                        }
                        if (!Configuration.JSONConfig.InfoSpoofingEnabled && (InfoSpoofing.wasEnabled1 || InfoSpoofing.wasEnabled2))
                        {
                            if (InfoSpoofing.objectRoots != null)
                            {
                                try
                                {
                                    if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
                                    {
                                        foreach (Transform objectRoot in InfoSpoofing.objectRoots)
                                        {
                                            if (objectRoot.gameObject.activeInHierarchy && InfoSpoofing.wasEnabled1)
                                            {
                                                foreach (Text componentsInChild in objectRoot.GetComponentsInChildren<Text>())
                                                {
                                                    if (componentsInChild.text.Contains(NameSpoofGenerator.spoofedName))
                                                        componentsInChild.text = componentsInChild.text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
                                                }
                                                InfoSpoofing.wasEnabled1 = false;
                                            }
                                        }
                                        if (((Component)QuickMenuUtils.GetQuickMenuInstance()).gameObject.activeInHierarchy)
                                        {
                                            if (InfoSpoofing.wasEnabled2)
                                            {
                                                foreach (Text componentsInChild in ((Component)QuickMenuUtils.GetQuickMenuInstance()).gameObject.GetComponentsInChildren<Text>())
                                                {
                                                    if (componentsInChild.text.Contains(NameSpoofGenerator.spoofedName))
                                                        componentsInChild.text = componentsInChild.text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
                                                }
                                                InfoSpoofing.wasEnabled2 = false;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    emmVRCLoader.Logger.LogError("Spoofer error: " + ex.ToString());
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
