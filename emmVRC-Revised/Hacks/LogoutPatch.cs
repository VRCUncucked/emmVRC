﻿
using emmVRC.Libraries;
using emmVRC.Network;
using Il2CppSystem.Collections.Generic;
using System;
using UnityEngine.Events;
using VRC.Core;

namespace emmVRC.Hacks
{
    public class LogoutPatch
    {
        public static void Initialize() => QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/Footer/Logout").GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(Action)(() =>
      {
          HTTPRequest.get(NetworkClient.baseURL + "/api/authentication/logout").NoAwait("Logout");
          NetworkClient.webToken = (string)null;
          CustomAvatarFavorites.LoadedAvatars = null;
      }));
    }
}
