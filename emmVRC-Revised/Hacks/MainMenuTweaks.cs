﻿
using MelonLoader;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;

namespace emmVRC.Hacks
{
    public class MainMenuTweaks
    {
        public static bool menuJustOpened;
        public static bool menuOpen;
        public static GameObject vrcPlusButton;
        public static GameObject userIconsButton;
        public static GameObject worldButton;
        public static GameObject avatarButton;
        public static GameObject socialButton;
        public static GameObject settingsButton;
        public static GameObject safetyButton;
        public static GameObject vrcPlusGetMoreFavorites;

        public static void Initialize() => MelonCoroutines.Start(MainMenuTweaks.Loop());

        public static IEnumerator Loop()
        {
            while (true)
            {
                if ((Object)MainMenuTweaks.vrcPlusButton == (Object)null)
                {
                    MainMenuTweaks.vrcPlusButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/VRC+PageTab/");
                    MainMenuTweaks.userIconsButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/GalleryTab/");
                    MainMenuTweaks.worldButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/WorldsPageTab/");
                    MainMenuTweaks.avatarButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/AvatarPageTab/");
                    MainMenuTweaks.socialButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/SocialPageTab/");
                    MainMenuTweaks.settingsButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/SettingsPageTab/");
                    MainMenuTweaks.safetyButton = GameObject.Find("/UserInterface/MenuContent/Backdrop/Header/Tabs/ViewPort/Content/SafetyPageTab/");
                    MainMenuTweaks.vrcPlusGetMoreFavorites = GameObject.Find("/UserInterface/MenuContent/Screens/Avatar/Vertical Scroll View/Viewport/Content/Favorite Avatar List/GetMoreFavorites/");
                    MainMenuTweaks.worldButton.GetComponent<LayoutElement>().preferredWidth = 250f;
                    MainMenuTweaks.avatarButton.GetComponent<LayoutElement>().preferredWidth = 250f;
                    MainMenuTweaks.socialButton.GetComponent<LayoutElement>().preferredWidth = 250f;
                    MainMenuTweaks.settingsButton.GetComponent<LayoutElement>().preferredWidth = 250f;
                    MainMenuTweaks.safetyButton.GetComponent<LayoutElement>().preferredWidth = 250f;
                }
                if (MainMenuTweaks.worldButton.activeInHierarchy && !MainMenuTweaks.menuOpen)
                {
                    MainMenuTweaks.menuJustOpened = true;
                    MainMenuTweaks.menuOpen = true;
                }
                if (MainMenuTweaks.menuOpen && MainMenuTweaks.menuJustOpened)
                {
                    while (APIUser.CurrentUser == null)
                        yield return (object)new WaitForEndOfFrame();
                    if (Configuration.JSONConfig.DisableVRCPlusMenuTabs)
                    {
                        MainMenuTweaks.vrcPlusButton.SetActive(false);
                        MainMenuTweaks.userIconsButton.GetComponent<LayoutElement>().enabled = false;
                        MainMenuTweaks.userIconsButton.GetComponent<Image>().enabled = false;
                        MainMenuTweaks.userIconsButton.transform.Find("Image_NEW").gameObject.SetActive(false);
                        MainMenuTweaks.userIconsButton.SetActive(false);
                        MainMenuTweaks.vrcPlusGetMoreFavorites.transform.localScale = Vector3.zero;
                    }
                    else
                    {
                        MainMenuTweaks.vrcPlusButton.SetActive(true);
                        MainMenuTweaks.userIconsButton.GetComponent<LayoutElement>().enabled = true;
                        MainMenuTweaks.userIconsButton.GetComponent<Image>().enabled = true;
                        MainMenuTweaks.userIconsButton.transform.Find("Image_NEW").gameObject.SetActive(true);
                        MainMenuTweaks.userIconsButton.SetActive(true);
                        MainMenuTweaks.vrcPlusGetMoreFavorites.transform.localScale = Vector3.one;
                    }
                    MainMenuTweaks.menuJustOpened = false;
                }
                if (!MainMenuTweaks.worldButton.activeInHierarchy && MainMenuTweaks.menuOpen)
                    MainMenuTweaks.menuOpen = false;
                yield return (object)new WaitForEndOfFrame();
            }
        }
    }
}
