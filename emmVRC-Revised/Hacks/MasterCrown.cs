﻿
using emmVRC.Libraries;
using MelonLoader;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VRC;
using VRC.Core;

namespace emmVRC.Hacks
{
    public class MasterCrown
    {
        private static GameObject masterIconObj;
        public static Sprite crownSprite;

        public static void Initialize() => MelonCoroutines.Start(MasterCrown.Loop());

        private static IEnumerator Loop()
        {
            while (true)
            {
                while ((UnityEngine.Object)Resources.crownSprite == (UnityEngine.Object)null)
                    yield return (object)null;
                try
                {
                    if (RoomManager.field_Internal_Static_ApiWorld_0 != null && Configuration.JSONConfig.MasterIconEnabled && (UnityEngine.Object)MasterCrown.masterIconObj == (UnityEngine.Object)null)
                    {
                        if (PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.Count > 1)
                        {
                            PlayerUtils.GetEachPlayer((System.Action<Player>)(player =>
                           {
                               if (!((UnityEngine.Object)MasterCrown.masterIconObj == (UnityEngine.Object)null) || !player.prop_VRCPlayerApi_0.isMaster || !(player.prop_APIUser_0.id != APIUser.CurrentUser.id))
                                   return;
                               emmVRCLoader.Logger.LogDebug("Initializing master icon...");
                               GameObject gameObject = player._vrcplayer.field_Private_Transform_0.parent.transform.Find("Player Nameplate/Canvas/Nameplate/Contents/Friend Marker").gameObject;
                               emmVRCLoader.Logger.LogDebug("1");
                               MasterCrown.masterIconObj = UnityEngine.Object.Instantiate<GameObject>(gameObject, gameObject.transform.parent);
                               emmVRCLoader.Logger.LogDebug("2");
                               MasterCrown.masterIconObj.GetComponent<RectTransform>().anchoredPosition += new Vector2(256f, 24f);
                               emmVRCLoader.Logger.LogDebug("3");
                               MasterCrown.masterIconObj.GetComponent<Image>().sprite = Resources.crownSprite;
                               emmVRCLoader.Logger.LogDebug("4");
                               MasterCrown.masterIconObj.SetActive(true);
                               emmVRCLoader.Logger.LogDebug("Master icon initialized");
                           }));
                        }
                        else
                        {
                            UnityEngine.Object.Destroy((UnityEngine.Object)MasterCrown.masterIconObj);
                            MasterCrown.masterIconObj = (GameObject)null;
                        }
                    }
                    else if ((UnityEngine.Object)MasterCrown.masterIconObj != (UnityEngine.Object)null)
                    {
                        if (!Configuration.JSONConfig.MasterIconEnabled)
                            UnityEngine.Object.Destroy((UnityEngine.Object)MasterCrown.masterIconObj);
                    }
                }
                catch (Exception ex)
                {
                    Exception exception = new Exception();
                }
                yield return (object)new WaitForSeconds(0.25f);
            }
        }
    }
}
