﻿
using System.Collections.Generic;
using UnityEngine;
using VRC.SDKBase;

namespace emmVRC.Hacks
{
    public class MirrorTweaks
    {
        public static List<OriginalMirror> originalMirrors = new List<OriginalMirror>();
        private static LayerMask optimizeMask = new LayerMask()
        {
            value = 263680
        };
        private static LayerMask beautifyMask = new LayerMask()
        {
            value = -1025
        };

        public static void FetchMirrors()
        {
            MirrorTweaks.originalMirrors = new List<OriginalMirror>();
            foreach (VRC_MirrorReflection mirrorReflection in UnityEngine.Resources.FindObjectsOfTypeAll<VRC_MirrorReflection>())
                MirrorTweaks.originalMirrors.Add(new OriginalMirror()
                {
                    MirrorParent = mirrorReflection,
                    OriginalLayers = mirrorReflection.m_ReflectLayers
                });
        }

        public static void Optimize()
        {
            if (MirrorTweaks.originalMirrors.Count == 0)
                return;
            foreach (OriginalMirror originalMirror in MirrorTweaks.originalMirrors)
                originalMirror.MirrorParent.m_ReflectLayers = MirrorTweaks.optimizeMask;
        }

        public static void Beautify()
        {
            if (MirrorTweaks.originalMirrors.Count == 0)
                return;
            foreach (OriginalMirror originalMirror in MirrorTweaks.originalMirrors)
                originalMirror.MirrorParent.m_ReflectLayers = MirrorTweaks.beautifyMask;
        }

        public static void Revert()
        {
            if (MirrorTweaks.originalMirrors.Count == 0)
                return;
            foreach (OriginalMirror originalMirror in MirrorTweaks.originalMirrors)
                originalMirror.MirrorParent.m_ReflectLayers = originalMirror.OriginalLayers;
        }
    }
}
