﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace emmVRC.Hacks
{
    public class NameSpoofGenerator
    {
        private static string generatedSpoofName = "";
        private static List<string> adjectiveList = new List<string>()
    {
      "Adorable",
      "Adorbs",
      "Alluring",
      "Appealing",
      "Aromatic",
      "Beautiful",
      "Beauteous",
      "Bewitching",
      "Bonny",
      "Cute",
      "Charming",
      "Comely",
      "Darling",
      "Delightful",
      "Daring",
      "Dark",
      "Dreary",
      "Enchanting",
      "Engaging",
      "Exquisite",
      "Fair",
      "Fit",
      "Foxy",
      "Glamorous",
      "Gorgeous",
      "Humble",
      "Happy",
      "Heroic",
      "Harmonious",
      "Hot",
      "Hopeful",
      "Homely",
      "Hex",
      "Hypnotic",
      "Hyper",
      "Luscious",
      "Luxurious",
      "Magnificent",
      "Mistress",
      "Nice",
      "Personable",
      "Pleasing",
      "Pretty",
      "Picturesque",
      "Ravishing",
      "Scenic",
      "Seductive",
      "Sexy",
      "Sightly",
      "Smashing",
      "Splendid",
      "Stunning",
      "Sweet",
      "Tasty"
    };
        private static List<string> nounList = new List<string>()
    {
      "Angel",
      "Aurora",
      "Abelia",
      "Acer",
      "Allium",
      "Alpine",
      "Almond",
      "Abode",
      "Abyss",
      "Ace",
      "Aerie",
      "Alum",
      "Bamboo",
      "Bay",
      "Bella",
      "Bunny",
      "Blossom",
      "Blueberry",
      "Crystal",
      "Camellia",
      "Canna",
      "Carnation",
      "Diamond",
      "Daffodil",
      "Daylight",
      "Demon",
      "Demoness",
      "Emilia",
      "Emerald",
      "Elf",
      "Elderberry",
      "Eucalyptus",
      "Flower",
      "Fairy",
      "Feather",
      "Fox",
      "Garnet",
      "Gamer",
      "Grace",
      "Galaxy",
      "Gift",
      "Gianna",
      "Grape",
      "Hazel",
      "Heart",
      "Humility",
      "Hypatia",
      "Lavender",
      "Lush",
      "Lore",
      "Lapis",
      "Mana",
      "Miracle",
      "Moon",
      "Nora",
      "Nebula",
      "Night",
      "Platinum",
      "Port",
      "Princess",
      "Rhodium",
      "Ruby",
      "Succubus",
      "Seductress",
      "Savior",
      "Topaz",
      "Tera",
      "Tulip",
      "Tale",
      "Tail"
    };

        public static string spoofedName => Configuration.JSONConfig.InfoSpoofingName != "" ? Configuration.JSONConfig.InfoSpoofingName : NameSpoofGenerator.generatedSpoofName;

        internal static void GenerateNewName()
        {
            Random random = new Random();
            int index1 = random.Next(NameSpoofGenerator.adjectiveList.Count<string>());
            string str1 = NameSpoofGenerator.adjectiveList[index1];
            while (str1 == NameSpoofGenerator.adjectiveList[index1])
            {
                int index2 = random.Next(NameSpoofGenerator.nounList.Count<string>());
                string str2 = str1 + NameSpoofGenerator.nounList[index2];
                if (str2.Length < 15)
                    str1 = str2;
            }
            NameSpoofGenerator.generatedSpoofName = str1;
        }
    }
}
