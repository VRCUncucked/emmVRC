﻿
using emmVRC.Libraries;
using emmVRCLoader;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class Nameplates
    {
        public static bool colorChanged = true;

        public static void Initialize() => MelonCoroutines.Start(Nameplates.Loop());

        private static IEnumerator Loop()
        {
            while (true)
            {
                if (Nameplates.colorChanged && Configuration.JSONConfig.NameplateColorChangingEnabled)
                {
                    VRCPlayer.field_Internal_Static_Color_1 = ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex);
                    VRCPlayer.field_Internal_Static_Color_2 = ColorConversion.HexToColor(Configuration.JSONConfig.VisitorNamePlateColorHex);
                    VRCPlayer.field_Internal_Static_Color_3 = ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex);
                    VRCPlayer.field_Internal_Static_Color_4 = ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex);
                    VRCPlayer.field_Internal_Static_Color_5 = ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex);
                    VRCPlayer.field_Internal_Static_Color_6 = ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex);
                    if (ModCompatibility.OGTrustRank)
                    {
                        try
                        {
                            ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "OGTrustRanks"))).Assembly.GetType("OGTrustRanks.OGTrustRanks").GetField("TrustedUserColor", BindingFlags.Static | BindingFlags.NonPublic).SetValue((object)null, (object)ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex));
                            ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "OGTrustRanks"))).Assembly.GetType("OGTrustRanks.OGTrustRanks").GetField("VeteranUserColor", BindingFlags.Static | BindingFlags.NonPublic).SetValue((object)null, (object)ColorConversion.HexToColor(Configuration.JSONConfig.VeteranUserNamePlateColorHex));
                            ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "OGTrustRanks"))).Assembly.GetType("OGTrustRanks.OGTrustRanks").GetField("LegendaryUserColor", BindingFlags.Static | BindingFlags.NonPublic).SetValue((object)null, (object)ColorConversion.HexToColor(Configuration.JSONConfig.LegendaryUserNamePlateColorHex));
                        }
                        catch (Exception ex)
                        {
                            emmVRCLoader.Logger.LogError(ex.ToString());
                        }
                    }
                    Nameplates.colorChanged = false;
                }
                else if (Nameplates.colorChanged && !Configuration.JSONConfig.NameplateColorChangingEnabled)
                {
                    VRCPlayer.field_Internal_Static_Color_1 = ColorConversion.HexToColor("#FFFF00");
                    VRCPlayer.field_Internal_Static_Color_2 = ColorConversion.HexToColor("#CCCCCC");
                    VRCPlayer.field_Internal_Static_Color_3 = ColorConversion.HexToColor("#1778FF");
                    VRCPlayer.field_Internal_Static_Color_4 = ColorConversion.HexToColor("#2BCE5C");
                    VRCPlayer.field_Internal_Static_Color_5 = ColorConversion.HexToColor("#FF7B42");
                    VRCPlayer.field_Internal_Static_Color_6 = ColorConversion.HexToColor("#8143E6");
                    Nameplates.colorChanged = false;
                }
                yield return (object)new WaitForSeconds(0.5f);
            }
        }
    }
}
