﻿
using UnityEngine;
using VRC.SDKBase;

namespace emmVRC.Hacks
{
    public class OriginalMirror
    {
        public VRC_MirrorReflection MirrorParent;
        public LayerMask OriginalLayers;
    }
}
