﻿
using System.Collections.Generic;
using VRC.SDKBase;

namespace emmVRC.Hacks
{
    public class PedestalTweaks
    {
        public static List<OriginalPedestal> originalPedestals;

        public static void FetchPedestals()
        {
            PedestalTweaks.originalPedestals = new List<OriginalPedestal>();
            foreach (VRC_AvatarPedestal vrcAvatarPedestal in UnityEngine.Resources.FindObjectsOfTypeAll<VRC_AvatarPedestal>())
                PedestalTweaks.originalPedestals.Add(new OriginalPedestal()
                {
                    PedestalParent = vrcAvatarPedestal.gameObject,
                    originalActiveStatus = vrcAvatarPedestal.gameObject.activeSelf
                });
        }

        public static void Disable()
        {
            if (PedestalTweaks.originalPedestals.Count == 0)
                return;
            foreach (OriginalPedestal originalPedestal in PedestalTweaks.originalPedestals)
                originalPedestal.PedestalParent.SetActive(false);
        }

        public static void Enable()
        {
            if (PedestalTweaks.originalPedestals.Count == 0)
                return;
            foreach (OriginalPedestal originalPedestal in PedestalTweaks.originalPedestals)
                originalPedestal.PedestalParent.SetActive(true);
        }

        public static void Revert()
        {
            if (PedestalTweaks.originalPedestals.Count == 0)
                return;
            foreach (OriginalPedestal originalPedestal in PedestalTweaks.originalPedestals)
                originalPedestal.PedestalParent.SetActive(originalPedestal.originalActiveStatus);
        }
    }
}
