﻿
using emmVRC.Libraries;
using emmVRC.TinyJSON;
using emmVRCLoader;
using Il2CppSystem.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class PlayerNotes
    {
        public static void Initialize()
        {
            if (Directory.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/UserNotes")))
                return;
            Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/UserNotes"));
        }

        public static void LoadNote(string userID, string displayName)
        {
            try
            {
                PlayerNote loadedNote;
                if (File.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json")))
                    loadedNote = Decoder.Decode(File.ReadAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + userID + ".json"))).Make<PlayerNote>();
                else
                    loadedNote = new PlayerNote()
                    {
                        UserID = userID,
                        NoteText = ""
                    };
                VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2(
                  "Note for " + displayName, string.IsNullOrWhiteSpace(loadedNote.NoteText) ? "There is currently no note for this user." : loadedNote.NoteText,
                  "Change Note",
                  () => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup(
                    "Enter a note for " + displayName + ":",
                    loadedNote.NoteText,
                    InputField.InputType.Standard,
                    false,
                    "Accept",
                    (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((newNoteText, keyk, tx) => PlayerNotes.SaveNote(new PlayerNote()
                    {
                        UserID = userID,
                        NoteText = newNoteText
                    })),
                    null,
                    "Enter note...."));
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("Failed to load note: " + ex.ToString());
            }
        }

        public static void SaveNote(PlayerNote note) => File.WriteAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/UserNotes/" + note.UserID + ".json"), Encoder.Encode((object)note, EncodeOptions.PrettyPrint));
    }
}
