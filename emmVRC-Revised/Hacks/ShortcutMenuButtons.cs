﻿
using emmVRC.Libraries;
using emmVRC.Objects;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;

namespace emmVRC.Hacks
{
    public class ShortcutMenuButtons
    {
        public static GameObject emojiButton;
        public static GameObject emoteButton;
        public static GameObject reportWorldButton;
        public static GameObject trustRankButton;
        public static GameObject socialNotifications;
        public static GameObject vrcPlusThankYouButton;
        public static GameObject vrcPlusUserIconButton;
        public static GameObject vrcPlusUserIconCameraButton;
        public static GameObject vrcPlusMiniBanner;
        public static GameObject vrcPlusMainBanner;

        public static IEnumerator Process()
        {
            ShortcutMenuButtons.emojiButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/EmojiButton").gameObject;
            ShortcutMenuButtons.emoteButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/EmoteButton").gameObject;
            ShortcutMenuButtons.reportWorldButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/ReportWorldButton").gameObject;
            ShortcutMenuButtons.trustRankButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors").gameObject;
            ShortcutMenuButtons.socialNotifications = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/HeaderContainer/SocialNotifications").gameObject;
            ShortcutMenuButtons.vrcPlusThankYouButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/VRCPlusThankYou").gameObject;
            ShortcutMenuButtons.vrcPlusUserIconButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/GalleryButton").gameObject;
            ShortcutMenuButtons.vrcPlusUserIconCameraButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/UserIconCameraButton").gameObject;
            ShortcutMenuButtons.vrcPlusMiniBanner = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/VRCPlusMiniBanner").gameObject;
            ShortcutMenuButtons.vrcPlusMainBanner = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/HeaderContainer/VRCPlusBanner").gameObject;
            ShortcutMenuButtons.emojiButton.SetActive(!Configuration.JSONConfig.DisableEmojiButton);
            ShortcutMenuButtons.emoteButton.SetActive(!Configuration.JSONConfig.DisableEmoteButton);
            ShortcutMenuButtons.reportWorldButton.SetActive(!Configuration.JSONConfig.DisableReportWorldButton);
            ShortcutMenuButtons.trustRankButton.transform.localScale = Configuration.JSONConfig.DisableRankToggleButton ? Vector3.zero : Vector3.one;
            ShortcutMenuButtons.socialNotifications.transform.localScale = Configuration.JSONConfig.DisableOldInviteButtons ? Vector3.zero : Vector3.one;
            while (APIUser.CurrentUser == null || NetworkConfig.Instance == null)
                yield return (object)new WaitForEndOfFrame();
            ShortcutMenuButtons.vrcPlusMiniBanner.GetComponent<Canvas>().enabled = !Configuration.JSONConfig.DisableVRCPlusAds;
            ShortcutMenuButtons.vrcPlusMainBanner.GetComponent<Canvas>().enabled = !Configuration.JSONConfig.DisableVRCPlusAds;
            ShortcutMenuButtons.vrcPlusThankYouButton.transform.localScale = Configuration.JSONConfig.DisableVRCPlusQMButtons ? Vector3.zero : Vector3.one;
            ShortcutMenuButtons.vrcPlusUserIconButton.transform.localScale = Configuration.JSONConfig.DisableVRCPlusQMButtons ? Vector3.zero : Vector3.one;
            ShortcutMenuButtons.vrcPlusUserIconCameraButton.transform.localScale = Configuration.JSONConfig.DisableVRCPlusQMButtons ? Vector3.zero : Vector3.one;
        }
    }
}
