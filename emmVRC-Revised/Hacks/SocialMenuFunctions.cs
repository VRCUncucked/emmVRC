﻿
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRCLoader;
using Il2CppSystem.Collections.Generic;
using MelonLoader;
using System.Collections;
using UnhollowerBaseLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using VRC;
using VRC.SDKBase;
using VRC.UI;

namespace emmVRC.Hacks
{
    internal class SocialMenuFunctions
    {
        private static GameObject SocialFunctionsButton;
        public static GameObject UserSendMessage;
        private static GameObject UserNotes;
        private static GameObject TeleportButton;
        private static GameObject AvatarSearchButton;
        private static GameObject VRCPlusSupporterButton;
        private static GameObject VRCPlusEarlyAdopterIcon;
        private static GameObject VRCPlusSubscriberIcon;
        private static GameObject ToggleBlockButton;
        private static GameObject PortalToUserButton;
        private static bool menuOpen;
        private static bool menuJustOpened;
        private static int PortalCooldownTimer;

        public static void Initialize()
        {
            SocialMenuFunctions.SocialFunctionsButton = UnityEngine.Object.Instantiate<GameObject>(GameObject.Find("MenuContent/Screens/UserInfo/Buttons/RightSideButtons/RightUpperButtonColumn/FavoriteButton"), GameObject.Find("MenuContent/Screens/UserInfo/Buttons/RightSideButtons/RightUpperButtonColumn").transform);
            SocialMenuFunctions.SocialFunctionsButton.GetComponentInChildren<Text>().text = "<color=#FF69B4>emmVRC</color> Functions";
            SocialMenuFunctions.SocialFunctionsButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            SocialMenuFunctions.UserSendMessage = UnityEngine.Object.Instantiate<GameObject>(SocialMenuFunctions.SocialFunctionsButton, SocialMenuFunctions.SocialFunctionsButton.transform.parent);
            SocialMenuFunctions.UserSendMessage.GetComponentInChildren<Text>().text = "Send Message";
            SocialMenuFunctions.UserSendMessage.SetActive(false);
            SocialMenuFunctions.UserNotes = UnityEngine.Object.Instantiate<GameObject>(SocialMenuFunctions.UserSendMessage, SocialMenuFunctions.SocialFunctionsButton.transform.parent);
            SocialMenuFunctions.UserNotes.GetComponentInChildren<Text>().text = "Notes";
            SocialMenuFunctions.UserNotes.SetActive(false);
            SocialMenuFunctions.TeleportButton = UnityEngine.Object.Instantiate<GameObject>(SocialMenuFunctions.UserNotes, SocialMenuFunctions.SocialFunctionsButton.transform.parent);
            SocialMenuFunctions.TeleportButton.GetComponentInChildren<Text>().text = "Teleport";
            SocialMenuFunctions.TeleportButton.SetActive(false);
            SocialMenuFunctions.AvatarSearchButton = UnityEngine.Object.Instantiate<GameObject>(SocialMenuFunctions.TeleportButton, SocialMenuFunctions.SocialFunctionsButton.transform.parent);
            SocialMenuFunctions.AvatarSearchButton.GetComponentInChildren<Text>().text = "Search Avatars";
            SocialMenuFunctions.AvatarSearchButton.SetActive(false);
            SocialMenuFunctions.ToggleBlockButton = UnityEngine.Object.Instantiate<GameObject>(SocialMenuFunctions.AvatarSearchButton, SocialMenuFunctions.SocialFunctionsButton.transform.parent);
            SocialMenuFunctions.ToggleBlockButton.GetComponentInChildren<Text>().text = "<color=#FF69B4>emmVRC</color> Block";
            SocialMenuFunctions.ToggleBlockButton.SetActive(false);
            SocialMenuFunctions.PortalToUserButton = UnityEngine.Object.Instantiate<GameObject>(SocialMenuFunctions.ToggleBlockButton, SocialMenuFunctions.SocialFunctionsButton.transform.parent);
            SocialMenuFunctions.PortalToUserButton.GetComponentInChildren<Text>().text = "Drop Portal";
            SocialMenuFunctions.PortalToUserButton.SetActive(false);
            SocialMenuFunctions.SocialFunctionsButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              SocialMenuFunctions.UserNotes.SetActive(!SocialMenuFunctions.UserNotes.activeSelf);
              SocialMenuFunctions.TeleportButton.SetActive(!SocialMenuFunctions.TeleportButton.activeSelf);
              if (NetworkClient.webToken != null && Configuration.JSONConfig.AvatarFavoritesEnabled)
                  SocialMenuFunctions.AvatarSearchButton.SetActive(!SocialMenuFunctions.AvatarSearchButton.activeSelf);
              try
              {
                  if ((UnityEngine.Object)QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>() != (UnityEngine.Object)null && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0 != null && (QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location != "private" && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location != "") && (!QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.statusIsSetToOffline && !QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location.Contains("friends")))
                      SocialMenuFunctions.PortalToUserButton.SetActive(!SocialMenuFunctions.PortalToUserButton.activeSelf);
                  else
                      SocialMenuFunctions.PortalToUserButton.SetActive(false);
              }
              catch
              {
              }
              try
              {
                  GameObject.Find("UserInterface/MenuContent/Screens/UserInfo/OnlineFriendButtons").transform.localScale = GameObject.Find("UserInterface/MenuContent/Screens/UserInfo/OnlineFriendButtons").transform.localScale == Vector3.zero ? Vector3.one : Vector3.zero;
              }
              catch
              {
              }
          }));
            SocialMenuFunctions.UserSendMessage.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Send a message to " + QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.GetName() + ":", "", InputField.InputType.Standard, false, "Send", (Il2CppSystem.Action<string, List<KeyCode>, Text>)(System.Action<string, List<KeyCode>, Text>)((msg, keyk, tx) => { }), (Il2CppSystem.Action)null, "Enter message...")));
            SocialMenuFunctions.UserNotes.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() => PlayerNotes.LoadNote(QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.id, QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.GetName())));
            SocialMenuFunctions.TeleportButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              Player plrToTP = (Player)null;
              PlayerUtils.GetEachPlayer((System.Action<Player>)(plr =>
         {
             if (!(plr.prop_APIUser_0.id == QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.id))
                 return;
             plrToTP = plr;
         }));
              if ((UnityEngine.Object)plrToTP != (UnityEngine.Object)null)
                  VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position = plrToTP._vrcplayer.transform.position;
              try
              {
                  CustomAvatarFavorites.baseChooseEvent.Invoke();
              }
              catch (System.Exception ex)
              {
                  System.Exception exception = new System.Exception();
              }
          }));
            SocialMenuFunctions.AvatarSearchButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              if (QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0 == null)
                  return;
              VRCUiManager.prop_VRCUiManager_0.ShowScreen(VRCUiManager.prop_VRCUiManager_0.menuContent().transform.Find("Screens/Avatar").GetComponent<VRCUiPage>());
              MelonCoroutines.Start(CustomAvatarFavorites.SearchAvatarsAfterDelay(QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.GetName()));
          }));
            SocialMenuFunctions.PortalToUserButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              if (QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location != "private" && !QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.statusIsSetToOffline && QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location != "")
              {
                  if (!QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location.Contains("friends"))
                  {
                      try
                      {
                          if (SocialMenuFunctions.PortalCooldownTimer == 0)
                          {
                              string[] strArray = QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.location.Split(':');
                              Networking.RPC(RPC.Destination.AllBufferOne, Networking.Instantiate(VRC_EventHandler.VrcBroadcastType.Always, "Portals/PortalInternalDynamic", VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position + VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.forward * 2f, VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation), "ConfigurePortal", (Il2CppReferenceArray<Il2CppSystem.Object>)new Il2CppSystem.Object[3]
                          {
                  (Il2CppSystem.Object) (Il2CppSystem.String) strArray[0],
                  (Il2CppSystem.Object) (Il2CppSystem.String) strArray[1],
                  new Il2CppSystem.Int32() { m_value = 0 }.BoxIl2CppObject()
                    });
                              SocialMenuFunctions.PortalCooldownTimer = 5;
                              MelonCoroutines.Start(SocialMenuFunctions.PortalCooldown());
                              return;
                          }
                          VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You must wait " + SocialMenuFunctions.PortalCooldownTimer.ToString() + " seconds before dropping another portal.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                          return;
                      }
                      catch (System.Exception ex)
                      {
                          emmVRCLoader.Logger.LogError(ex.ToString());
                          return;
                      }
                  }
              }
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You cannot drop a portal to this user.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
          }));
            SocialMenuFunctions.ToggleBlockButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              if (NetworkClient.webToken == null)
                  return;
              HTTPRequest.post(NetworkClient.baseURL + "/api/blocked/" + QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageUserInfo>().field_Public_APIUser_0.id, (object)null).NoAwait("emmVRC block");
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowAlert("emmVRC", "The block state for this user has been toggled.", 5f);
          }));
            MelonCoroutines.Start(SocialMenuFunctions.Loop());
        }

        public static IEnumerator PortalCooldown()
        {
            for (; SocialMenuFunctions.PortalCooldownTimer > 0; --SocialMenuFunctions.PortalCooldownTimer)
                yield return (object)new WaitForSeconds(1f);
        }

        private static IEnumerator Loop()
        {
            while (true)
            {
                do
                {
                    do
                    {
                        yield return (object)new WaitForEndOfFrame();
                    }
                    while (RoomManager.field_Internal_Static_ApiWorld_0 == null);
                    if ((UnityEngine.Object)SocialMenuFunctions.VRCPlusSupporterButton == (UnityEngine.Object)null)
                    {
                        SocialMenuFunctions.VRCPlusSupporterButton = GameObject.Find("MenuContent/Screens/UserInfo/Buttons/RightSideButtons/RightUpperButtonColumn/Supporter/SupporterButton");
                        SocialMenuFunctions.VRCPlusEarlyAdopterIcon = GameObject.Find("MenuContent/Screens/UserInfo/User Panel/VRCIcons/VRCPlusEarlyAdopterIcon");
                        SocialMenuFunctions.VRCPlusSubscriberIcon = GameObject.Find("MenuContent/Screens/UserInfo/User Panel/VRCIcons/VRCPlusSubscriberIcon");
                    }
                    if (!SocialMenuFunctions.menuOpen && GameObject.Find("MenuContent/Screens/UserInfo").activeInHierarchy)
                    {
                        SocialMenuFunctions.menuOpen = true;
                        SocialMenuFunctions.menuJustOpened = true;
                    }
                    if (SocialMenuFunctions.menuOpen && SocialMenuFunctions.menuJustOpened)
                    {
                        if (Configuration.JSONConfig.DisableVRCPlusUserInfo)
                        {
                            SocialMenuFunctions.VRCPlusSupporterButton.transform.localScale = Vector3.zero;
                            SocialMenuFunctions.VRCPlusEarlyAdopterIcon.transform.localScale = Vector3.zero;
                            SocialMenuFunctions.VRCPlusSubscriberIcon.transform.localScale = Vector3.zero;
                        }
                        else
                        {
                            SocialMenuFunctions.VRCPlusSupporterButton.transform.localScale = Vector3.one;
                            SocialMenuFunctions.VRCPlusEarlyAdopterIcon.transform.localScale = Vector3.one;
                            SocialMenuFunctions.VRCPlusSubscriberIcon.transform.localScale = Vector3.one;
                        }
                        SocialMenuFunctions.menuJustOpened = false;
                    }
                    if (SocialMenuFunctions.menuOpen && !GameObject.Find("MenuContent/Screens/UserInfo").activeInHierarchy)
                        SocialMenuFunctions.menuOpen = false;
                }
                while (GameObject.Find("MenuContent/Screens/UserInfo").activeSelf);
                GameObject.Find("UserInterface/MenuContent/Screens/UserInfo/OnlineFriendButtons").transform.localScale = Vector3.one;
                SocialMenuFunctions.UserNotes.SetActive(false);
                SocialMenuFunctions.TeleportButton.SetActive(false);
                SocialMenuFunctions.AvatarSearchButton.SetActive(false);
                SocialMenuFunctions.PortalToUserButton.SetActive(false);
            }
        }
    }
}
