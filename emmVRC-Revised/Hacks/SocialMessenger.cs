﻿
using emmVRC.Libraries;
using emmVRC.Network.Objects;
using emmVRCLoader;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class SocialMessenger
    {
        public static void Initialize()
        {
        }

        public static void OpenText(string userID, string displayName)
        {
            try
            {
                VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Send message to " + displayName + ":", "", InputField.InputType.Standard, false, "Send", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, UnityEngine.UI.Text>)((newMessageText, keyk, tx) => SocialMessenger.SendMessage(new SocialMessage()
                {
                    UserID = userID,
                    MessageText = newMessageText
                })), (Il2CppSystem.Action)null, "Enter message....");
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("Failed to send message: " + ex.ToString());
            }
        }

        public static void OpenConversation(string userID, string displayName, Message[] messageConvo)
        {
            try
            {
                VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("Conversation with " + Encoding.UTF8.GetString(System.Convert.FromBase64String(displayName)), messageConvo.Length == 0 ? "No messages with this user." : SocialMessenger.loadContext(messageConvo), "Send Message", (System.Action)(() => SocialMessenger.OpenText(userID, Encoding.UTF8.GetString(System.Convert.FromBase64String(displayName)))));
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("Failed to send message: " + ex.ToString());
            }
        }

        private static string loadContext(Message[] convo)
        {
            string str1 = "    ";
            string str2 = "\n";
            string str3 = "";
            foreach (Message message in ((IEnumerable<Message>)convo).Reverse<Message>())
                str3 = str3 + "[ " + message.rest_message_created + " ]" + str1 + Encoding.UTF8.GetString(System.Convert.FromBase64String(message.rest_message_sender_name)) + str1 + str2 + str1 + Encoding.UTF8.GetString(System.Convert.FromBase64String(message.rest_message_body)) + str2;
            return str3;
        }

        public static void SendMessage(SocialMessage message)
        {
        }
    }
}
