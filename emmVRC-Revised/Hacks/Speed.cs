﻿
using MelonLoader;
using System.Collections;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class Speed
    {
        private static float originalWalkSpeed = 0.0f;
        private static float originalRunSpeed = 0.0f;
        private static float originalStrafeSpeed = 0.0f;
        public static float Modifier = 1f;
        public static bool SpeedModified = false;

        public static void Initialize() => MelonCoroutines.Start(Speed.Loop());

        private static IEnumerator Loop()
        {
            while (true)
            {
                do
                {
                    do
                    {
                        if (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                        {
                            Speed.originalRunSpeed = 0.0f;
                            Speed.originalWalkSpeed = 0.0f;
                            Speed.originalStrafeSpeed = 0.0f;
                        }
                        while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                            yield return (object)new WaitForSeconds(1f);
                        yield return (object)new WaitForEndOfFrame();
                    }
                    while (!((Object)VRCPlayer.field_Internal_Static_VRCPlayer_0 != (Object)null) || VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0 == null || RoomManager.field_Internal_Static_ApiWorld_0 == null);
                    if (Speed.SpeedModified && ((double)Speed.originalRunSpeed == 0.0 || (double)Speed.originalWalkSpeed == 0.0 || (double)Speed.originalStrafeSpeed == 0.0))
                    {
                        Speed.originalWalkSpeed = VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.GetWalkSpeed();
                        Speed.originalRunSpeed = VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.GetRunSpeed();
                        Speed.originalStrafeSpeed = VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.GetStrafeSpeed();
                    }
                    if (!Speed.SpeedModified && (double)Speed.originalRunSpeed != 0.0 && ((double)Speed.originalWalkSpeed != 0.0 && (double)Speed.originalStrafeSpeed != 0.0))
                    {
                        VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetWalkSpeed(Speed.originalWalkSpeed);
                        VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetRunSpeed(Speed.originalRunSpeed);
                        VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetStrafeSpeed(Speed.originalStrafeSpeed);
                        Speed.originalRunSpeed = 0.0f;
                        Speed.originalWalkSpeed = 0.0f;
                        Speed.originalStrafeSpeed = 0.0f;
                        Speed.Modifier = 1f;
                    }
                }
                while (!Speed.SpeedModified || (double)Speed.originalWalkSpeed == 0.0 || ((double)Speed.originalRunSpeed == 0.0 || (double)Speed.originalStrafeSpeed == 0.0));
                VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetWalkSpeed(Speed.originalWalkSpeed * Speed.Modifier);
                VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetRunSpeed(Speed.originalRunSpeed * Speed.Modifier);
                VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetStrafeSpeed(Speed.originalStrafeSpeed * Speed.Modifier);
            }
        }
    }
}
