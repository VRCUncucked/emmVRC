﻿
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Menus;
using emmVRC.Objects;
using Il2CppSystem.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnhollowerBaseLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class TabMenu
    {
        public static GameObject newTab;
        public static QMNestedButton tabMenu;
        public static QMSingleButton functionsButton;
        public static QMSingleButton notificationsButton;
        public static GameObject menuTitle;
        public static GameObject menuSubTitle;
        public static GameObject badge;

        public static IEnumerator Initialize()
        {
            while ((UnityEngine.Object)Resources.onlineSprite == (UnityEngine.Object)null || (UnityEngine.Object)Resources.TabIcon == (UnityEngine.Object)null || ((UnityEngine.Object)MonoBehaviourPublicObCoGaCoObCoObCoUnique.prop_MonoBehaviourPublicObCoGaCoObCoObCoUnique_0 == (UnityEngine.Object)null || MonoBehaviourPublicObCoGaCoObCoObCoUnique.prop_MonoBehaviourPublicObCoGaCoObCoObCoUnique_0.field_Public_ArrayOf_GameObject_0 == null))
                yield return (object)null;
            TabMenu.newTab = UnityEngine.Object.Instantiate<GameObject>(GameObject.Find("UserInterface/QuickMenu/QuickModeTabs/NotificationsTab"), GameObject.Find("UserInterface/QuickMenu/QuickModeTabs/NotificationsTab").transform.parent);
            TabMenu.newTab.name = "emmVRCTab";
            TabMenu.newTab.GetComponent<UiTooltip>().field_Public_String_0 = "emmVRC Menu";
            TabMenu.newTab.GetComponent<UiTooltip>().field_Public_String_1 = "emmVRC Menu";
            TabMenu.newTab.transform.Find("Icon").GetComponent<Image>().sprite = Resources.TabIcon;
            TabMenu.tabMenu = new QMNestedButton("ShortcutMenu", 1923, 10394, nameof(TabMenu), "dasdasdas");
            GameObject tabMenuObj = ((Component)QuickMenu.prop_QuickMenu_0).gameObject.transform.Find(TabMenu.tabMenu.getMenuName()).gameObject;
            TabMenu.functionsButton = new QMSingleButton(TabMenu.tabMenu, 1, 1, "Functions", (System.Action)(() =>
           {
               ((Component)QuickMenu.prop_QuickMenu_0).transform.Find("QuickModeTabs/HomeTab").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
               FunctionsMenu.baseMenu.menuEntryButton.getGameObject().GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
           }), "Extra functions that can enhance the user experience or provide practical features");
            TabMenu.notificationsButton = new QMSingleButton(TabMenu.tabMenu, 2, 1, "0\n<color=#FF69B4>emmVRC</color>\nNotifications", (System.Action)(() =>
           {
               ((Component)QuickMenu.prop_QuickMenu_0).transform.Find("QuickModeTabs/HomeTab").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
               if (Managers.NotificationManager.Notifications.Count <= 0)
                   return;
               Managers.NotificationManager.NotificationMenu.getMainButton().getGameObject().GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
           }), "View your current emmVRC notifications");
            GameObject gameObject1 = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, tabMenuObj.transform);
            gameObject1.GetComponent<Text>().fontStyle = FontStyle.Normal;
            gameObject1.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            gameObject1.GetComponent<Text>().color = Color.white;
            gameObject1.GetComponent<Text>().text = "      <color=#FF69B4>emmVRC </color>v" + Attributes.Version;
            gameObject1.GetComponent<Text>().fontSize *= 2;
            gameObject1.GetComponent<RectTransform>().anchoredPosition += new Vector2(580f, -495f);
            GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, tabMenuObj.transform);
            gameObject2.GetComponent<Text>().fontStyle = FontStyle.Normal;
            gameObject2.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            gameObject2.GetComponent<Text>().color = Color.white;
            gameObject2.GetComponent<Text>().fontSize = (int)((double)gameObject2.GetComponent<Text>().fontSize * 0.85);
            int index = new System.Random().Next(Attributes.FlavourTextList.Length);
            gameObject2.GetComponent<Text>().text = Attributes.FlavourTextList[index];
            gameObject2.GetComponent<RectTransform>().anchoredPosition += new Vector2(725f, -620f);
            TabMenu.tabMenu.getMainButton().DestroyMe();
            TabMenu.tabMenu.getBackButton().DestroyMe();
            MonoBehaviour monoBehaviour = TabMenu.newTab.GetComponents<MonoBehaviour>().First<MonoBehaviour>((Func<MonoBehaviour, bool>)(c => c.GetIl2CppType().GetMethod("ShowTabContent") != (MethodInfo)null));
            List<GameObject> list = MonoBehaviourPublicObCoGaCoObCoObCoUnique.prop_MonoBehaviourPublicObCoGaCoObCoObCoUnique_0.field_Public_ArrayOf_GameObject_0.ToList<GameObject>();
            list.Add(TabMenu.newTab);
            MonoBehaviourPublicObCoGaCoObCoObCoUnique.prop_MonoBehaviourPublicObCoGaCoObCoObCoUnique_0.field_Public_ArrayOf_GameObject_0 = (Il2CppReferenceArray<GameObject>)list.ToArray();
            monoBehaviour.GetIl2CppType().GetFields().First<FieldInfo>((Func<FieldInfo, bool>)(f => f.FieldType.IsEnum)).SetValue((Il2CppSystem.Object)monoBehaviour, new Il2CppSystem.Int32()
            {
                m_value = list.Count
            }.BoxIl2CppObject());
            TabMenu.newTab.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              QuickMenuUtils.ShowQuickmenuPage(tabMenuObj.name);
              TabMenu.notificationsButton.setButtonText(Managers.NotificationManager.Notifications.Count.ToString() + "\nNotifications");
              if (Managers.NotificationManager.Notifications.Count <= 0)
                  TabMenu.notificationsButton.getGameObject().GetComponent<UnityEngine.UI.Button>().interactable = false;
              else
                  TabMenu.notificationsButton.getGameObject().GetComponent<UnityEngine.UI.Button>().interactable = true;
          }));
            if (!Configuration.JSONConfig.TabMode)
                TabMenu.newTab.transform.localScale = Vector3.zero;
            TabMenu.badge = TabMenu.newTab.transform.Find("Badge").gameObject;
            while ((UnityEngine.Object)TabMenu.newTab.GetComponent<MonoBehaviourPublicGaTeSiSiUnique>() == (UnityEngine.Object)null)
                yield return (object)new WaitForSeconds(1f);
            UnityEngine.Object.Destroy((UnityEngine.Object)TabMenu.newTab.GetComponent<MonoBehaviourPublicGaTeSiSiUnique>());
        }
    }
}
