﻿
using MelonLoader;
using System.Collections;
using UnityEngine;

namespace emmVRC.Hacks
{
    internal class ThirdPerson
    {
        internal static GameObject TPCameraBack;
        internal static GameObject TPCameraFront;
        internal static GameObject referenceCamera;
        internal static float zoomOffset;
        internal static float offsetX;
        internal static float offsetY;
        public static int CameraSetup;

        internal static void Initialize()
        {
            GameObject primitive1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Object.Destroy((Object)primitive1.GetComponent<MeshRenderer>());
            ThirdPerson.referenceCamera = GameObject.Find("Camera (eye)");
            if ((Object)ThirdPerson.referenceCamera == (Object)null)
                ThirdPerson.referenceCamera = GameObject.Find("CenterEyeAnchor");
            if (!((Object)ThirdPerson.referenceCamera != (Object)null))
                return;
            primitive1.transform.localScale = ThirdPerson.referenceCamera.transform.localScale;
            Rigidbody rigidbody1 = primitive1.AddComponent<Rigidbody>();
            rigidbody1.isKinematic = true;
            rigidbody1.useGravity = false;
            if ((bool)(Object)primitive1.GetComponent<Collider>())
                primitive1.GetComponent<Collider>().enabled = false;
            primitive1.GetComponent<Renderer>().enabled = false;
            primitive1.AddComponent<Camera>();
            GameObject referenceCamera = ThirdPerson.referenceCamera;
            primitive1.transform.parent = referenceCamera.transform;
            primitive1.transform.rotation = referenceCamera.transform.rotation;
            primitive1.transform.position = referenceCamera.transform.position;
            primitive1.transform.position -= primitive1.transform.forward * 2f;
            referenceCamera.GetComponent<Camera>().enabled = false;
            primitive1.GetComponent<Camera>().fieldOfView = 75f;
            primitive1.GetComponent<Camera>().nearClipPlane /= 4f;
            ThirdPerson.TPCameraBack = primitive1;
            GameObject primitive2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            Object.Destroy((Object)primitive2.GetComponent<MeshRenderer>());
            primitive2.transform.localScale = ThirdPerson.referenceCamera.transform.localScale;
            Rigidbody rigidbody2 = primitive2.AddComponent<Rigidbody>();
            rigidbody2.isKinematic = true;
            rigidbody2.useGravity = false;
            if ((bool)(Object)primitive2.GetComponent<Collider>())
                primitive2.GetComponent<Collider>().enabled = false;
            primitive2.GetComponent<Renderer>().enabled = false;
            primitive2.AddComponent<Camera>();
            primitive2.transform.parent = referenceCamera.transform;
            primitive2.transform.rotation = referenceCamera.transform.rotation;
            primitive2.transform.Rotate(0.0f, 180f, 0.0f);
            primitive2.transform.position = referenceCamera.transform.position;
            primitive2.transform.position += -primitive2.transform.forward * 2f;
            referenceCamera.GetComponent<Camera>().enabled = false;
            primitive2.GetComponent<Camera>().fieldOfView = 75f;
            primitive2.GetComponent<Camera>().nearClipPlane /= 4f;
            ThirdPerson.TPCameraFront = primitive2;
            ThirdPerson.TPCameraBack.GetComponent<Camera>().enabled = false;
            ThirdPerson.TPCameraFront.GetComponent<Camera>().enabled = false;
            ThirdPerson.referenceCamera.GetComponent<Camera>().enabled = true;
            MelonCoroutines.Start(ThirdPerson.Loop());
        }

        internal static IEnumerator Loop()
        {
            while (true)
            {
                if ((Object)ThirdPerson.TPCameraBack != (Object)null && (Object)ThirdPerson.TPCameraFront != (Object)null)
                {
                    if (ThirdPerson.CameraSetup == 0)
                    {
                        ThirdPerson.TPCameraBack.GetComponent<Camera>().enabled = false;
                        ThirdPerson.TPCameraFront.GetComponent<Camera>().enabled = false;
                    }
                    else if (ThirdPerson.CameraSetup == 1)
                    {
                        ThirdPerson.TPCameraBack.GetComponent<Camera>().enabled = true;
                        ThirdPerson.TPCameraFront.GetComponent<Camera>().enabled = false;
                    }
                    else if (ThirdPerson.CameraSetup == 2)
                    {
                        ThirdPerson.TPCameraBack.GetComponent<Camera>().enabled = false;
                        ThirdPerson.TPCameraFront.GetComponent<Camera>().enabled = true;
                    }
                    if (ThirdPerson.CameraSetup != 0)
                    {
                        if (Input.GetKeyDown(KeyCode.Escape))
                            ThirdPerson.CameraSetup = 0;
                        float axis = Input.GetAxis("Mouse ScrollWheel");
                        if ((double)axis > 0.0)
                        {
                            ThirdPerson.TPCameraBack.transform.position += ThirdPerson.TPCameraBack.transform.forward * 0.1f;
                            ThirdPerson.TPCameraFront.transform.position -= ThirdPerson.TPCameraBack.transform.forward * 0.1f;
                            ThirdPerson.zoomOffset += 0.1f;
                        }
                        else if ((double)axis < 0.0)
                        {
                            ThirdPerson.TPCameraBack.transform.position -= ThirdPerson.TPCameraBack.transform.forward * 0.1f;
                            ThirdPerson.TPCameraFront.transform.position += ThirdPerson.TPCameraBack.transform.forward * 0.1f;
                            ThirdPerson.zoomOffset -= 0.1f;
                        }
                    }
                }
                yield return (object)new WaitForEndOfFrame();
            }
        }
    }
}
