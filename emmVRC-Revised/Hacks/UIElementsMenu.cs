﻿
using emmVRC.Libraries;
using System;
using System.Collections;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class UIElementsMenu
    {
        public static QMToggleButton ToggleHUD;

        public static IEnumerator Initialize()
        {
            UIElementsMenu.ToggleHUD = new QMToggleButton(nameof(UIElementsMenu), 1, 2, "HUD On", (System.Action)(() =>
          {
              Configuration.JSONConfig.UIVisible = true;
              Configuration.SaveConfig();
              ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UIElementsMenu/ToggleHUDButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
          }), "HUD Off", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIVisible = false;
         Configuration.SaveConfig();
         ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UIElementsMenu/ToggleHUDButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
     }), "TOGGLE: Select to Turn the HUD On/Off");
            UIElementsMenu.ToggleHUD.setToggleState(Configuration.JSONConfig.UIVisible);
            while (RoomManager.field_Internal_Static_ApiWorld_0 == null || (UnityEngine.Object)VRCPlayer.field_Internal_Static_VRCPlayer_0 == (UnityEngine.Object)null)
                yield return (object)new WaitForSeconds(0.1f);
            ((System.Action)(() =>
           {
               try
               {
                   if (Configuration.JSONConfig.UIVisible)
                       return;
                   ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UIElementsMenu/ToggleHUDButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
               }
               catch (Exception ex)
               {
                   Exception exception = new Exception();
               }
           }))();
            ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UIElementsMenu/ToggleHUDButton").gameObject.SetActive(false);
            yield return (object)null;
        }
    }
}
