﻿
using UnityEngine;

namespace emmVRC.Hacks
{
    public class UnscaledUITweaks
    {
        private static GameObject micTooltipDesktop;
        private static GameObject micTooltipXbox;

        public static void Process()
        {
            if ((Object)UnscaledUITweaks.micTooltipDesktop == (Object)null)
                UnscaledUITweaks.micTooltipDesktop = GameObject.Find("UserInterface/UnscaledUI/HudContent/Hud/VoiceDotParent/PushToTalkKeybd");
            if ((Object)UnscaledUITweaks.micTooltipXbox == (Object)null)
                UnscaledUITweaks.micTooltipXbox = GameObject.Find("UserInterface/UnscaledUI/HudContent/Hud/VoiceDotParent/PushToTalkXbox");
            UnscaledUITweaks.micTooltipDesktop.transform.localScale = Configuration.JSONConfig.DisableMicTooltip ? Vector3.zero : Vector3.one;
            UnscaledUITweaks.micTooltipXbox.transform.localScale = Configuration.JSONConfig.DisableMicTooltip ? Vector3.zero : Vector3.one;
        }
    }
}
