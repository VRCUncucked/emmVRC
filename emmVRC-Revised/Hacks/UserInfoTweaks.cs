﻿
using MelonLoader;
using System;
using System.Collections;
using UnhollowerBaseLib.Attributes;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;
using VRC.UI;

namespace emmVRC.Hacks
{
    public class UserInfoTweaks : MonoBehaviour
    {
        public static bool classInjected;
        public GameObject lastSeenText;
        public GameObject platformIcon;
        public PageUserInfo userInfo;
        private static UiPlatformIcon icon;
        public string lastCheckedId;

        public UserInfoTweaks(IntPtr ptr)
          : base(ptr)
        {
        }

        public static UserInfoTweaks Instantiate(GameObject parent)
        {
            if (!UserInfoTweaks.classInjected)
            {
                ClassInjector.RegisterTypeInIl2Cpp<UserInfoTweaks>();
                UserInfoTweaks.classInjected = true;
            }
            return parent.AddComponent<UserInfoTweaks>();
        }

        public static void Initialize()
        {
            UserInfoTweaks userInfoTweaks = UserInfoTweaks.Instantiate(GameObject.Find("UserInterface/MenuContent/Screens/UserInfo"));
            userInfoTweaks.lastSeenText = UnityEngine.Object.Instantiate<GameObject>(userInfoTweaks.transform.Find("User Panel/NameText").gameObject, userInfoTweaks.transform);
            userInfoTweaks.lastSeenText.GetComponent<RectTransform>().anchoredPosition = new Vector2(975f, -795f);
            userInfoTweaks.lastSeenText.GetComponent<Text>().alignment = TextAnchor.MiddleRight;
            userInfoTweaks.lastSeenText.GetComponent<Text>().fontSize = 30;
            userInfoTweaks.lastSeenText.GetComponent<Text>().text = "";
            userInfoTweaks.userInfo = userInfoTweaks.transform.GetComponent<PageUserInfo>();
            userInfoTweaks.platformIcon = UnityEngine.Object.Instantiate<GameObject>(userInfoTweaks.transform.Find("AvatarImage/SupporterIcon").gameObject, userInfoTweaks.transform);
            userInfoTweaks.platformIcon.GetComponent<RectTransform>().anchoredPosition = new Vector2(390f, -170f);
            UnityEngine.Object.DestroyImmediate((UnityEngine.Object)userInfoTweaks.platformIcon.GetComponent<Image>());
            userInfoTweaks.platformIcon.AddComponent<RawImage>();
            userInfoTweaks.platformIcon.SetActive(true);
            UserInfoTweaks.icon = GameObject.Find("UserInterface/QuickMenu/QuickMenu_NewElements/_CONTEXT/QM_Context_User_Hover/AvatarImage/PlatformIcon").GetComponent<UiPlatformIcon>();
        }

        public void OnEnable() => MelonCoroutines.Start(this.WaitForUserReady());

        [HideFromIl2Cpp]
        public IEnumerator WaitForUserReady()
        {
            UserInfoTweaks userInfoTweaks = this;
            while (userInfoTweaks.userInfo.field_Public_APIUser_0 == null || userInfoTweaks.userInfo.field_Public_APIUser_0.id == userInfoTweaks.lastCheckedId)
                yield return (object)new WaitForEndOfFrame();
            if (userInfoTweaks.gameObject.activeInHierarchy)
            {
                try
                {
                    userInfoTweaks.lastCheckedId = userInfoTweaks.userInfo.field_Public_APIUser_0.id;
                    if ((UnityEngine.Object)userInfoTweaks.lastSeenText != (UnityEngine.Object)null)
                    {
                        try
                        {
                            if (userInfoTweaks.userInfo.field_Public_APIUser_0.statusValue != APIUser.UserStatus.Offline || !userInfoTweaks.userInfo.field_Public_APIUser_0.isFriend)
                            {
                                userInfoTweaks.lastSeenText.GetComponent<Text>().text = "";
                            }
                            else
                            {
                                DateTime dateTime = DateTime.Parse(userInfoTweaks.userInfo.field_Public_APIUser_0.last_login);
                                userInfoTweaks.lastSeenText.GetComponent<Text>().text = "Last login: " + dateTime.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            emmVRCLoader.Logger.LogError("Error parsing last login: " + ex.ToString());
                        }
                    }
                    if ((UnityEngine.Object)userInfoTweaks.platformIcon != (UnityEngine.Object)null)
                    {
                        try
                        {
                            if (userInfoTweaks.userInfo.field_Public_APIUser_0.last_platform == "standalonewindows")
                                userInfoTweaks.platformIcon.GetComponent<RawImage>().texture = UserInfoTweaks.icon.field_Public_GameObject_1.GetComponent<RawImage>().texture;
                            else
                                userInfoTweaks.platformIcon.GetComponent<RawImage>().texture = UserInfoTweaks.icon.field_Public_GameObject_2.GetComponent<RawImage>().texture;
                        }
                        catch (Exception ex)
                        {
                            emmVRCLoader.Logger.LogError("Error with platform icon: " + ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    emmVRCLoader.Logger.LogError("Error while waiting for user: " + ex.ToString());
                }
            }
        }
    }
}
