﻿
using emmVRC.Libraries;
using MelonLoader;
using System;
using System.Collections;
using UnityEngine;

namespace emmVRC.Hacks
{
    public class UserInteractMenuButtons
    {
        public static GameObject playlistButton;
        public static GameObject avatarStatsButton;
        public static GameObject reportUserButton;
        public static GameObject warnButton;
        public static GameObject kickButton;
        public static QMSingleButton HalfWarnButton;
        public static QMSingleButton HalfKickButton;
        private static bool LoopRunning;

        public static void Initialize()
        {
            if ((UnityEngine.Object)UserInteractMenuButtons.playlistButton == (UnityEngine.Object)null)
                UserInteractMenuButtons.playlistButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/ViewPlaylistsButton").gameObject;
            if ((UnityEngine.Object)UserInteractMenuButtons.avatarStatsButton == (UnityEngine.Object)null)
                UserInteractMenuButtons.avatarStatsButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/ShowAvatarStatsButton").gameObject;
            if ((UnityEngine.Object)UserInteractMenuButtons.reportUserButton == (UnityEngine.Object)null)
                UserInteractMenuButtons.reportUserButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/ReportAbuseButton").gameObject;
            if ((UnityEngine.Object)UserInteractMenuButtons.warnButton == (UnityEngine.Object)null)
                UserInteractMenuButtons.warnButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/WarnButton").gameObject;
            if ((UnityEngine.Object)UserInteractMenuButtons.kickButton == (UnityEngine.Object)null)
                UserInteractMenuButtons.kickButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/KickButton").gameObject;
            if (UserInteractMenuButtons.HalfWarnButton == null)
            {
                UserInteractMenuButtons.HalfWarnButton = new QMSingleButton("UserInteractMenu", 2, 3, "Warn", (Action)(() => ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/WarnButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke()), "World Owner Only: Warn this User of Bad Behavior");
                UserInteractMenuButtons.HalfWarnButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
                UserInteractMenuButtons.HalfWarnButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, 96f);
            }
            if (UserInteractMenuButtons.HalfKickButton == null)
            {
                UserInteractMenuButtons.HalfKickButton = new QMSingleButton("UserInteractMenu", 2, 3, "Kick", (Action)(() => ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/KickButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke()), "World Owner Only: Kick this User from The World");
                UserInteractMenuButtons.HalfKickButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
                UserInteractMenuButtons.HalfKickButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, -96f);
            }
            if (Configuration.JSONConfig.DisablePlaylistsButton)
                UserInteractMenuButtons.playlistButton.gameObject.SetActive(false);
            else
                UserInteractMenuButtons.playlistButton.gameObject.SetActive(true);
            UserInteractMenuButtons.avatarStatsButton.transform.localScale = !Configuration.JSONConfig.DisableAvatarStatsButton ? new Vector3(1f, 1f, 1f) : new Vector3(0.0f, 0.0f, 0.0f);
            UserInteractMenuButtons.reportUserButton.transform.localScale = !Configuration.JSONConfig.DisableReportUserButton ? new Vector3(1f, 1f, 1f) : new Vector3(0.0f, 0.0f, 0.0f);
            if (Configuration.JSONConfig.MinimalWarnKickButton)
            {
                UserInteractMenuButtons.warnButton.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
                UserInteractMenuButtons.kickButton.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            }
            else
            {
                UserInteractMenuButtons.warnButton.transform.localScale = new Vector3(1f, 1f, 1f);
                UserInteractMenuButtons.kickButton.transform.localScale = new Vector3(1f, 1f, 1f);
            }
            if (UserInteractMenuButtons.LoopRunning)
                return;
            MelonCoroutines.Start(UserInteractMenuButtons.Loop());
        }

        public static IEnumerator Loop()
        {
            while (true)
            {
                UserInteractMenuButtons.LoopRunning = true;
                yield return (object)new WaitForSeconds(0.125f);
                if (Configuration.JSONConfig.MinimalWarnKickButton)
                {
                    UserInteractMenuButtons.HalfWarnButton.setActive(UserInteractMenuButtons.warnButton.activeSelf);
                    UserInteractMenuButtons.HalfKickButton.setActive(UserInteractMenuButtons.kickButton.activeSelf);
                }
                else
                {
                    UserInteractMenuButtons.HalfWarnButton.setActive(false);
                    UserInteractMenuButtons.HalfKickButton.setActive(false);
                }
            }
        }
    }
}
