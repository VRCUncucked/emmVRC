﻿
using emmVRC.Libraries;
using emmVRCLoader;
using MelonLoader;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class UserListRefresh
    {
        public static int cooldown;
        public static GameObject refreshButton;

        public static void Initialize()
        {
            UserListRefresh.refreshButton = UnityEngine.Object.Instantiate<GameObject>(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Social/UserProfileAndStatusSection/Status/EditStatusButton").gameObject, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Social"));
            UserListRefresh.refreshButton.GetComponent<RectTransform>().anchoredPosition += new Vector2(1260f, 370f);
            UserListRefresh.refreshButton.GetComponentInChildren<Text>().text = "<color=#FFFFFF>Refresh</color>";
            UserListRefresh.refreshButton.GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            UserListRefresh.refreshButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() => UserListRefresh.RefreshMenu()));
        }

        public static void RefreshMenu()
        {
            if (UserListRefresh.cooldown <= 0)
            {
                foreach (UiUserList uiUserList in UnityEngine.Resources.FindObjectsOfTypeAll<UiUserList>())
                {
                    try
                    {
                        uiUserList.Method_Public_Void_0();
                        uiUserList.Method_Public_Void_1();
                        uiUserList.Method_Public_Void_2();
                    }
                    catch (Exception ex)
                    {
                        emmVRCLoader.Logger.LogError(ex.ToString());
                    }
                    UserListRefresh.cooldown = 10;
                    MelonCoroutines.Start(UserListRefresh.CooldownTimer());
                }
            }
            else
                VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowAlert("emmVRC", "You must wait " + UserListRefresh.cooldown.ToString() + " seconds before refreshing again.", 10f);
        }

        public static IEnumerator CooldownTimer()
        {
            yield return (object)new WaitForSecondsRealtime((float)UserListRefresh.cooldown);
            UserListRefresh.cooldown = 0;
        }
    }
}
