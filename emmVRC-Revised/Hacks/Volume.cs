﻿
using emmVRC.Libraries;
using emmVRCLoader;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class Volume
    {
        private static Slider UIVolumeSlider;
        private static Slider WorldVolumeSlider;
        private static Slider VoicesVolumeSlider;
        private static Slider AvatarVolumeSlider;
        private static GameObject UIVolumeMuteButton;
        private static GameObject WorldVolumeMuteButton;
        private static GameObject VoiceVolumeMuteButton;
        private static GameObject AvatarVolumeMuteButton;

        public static void Initialize()
        {
            try
            {
                Volume.UIVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeUi").GetComponent<Slider>();
                Volume.UIVolumeSlider.onValueChanged.AddListener((UnityAction<float>)(Action<float>)(flt =>
              {
                  if (!Configuration.JSONConfig.UIVolumeMute || (double)flt == 0.0)
                      return;
                  Configuration.JSONConfig.UIVolumeMute = false;
                  Configuration.SaveConfig();
                  Volume.UIVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.UIVolumeMute ? "U" : "M";
              }));
                Volume.WorldVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeGameWorld").GetComponent<Slider>();
                Volume.WorldVolumeSlider.onValueChanged.AddListener((UnityAction<float>)(Action<float>)(flt =>
              {
                  if (!Configuration.JSONConfig.WorldVolumeMute || (double)flt == 0.0)
                      return;
                  Configuration.JSONConfig.WorldVolumeMute = false;
                  Configuration.SaveConfig();
                  Volume.WorldVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.WorldVolumeMute ? "U" : "M";
              }));
                Volume.VoicesVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeGameVoice").GetComponent<Slider>();
                Volume.VoicesVolumeSlider.onValueChanged.AddListener((UnityAction<float>)(Action<float>)(flt =>
              {
                  if (!Configuration.JSONConfig.VoiceVolumeMute || (double)flt == 0.0)
                      return;
                  Configuration.JSONConfig.VoiceVolumeMute = false;
                  Configuration.SaveConfig();
                  Volume.VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.VoiceVolumeMute ? "U" : "M";
              }));
                Volume.AvatarVolumeSlider = QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel/VolumeGameAvatars").GetComponent<Slider>();
                Volume.AvatarVolumeSlider.onValueChanged.AddListener((UnityAction<float>)(Action<float>)(flt =>
              {
                  if (!Configuration.JSONConfig.AvatarVolumeMute || (double)flt == 0.0)
                      return;
                  Configuration.JSONConfig.AvatarVolumeMute = false;
                  Configuration.SaveConfig();
                  Volume.AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.AvatarVolumeMute ? "U" : "M";
              }));
                Volume.UIVolumeMuteButton = UnityEngine.Object.Instantiate<GameObject>(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/Footer/Exit").gameObject, QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/VolumePanel").transform);
                Volume.UIVolumeMuteButton.GetComponent<RectTransform>().sizeDelta /= new Vector2(5.25f, 1.75f);
                Volume.UIVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition += new Vector2(180f, 468.5f);
                Volume.UIVolumeMuteButton.GetComponentInChildren<Text>().fontSize = (int)((double)Volume.UIVolumeMuteButton.GetComponentInChildren<Text>().fontSize / 1.75);
                Volume.UIVolumeMuteButton.GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
                Volume.WorldVolumeMuteButton = UnityEngine.Object.Instantiate<GameObject>(Volume.UIVolumeMuteButton, Volume.UIVolumeMuteButton.transform.parent);
                Volume.WorldVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0.0f, 47f);
                Volume.VoiceVolumeMuteButton = UnityEngine.Object.Instantiate<GameObject>(Volume.WorldVolumeMuteButton, Volume.UIVolumeMuteButton.transform.parent);
                Volume.VoiceVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0.0f, 47f);
                Volume.AvatarVolumeMuteButton = UnityEngine.Object.Instantiate<GameObject>(Volume.VoiceVolumeMuteButton, Volume.UIVolumeMuteButton.transform.parent);
                Volume.AvatarVolumeMuteButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0.0f, 47f);
                Volume.UIVolumeMuteButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(Action)(() =>
              {
                  if (!Configuration.JSONConfig.UIVolumeMute)
                  {
                      Configuration.JSONConfig.UIVolumeMute = true;
                      Configuration.JSONConfig.UIVolume = Volume.UIVolumeSlider.value;
                      Volume.UIVolumeSlider.value = 0.0f;
                      Configuration.SaveConfig();
                      Volume.UIVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
                  }
                  else
                  {
                      Configuration.JSONConfig.UIVolumeMute = false;
                      Volume.UIVolumeSlider.value = Configuration.JSONConfig.UIVolume;
                      Configuration.SaveConfig();
                      Volume.UIVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
                  }
              }));
                Volume.WorldVolumeMuteButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(Action)(() =>
              {
                  if (!Configuration.JSONConfig.WorldVolumeMute)
                  {
                      Configuration.JSONConfig.WorldVolumeMute = true;
                      Configuration.JSONConfig.WorldVolume = Volume.WorldVolumeSlider.value;
                      Volume.WorldVolumeSlider.value = 0.0f;
                      Configuration.SaveConfig();
                      Volume.WorldVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
                  }
                  else
                  {
                      Configuration.JSONConfig.WorldVolumeMute = false;
                      Volume.WorldVolumeSlider.value = Configuration.JSONConfig.WorldVolume;
                      Configuration.SaveConfig();
                      Volume.WorldVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
                  }
              }));
                Volume.VoiceVolumeMuteButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(Action)(() =>
              {
                  if (!Configuration.JSONConfig.VoiceVolumeMute)
                  {
                      Configuration.JSONConfig.VoiceVolumeMute = true;
                      Configuration.JSONConfig.VoiceVolume = Volume.VoicesVolumeSlider.value;
                      Volume.VoicesVolumeSlider.value = 0.0f;
                      Configuration.SaveConfig();
                      Volume.VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
                  }
                  else
                  {
                      Configuration.JSONConfig.VoiceVolumeMute = false;
                      Volume.VoicesVolumeSlider.value = Configuration.JSONConfig.VoiceVolume;
                      Configuration.SaveConfig();
                      Volume.VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
                  }
              }));
                Volume.AvatarVolumeMuteButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(Action)(() =>
              {
                  if (!Configuration.JSONConfig.AvatarVolumeMute)
                  {
                      Configuration.JSONConfig.AvatarVolumeMute = true;
                      Configuration.JSONConfig.AvatarVolume = Volume.AvatarVolumeSlider.value;
                      Volume.AvatarVolumeSlider.value = 0.0f;
                      Configuration.SaveConfig();
                      Volume.AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = "U";
                  }
                  else
                  {
                      Configuration.JSONConfig.AvatarVolumeMute = false;
                      Volume.AvatarVolumeSlider.value = Configuration.JSONConfig.AvatarVolume;
                      Configuration.SaveConfig();
                      Volume.AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = "M";
                  }
              }));
                Volume.UIVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.UIVolumeMute ? "U" : "M";
                Volume.WorldVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.WorldVolumeMute ? "U" : "M";
                Volume.VoiceVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.VoiceVolumeMute ? "U" : "M";
                Volume.AvatarVolumeMuteButton.GetComponentInChildren<Text>().text = Configuration.JSONConfig.AvatarVolumeMute ? "U" : "M";
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError(ex.ToString());
            }
        }
    }
}
