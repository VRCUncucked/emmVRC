﻿
using emmVRC.Libraries;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using VRC.UI;

namespace emmVRC.Hacks
{
    public class WorldFunctions
    {
        private static GameObject WorldNotesButton;
        private static GameObject ViewFullDescButton;

        public static void Initialize()
        {
            WorldFunctions.WorldNotesButton = UnityEngine.Object.Instantiate<GameObject>(GameObject.Find("MenuContent/Screens/WorldInfo/ReportButton"), GameObject.Find("MenuContent/Screens/WorldInfo").transform);
            WorldFunctions.WorldNotesButton.GetComponentInChildren<Text>().text = "Notes";
            WorldFunctions.WorldNotesButton.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0.0f, 130f);
            WorldFunctions.WorldNotesButton.SetActive(true);
            WorldFunctions.WorldNotesButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            WorldFunctions.WorldNotesButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() => WorldNotes.LoadNote(QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>().field_Private_ApiWorld_0.id, QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>().field_Private_ApiWorld_0.name)));
            WorldFunctions.ViewFullDescButton = UnityEngine.Object.Instantiate<GameObject>(GameObject.Find("MenuContent/Screens/WorldInfo/ReportButton"), GameObject.Find("MenuContent/Screens/WorldInfo").transform);
            WorldFunctions.ViewFullDescButton.transform.Find("Text").GetComponent<RectTransform>().offsetMax = new Vector2(-70.45f, 10f);
            WorldFunctions.ViewFullDescButton.transform.Find("Text").GetComponent<RectTransform>().anchoredPosition = new Vector2(0.0f, 0.0f);
            WorldFunctions.ViewFullDescButton.GetComponentInChildren<Text>().text = "View Full";
            WorldFunctions.ViewFullDescButton.GetComponentInChildren<Text>().resizeTextForBestFit = true;
            WorldFunctions.ViewFullDescButton.GetComponent<RectTransform>().offsetMax = new Vector2(-400f, 100f);
            WorldFunctions.ViewFullDescButton.GetComponent<RectTransform>().offsetMin = new Vector2(-650f, 35f);
            WorldFunctions.ViewFullDescButton.SetActive(true);
            WorldFunctions.ViewFullDescButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            WorldFunctions.ViewFullDescButton.GetComponentInChildren<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2("Description for " + QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>().field_Private_ApiWorld_0.name, QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentInChildren<PageWorldInfo>().field_Private_ApiWorld_0.description, "Close", (System.Action)(() => VRCUiPopupManager.prop_VRCUiPopupManager_0.HideCurrentPopup()))));
        }

        internal static void ToggleItemESP(bool toggle)
        {
            foreach (Component component1 in ComponentToggle.Pickup_stored)
            {
                Renderer component2 = component1.GetComponent<Renderer>();
                if ((UnityEngine.Object)component2 != (UnityEngine.Object)null)
                    HighlightsFX.prop_HighlightsFX_0.Method_Public_Void_Renderer_Boolean_0(component2, toggle);
            }
        }

        internal static void ToggleTriggerESP(bool toggle)
        {
            foreach (Component component1 in ComponentToggle.Trigger_stored)
            {
                Renderer component2 = component1.GetComponent<Renderer>();
                if ((UnityEngine.Object)component2 != (UnityEngine.Object)null)
                    HighlightsFX.prop_HighlightsFX_0.Method_Public_Void_Renderer_Boolean_0(component2, toggle);
            }
        }
    }
}
