﻿
using emmVRC.Libraries;
using emmVRC.TinyJSON;
using emmVRCLoader;
using Il2CppSystem.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Hacks
{
    public class WorldNotes
    {
        public static void Initialize()
        {
            if (Directory.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/worldNotes")))
                return;
            Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/worldNotes"));
        }

        public static void LoadNote(string worldID, string displayName)
        {
            try
            {
                WorldNote loadedNote;
                if (File.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + worldID + ".json")))
                    loadedNote = Decoder.Decode(File.ReadAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + worldID + ".json"))).Make<WorldNote>();
                else
                    loadedNote = new WorldNote()
                    {
                        worldID = worldID,
                        NoteText = ""
                    };
                VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2(
                  "Note for " + displayName,
                  string.IsNullOrWhiteSpace(loadedNote.NoteText) ? "There is currently no note for this world." : loadedNote.NoteText,
                  "Change Note",
                  () => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup(
                    "Enter a note for " + displayName + ":",
                    loadedNote.NoteText,
                    InputField.InputType.Standard,
                    false,
                    "Accept",
                    (System.Action<string, List<KeyCode>, Text>)((newNoteText, keyk, tx) => WorldNotes.SaveNote(new WorldNote()
                    {
                        worldID = worldID,
                        NoteText = newNoteText
                    })),
                    null,
                    "Enter note...."
                  )
                );
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("Failed to load note: " + ex.ToString());
            }
        }

        public static void SaveNote(WorldNote note) => File.WriteAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/worldNotes/" + note.worldID + ".json"), Encoder.Encode((object)note, EncodeOptions.PrettyPrint));
    }
}
