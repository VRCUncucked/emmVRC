﻿
using MelonLoader;
using System.Collections;
using System.Collections.Generic;
using UnhollowerBaseLib;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Libraries
{
    public class AvatarUtilities
    {
        public static List<ApiAvatar> processingList;
        public static bool requestFinished = true;
        public static bool errored = false;

        public static IEnumerator fetchAvatars(
          List<string> avatars,
          System.Action<List<ApiAvatar>, bool> callBack)
        {
            AvatarUtilities.processingList = new List<ApiAvatar>();
            AvatarUtilities.requestFinished = true;
            AvatarUtilities.errored = false;
            foreach (string avatar in avatars)
            {
                string avatarId = avatar;
                while (!AvatarUtilities.requestFinished)
                    yield return (object)new WaitForEndOfFrame();
                AvatarUtilities.requestFinished = false;
                API.Fetch<ApiAvatar>(avatarId, (Il2CppSystem.Action<ApiContainer>)(System.Action<ApiContainer>)(container =>
              {
                  AvatarUtilities.processingList.Add(((Il2CppObjectBase)container.Model).Cast<ApiAvatar>());
                  emmVRCLoader.Logger.LogDebug("Found avatar " + ((Il2CppObjectBase)container.Model).Cast<ApiAvatar>().name);
                  MelonCoroutines.Start(AvatarUtilities.Delay());
              }), (Il2CppSystem.Action<ApiContainer>)(System.Action<ApiContainer>)(container =>
      {
          AvatarUtilities.errored = true;
          emmVRCLoader.Logger.LogDebug("Current avatar is not available.");
          MelonCoroutines.Start(AvatarUtilities.Delay());
      }), false);
                avatarId = (string)null;
            }
            while (!AvatarUtilities.requestFinished)
                yield return (object)new WaitForEndOfFrame();
            callBack(AvatarUtilities.processingList, AvatarUtilities.errored);
        }

        public static IEnumerator Delay()
        {
            yield return (object)new WaitForSeconds(2.5f);
            AvatarUtilities.requestFinished = true;
        }
    }
}
