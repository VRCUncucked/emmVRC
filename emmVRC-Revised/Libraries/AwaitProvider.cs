﻿
using emmVRCLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace emmVRC.Libraries
{
    public class AwaitProvider
    {
        private readonly Queue<Action> myToMainThreadQueue = new Queue<Action>();

        public void Flush()
        {
            if (this.myToMainThreadQueue.Count == 0)
                return;
            List<Action> list;
            lock (this.myToMainThreadQueue)
            {
                list = this.myToMainThreadQueue.ToList<Action>();
                this.myToMainThreadQueue.Clear();
            }
            foreach (Action action in list)
            {
                try
                {
                    action();
                }
                catch (Exception ex)
                {
                    emmVRCLoader.Logger.LogError(string.Format("Exception in task: {0}", (object)ex));
                }
            }
        }

        public AwaitProvider.YieldAwaitable Yield() => new AwaitProvider.YieldAwaitable(this.myToMainThreadQueue);

        public readonly struct YieldAwaitable : INotifyCompletion
        {
            private readonly Queue<Action> myToMainThreadQueue;

            public YieldAwaitable(Queue<Action> toMainThreadQueue) => this.myToMainThreadQueue = toMainThreadQueue;

            public bool IsCompleted => false;

            public AwaitProvider.YieldAwaitable GetAwaiter() => this;

            public void GetResult()
            {
            }

            public void OnCompleted(Action continuation)
            {
                lock (this.myToMainThreadQueue)
                    this.myToMainThreadQueue.Enqueue(continuation);
            }
        }
    }
}
