﻿
using System.Collections.Generic;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class ButtonConfigurationMenu
    {
        public System.Action<Vector2> acceptAction;
        public System.Action<ButtonConfigurationMenu> openAction;
        public string menuTitle;
        private QMNestedButton baseMenu;
        private QMSingleButton Button_0_N1;
        private QMSingleButton Button_5_N1;
        private QMSingleButton Button_0_0;
        private QMSingleButton Button_1_0;
        private QMSingleButton Button_2_0;
        private QMSingleButton Button_3_0;
        private QMSingleButton Button_4_0;
        private QMSingleButton Button_5_0;
        private QMSingleButton Button_0_1;
        private QMSingleButton Button_1_1;
        private QMSingleButton Button_2_1;
        private QMSingleButton Button_3_1;
        private QMSingleButton Button_4_1;
        private QMSingleButton Button_5_1;
        private QMSingleButton Button_1_2;
        private QMSingleButton Button_2_2;
        private QMSingleButton Button_3_2;
        private QMSingleButton Button_4_2;
        private QMSingleButton Button_5_2;

        public ButtonConfigurationMenu(
          string baseMenuName,
          int x,
          int y,
          string menuName,
          System.Action<ButtonConfigurationMenu> openAction,
          System.Action<Vector2> acceptAction,
          string tooltip,
          string menuTitle,
          List<KeyValuePair<string, Vector2>> disabledButtons = null)
        {
            this.baseMenu = new QMNestedButton(baseMenuName, x, y, menuName, tooltip);
            this.Button_0_N1 = new QMSingleButton(this.baseMenu, 0, -1, "", (System.Action)(() => acceptAction(new Vector2(0.0f, -1f))), "Button Position: 0, -1");
            this.Button_5_N1 = new QMSingleButton(this.baseMenu, 5, -1, "", (System.Action)(() => acceptAction(new Vector2(5f, -1f))), "Button Position: 5, -1");
            this.Button_0_0 = new QMSingleButton(this.baseMenu, 0, 0, "", (System.Action)(() => acceptAction(new Vector2(0.0f, 0.0f))), "Button Position: 0, 0");
            this.Button_1_0 = new QMSingleButton(this.baseMenu, 1, 0, "", (System.Action)(() => acceptAction(new Vector2(1f, 0.0f))), "Button Position: 1, 0");
            this.Button_2_0 = new QMSingleButton(this.baseMenu, 2, 0, "", (System.Action)(() => acceptAction(new Vector2(2f, 0.0f))), "Button Position: 2, 0");
            this.Button_3_0 = new QMSingleButton(this.baseMenu, 3, 0, "", (System.Action)(() => acceptAction(new Vector2(3f, 0.0f))), "Button Position: 3, 0");
            this.Button_4_0 = new QMSingleButton(this.baseMenu, 4, 0, "", (System.Action)(() => acceptAction(new Vector2(4f, 0.0f))), "Button Position: 4, 0");
            this.Button_5_0 = new QMSingleButton(this.baseMenu, 5, 0, "", (System.Action)(() => acceptAction(new Vector2(5f, 0.0f))), "Button Position: 5, 0");
            this.Button_0_1 = new QMSingleButton(this.baseMenu, 0, 1, "", (System.Action)(() => acceptAction(new Vector2(0.0f, 1f))), "Button Position: 0, 1");
            this.Button_1_1 = new QMSingleButton(this.baseMenu, 1, 1, "", (System.Action)(() => acceptAction(new Vector2(1f, 1f))), "Button Position: 1, 1");
            this.Button_2_1 = new QMSingleButton(this.baseMenu, 2, 1, "", (System.Action)(() => acceptAction(new Vector2(2f, 1f))), "Button Position: 2, 1");
            this.Button_3_1 = new QMSingleButton(this.baseMenu, 3, 1, "", (System.Action)(() => acceptAction(new Vector2(3f, 1f))), "Button Position: 3, 1");
            this.Button_4_1 = new QMSingleButton(this.baseMenu, 4, 1, "", (System.Action)(() => acceptAction(new Vector2(4f, 1f))), "Button Position: 4, 1");
            this.Button_5_1 = new QMSingleButton(this.baseMenu, 5, 1, "", (System.Action)(() => acceptAction(new Vector2(5f, 1f))), "Button Position: 5, 1");
            this.Button_1_2 = new QMSingleButton(this.baseMenu, 1, 2, "", (System.Action)(() => acceptAction(new Vector2(1f, 2f))), "Button Position: 1, 2");
            this.Button_2_2 = new QMSingleButton(this.baseMenu, 2, 2, "", (System.Action)(() => acceptAction(new Vector2(2f, 2f))), "Button Position: 2, 2");
            this.Button_3_2 = new QMSingleButton(this.baseMenu, 3, 2, "", (System.Action)(() => acceptAction(new Vector2(3f, 2f))), "Button Position: 3, 2");
            this.Button_4_2 = new QMSingleButton(this.baseMenu, 4, 2, "", (System.Action)(() => acceptAction(new Vector2(4f, 2f))), "Button Position: 4, 2");
            this.Button_5_2 = new QMSingleButton(this.baseMenu, 5, 2, "", (System.Action)(() => acceptAction(new Vector2(5f, 2f))), "Button Position: 5, 2");
            this.baseMenu.getBackButton().DestroyMe();
            this.openAction = openAction;
            this.acceptAction = acceptAction;
            this.menuTitle = menuTitle;
            if (disabledButtons != null)
                this.ChangeDisabledButtons(disabledButtons);
            this.baseMenu.getMainButton().setAction(new System.Action(this.OpenMenu));
        }

        public void OpenMenu()
        {
            this.openAction(this);
            QuickMenuUtils.ShowQuickmenuPage(this.baseMenu.getMenuName());
            QuickMenuUtils.GetQuickMenuInstance().SetQuickMenuContext(QuickMenuContextualDisplay.EnumNPublicSealedvaUnNoToUs7vUsNoUnique.Notification, text: this.menuTitle);
        }

        public void ChangeDisabledButtons(
          List<KeyValuePair<string, Vector2>> newDisabledButtons)
        {
            this.Button_0_N1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_5_N1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_0_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_1_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_2_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_3_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_4_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_5_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_0_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_1_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_2_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_3_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_4_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_5_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_1_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_2_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_3_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_4_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_5_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            this.Button_0_N1.setButtonText("");
            this.Button_5_N1.setButtonText("");
            this.Button_0_0.setButtonText("");
            this.Button_1_0.setButtonText("");
            this.Button_2_0.setButtonText("");
            this.Button_3_0.setButtonText("");
            this.Button_4_0.setButtonText("");
            this.Button_5_0.setButtonText("");
            this.Button_0_1.setButtonText("");
            this.Button_1_1.setButtonText("");
            this.Button_2_1.setButtonText("");
            this.Button_3_1.setButtonText("");
            this.Button_4_1.setButtonText("");
            this.Button_5_1.setButtonText("");
            this.Button_1_2.setButtonText("");
            this.Button_2_2.setButtonText("");
            this.Button_3_2.setButtonText("");
            this.Button_4_2.setButtonText("");
            this.Button_5_2.setButtonText("");
            foreach (KeyValuePair<string, Vector2> newDisabledButton in newDisabledButtons)
            {
                if (newDisabledButton.Value == new Vector2(0.0f, -1f))
                {
                    this.Button_0_N1.setButtonText(newDisabledButton.Key);
                    this.Button_0_N1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(5f, -1f))
                {
                    this.Button_5_N1.setButtonText(newDisabledButton.Key);
                    this.Button_5_N1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(0.0f, 0.0f))
                {
                    this.Button_0_0.setButtonText(newDisabledButton.Key);
                    this.Button_0_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(1f, 0.0f))
                {
                    this.Button_1_0.setButtonText(newDisabledButton.Key);
                    this.Button_1_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(2f, 0.0f))
                {
                    this.Button_2_0.setButtonText(newDisabledButton.Key);
                    this.Button_2_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(3f, 0.0f))
                {
                    this.Button_3_0.setButtonText(newDisabledButton.Key);
                    this.Button_3_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(4f, 0.0f))
                {
                    this.Button_4_0.setButtonText(newDisabledButton.Key);
                    this.Button_4_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(5f, 0.0f))
                {
                    this.Button_5_0.setButtonText(newDisabledButton.Key);
                    this.Button_5_0.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(0.0f, 1f))
                {
                    this.Button_0_1.setButtonText(newDisabledButton.Key);
                    this.Button_0_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(1f, 1f))
                {
                    this.Button_1_1.setButtonText(newDisabledButton.Key);
                    this.Button_1_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(2f, 1f))
                {
                    this.Button_2_1.setButtonText(newDisabledButton.Key);
                    this.Button_2_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(3f, 1f))
                {
                    this.Button_3_1.setButtonText(newDisabledButton.Key);
                    this.Button_3_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(4f, 1f))
                {
                    this.Button_4_1.setButtonText(newDisabledButton.Key);
                    this.Button_4_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(5f, 1f))
                {
                    this.Button_5_1.setButtonText(newDisabledButton.Key);
                    this.Button_5_1.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(1f, 2f))
                {
                    this.Button_1_2.setButtonText(newDisabledButton.Key);
                    this.Button_1_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(2f, 2f))
                {
                    this.Button_2_2.setButtonText(newDisabledButton.Key);
                    this.Button_2_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(3f, 2f))
                {
                    this.Button_3_2.setButtonText(newDisabledButton.Key);
                    this.Button_3_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(4f, 2f))
                {
                    this.Button_4_2.setButtonText(newDisabledButton.Key);
                    this.Button_4_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
                if (newDisabledButton.Value == new Vector2(5f, 2f))
                {
                    this.Button_5_2.setButtonText(newDisabledButton.Key);
                    this.Button_5_2.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                }
            }
        }
    }
}
