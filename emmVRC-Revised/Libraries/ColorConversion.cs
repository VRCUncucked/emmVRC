﻿
using System;
using System.Globalization;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class ColorConversion
    {
        public static Color HexToColor(string hexColor)
        {
            if (hexColor.IndexOf('#') != -1)
                hexColor = hexColor.Replace("#", "");
            double num1 = (double)int.Parse(hexColor.Substring(0, 2), NumberStyles.AllowHexSpecifier) / (double)byte.MaxValue;
            float num2 = (float)int.Parse(hexColor.Substring(2, 2), NumberStyles.AllowHexSpecifier) / (float)byte.MaxValue;
            float num3 = (float)int.Parse(hexColor.Substring(4, 2), NumberStyles.AllowHexSpecifier) / (float)byte.MaxValue;
            double num4 = (double)num2;
            double num5 = (double)num3;
            return new Color((float)num1, (float)num4, (float)num5);
        }

        public static string ColorToHex(Color baseColor, bool hash = false)
        {
            int int32_1 = Convert.ToInt32(baseColor.r * (float)byte.MaxValue);
            int int32_2 = Convert.ToInt32(baseColor.g * (float)byte.MaxValue);
            int int32_3 = Convert.ToInt32(baseColor.b * (float)byte.MaxValue);
            string str = int32_1.ToString("X2") + int32_2.ToString("X2") + int32_3.ToString("X2");
            if (hash)
                str = "#" + str;
            return str;
        }
    }
}
