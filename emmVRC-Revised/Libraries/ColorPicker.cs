﻿
using System;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class ColorPicker
    {
        internal Objects.Slider r;
        internal Objects.Slider g;
        internal Objects.Slider b;
        internal QMSingleButton rButton;
        internal QMSingleButton gButton;
        internal QMSingleButton bButton;
        internal float rValue;
        internal float gValue;
        internal float bValue;
        internal Color defaultColor;
        internal QMNestedButton baseMenu;
        internal QMSingleButton preview;
        internal QMSingleButton acceptButton;
        internal QMSingleButton defaultButton;
        internal QMSingleButton cancelButton;

        public ColorPicker(
          string parentMenu,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Action<Color> accept,
          Action cancel,
          Color? currentColor = null,
          Color? defaultColor = null)
        {
            ColorPicker colorPicker = this;
            this.baseMenu = new QMNestedButton(parentMenu, x, y, menuName, menuTooltip);
            this.baseMenu.getBackButton().DestroyMe();
            this.preview = new QMSingleButton(this.baseMenu, 4, 0, "Preview", (Action)null, "Preview of the color selected");
            this.preview.getGameObject().name = "ColorPickPreviewButton";
            this.acceptButton = new QMSingleButton(this.baseMenu, 4, 1, "Accept", (Action)(() => accept(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue))), "Accept the color changes");
            this.defaultButton = new QMSingleButton(this.baseMenu, 4, 2, "Default", (Action)(() =>
           {
               Color defaultColor1 = colorPicker.defaultColor;
               colorPicker.r.slider.GetComponent<UnityEngine.UI.Slider>().value = colorPicker.defaultColor.r;
               colorPicker.g.slider.GetComponent<UnityEngine.UI.Slider>().value = colorPicker.defaultColor.g;
               colorPicker.b.slider.GetComponent<UnityEngine.UI.Slider>().value = colorPicker.defaultColor.b;
           }), "Set the color to the default value");
            this.cancelButton = new QMSingleButton(this.baseMenu, 4, 2, "Cancel", (Action)(() => cancel()), "Cancels the color changes");
            this.defaultButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            this.defaultButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, 96f);
            this.cancelButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            this.cancelButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, -96f);
            this.rButton = new QMSingleButton(this.baseMenu, 1, 0, "R\n1f", (Action)null, "Float value for Red", new Color?(Color.red));
            this.rButton.getGameObject().name = "rColorButton";
            this.gButton = new QMSingleButton(this.baseMenu, 1, 1, "G\n1f", (Action)null, "Float value for Green", new Color?(Color.green));
            this.gButton.getGameObject().name = "gColorButton";
            this.bButton = new QMSingleButton(this.baseMenu, 1, 2, "B\n1f", (Action)null, "Float value for Blue", new Color?(Color.blue));
            this.bButton.getGameObject().name = "bColorButton";
            this.r = new Objects.Slider(this.baseMenu.getMenuName(), 2, 0, (Action<float>)(val =>
           {
               colorPicker.rValue = val;
               colorPicker.preview.setBackgroundColor(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue), true);
               colorPicker.preview.setTextColor(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue), true);
               colorPicker.rButton.setButtonText("R\n" + colorPicker.rValue.ToString("0.00"));
               colorPicker.gButton.setButtonText("G\n" + colorPicker.gValue.ToString("0.00"));
               colorPicker.bButton.setButtonText("B\n" + colorPicker.bValue.ToString("0.00"));
           }));
            this.g = new Objects.Slider(this.baseMenu.getMenuName(), 2, 1, (Action<float>)(val =>
           {
               colorPicker.gValue = val;
               colorPicker.preview.setBackgroundColor(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue), true);
               colorPicker.preview.setTextColor(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue), true);
               colorPicker.rButton.setButtonText("R\n" + colorPicker.rValue.ToString("0.00"));
               colorPicker.gButton.setButtonText("G\n" + colorPicker.gValue.ToString("0.00"));
               colorPicker.bButton.setButtonText("B\n" + colorPicker.bValue.ToString("0.00"));
           }));
            this.b = new Objects.Slider(this.baseMenu.getMenuName(), 2, 2, (Action<float>)(val =>
           {
               colorPicker.bValue = val;
               colorPicker.preview.setBackgroundColor(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue), true);
               colorPicker.preview.setTextColor(new Color(colorPicker.rValue, colorPicker.gValue, colorPicker.bValue), true);
               colorPicker.rButton.setButtonText("R\n" + colorPicker.rValue.ToString("0.00"));
               colorPicker.gButton.setButtonText("G\n" + colorPicker.gValue.ToString("0.00"));
               colorPicker.bButton.setButtonText("B\n" + colorPicker.bValue.ToString("0.00"));
           }));
            if (!defaultColor.HasValue)
                return;
            this.defaultColor = defaultColor.Value;
        }
    }
}
