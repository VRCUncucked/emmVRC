﻿
using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnhollowerRuntimeLib;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;

namespace emmVRC.Libraries
{
    public static class CustomActionMenu
    {
        private static List<CustomActionMenu.Page> customPages = new List<CustomActionMenu.Page>();
        private static List<CustomActionMenu.Button> mainMenuButtons = new List<CustomActionMenu.Button>();
        private static bool Initialized = false;
        public static ActionMenu activeActionMenu;
        public static Texture2D ToggleOffTexture;
        public static Texture2D ToggleOnTexture;

        public static bool isOpen(this ActionMenuOpener actionMenuOpener) => actionMenuOpener.field_Private_Boolean_0;

        private static ActionMenuOpener GetActionMenuOpener()
        {
            if (!ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_0.isOpen() && ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_1.isOpen())
                return ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_1;
            return ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_0.isOpen() && !ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_1.isOpen() ? ActionMenuDriver.field_Public_Static_ActionMenuDriver_0.field_Public_ActionMenuOpener_0 : (ActionMenuOpener)null;
        }

        private static void Initialize()
        {
            HarmonyInstance.Create("customActionMenuHarmony").Patch((MethodBase)(typeof(ActionMenu).GetMethods()).FirstOrDefault<MethodInfo>((Func<MethodInfo, bool>)(it => XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "Emojis")))), (HarmonyMethod)null, new HarmonyMethod(typeof(CustomActionMenu).GetMethod("OpenMainPage", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null);
            CustomActionMenu.ToggleOffTexture = UnityEngine.Resources.Load<Texture2D>("GUI_Toggle_OFF");
            CustomActionMenu.ToggleOnTexture = UnityEngine.Resources.Load<Texture2D>("GUI_Toggle_ON");
            CustomActionMenu.Initialized = true;
        }

        private static void OpenMainPage(ActionMenu __instance)
        {
            CustomActionMenu.activeActionMenu = __instance;
            if (!Configuration.JSONConfig.ActionMenuIntegration)
                return;
            foreach (CustomActionMenu.Button mainMenuButton in CustomActionMenu.mainMenuButtons)
            {
                PedalOption pedalOption = CustomActionMenu.activeActionMenu.Method_Private_PedalOption_0();
                pedalOption.prop_String_0 = mainMenuButton.ButtonText;
                pedalOption.field_Public_MulticastDelegateNPublicSealedBoUnique_0 = DelegateSupport.ConvertDelegate<PedalOption.MulticastDelegateNPublicSealedBoUnique>((System.Delegate)mainMenuButton.ButtonAction);
                if ((UnityEngine.Object)mainMenuButton.ButtonIcon != (UnityEngine.Object)null)
                    pedalOption.prop_Texture2D_0 = mainMenuButton.ButtonIcon;
                mainMenuButton.currentPedalOption = pedalOption;
            }
        }

        public enum BaseMenu
        {
            MainMenu = 1,
        }

        public class Page
        {
            public List<CustomActionMenu.Button> buttons = new List<CustomActionMenu.Button>();
            public CustomActionMenu.Page previousPage;
            public CustomActionMenu.Button menuEntryButton;

            public Page(CustomActionMenu.BaseMenu baseMenu, string buttonText, Texture2D buttonIcon = null)
            {
                if (!CustomActionMenu.Initialized)
                    CustomActionMenu.Initialize();
                if (baseMenu != CustomActionMenu.BaseMenu.MainMenu)
                    return;
                this.menuEntryButton = new CustomActionMenu.Button(CustomActionMenu.BaseMenu.MainMenu, buttonText, (System.Action)(() => this.OpenMenu((CustomActionMenu.Page)null)), buttonIcon);
            }

            public Page(CustomActionMenu.Page basePage, string buttonText, Texture2D buttonIcon = null)
            {
                CustomActionMenu.Page page = this;
                if (!CustomActionMenu.Initialized)
                    CustomActionMenu.Initialize();
                this.previousPage = basePage;
                this.menuEntryButton = new CustomActionMenu.Button(this.previousPage, buttonText, (System.Action)(() => page.OpenMenu(basePage)), buttonIcon);
            }

            public void OpenMenu(CustomActionMenu.Page currentPage)
            {
                if (!CustomActionMenu.Initialized)
                    CustomActionMenu.Initialize();
                CustomActionMenu.GetActionMenuOpener().field_Public_ActionMenu_0.Method_Public_ObjectNPublicAcTeAcStGaUnique_Action_Action_Texture2D_String_0((Il2CppSystem.Action)(System.Action)(() =>
              {
                  foreach (CustomActionMenu.Button button in this.buttons)
                  {
                      PedalOption pedalOption = CustomActionMenu.GetActionMenuOpener().field_Public_ActionMenu_0.Method_Private_PedalOption_0();
                      pedalOption.prop_String_0 = button.ButtonText;
                      pedalOption.field_Public_MulticastDelegateNPublicSealedBoUnique_0 = DelegateSupport.ConvertDelegate<PedalOption.MulticastDelegateNPublicSealedBoUnique>((System.Delegate)button.ButtonAction);
                      pedalOption.prop_Boolean_0 = button.IsEnabled;
                      if ((UnityEngine.Object)button.ButtonIcon != (UnityEngine.Object)null)
                          pedalOption.prop_Texture2D_0 = button.ButtonIcon;
                      button.currentPedalOption = pedalOption;
                  }
              }));
            }
        }

        public class Button
        {
            public string ButtonText;
            public bool IsEnabled;
            public System.Action ButtonAction;
            public Texture2D ButtonIcon;
            public PedalOption currentPedalOption;

            public Button(
              CustomActionMenu.BaseMenu baseMenu,
              string buttonText,
              System.Action buttonAction,
              Texture2D buttonIcon = null)
            {
                if (!CustomActionMenu.Initialized)
                    CustomActionMenu.Initialize();
                this.ButtonText = buttonText;
                this.ButtonAction = buttonAction;
                this.ButtonIcon = buttonIcon;
                if (baseMenu != CustomActionMenu.BaseMenu.MainMenu)
                    return;
                CustomActionMenu.mainMenuButtons.Add(this);
            }

            public Button(
              CustomActionMenu.Page basePage,
              string buttonText,
              System.Action buttonAction,
              Texture2D buttonIcon = null)
            {
                if (!CustomActionMenu.Initialized)
                    CustomActionMenu.Initialize();
                this.ButtonText = buttonText;
                this.ButtonAction = buttonAction;
                this.ButtonIcon = buttonIcon;
                basePage.buttons.Add(this);
            }

            public void SetButtonText(string newText)
            {
                this.ButtonText = newText;
                if (!((UnityEngine.Object)this.currentPedalOption != (UnityEngine.Object)null))
                    return;
                this.currentPedalOption.prop_String_0 = newText;
            }

            public void SetIcon(Texture2D newTexture)
            {
                this.ButtonIcon = newTexture;
                if (!((UnityEngine.Object)this.currentPedalOption != (UnityEngine.Object)null))
                    return;
                this.currentPedalOption.prop_Texture2D_0 = newTexture;
            }

            public void SetEnabled(bool enabled)
            {
                this.IsEnabled = enabled;
                if (!((UnityEngine.Object)this.currentPedalOption != (UnityEngine.Object)null))
                    return;
                this.currentPedalOption.prop_Boolean_0 = !enabled;
            }
        }
    }
}
