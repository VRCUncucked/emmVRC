﻿
using emmVRC.Hacks;
using emmVRC.Managers;
using Harmony;
using MelonLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using UnityEngine.Events;
using VRC;
using VRC.Core;
using VRC.SDKBase;

namespace emmVRC.Libraries
{
    public class Hooking
    {
        private static HarmonyInstance instanceHarmony;
        private static System.Action<Player> event1Action;
        private static System.Action<Player> event2Action;
        private static Regex methodMatchRegex = new Regex("Method_Public_Void_\\d", RegexOptions.Compiled);

        public static void Initialize()
        {
            Hooking.instanceHarmony = HarmonyInstance.Create("emmVRCHarmony");
            try
            {
                Hooking.instanceHarmony.Patch((MethodBase)typeof(VRCPlayer).GetMethod("Awake"), new HarmonyMethod(typeof(Hooking).GetMethod("VRCPlayerAwake", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null, (HarmonyMethod)null);
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError("Avatar patching failed: " + ex.ToString());
            }
            try
            {
                if (!ModCompatibility.PortalConfirmation)
                    Hooking.instanceHarmony.Patch((MethodBase)((IEnumerable<MethodInfo>)typeof(PortalInternal).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(void) && it.GetParameters().Length == 0 && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == " was at capacity, cannot enter.")))), new HarmonyMethod(typeof(Hooking).GetMethod("OnPortalEntered", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null, (HarmonyMethod)null);
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError("Portal blocking failed: " + ex.ToString());
            }
            try
            {
                Hooking.instanceHarmony.Patch((MethodBase)typeof(VRC_StationInternal).GetMethod("Method_Public_Boolean_Player_Boolean_0"), new HarmonyMethod(typeof(Hooking).GetMethod("PlayerCanUseStation", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null, (HarmonyMethod)null);
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError("Station patching failed: " + ex.ToString());
            }
            try
            {
                NetworkManager.field_Internal_Static_NetworkManager_0.field_Internal_VRCEventDelegate_1_Player_0.Method_Public_Void_UnityAction_1_T_0((UnityAction<Player>)(System.Action<Player>)(plr =>
              {
                  if (Hooking.event1Action == null && Hooking.event2Action == null)
                  {
                      Hooking.event1Action = (System.Action<Player>)(plr2 => NetworkManagerHooking.OnPlayerJoined(plr2));
                      Hooking.event2Action = (System.Action<Player>)(plr3 => NetworkManagerHooking.OnPlayerLeft(plr3));
                      Hooking.event1Action(plr);
                  }
                  else
                      Hooking.event1Action(plr);
              }));
                NetworkManager.field_Internal_Static_NetworkManager_0.field_Internal_VRCEventDelegate_1_Player_1.Method_Public_Void_UnityAction_1_T_1((UnityAction<Player>)(System.Action<Player>)(plr =>
              {
                  if (Hooking.event1Action == null && Hooking.event2Action == null)
                  {
                      Hooking.event2Action = (System.Action<Player>)(plr2 => NetworkManagerHooking.OnPlayerJoined(plr2));
                      Hooking.event1Action = (System.Action<Player>)(plr3 => NetworkManagerHooking.OnPlayerLeft(plr3));
                      Hooking.event2Action(plr);
                  }
                  else
                      Hooking.event2Action(plr);
              }));
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError("Network Manager hooking failed: " + ex.ToString());
            }
            if (!ModCompatibility.FBTSaver && !ModCompatibility.IKTweaks)
            {
                if (!Environment.CurrentDirectory.Contains("vrchat-vrchat"))
                {
                    try
                    {
                        foreach (MethodInfo method in Assembly.GetAssembly(typeof(QuickMenuContextualDisplay)).GetType("VRCTrackingSteam", true, true).GetMethods())
                        {
                            if (method.GetParameters().Length == 1 && ((IEnumerable<ParameterInfo>)method.GetParameters()).First<ParameterInfo>().ParameterType == typeof(string) && (method.ReturnType == typeof(bool) && method.GetRuntimeBaseDefinition() == method))
                                Hooking.instanceHarmony.Patch((MethodBase)method, new HarmonyMethod(typeof(Hooking).GetMethod("IsCalibratedForAvatar", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null, (HarmonyMethod)null);
                        }
                        foreach (MethodInfo method in Assembly.GetAssembly(typeof(QuickMenuContextualDisplay)).GetType("VRCTrackingSteam", true, true).GetMethods())
                        {
                            if (method.GetParameters().Length == 3 && ((IEnumerable<ParameterInfo>)method.GetParameters()).First<ParameterInfo>().ParameterType == typeof(Animator) && (method.ReturnType == typeof(void) && method.GetRuntimeBaseDefinition() == method))
                                Hooking.instanceHarmony.Patch((MethodBase)method, new HarmonyMethod(typeof(Hooking).GetMethod("PerformCalibration", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null, (HarmonyMethod)null);
                        }
                    }
                    catch (Exception ex)
                    {
                        emmVRCLoader.Logger.LogError("VRCTrackingSteam hooking failed: " + ex.ToString());
                    }
                }
            }
            try
            {
                foreach (MethodInfo methodInfo in ((IEnumerable<MethodInfo>)typeof(PlayerNameplate).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Where<MethodInfo>((Func<MethodInfo, bool>)(x => Hooking.methodMatchRegex.IsMatch(x.Name))))
                {
                    emmVRCLoader.Logger.LogDebug("Found target Rebuild method (" + methodInfo.Name + ")", new object[1]
                    {
            (object) true
                    });
                    Hooking.instanceHarmony.Patch((MethodBase)methodInfo, (HarmonyMethod)null, new HarmonyMethod(typeof(Hooking).GetMethod("OnRebuild", BindingFlags.Static | BindingFlags.NonPublic)), (HarmonyMethod)null);
                }
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError("Avatar OnRebuild Failed: " + ex.ToString());
            }
        }

        private static bool PlayerCanUseStation(ref bool __result, Player __0, bool __1)
        {
            if (!((UnityEngine.Object)__0 != (UnityEngine.Object)null) || !((UnityEngine.Object)__0 == (UnityEngine.Object)VRCPlayer.field_Internal_Static_VRCPlayer_0._player) || !Configuration.JSONConfig.ChairBlockingEnable)
                return true;
            __result = false;
            return false;
        }

        private static bool VRCPlayerAwake(VRCPlayer __instance)
        {
            __instance.Method_Public_add_Void_MulticastDelegateNPublicSealedVoUnique_0((VRCPlayer.MulticastDelegateNPublicSealedVoUnique)(System.Action)(() =>
          {
              if (!((UnityEngine.Object)__instance != (UnityEngine.Object)null) || !((UnityEngine.Object)__instance._player != (UnityEngine.Object)null) || (__instance._player.prop_APIUser_0 == null || !((UnityEngine.Object)__instance._player._vrcplayer.prop_VRCAvatarManager_0 != (UnityEngine.Object)null)))
                  return;
              Hooking.OnAvatarInstantiate(__instance.prop_VRCAvatarManager_0, __instance.prop_VRCAvatarManager_0.prop_GameObject_0);
          }));
            return true;
        }

        private static void OnAvatarInstantiate(VRCAvatarManager __instance, GameObject __0)
        {
            emmVRCLoader.Logger.LogDebug("Avatar loaded");
            emmVRCLoader.Logger.LogDebug("Field Descriptor 1 is " + ((UnityEngine.Object)__instance.field_Private_VRC_AvatarDescriptor_0 == (UnityEngine.Object)null ? "null" : "not null"));
            emmVRCLoader.Logger.LogDebug("Field Descriptor 2 is " + ((UnityEngine.Object)__instance.field_Private_VRC_AvatarDescriptor_1 == (UnityEngine.Object)null ? "null" : "not null"));
            emmVRCLoader.Logger.LogDebug("Field AvatarDescriptor is " + ((UnityEngine.Object)__instance.field_Private_VRCAvatarDescriptor_0 == (UnityEngine.Object)null ? "null" : "not null"));
            emmVRCLoader.Logger.LogDebug("Prop Descriptor 1 is " + ((UnityEngine.Object)__instance.prop_VRC_AvatarDescriptor_0 == (UnityEngine.Object)null ? "null" : "not null"));
            emmVRCLoader.Logger.LogDebug("Prop Descriptor 2 is " + ((UnityEngine.Object)__instance.prop_VRC_AvatarDescriptor_1 == (UnityEngine.Object)null ? "null" : "not null"));
            emmVRCLoader.Logger.LogDebug("Prop AvatarDescriptor is " + ((UnityEngine.Object)__instance.prop_VRCAvatarDescriptor_0 == (UnityEngine.Object)null ? "null" : "not null"));
            VRC_AvatarDescriptor avatarDescriptor = (VRC_AvatarDescriptor)null;
            if ((UnityEngine.Object)__instance.field_Private_VRC_AvatarDescriptor_0 != (UnityEngine.Object)null)
                avatarDescriptor = __instance.field_Private_VRC_AvatarDescriptor_0;
            else if ((UnityEngine.Object)__instance.field_Private_VRC_AvatarDescriptor_1 != (UnityEngine.Object)null)
                avatarDescriptor = (VRC_AvatarDescriptor)__instance.field_Private_VRC_AvatarDescriptor_1;
            if ((UnityEngine.Object)avatarDescriptor == (UnityEngine.Object)null)
            {
                emmVRCLoader.Logger.LogError("The avatar descriptor could not be obtained from this avatar. Global Dynamic Bones and Avatar Permissions will not function.");
            }
            else
            {
                AvatarPermissionManager.ProcessAvatar(__0, avatarDescriptor);
                if (ModCompatibility.MultiplayerDynamicBones)
                    return;
                GlobalDynamicBones.ProcessDynamicBones(__0, avatarDescriptor);
            }
        }

        private static bool OnPortalEntered(PortalInternal __instance) => !Configuration.JSONConfig.PortalBlockingEnable;

        private static void OnRebuild(PlayerNameplate __instance)
        {
            if (Configuration.JSONConfig.StealthMode || (UnityEngine.Object)__instance.field_Private_VRCPlayer_0 == (UnityEngine.Object)null || (!((UnityEngine.Object)__instance.field_Private_VRCPlayer_0._player != (UnityEngine.Object)null) || __instance.field_Private_VRCPlayer_0._player.prop_APIUser_0 == null))
                return;
            if (Configuration.JSONConfig.InfoSpoofingEnabled)
                VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text = Configuration.JSONConfig.InfoSpoofingName;
            else if (!Configuration.JSONConfig.InfoSpoofingEnabled && VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text.Contains(NameSpoofGenerator.spoofedName))
                VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text = VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
            if (!Configuration.JSONConfig.NameplateColorChangingEnabled || ModCompatibility.OGTrustRank)
                return;
            APIUser propApiUser0 = __instance.field_Private_VRCPlayer_0._player.prop_APIUser_0;
            if (propApiUser0.isFriend)
                __instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex);
            else if (propApiUser0.hasVeteranTrustLevel)
                __instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex);
            else if (propApiUser0.hasTrustedTrustLevel)
                __instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex);
            else if (propApiUser0.hasKnownTrustLevel)
            {
                __instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex);
            }
            else
            {
                if (!propApiUser0.hasBasicTrustLevel)
                    return;
                __instance.field_Private_VRCPlayer_0.GetNameplateBackground().color = ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex);
            }
        }

        private static bool IsCalibratedForAvatar(
          ref VRCTrackingSteam __instance,
          ref bool __result,
          string __0)
        {
            if (__0 != null && FBTSaving.IsPreviouslyCalibrated(__0) && (RoomManager.field_Internal_Static_ApiWorld_0 != null && Configuration.JSONConfig.TrackingSaving))
            {
                emmVRCLoader.Logger.LogDebug("Avatar was previously calibrated, loading calibration data");
                __result = true;
                FBTSaving.LoadCalibrationInfo(__instance, __0);
                return false;
            }
            emmVRCLoader.Logger.LogDebug("Avatar was not previously calibrated, or tracking saving is off");
            __result = false;
            return true;
        }

        private static bool PerformCalibration(
          ref VRCTrackingSteam __instance,
          Animator __0,
          bool __1,
          bool __2)
        {
            if ((UnityEngine.Object)__0 == (UnityEngine.Object)null || (UnityEngine.Object)__instance == (UnityEngine.Object)null || !Configuration.JSONConfig.TrackingSaving)
                return true;
            emmVRCLoader.Logger.LogDebug("Saving calibration info...");
            if (VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_0 != null)
                MelonCoroutines.Start(FBTSaving.SaveCalibrationInfo(__instance, VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_0.id));
            else if (VRCPlayer.field_Internal_Static_VRCPlayer_0.field_Private_VRCAvatarManager_0.field_Private_ApiAvatar_1 != null)
                MelonCoroutines.Start(FBTSaving.SaveCalibrationInfo(__instance, VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCAvatarManager_0.field_Private_ApiAvatar_1.id));
            else
                emmVRCLoader.Logger.LogError("Could not fetch avatar information for this avatar");
            return true;
        }

        private static bool SetControllerVisibility(VRCTrackingManager __instance, bool __0) => true;
    }
}
