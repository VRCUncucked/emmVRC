﻿
using System.Linq;

namespace emmVRC.Libraries
{
    public class InstanceIDUtilities
    {
        public static string GetInstanceID(string baseID) => baseID.Contains<char>('~') ? baseID.Substring(0, baseID.IndexOf('~')) : baseID;
    }
}
