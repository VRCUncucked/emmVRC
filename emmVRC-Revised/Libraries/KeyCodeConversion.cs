﻿
using UnityEngine;

namespace emmVRC.Libraries
{
    public class KeyCodeConversion
    {
        public static string Stringify(KeyCode keyCode, KeyCodeStringStyle style = KeyCodeStringStyle.Clean)
        {
            if (style == KeyCodeStringStyle.Unity)
                return keyCode.ToString();
            switch (keyCode)
            {
                case KeyCode.Alpha0:
                    return "0";
                case KeyCode.Alpha1:
                    return "1";
                case KeyCode.Alpha2:
                    return "2";
                case KeyCode.Alpha3:
                    return "3";
                case KeyCode.Alpha4:
                    return "4";
                case KeyCode.Alpha5:
                    return "5";
                case KeyCode.Alpha6:
                    return "6";
                case KeyCode.Alpha7:
                    return "7";
                case KeyCode.Alpha8:
                    return "8";
                case KeyCode.Alpha9:
                    return "9";
                case KeyCode.RightControl:
                    return "RightCTRL";
                case KeyCode.LeftControl:
                    return "LeftCTRL";
                case KeyCode.RightCommand:
                    return "RightWin";
                case KeyCode.LeftCommand:
                    return "LeftWin";
                default:
                    return keyCode.ToString();
            }
        }
    }
}
