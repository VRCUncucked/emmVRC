﻿
using emmVRC.Menus;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class KeybindChanger
    {
        private static GameObject textObject;
        private static QMNestedButton baseMenu;
        private static QMSingleButton acceptButton;
        private static QMSingleButton cancelButton;
        private static string title;
        private static KeyCode mainKey = KeyCode.None;
        private static KeyCode modifierKey1;
        private static Action<KeyCode, KeyCode> acceptAction;
        private static Action cancelAction;
        private static bool fetchingKeys = false;
        private static List<KeyCode> allowedKeyCodes = new List<KeyCode>()
    {
      KeyCode.Q,
      KeyCode.E,
      KeyCode.R,
      KeyCode.T,
      KeyCode.Y,
      KeyCode.U,
      KeyCode.I,
      KeyCode.O,
      KeyCode.P,
      KeyCode.F,
      KeyCode.G,
      KeyCode.H,
      KeyCode.J,
      KeyCode.K,
      KeyCode.L,
      KeyCode.Z,
      KeyCode.X,
      KeyCode.B,
      KeyCode.N,
      KeyCode.M,
      KeyCode.Alpha0,
      KeyCode.Alpha1,
      KeyCode.Alpha2,
      KeyCode.Alpha3,
      KeyCode.Alpha4,
      KeyCode.Alpha5,
      KeyCode.Alpha6,
      KeyCode.Alpha7,
      KeyCode.Alpha8,
      KeyCode.Alpha9,
      KeyCode.Tilde,
      KeyCode.Minus,
      KeyCode.Plus,
      KeyCode.Backslash,
      KeyCode.Slash,
      KeyCode.Period,
      KeyCode.Comma,
      KeyCode.F1,
      KeyCode.F2,
      KeyCode.F3,
      KeyCode.F4,
      KeyCode.F5,
      KeyCode.F6,
      KeyCode.F7,
      KeyCode.F8,
      KeyCode.F9,
      KeyCode.F10,
      KeyCode.F11,
      KeyCode.F12
    };
        private static List<KeyCode> allowedKeyModifiers = new List<KeyCode>()
    {
      KeyCode.LeftControl,
      KeyCode.LeftCommand,
      KeyCode.LeftAlt,
      KeyCode.LeftShift,
      KeyCode.RightControl,
      KeyCode.RightCommand,
      KeyCode.RightAlt,
      KeyCode.RightShift
    };
        public static bool InMenu = false;

        public static void Initialize()
        {
            KeybindChanger.baseMenu = new QMNestedButton(SettingsMenu.baseMenu.menuBase, 10293, 10239, "a02k3212", "");
            KeybindChanger.baseMenu.getMainButton().DestroyMe();
            KeybindChanger.acceptButton = new QMSingleButton(KeybindChanger.baseMenu, 1, 2, "Accept", (Action)(() =>
           {
               KeybindChanger.acceptAction(KeybindChanger.mainKey, KeybindChanger.modifierKey1);
               KeybindChanger.fetchingKeys = false;
               KeybindChanger.modifierKey1 = KeyCode.None;
               KeybindChanger.mainKey = KeyCode.None;
           }), "Accept this keybind");
            KeybindChanger.cancelButton = new QMSingleButton(KeybindChanger.baseMenu, 4, 2, "Cancel", (Action)(() =>
           {
               KeybindChanger.cancelAction();
               KeybindChanger.fetchingKeys = false;
               KeybindChanger.modifierKey1 = KeyCode.None;
               KeybindChanger.mainKey = KeyCode.None;
           }), "Cancel keybind setup");
            KeybindChanger.textObject = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, KeybindChanger.baseMenu.getBackButton().getGameObject().transform.parent);
            KeybindChanger.baseMenu.getBackButton().DestroyMe();
            KeybindChanger.textObject.GetComponent<Text>().fontStyle = FontStyle.Normal;
            KeybindChanger.textObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            KeybindChanger.textObject.GetComponent<Text>().text = "Press a key...";
            KeybindChanger.textObject.GetComponent<RectTransform>().anchoredPosition += new Vector2(580f, -920f);
            MelonCoroutines.Start(KeybindChanger.Loop());
        }

        public static void Show(
          string title,
          Action<KeyCode, KeyCode> acceptAction,
          Action cancelAction)
        {
            KeybindChanger.title = title;
            KeybindChanger.acceptAction = acceptAction;
            KeybindChanger.cancelAction = cancelAction;
            QuickMenuUtils.ShowQuickmenuPage(KeybindChanger.baseMenu.getMenuName());
            KeybindChanger.fetchingKeys = true;
        }

        public static IEnumerator Loop()
        {
            while (true)
            {
                if (KeybindChanger.fetchingKeys)
                {
                    bool flag = false;
                    foreach (KeyCode allowedKeyModifier in KeybindChanger.allowedKeyModifiers)
                    {
                        if (Input.GetKey(allowedKeyModifier))
                        {
                            KeybindChanger.modifierKey1 = allowedKeyModifier;
                            flag = true;
                        }
                    }
                    foreach (KeyCode allowedKeyCode in KeybindChanger.allowedKeyCodes)
                    {
                        if (Input.GetKey(allowedKeyCode))
                        {
                            KeybindChanger.mainKey = allowedKeyCode;
                            if (!flag)
                                KeybindChanger.modifierKey1 = KeyCode.None;
                        }
                    }
                    if ((UnityEngine.Object)KeybindChanger.textObject != (UnityEngine.Object)null)
                        KeybindChanger.textObject.GetComponent<Text>().text = KeybindChanger.title + "\n\n\n" + (KeybindChanger.modifierKey1 != KeyCode.None ? KeybindChanger.modifierKey1.ToString() + " + " : "") + KeybindChanger.mainKey.ToString();
                }
                yield return (object)new WaitForEndOfFrame();
            }
        }
    }
}
