﻿
using System.Runtime.InteropServices;

namespace emmVRC.Libraries
{
    internal class MediaControl
    {
        private static int KEYEVENTF_KEYUP = 2;
        private static byte mediaPlayPause = 179;
        private static byte mediaNextTrack = 176;
        private static byte mediaPreviousTrack = 177;
        private static byte mediaStop = 178;
        private static byte volUp = 175;
        private static byte volDown = 174;
        private static byte volMute = 173;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        internal static void PlayPause()
        {
            MediaControl.keybd_event(MediaControl.mediaPlayPause, MediaControl.mediaPlayPause, 0, 0);
            MediaControl.keybd_event(MediaControl.mediaPlayPause, MediaControl.mediaPlayPause, MediaControl.KEYEVENTF_KEYUP, 0);
        }

        internal static void PrevTrack()
        {
            MediaControl.keybd_event(MediaControl.mediaPreviousTrack, MediaControl.mediaPreviousTrack, 0, 0);
            MediaControl.keybd_event(MediaControl.mediaPreviousTrack, MediaControl.mediaPreviousTrack, MediaControl.KEYEVENTF_KEYUP, 0);
        }

        internal static void NextTrack()
        {
            MediaControl.keybd_event(MediaControl.mediaNextTrack, MediaControl.mediaNextTrack, 0, 0);
            MediaControl.keybd_event(MediaControl.mediaNextTrack, MediaControl.mediaNextTrack, MediaControl.KEYEVENTF_KEYUP, 0);
        }

        internal static void Stop()
        {
            MediaControl.keybd_event(MediaControl.mediaStop, MediaControl.mediaStop, 0, 0);
            MediaControl.keybd_event(MediaControl.mediaStop, MediaControl.mediaStop, MediaControl.KEYEVENTF_KEYUP, 0);
        }

        internal static void VolumeUp()
        {
            MediaControl.keybd_event(MediaControl.volUp, MediaControl.volUp, 0, 0);
            MediaControl.keybd_event(MediaControl.volUp, MediaControl.volUp, MediaControl.KEYEVENTF_KEYUP, 0);
        }

        internal static void VolumeDown()
        {
            MediaControl.keybd_event(MediaControl.volDown, MediaControl.volDown, 0, 0);
            MediaControl.keybd_event(MediaControl.volDown, MediaControl.volDown, MediaControl.KEYEVENTF_KEYUP, 0);
        }

        internal static void VolumeMute()
        {
            MediaControl.keybd_event(MediaControl.volMute, MediaControl.volMute, 0, 0);
            MediaControl.keybd_event(MediaControl.volMute, MediaControl.volMute, MediaControl.KEYEVENTF_KEYUP, 0);
        }
    }
}
