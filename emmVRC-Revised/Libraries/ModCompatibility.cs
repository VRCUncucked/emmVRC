﻿
using emmVRC.Hacks;
using emmVRC.Managers;
using emmVRC.Menus;
using emmVRCLoader;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class ModCompatibility
    {
        public static bool MultiplayerDynamicBones;
        public static bool PortalConfirmation;
        public static bool MControl;
        public static bool OGTrustRank;
        public static bool UIExpansionKit;
        public static bool FBTSaver;
        public static bool IKTweaks;
        public static bool BetterLoadingScreen;
        public static bool VRCMinus;
        public static bool CameraPlus;
        public static GameObject FlightButton;
        public static GameObject NoclipButton;
        public static GameObject SpeedButton;
        public static GameObject ESPButton;

        public static void Initialize()
        {
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "MultiplayerDynamicBones" || ((MelonBase)i).Info.Name == "MultiplayerDynamicBonesMod")) != -1)
                ModCompatibility.MultiplayerDynamicBones = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "Portal Confirmation")) != -1)
                ModCompatibility.PortalConfirmation = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "MControl")) != -1)
                ModCompatibility.MControl = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "OGTrustRanks")) != -1)
                ModCompatibility.OGTrustRank = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "UI Expansion Kit")) != -1)
                ModCompatibility.UIExpansionKit = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "FBT Saver")) != -1)
                ModCompatibility.FBTSaver = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "IKTweaks")) != -1)
                ModCompatibility.IKTweaks = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "BetterLoadingScreen")) != -1)
                ModCompatibility.BetterLoadingScreen = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "VRC-Minus")) != -1)
                ModCompatibility.VRCMinus = true;
            if (MelonHandler.Mods.FindIndex((Predicate<MelonMod>)(i => ((MelonBase)i).Info.Name == "CameraPlus")) != -1)
                ModCompatibility.CameraPlus = true;
            if (ModCompatibility.MultiplayerDynamicBones)
                emmVRCLoader.Logger.LogDebug("Detected MultiplayerDynamicBones");
            if (ModCompatibility.PortalConfirmation)
                emmVRCLoader.Logger.LogDebug("Detected PortalConfirmation");
            if (ModCompatibility.MControl)
                emmVRCLoader.Logger.LogDebug("Detected MControl");
            if (ModCompatibility.OGTrustRank)
                emmVRCLoader.Logger.LogDebug("Detected OGTrustRank");
            if (ModCompatibility.UIExpansionKit)
            {
                emmVRCLoader.Logger.LogDebug("Detected UIExpansionKit");
                ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "UI Expansion Kit"))).Assembly.GetType("UIExpansionKit.API.ExpansionKitApi").GetMethod("RegisterSimpleMenuButton", BindingFlags.Static | BindingFlags.Public).Invoke((object)null, new object[4]
                {
          (object) 0,
          (object) "Toggle\nFlight",
          (object) (Action) (() =>
          {
            PlayerTweaksMenu.FlightToggle.setToggleState(!Flight.FlightEnabled, true);
          }),
          (object) (Action<GameObject>) (obj =>
          {
            ModCompatibility.FlightButton = obj;
            obj.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
          })
                });
                ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "UI Expansion Kit"))).Assembly.GetType("UIExpansionKit.API.ExpansionKitApi").GetMethod("RegisterSimpleMenuButton", BindingFlags.Static | BindingFlags.Public).Invoke((object)null, new object[4]
                {
          (object) 0,
          (object) "Toggle\nNoclip",
          (object) (Action) (() =>
          {
            PlayerTweaksMenu.NoclipToggle.setToggleState(!Flight.NoclipEnabled, true);
          }),
          (object) (Action<GameObject>) (obj =>
          {
            ModCompatibility.NoclipButton = obj;
            obj.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
          })
                });
                ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "UI Expansion Kit"))).Assembly.GetType("UIExpansionKit.API.ExpansionKitApi").GetMethod("RegisterSimpleMenuButton", BindingFlags.Static | BindingFlags.Public).Invoke((object)null, new object[4]
                {
          (object) 0,
          (object) "Toggle\nSpeed",
          (object) (Action) (() =>
          {
            PlayerTweaksMenu.SpeedToggle.setToggleState(!Speed.SpeedModified, true);
          }),
          (object) (Action<GameObject>) (obj =>
          {
            ModCompatibility.SpeedButton = obj;
            obj.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
          })
                });
                ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "UI Expansion Kit"))).Assembly.GetType("UIExpansionKit.API.ExpansionKitApi").GetMethod("RegisterSimpleMenuButton", BindingFlags.Static | BindingFlags.Public).Invoke((object)null, new object[4]
                {
          (object) 0,
          (object) "Toggle\nESP",
          (object) (Action) (() =>
          {
            PlayerTweaksMenu.ESPToggle.setToggleState(!ESP.ESPEnabled, true);
          }),
          (object) (Action<GameObject>) (obj =>
          {
            ModCompatibility.ESPButton = obj;
            obj.SetActive(Configuration.JSONConfig.UIExpansionKitIntegration);
          })
                });
                if (Configuration.JSONConfig.UIExpansionKitColorChangingEnabled)
                    ((MelonBase)((IEnumerable<MelonMod>)MelonHandler.Mods).First<MelonMod>((Func<MelonMod, bool>)(i => ((MelonBase)i).Info.Name == "UI Expansion Kit"))).Assembly.GetType("UIExpansionKit.API.ExpansionKitApi").GetMethod("RegisterWaitConditionBeforeDecorating", BindingFlags.Static | BindingFlags.Public).Invoke((object)null, new object[1]
                    {
            (object) ModCompatibility.ColorUIExpansionKit()
                    });
            }
            if (ModCompatibility.FBTSaver)
                emmVRCLoader.Logger.LogDebug("Detected FBTSaver");
            if (ModCompatibility.IKTweaks)
                emmVRCLoader.Logger.LogDebug("Detected IKTweaks");
            if (ModCompatibility.BetterLoadingScreen)
                emmVRCLoader.Logger.LogDebug("Detected BetterLoadingScreen");
            if (ModCompatibility.VRCMinus)
                emmVRCLoader.Logger.LogDebug("Detected VRCMinus");
            if (!ModCompatibility.CameraPlus)
                return;
            emmVRCLoader.Logger.LogDebug("Detected CameraPlus");
        }

        public static IEnumerator ColorUIExpansionKit()
        {
            yield return (object)null;
            Color color = Configuration.JSONConfig.UIColorChangingEnabled ? Configuration.menuColor() : Configuration.menuColor();
            ColorBlock colorBlock = new ColorBlock()
            {
                colorMultiplier = 1f,
                disabledColor = Color.grey,
                highlightedColor = new Color(color.r * 1.5f, color.g * 1.5f, color.b * 1.5f),
                normalColor = color,
                pressedColor = Color.gray,
                fadeDuration = 0.1f
            };
            Transform transform = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ModUiPreloadedBundleContents");
            foreach (Image componentsInChild in transform.GetComponentsInChildren<Image>(true))
            {
                if (componentsInChild.transform.parent.name != "PinToggle" && componentsInChild.transform.parent.parent.name != "PinToggle")
                    componentsInChild.color = new Color(color.r * 0.5f, color.g * 0.5f, color.b * 0.5f);
            }
            foreach (Selectable componentsInChild in transform.GetComponentsInChildren<UnityEngine.UI.Button>(true))
                componentsInChild.colors = colorBlock;
            foreach (Toggle componentsInChild in transform.GetComponentsInChildren<Toggle>(true))
            {
                if (componentsInChild.gameObject.name != "PinToggle")
                    componentsInChild.colors = colorBlock;
            }
        }
    }
}
