﻿
using emmVRC.Menus;
using emmVRCLoader;
using System;
using VRC;
using VRC.Core;

namespace emmVRC.Libraries
{
    public class NetworkManagerHooking
    {
        public static void OnPlayerJoined(Player plr)
        {
            if (Configuration.JSONConfig.PlayerHistoryEnable && plr.prop_APIUser_0 != null && plr.prop_APIUser_0.id != APIUser.CurrentUser.id)
                PlayerHistoryMenu.currentPlayers.Add(new InstancePlayer()
                {
                    Name = plr.prop_APIUser_0.GetName(),
                    UserID = plr.prop_APIUser_0.id,
                    TimeJoinedStamp = DateTime.Now.ToShortTimeString()
                });
            if (!Configuration.JSONConfig.LogPlayerJoin)
                return;
            emmVRCLoader.Logger.Log("Player joined: " + plr.prop_APIUser_0.GetName());
        }

        public static void OnPlayerLeft(Player plr)
        {
            int num = Configuration.JSONConfig.PlayerHistoryEnable ? 1 : 0;
            if (!Configuration.JSONConfig.LogPlayerJoin)
                return;
            emmVRCLoader.Logger.Log("Player left: " + plr.prop_APIUser_0.GetName());
        }
    }
}
