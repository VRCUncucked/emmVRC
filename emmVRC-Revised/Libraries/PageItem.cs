﻿
using System;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class PageItem
    {
        public string Name = "";
        public Action Action;
        public string Tooltip = "";
        public Sprite buttonSprite;
        public bool Active = true;
        public string onName = "";
        public string offName = "";
        public Action OnAction;
        public Action OffAction;
        public bool ToggleState = true;
        public PageItems type = PageItems.Button;

        public PageItem(string name, Action action, string tooltip, bool active = true)
        {
            this.Name = name;
            this.Action = action;
            this.Tooltip = tooltip;
            this.Active = active;
            this.type = PageItems.Button;
        }

        public PageItem(
          string onName,
          Action onAction,
          string offName,
          Action offAction,
          string tooltip,
          bool active = true,
          bool defaultState = true)
        {
            this.onName = onName;
            this.offName = offName;
            this.OnAction = onAction;
            this.OffAction = offAction;
            this.Tooltip = tooltip;
            this.Active = active;
            this.ToggleState = defaultState;
            this.type = PageItems.Toggle;
        }

        public static PageItem Space => new PageItem("", (Action)null, "", false);

        public void SetToggleState(bool newBool, bool triggerAction = false)
        {
            if (this.type == PageItems.Button)
                return;
            this.ToggleState = newBool;
            if (!triggerAction)
                return;
            this.ButtonAction();
        }

        public void ButtonAction()
        {
            this.SetToggleState(!this.ToggleState);
            if (this.ToggleState)
            {
                this.OnAction();
            }
            else
            {
                if (this.ToggleState)
                    return;
                this.OffAction();
            }
        }
    }
}
