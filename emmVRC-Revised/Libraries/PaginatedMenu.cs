﻿
using emmVRCLoader;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class PaginatedMenu
    {
        public string menuName;
        public QMSingleButton menuEntryButton;
        public int currentPage;
        public List<string> pageTitles = new List<string>();
        public List<PageItem> pageItems = new List<PageItem>();
        public QMNestedButton menuBase;
        private QMSingleButton previousPageButton;
        private QMSingleButton pageCount;
        private QMSingleButton nextPageButton;
        private QMSingleButton templateButton;
        private List<QMSingleButton> items = new List<QMSingleButton>();
        private List<QMToggleButton> toggleItems = new List<QMToggleButton>();
        private GameObject menuTitle;

        public PaginatedMenu(
          string parentPath,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Color? buttonColor)
        {
            emmVRCLoader.Logger.LogDebug("Initializing paginated menu...");
            this.menuBase = new QMNestedButton(parentPath, x, y, menuName, "");
            this.menuBase.getMainButton().DestroyMe();
            this.menuEntryButton = new QMSingleButton(parentPath, x, y, menuName, new System.Action(this.OpenMenu), menuTooltip, buttonColor);
            this.previousPageButton = new QMSingleButton(this.menuBase, 4, 0, "", (System.Action)(() =>
           {
               if (this.currentPage != 0)
                   --this.currentPage;
               this.UpdateMenu();
           }), "Move to the previous page", buttonColor);
            this.menuTitle = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, this.menuBase.getBackButton().getGameObject().transform.parent);
            this.menuTitle.GetComponent<Text>().fontStyle = FontStyle.Normal;
            this.menuTitle.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            this.menuTitle.GetComponent<Text>().text = "";
            this.menuTitle.GetComponent<RectTransform>().anchoredPosition += new Vector2(580f, -1700f);
            this.previousPageButton.getGameObject().GetComponent<Image>().sprite = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("EmojiMenu/PageUp").GetComponent<Image>().sprite;
            this.pageCount = new QMSingleButton(this.menuBase, 4, 1, "Page\n0/0", (System.Action)null, "Indicates the page you are on");
            UnityEngine.Object.DestroyObject((UnityEngine.Object)this.pageCount.getGameObject().GetComponentInChildren<ButtonReaction>());
            UnityEngine.Object.DestroyObject((UnityEngine.Object)this.pageCount.getGameObject().GetComponentInChildren<UiTooltip>());
            UnityEngine.Object.DestroyObject((UnityEngine.Object)this.pageCount.getGameObject().GetComponentInChildren<Image>());
            this.nextPageButton = new QMSingleButton(this.menuBase, 4, 2, "", (System.Action)(() =>
           {
               if (this.pageItems.Count > 9)
                   ++this.currentPage;
               this.UpdateMenu();
           }), "Move to the next page", buttonColor);
            this.nextPageButton.getGameObject().GetComponent<Image>().sprite = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("EmojiMenu/PageDown").GetComponent<Image>().sprite;
            for (int btnYLocation = 0; btnYLocation < 3; ++btnYLocation)
            {
                for (int index = 0; index < 3; ++index)
                {
                    this.items.Add(new QMSingleButton(this.menuBase, index + 1, btnYLocation, "", (System.Action)null, "", buttonColor));
                    this.toggleItems.Add(new QMToggleButton(this.menuBase, index + 1, btnYLocation, "", (System.Action)null, "", (System.Action)null, "", buttonColor));
                }
            }
            this.templateButton = new QMSingleButton(this.menuBase, 1923, 1023, "", (System.Action)null, "");
            this.templateButton.setActive(false);
        }

        public PaginatedMenu(
          QMNestedButton menuButton,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Color? buttonColor)
          : this(menuButton.getMenuName(), x, y, menuName, menuTooltip, buttonColor)
        {
        }

        public void OpenMenu()
        {
            this.currentPage = 0;
            this.UpdateMenu();
            QuickMenuUtils.ShowQuickmenuPage(this.menuBase.getMenuName());
        }

        public void UpdateMenu()
        {
            this.pageCount.setActive(false);
            foreach (QMButtonBase qmButtonBase in this.items)
                qmButtonBase.setActive(false);
            foreach (QMButtonBase toggleItem in this.toggleItems)
                toggleItem.setActive(false);
            int num1 = (int)Math.Ceiling((double)this.pageItems.Count / 9.0) - 1;
            if (this.currentPage < 0)
                this.currentPage = 0;
            if (this.currentPage > num1)
                this.currentPage = num1;
            if (this.pageItems.Count > 9)
            {
                this.pageCount.setActive(true);
                QMSingleButton pageCount = this.pageCount;
                int num2 = this.currentPage + 1;
                string str1 = num2.ToString();
                num2 = (int)Math.Ceiling((double)this.pageItems.Count / 9.0);
                string str2 = num2.ToString();
                string buttonText = "Page\n" + str1 + " of " + str2;
                pageCount.setButtonText(buttonText);
            }
            List<PageItem> range = this.pageItems.GetRange(this.currentPage * 9, Math.Abs(this.currentPage * 9 - this.pageItems.Count));
            if (range == null)
            {
                emmVRCLoader.Logger.LogError("The page list of items is null. This is a problem.");
            }
            else
            {
                if (range.Count <= 0)
                    return;
                for (int index = 0; index < (range.Count > 9 ? 9 : range.Count); ++index)
                {
                    if (range[index].type == PageItems.Button)
                    {
                        this.items[index].setButtonText(range[index].Name);
                        if ((UnityEngine.Object)range[index].buttonSprite != (UnityEngine.Object)null)
                        {
                            this.items[index].getGameObject().GetComponent<Image>().sprite = range[index].buttonSprite;
                            this.items[index].getGameObject().GetComponent<UnityEngine.UI.Button>().colors = new ColorBlock()
                            {
                                normalColor = new Color(1f, 1f, 1f, 1f),
                                disabledColor = new Color(0.75f, 0.75f, 0.75f, 1f),
                                highlightedColor = new Color(0.975f, 0.975f, 0.975f, 1f),
                                pressedColor = new Color(0.75f, 0.75f, 0.75f, 1f),
                                colorMultiplier = 1f,
                                fadeDuration = 0.1f
                            };
                        }
                        else
                            this.items[index].getGameObject().GetComponent<Image>().sprite = this.templateButton.getGameObject().GetComponent<Image>().sprite;
                        this.items[index].setAction(range[index].Action);
                        this.items[index].setToolTip(range[index].Tooltip);
                        this.items[index].setActive(range[index].Active);
                    }
                    else
                    {
                        this.toggleItems[index].setOnText(range[index].onName);
                        this.toggleItems[index].setOffText(range[index].offName);
                        this.toggleItems[index].setAction(new System.Action(range[index].ButtonAction), new System.Action(range[index].ButtonAction));
                        this.toggleItems[index].setToggleState(range[index].ToggleState);
                        this.toggleItems[index].setToolTip(range[index].Tooltip);
                        this.toggleItems[index].setActive(range[index].Active);
                    }
                    if ((this.currentPage + 1 > this.pageTitles.Count || this.pageTitles.Count <= 0) && (UnityEngine.Object)this.menuTitle != (UnityEngine.Object)null)
                        this.menuTitle.GetComponent<Text>().text = "";
                    else if ((UnityEngine.Object)this.menuTitle != (UnityEngine.Object)null)
                        this.menuTitle.GetComponent<Text>().text = this.pageTitles[this.currentPage];
                }
            }
        }
    }
}
