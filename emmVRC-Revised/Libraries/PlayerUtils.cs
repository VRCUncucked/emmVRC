﻿
using System;
using VRC;

namespace emmVRC.Libraries
{
    public class PlayerUtils
    {
        private static Action<Player> requestedAction;

        public static void GetEachPlayer(Action<Player> act)
        {
            PlayerUtils.requestedAction = act;
            foreach (Player player in PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0)
                PlayerUtils.requestedAction(player);
        }
    }
}
