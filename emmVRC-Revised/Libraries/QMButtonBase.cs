﻿
using System;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class QMButtonBase
    {
        protected GameObject button;
        protected string btnQMLoc;
        protected string btnType;
        protected string btnTag;
        protected int[] initShift = new int[2];
        protected Color OrigBackground;
        protected Color OrigText;

        public GameObject getGameObject() => this.button;

        public void setActive(bool isActive) => this.button.gameObject.SetActive(isActive);

        public void setIntractable(bool isIntractable)
        {
            if (isIntractable)
            {
                this.setBackgroundColor(this.OrigBackground, false);
                this.setTextColor(this.OrigText, false);
            }
            else
            {
                this.setBackgroundColor(new Color(0.5f, 0.5f, 0.5f, 1f), false);
                this.setTextColor(new Color(0.7f, 0.7f, 0.7f, 1f), false);
            }
            this.button.gameObject.GetComponent<UnityEngine.UI.Button>().interactable = isIntractable;
        }

        public void setLocation(int buttonXLoc, int buttonYLoc)
        {
            this.button.GetComponent<RectTransform>().anchoredPosition += Vector2.right * (float)(420 * (buttonXLoc + this.initShift[0]));
            this.button.GetComponent<RectTransform>().anchoredPosition += Vector2.down * (float)(420 * (buttonYLoc + this.initShift[1]));
            this.btnTag = Guid.NewGuid().ToString();
            this.button.name = this.btnQMLoc + "/" + this.btnType + "_" + this.btnTag;
            this.button.GetComponent<UnityEngine.UI.Button>().name = this.btnType + this.btnTag;
        }

        public void setToolTip(string buttonToolTip)
        {
            this.button.GetComponent<UiTooltip>().field_Public_String_0 = buttonToolTip;
            this.button.GetComponent<UiTooltip>().field_Public_String_1 = buttonToolTip;
        }

        public void DestroyMe()
        {
            try
            {
                UnityEngine.Object.Destroy((UnityEngine.Object)this.button);
            }
            catch
            {
            }
        }

        public virtual void setBackgroundColor(Color buttonBackgroundColor, bool save = true)
        {
        }

        public virtual void setTextColor(Color buttonTextColor, bool save = true)
        {
        }
    }
}
