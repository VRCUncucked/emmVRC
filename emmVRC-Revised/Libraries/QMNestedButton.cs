﻿
using System;
using UnhollowerBaseLib;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class QMNestedButton
    {
        protected QMSingleButton mainButton;
        protected QMSingleButton backButton;
        protected string menuName;
        protected string btnQMLoc;
        protected string btnType;

        public QMNestedButton(
          QMNestedButton btnMenu,
          int btnXLocation,
          int btnYLocation,
          string btnText,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null,
          Color? backbtnBackgroundColor = null,
          Color? backbtnTextColor = null)
        {
            this.btnQMLoc = btnMenu.getMenuName();
            this.initButton(btnXLocation, btnYLocation, btnText, btnToolTip, btnBackgroundColor, btnTextColor, backbtnBackgroundColor, backbtnTextColor);
        }

        public QMNestedButton(
          string btnMenu,
          int btnXLocation,
          int btnYLocation,
          string btnText,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null,
          Color? backbtnBackgroundColor = null,
          Color? backbtnTextColor = null)
        {
            this.btnQMLoc = btnMenu;
            this.initButton(btnXLocation, btnYLocation, btnText, btnToolTip, btnBackgroundColor, btnTextColor, backbtnBackgroundColor, backbtnTextColor);
        }

        public void initButton(
          int btnXLocation,
          int btnYLocation,
          string btnText,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null,
          Color? backbtnBackgroundColor = null,
          Color? backbtnTextColor = null)
        {
            this.btnType = "NestedButton";
            Transform transform1 = UnityEngine.Object.Instantiate<Transform>(QuickMenuUtils.NestedMenuTemplate(), ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform);
            this.menuName = "emmVRC_Menu_" + Guid.NewGuid().ToString();
            transform1.name = this.menuName;
            this.mainButton = new QMSingleButton(this.btnQMLoc, btnXLocation, btnYLocation, btnText, (Action)(() => QuickMenuUtils.ShowQuickmenuPage(this.menuName)), btnToolTip, btnBackgroundColor, btnTextColor);
            foreach (Il2CppObjectBase il2CppObjectBase in transform1.transform)
            {
                Transform transform2 = il2CppObjectBase.Cast<Transform>();
                if ((UnityEngine.Object)transform2 != (UnityEngine.Object)null)
                    UnityEngine.Object.Destroy((UnityEngine.Object)transform2.gameObject);
            }
            if (!backbtnTextColor.HasValue)
                backbtnTextColor = new Color?(Color.yellow);
            QMButtonAPI.allNestedButtons.Add(this);
            this.backButton = new QMSingleButton(this, 5, 2, "Back", (Action)(() => QuickMenuUtils.ShowQuickmenuPage(this.btnQMLoc)), "Go Back", backbtnBackgroundColor, backbtnTextColor);
        }

        public string getMenuName() => this.menuName;

        public QMSingleButton getMainButton() => this.mainButton;

        public QMSingleButton getBackButton() => this.backButton;

        public void DestroyMe()
        {
            this.mainButton.DestroyMe();
            this.backButton.DestroyMe();
        }
    }
}
