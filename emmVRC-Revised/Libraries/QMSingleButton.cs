﻿
using System;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class QMSingleButton : QMButtonBase
    {
        public QMSingleButton(
          QMNestedButton btnMenu,
          int btnXLocation,
          int btnYLocation,
          string btnText,
          Action btnAction,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null)
        {
            this.btnQMLoc = btnMenu.getMenuName();
            this.initButton(btnXLocation, btnYLocation, btnText, btnAction, btnToolTip, btnBackgroundColor, btnTextColor);
        }

        public QMSingleButton(
          string btnMenu,
          int btnXLocation,
          int btnYLocation,
          string btnText,
          Action btnAction,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null)
        {
            this.btnQMLoc = btnMenu;
            this.initButton(btnXLocation, btnYLocation, btnText, btnAction, btnToolTip, btnBackgroundColor, btnTextColor);
        }

        private void initButton(
          int btnXLocation,
          int btnYLocation,
          string btnText,
          Action btnAction,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null)
        {
            this.btnType = "SingleButton";
            this.button = UnityEngine.Object.Instantiate<GameObject>(QuickMenuUtils.SingleButtonTemplate(), ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find(this.btnQMLoc), true);
            this.initShift[0] = -1;
            this.initShift[1] = 0;
            this.setLocation(btnXLocation, btnYLocation);
            this.setButtonText(btnText);
            this.setToolTip(btnToolTip);
            this.setAction(btnAction);
            if (btnBackgroundColor.HasValue)
                this.setBackgroundColor(btnBackgroundColor.Value, true);
            else
                this.OrigBackground = this.button.GetComponentInChildren<Image>().color;
            if (btnTextColor.HasValue)
                this.setTextColor(btnTextColor.Value, true);
            else
                this.OrigText = this.button.GetComponentInChildren<Text>().color;
            this.setActive(true);
            QMButtonAPI.allSingleButtons.Add(this);
        }

        public void setButtonText(string buttonText) => this.button.GetComponentInChildren<Text>().text = buttonText;

        public void setAction(Action buttonAction)
        {
            this.button.GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            if (buttonAction == null)
                return;
            this.button.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(DelegateSupport.ConvertDelegate<UnityAction>((Delegate)buttonAction));
        }

        public override void setBackgroundColor(Color buttonBackgroundColor, bool save = true)
        {
            if (save)
                this.OrigBackground = buttonBackgroundColor;
            this.button.GetComponentInChildren<UnityEngine.UI.Button>().colors = new ColorBlock()
            {
                colorMultiplier = 1f,
                disabledColor = Color.grey,
                highlightedColor = buttonBackgroundColor * 1.5f,
                normalColor = buttonBackgroundColor / 1.5f,
                pressedColor = Color.grey * 1.5f
            };
        }

        public override void setTextColor(Color buttonTextColor, bool save = true)
        {
            this.button.GetComponentInChildren<Text>().color = buttonTextColor;
            if (!save)
                return;
            this.OrigText = buttonTextColor;
        }
    }
}
