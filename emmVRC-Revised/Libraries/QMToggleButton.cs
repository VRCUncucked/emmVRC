﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class QMToggleButton : QMButtonBase
    {
        public GameObject btnOn;
        public GameObject btnOff;
        public List<QMButtonBase> showWhenOn = new List<QMButtonBase>();
        public List<QMButtonBase> hideWhenOn = new List<QMButtonBase>();
        public bool shouldSaveInConfig;
        private Action btnOnAction;
        private Action btnOffAction;

        public QMToggleButton(
          QMNestedButton btnMenu,
          int btnXLocation,
          int btnYLocation,
          string btnTextOn,
          Action btnActionOn,
          string btnTextOff,
          Action btnActionOff,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null,
          bool shouldSaveInConfig = false,
          bool defaultPosition = false)
        {
            this.btnQMLoc = btnMenu.getMenuName();
            this.initButton(btnXLocation, btnYLocation, btnTextOn, btnActionOn, btnTextOff, btnActionOff, btnToolTip, btnBackgroundColor, btnTextColor, shouldSaveInConfig, defaultPosition);
        }

        public QMToggleButton(
          string btnMenu,
          int btnXLocation,
          int btnYLocation,
          string btnTextOn,
          Action btnActionOn,
          string btnTextOff,
          Action btnActionOff,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null,
          bool shouldSaveInConfig = false,
          bool defaultPosition = false)
        {
            this.btnQMLoc = btnMenu;
            this.initButton(btnXLocation, btnYLocation, btnTextOn, btnActionOn, btnTextOff, btnActionOff, btnToolTip, btnBackgroundColor, btnTextColor, shouldSaveInConfig, defaultPosition);
        }

        private void initButton(
          int btnXLocation,
          int btnYLocation,
          string btnTextOn,
          Action btnActionOn,
          string btnTextOff,
          Action btnActionOff,
          string btnToolTip,
          Color? btnBackgroundColor = null,
          Color? btnTextColor = null,
          bool shouldSaveInConf = false,
          bool defaultPosition = false)
        {
            this.btnType = "ToggleButton";
            this.button = UnityEngine.Object.Instantiate<GameObject>(QuickMenuUtils.ToggleButtonTemplate(), ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find(this.btnQMLoc), true);
            this.btnOn = this.button.transform.Find("Toggle_States_Visible/ON").gameObject;
            this.btnOff = this.button.transform.Find("Toggle_States_Visible/OFF").gameObject;
            this.initShift[0] = -3;
            this.initShift[1] = -1;
            this.setLocation(btnXLocation, btnYLocation);
            this.setOnText(btnTextOn);
            this.setOffText(btnTextOff);
            Text[] componentsInChildren1 = (Text[])this.btnOn.GetComponentsInChildren<Text>();
            componentsInChildren1[0].name = "Text_ON";
            componentsInChildren1[0].resizeTextForBestFit = true;
            componentsInChildren1[1].name = "Text_OFF";
            componentsInChildren1[1].resizeTextForBestFit = true;
            Text[] componentsInChildren2 = (Text[])this.btnOff.GetComponentsInChildren<Text>();
            componentsInChildren2[0].name = "Text_ON";
            componentsInChildren2[0].resizeTextForBestFit = true;
            componentsInChildren2[1].name = "Text_OFF";
            componentsInChildren2[1].resizeTextForBestFit = true;
            this.setToolTip(btnToolTip);
            this.setAction(btnActionOn, btnActionOff);
            this.btnOn.SetActive(false);
            this.btnOff.SetActive(true);
            if (btnBackgroundColor.HasValue)
                this.setBackgroundColor(btnBackgroundColor.Value, true);
            else
                this.OrigBackground = this.btnOn.GetComponentsInChildren<Text>().First<Text>().color;
            if (btnTextColor.HasValue)
                this.setTextColor(btnTextColor.Value, true);
            else
                this.OrigText = this.btnOn.GetComponentsInChildren<Image>().First<Image>().color;
            this.setActive(true);
            this.shouldSaveInConfig = shouldSaveInConf;
            if (defaultPosition)
                this.setToggleState(true);
            QMButtonAPI.allToggleButtons.Add(this);
        }

        public override void setBackgroundColor(Color buttonBackgroundColor, bool save = true)
        {
            foreach (Graphic graphic in ((IEnumerable<Image>)this.btnOn.GetComponentsInChildren<Image>().Concat<Image>((IEnumerable<Image>)this.btnOff.GetComponentsInChildren<Image>()).ToArray<Image>()).Concat<Image>((IEnumerable<Image>)this.button.GetComponentsInChildren<Image>()).ToArray<Image>())
                graphic.color = buttonBackgroundColor;
            if (!save)
                return;
            this.OrigBackground = buttonBackgroundColor;
        }

        public override void setTextColor(Color buttonTextColor, bool save = true)
        {
            foreach (Graphic graphic in this.btnOn.GetComponentsInChildren<Text>().Concat<Text>((IEnumerable<Text>)this.btnOff.GetComponentsInChildren<Text>()).ToArray<Text>())
                graphic.color = buttonTextColor;
            if (!save)
                return;
            this.OrigText = buttonTextColor;
        }

        public void setAction(Action buttonOnAction, Action buttonOffAction)
        {
            this.btnOnAction = buttonOnAction;
            this.btnOffAction = buttonOffAction;
            this.button.GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            this.button.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(DelegateSupport.ConvertDelegate<UnityAction>((Delegate)(() =>
           {
               if (this.btnOn.activeSelf)
                   this.setToggleState(false, true);
               else
                   this.setToggleState(true, true);
           })));
        }

        public void setToggleState(bool toggleOn, bool shouldInvoke = false)
        {
            this.btnOn.SetActive(toggleOn);
            this.btnOff.SetActive(!toggleOn);
            try
            {
                if (toggleOn & shouldInvoke)
                {
                    this.btnOnAction();
                    this.showWhenOn.ForEach((Action<QMButtonBase>)(x => x.setActive(true)));
                    this.hideWhenOn.ForEach((Action<QMButtonBase>)(x => x.setActive(false)));
                }
                else if (!toggleOn & shouldInvoke)
                {
                    this.btnOffAction();
                    this.showWhenOn.ForEach((Action<QMButtonBase>)(x => x.setActive(false)));
                    this.hideWhenOn.ForEach((Action<QMButtonBase>)(x => x.setActive(true)));
                }
            }
            catch
            {
            }
            int num = this.shouldSaveInConfig ? 1 : 0;
        }

        public string getOnText() => this.btnOn.GetComponentsInChildren<Text>()[0].text;

        public void setOnText(string buttonOnText)
        {
            Text[] componentsInChildren1 = (Text[])this.btnOn.GetComponentsInChildren<Text>();
            componentsInChildren1[0].text = buttonOnText;
            componentsInChildren1[0].supportRichText = true;
            Text[] componentsInChildren2 = (Text[])this.btnOff.GetComponentsInChildren<Text>();
            componentsInChildren2[0].text = buttonOnText;
            componentsInChildren2[0].supportRichText = true;
        }

        public void setOffText(string buttonOffText)
        {
            Text[] componentsInChildren1 = (Text[])this.btnOn.GetComponentsInChildren<Text>();
            componentsInChildren1[1].text = buttonOffText;
            componentsInChildren1[1].supportRichText = true;
            Text[] componentsInChildren2 = (Text[])this.btnOff.GetComponentsInChildren<Text>();
            componentsInChildren2[1].text = buttonOffText;
            componentsInChildren2[1].supportRichText = true;
        }
    }
}
