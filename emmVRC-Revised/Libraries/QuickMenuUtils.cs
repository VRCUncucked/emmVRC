﻿
using emmVRCLoader;
using Il2CppSystem.Reflection;
using System;
using System.Linq;
using UnhollowerRuntimeLib;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class QuickMenuUtils
    {
        private static BoxCollider QuickMenuBackgroundReference;
        private static GameObject SingleButtonReference;
        private static GameObject ToggleButtonReference;
        private static Transform NestedButtonReference;
        private static Vector3 QuickMenuColliderSizeNormal = Vector3.zero;
        private static Vector3 QuickMenuColliderPositionNormal = Vector3.zero;
        public static FieldInfo currentPageGetter;
        private static GameObject shortcutMenu;
        private static GameObject userInteractMenu;

        public static BoxCollider QuickMenuBackground()
        {
            if ((UnityEngine.Object)QuickMenuUtils.QuickMenuBackgroundReference == (UnityEngine.Object)null)
                QuickMenuUtils.QuickMenuBackgroundReference = ((Component)QuickMenuUtils.GetQuickMenuInstance()).GetComponent<BoxCollider>();
            return QuickMenuUtils.QuickMenuBackgroundReference;
        }

        public static void ResizeQuickMenuCollider()
        {
            if (QuickMenuUtils.QuickMenuColliderPositionNormal == Vector3.zero && QuickMenuUtils.QuickMenuColliderSizeNormal == Vector3.zero)
            {
                QuickMenuUtils.QuickMenuColliderSizeNormal = QuickMenuUtils.QuickMenuBackground().size;
                QuickMenuUtils.QuickMenuColliderPositionNormal = QuickMenuUtils.QuickMenuBackground().center;
                QuickMenuUtils.QuickMenuBackground().size = new Vector3(QuickMenuUtils.QuickMenuColliderSizeNormal.x, QuickMenuUtils.QuickMenuColliderSizeNormal.y + QuickMenuUtils.QuickMenuColliderSizeNormal.y / 4f, QuickMenuUtils.QuickMenuColliderSizeNormal.z);
                QuickMenuUtils.QuickMenuBackground().center = new Vector3(QuickMenuUtils.QuickMenuColliderPositionNormal.x, QuickMenuUtils.QuickMenuColliderPositionNormal.y + QuickMenuUtils.QuickMenuColliderPositionNormal.y / 8f, QuickMenuUtils.QuickMenuColliderPositionNormal.z);
            }
            else
            {
                QuickMenuUtils.QuickMenuBackground().size = QuickMenuUtils.QuickMenuColliderSizeNormal;
                QuickMenuUtils.QuickMenuBackground().center = QuickMenuUtils.QuickMenuColliderPositionNormal;
                QuickMenuUtils.QuickMenuColliderSizeNormal = Vector3.zero;
                QuickMenuUtils.QuickMenuColliderPositionNormal = Vector3.zero;
            }
        }

        public static GameObject SingleButtonTemplate()
        {
            if ((UnityEngine.Object)QuickMenuUtils.SingleButtonReference == (UnityEngine.Object)null)
                QuickMenuUtils.SingleButtonReference = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/WorldsButton").gameObject;
            return QuickMenuUtils.SingleButtonReference;
        }

        public static GameObject ToggleButtonTemplate()
        {
            if ((UnityEngine.Object)QuickMenuUtils.ToggleButtonReference == (UnityEngine.Object)null)
                QuickMenuUtils.ToggleButtonReference = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("UserInteractMenu/BlockButton").gameObject;
            return QuickMenuUtils.ToggleButtonReference;
        }

        public static Transform NestedMenuTemplate()
        {
            if ((UnityEngine.Object)QuickMenuUtils.NestedButtonReference == (UnityEngine.Object)null)
                QuickMenuUtils.NestedButtonReference = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("CameraMenu");
            return QuickMenuUtils.NestedButtonReference;
        }

        public static QuickMenu GetQuickMenuInstance() => QuickMenu.prop_QuickMenu_0;

        public static VRCUiManager GetVRCUiMInstance() => VRCUiManager.prop_VRCUiManager_0;

        public static void ShowQuickmenuPage(string pagename)
        {
            QuickMenu quickMenuInstance = QuickMenuUtils.GetQuickMenuInstance();
            Transform transform = ((Component)quickMenuInstance)?.transform.Find(pagename);
            if ((UnityEngine.Object)transform == (UnityEngine.Object)null)
                emmVRCLoader.Logger.LogError("pageTransform is null!");
            if ((UnityEngine.Object)QuickMenuUtils.shortcutMenu == (UnityEngine.Object)null)
                QuickMenuUtils.shortcutMenu = ((Component)QuickMenu.prop_QuickMenu_0).transform.Find("ShortcutMenu")?.gameObject;
            if ((UnityEngine.Object)QuickMenuUtils.userInteractMenu == (UnityEngine.Object)null)
                QuickMenuUtils.userInteractMenu = ((Component)QuickMenu.prop_QuickMenu_0).transform.Find("UserInteractMenu")?.gameObject;
            if (QuickMenuUtils.currentPageGetter == (FieldInfo)null)
            {
                GameObject gameObject1 = ((Component)quickMenuInstance).transform.Find("ShortcutMenu").gameObject;
                GameObject gameObject2 = ((Component)quickMenuInstance).transform.Find("UserInteractMenu").gameObject;
                FieldInfo[] array = Il2CppType.Of<QuickMenu>().GetFields(BindingFlags.Instance | BindingFlags.NonPublic).Where<FieldInfo>((Func<FieldInfo, bool>)(fi => fi.FieldType == Il2CppType.Of<GameObject>())).ToArray<FieldInfo>();
                int num = 0;
                bool flag = true;
                foreach (FieldInfo fieldInfo in array)
                {
                    GameObject gameObject3 = fieldInfo.GetValue((Il2CppSystem.Object)quickMenuInstance)?.TryCast<GameObject>();
                    if (flag)
                    {
                        if (QuickMenu.prop_QuickMenu_0.field_Private_EnumNPublicSealedvaUnShEmUsEmNoCaMo_nUnique_0 != QuickMenu.EnumNPublicSealedvaUnShEmUsEmNoCaMo_nUnique.UserInteractMenu)
                        {
                            if ((UnityEngine.Object)gameObject3 == (UnityEngine.Object)gameObject1 && ++num == 3)
                            {
                                emmVRCLoader.Logger.LogDebug("currentPage field: " + fieldInfo.Name);
                                QuickMenuUtils.currentPageGetter = fieldInfo;
                                flag = false;
                            }
                        }
                        else if ((UnityEngine.Object)gameObject3 == (UnityEngine.Object)gameObject2 && ++num == 2)
                        {
                            emmVRCLoader.Logger.LogDebug("currentPage field: " + fieldInfo.Name);
                            QuickMenuUtils.currentPageGetter = fieldInfo;
                            flag = false;
                        }
                    }
                }
                if (QuickMenuUtils.currentPageGetter == (FieldInfo)null)
                {
                    emmVRCLoader.Logger.LogError("Unable to find field \"currentPage\" in QuickMenu");
                    return;
                }
            }
            QuickMenuUtils.currentPageGetter.GetValue((Il2CppSystem.Object)quickMenuInstance)?.Cast<GameObject>().SetActive(false);
            ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar").gameObject.SetActive(pagename == "ShortcutMenu");
            GameObject gameObject = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickModeTabs").gameObject;
            QuickMenuUtils.GetQuickMenuInstance().field_Private_QuickMenuContextualDisplay_0.Method_Public_Void_EnumNPublicSealedvaUnNoToUs7vUsNoUnique_0(QuickMenuContextualDisplay.EnumNPublicSealedvaUnNoToUs7vUsNoUnique.NoSelection);
            transform.gameObject.SetActive(true);
            QuickMenuUtils.currentPageGetter.SetValue((Il2CppSystem.Object)quickMenuInstance, (Il2CppSystem.Object)transform.gameObject);
            if (pagename == "ShortcutMenu")
            {
                QuickMenuUtils.SetIndex(0);
                gameObject.SetActive(true);
            }
            else if (pagename == "UserInteractMenu")
            {
                QuickMenuUtils.SetIndex(3);
            }
            else
            {
                QuickMenuUtils.SetIndex(-1);
                QuickMenuUtils.shortcutMenu?.SetActive(false);
                QuickMenuUtils.userInteractMenu?.SetActive(false);
                gameObject.SetActive(false);
            }
        }

        public static void SetIndex(int index)
        {
            emmVRCLoader.Logger.LogDebug("Menu index enum is set to " + ((QuickMenu.EnumNPublicSealedvaUnShEmUsEmNoCaMo_nUnique)index).ToString() + " (int value " + index.ToString() + ")");
            QuickMenuUtils.GetQuickMenuInstance().field_Private_EnumNPublicSealedvaUnShEmUsEmNoCaMo_nUnique_0 = (QuickMenu.EnumNPublicSealedvaUnShEmUsEmNoCaMo_nUnique)index;
        }
    }
}
