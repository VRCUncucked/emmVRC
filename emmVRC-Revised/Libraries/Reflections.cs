﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TMPro;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using VRC;
using VRC.Animation;
using VRC.Core;

namespace emmVRC.Libraries
{
    public static class Reflections
    {
        private static Reflections.ResetLastPositionAction ourResetLastPositionAction;
        private static Reflections.ResetAction ourResetAction;
        private static Reflections.ReloadAvatarAction ourReloadAvatarAction;
        private static MethodInfo reloadAvatarsMethod;
        private static Reflections.TriggerEmoteAction ourTriggerEmoteAction;
        private static Reflections.ApplyParametersAction ourApplyParametersAction;
        private static MethodInfo ourGetPlayerHeightMethod;
        private static MethodInfo ourSetPlayerHeightMethod;
        private static MethodInfo ourSetControllerVisibilityMethod;
        private static MethodInfo ourSetQuickMenuContextMethod;
        private static MethodInfo ourSelectPlayerMethod;
        private static MethodInfo ourViewUserInfoMethod;
        private static MethodInfo enterWorldMethod;
        private static System.Type transitionInfoEnum;

        public static Reflections.ResetLastPositionAction ResetLastPositionAct
        {
            get
            {
                if (Reflections.ourResetLastPositionAction != null)
                    return Reflections.ourResetLastPositionAction;
                Reflections.ourResetLastPositionAction = (Reflections.ResetLastPositionAction)Delegate.CreateDelegate(typeof(Reflections.ResetLastPositionAction), ((IEnumerable<MethodInfo>)typeof(InputStateController).GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Method && jt.TryResolve() != (MethodBase)null && jt.TryResolve().Name == "get_transform")))));
                return Reflections.ourResetLastPositionAction;
            }
        }

        public static void ResetLastPosition(this InputStateController instance) => Reflections.ResetLastPositionAct(instance);

        public static Reflections.ResetAction ResetAct
        {
            get
            {
                if (Reflections.ourResetAction != null)
                    return Reflections.ourResetAction;
                Reflections.ourResetAction = (Reflections.ResetAction)Delegate.CreateDelegate(typeof(Reflections.ResetAction), ((IEnumerable<MethodInfo>)typeof(VRCMotionState).GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => XrefScanner.XrefScan((MethodBase)it).Count<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Method && jt.TryResolve() != (MethodBase)null && jt.TryResolve().ReflectedType == typeof(Vector3))) == 4)));
                return Reflections.ourResetAction;
            }
        }

        public static void Reset(this VRCMotionState instance, bool something = false) => Reflections.ResetAct(instance);

        public static Reflections.ReloadAvatarAction ReloadAvatarAct
        {
            get
            {
                if (Reflections.ourReloadAvatarAction != null)
                    return Reflections.ourReloadAvatarAction;
                Reflections.ourReloadAvatarAction = (Reflections.ReloadAvatarAction)Delegate.CreateDelegate(typeof(Reflections.ReloadAvatarAction), ((IEnumerable<MethodInfo>)typeof(VRCPlayer).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(void) && (it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(bool)) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "Switching {0} to avatar {1}")))));
                return Reflections.ourReloadAvatarAction;
            }
        }

        public static void ReloadAvatar(this VRCPlayer instance)
        {
            emmVRCLoader.Logger.LogDebug("ReloadAvatar called");
            if (Reflections.reloadAvatarsMethod == (MethodInfo)null)
                Reflections.reloadAvatarsMethod = ((IEnumerable<MethodInfo>)typeof(VRCPlayer).GetMethods()).First<MethodInfo>((Func<MethodInfo, bool>)(mi => mi.Name.StartsWith("Method_Private_Void_Boolean_") && mi.Name.Length < 31 && ((IEnumerable<ParameterInfo>)mi.GetParameters()).Any<ParameterInfo>((Func<ParameterInfo, bool>)(pi => pi.IsOptional))));
            Reflections.reloadAvatarsMethod.Invoke((object)instance, new object[1]
            {
        (object) true
            });
        }

        public static void ReloadAllAvatars(this VRCPlayer instance)
        {
            foreach (Player player in PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0)
                player._vrcplayer.ReloadAvatar();
        }

        public static Reflections.TriggerEmoteAction TriggerEmoteAct
        {
            get
            {
                if (Reflections.ourTriggerEmoteAction != null)
                    return Reflections.ourTriggerEmoteAction;
                Reflections.ourTriggerEmoteAction = (Reflections.TriggerEmoteAction)Delegate.CreateDelegate(typeof(Reflections.TriggerEmoteAction), ((IEnumerable<MethodInfo>)typeof(VRCPlayer).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(void) && (it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(int)) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "PlayEmoteRPC")))));
                return Reflections.ourTriggerEmoteAction;
            }
        }

        public static void TriggerEmote(this VRCPlayer instance, int emote) => Reflections.TriggerEmoteAct(instance, emote);

        public static Reflections.ApplyParametersAction ApplyParametersAct
        {
            get
            {
                if (Reflections.ourApplyParametersAction != null)
                    return Reflections.ourApplyParametersAction;
                Reflections.ourApplyParametersAction = (Reflections.ApplyParametersAction)Delegate.CreateDelegate(typeof(Reflections.ApplyParametersAction), ((IEnumerable<MethodInfo>)typeof(AvatarPlayableController).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(void) && (it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(int)) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "Tried to clear an unassigned puppet channel!")))));
                return Reflections.ourApplyParametersAction;
            }
        }

        public static void ApplyParameters(this AvatarPlayableController instance, int value) => Reflections.ApplyParametersAct(instance, value);

        public static MethodInfo getPlayerHeightMethod
        {
            get
            {
                if (Reflections.ourGetPlayerHeightMethod != (MethodInfo)null)
                    return Reflections.ourGetPlayerHeightMethod;
                Reflections.ourGetPlayerHeightMethod = ((IEnumerable<MethodInfo>)typeof(VRCTrackingManager).GetMethods()).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(float) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "PlayerHeight"))));
                return Reflections.ourGetPlayerHeightMethod;
            }
        }

        public static float GetPlayerHeight(this VRCTrackingManager instance) => (float)Reflections.getPlayerHeightMethod.Invoke((object)instance, (object[])null);

        public static MethodInfo SetPlayerHeightMethod
        {
            get
            {
                if (Reflections.ourSetPlayerHeightMethod != (MethodInfo)null)
                    return Reflections.ourSetPlayerHeightMethod;
                Reflections.ourSetPlayerHeightMethod = ((IEnumerable<MethodInfo>)typeof(VRCTrackingManager).GetMethods()).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(void) && (it.GetParameters().Length == 1 && ((IEnumerable<ParameterInfo>)it.GetParameters()).First<ParameterInfo>().ParameterType == typeof(float)) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "PlayerHeight"))));
                return Reflections.ourSetPlayerHeightMethod;
            }
        }

        public static void SetPlayerHeight(this VRCTrackingManager instance, float height) => Reflections.SetPlayerHeightMethod.Invoke((object)instance, new object[1]
        {
      (object) height
        });

        public static MethodInfo SetControllerVisibilityMethod
        {
            get
            {
                if (Reflections.ourSetControllerVisibilityMethod != (MethodInfo)null)
                    return Reflections.ourSetControllerVisibilityMethod;
                Reflections.ourSetControllerVisibilityMethod = ((IEnumerable<MethodInfo>)typeof(VRCTrackingManager).GetMethods()).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.ReturnType == typeof(void) && (it.GetParameters().Length == 1 && ((IEnumerable<ParameterInfo>)it.GetParameters()).First<ParameterInfo>().ParameterType == typeof(bool)) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Method && jt.TryResolve() != (MethodBase)null && jt.TryResolve().ReflectedType != (System.Type)null && jt.TryResolve().ReflectedType.Name == "Whiteboard"))));
                return Reflections.ourSetControllerVisibilityMethod;
            }
        }

        public static void SetControllerVisibility(this VRCTrackingManager instance, bool value) => Reflections.SetControllerVisibilityMethod.Invoke((object)instance, new object[1]
        {
      (object) value
        });

        public static MethodInfo SetQuickMenuContextMethod
        {
            get
            {
                if (Reflections.ourSetQuickMenuContextMethod != (MethodInfo)null)
                    return Reflections.ourSetQuickMenuContextMethod;
                Reflections.ourSetQuickMenuContextMethod = ((IEnumerable<MethodInfo>)typeof(QuickMenu).GetMethods()).First<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.Name.Contains("Method_Public_Void_EnumNPublicSealedvaUnNoToUs7vUsNoUnique_APIUser_String")));
                return Reflections.ourSetQuickMenuContextMethod;
            }
        }

        public static void SetQuickMenuContext(
          this QuickMenu instance,
          QuickMenuContextualDisplay.EnumNPublicSealedvaUnNoToUs7vUsNoUnique context,
          APIUser user = null,
          string text = "")
        {
            Reflections.SetQuickMenuContextMethod.Invoke((object)instance, new object[3]
            {
        (object) context,
        (object) user,
        (object) text
            });
        }

        public static MethodInfo SelectPlayerMethod
        {
            get
            {
                if (Reflections.ourSelectPlayerMethod != (MethodInfo)null)
                    return Reflections.ourSelectPlayerMethod;
                Reflections.ourSelectPlayerMethod = ((IEnumerable<MethodInfo>)typeof(QuickMenu).GetMethods()).First<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.GetParameters().Length == 1 && ((IEnumerable<ParameterInfo>)it.GetParameters()).First<ParameterInfo>().ParameterType == typeof(Player)));
                return Reflections.ourSelectPlayerMethod;
            }
        }

        public static void SelectPlayer(this QuickMenu instance, Player player) => Reflections.SelectPlayerMethod.Invoke((object)instance, new object[1]
        {
      (object) player
        });

        public static MethodInfo ViewUserInfoMethod
        {
            get
            {
                if (Reflections.ourViewUserInfoMethod != (MethodInfo)null)
                    return Reflections.ourViewUserInfoMethod;
                Reflections.ourViewUserInfoMethod = ((IEnumerable<MethodInfo>)typeof(MenuController).GetMethods()).First<MethodInfo>((Func<MethodInfo, bool>)(it => it != (MethodInfo)null && it.GetParameters().Length == 1 && ((IEnumerable<ParameterInfo>)it.GetParameters()).First<ParameterInfo>().ParameterType == typeof(string) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Method && jt.TryResolve()?.Name == "FetchUser"))));
                return Reflections.ourViewUserInfoMethod;
            }
        }

        public static void ViewUserInfo(this MenuController instance, string userID) => Reflections.ViewUserInfoMethod.Invoke((object)instance, new object[1]
        {
      (object) userID
        });

        public static TextMeshProUGUI GetNameplateText(this VRCPlayer player) => player.field_Public_PlayerNameplate_0.gameObject.transform.Find("Contents/Main/Text Container/Name").GetComponent<TextMeshProUGUI>();

        public static ImageThreeSlice GetNameplateBackground(this VRCPlayer player) => player.field_Public_PlayerNameplate_0.gameObject.transform.Find("Contents/Main/Background").GetComponent<ImageThreeSlice>();

        public static float GetFramerate(this PlayerNet net)
        {
            int fieldPrivateByte0 = (int)net.field_Private_Byte_0;
            return (double)net.field_Private_Byte_0 == 0.0 ? -1f : Mathf.Floor(1000f / (float)net.field_Private_Byte_0);
        }

        public static UnityEngine.Color LerpFramerateColor(this PlayerNet net) => (double)net.GetFramerate() == -1.0 ? UnityEngine.Color.grey : UnityEngine.Color.Lerp(UnityEngine.Color.red, UnityEngine.Color.green, net.GetFramerate() / 100f);

        public static GameObject menuContent(this VRCUiManager mngr) => mngr.field_Public_GameObject_0;

        public static void EnterWorld(this VRCFlowManager mngr, string id, string tags)
        {
            if (Reflections.enterWorldMethod == (MethodInfo)null || Reflections.transitionInfoEnum == (System.Type)null)
            {
                Reflections.transitionInfoEnum = ((IEnumerable<System.Type>)typeof(WorldTransitionInfo).GetNestedTypes()).First<System.Type>();
                Reflections.enterWorldMethod = typeof(VRCFlowManager).GetMethod("Method_Public_Void_String_String_WorldTransitionInfo_Action_1_String_Boolean_0");
            }
            object instance = Activator.CreateInstance(typeof(WorldTransitionInfo), Enum.Parse(Reflections.transitionInfoEnum, "Menu"), (object)"WorldInfo_Go");
            instance.GetType().GetProperty("field_Public_" + Reflections.transitionInfoEnum.Name + "_0").SetValue(instance, Reflections.transitionInfoEnum.GetEnumValues().GetValue(3));
            Reflections.enterWorldMethod.Invoke((object)VRCFlowManager.prop_VRCFlowManager_0, new object[5]
            {
        (object) id,
        (object) tags,
        instance,
        null,
        (object) false
            });
        }

        public delegate void ResetLastPositionAction(InputStateController @this);

        public delegate void ResetAction(VRCMotionState @this);

        public delegate void ReloadAvatarAction(VRCPlayer @this, bool something = false);

        public delegate void TriggerEmoteAction(VRCPlayer @this, int emote);

        public delegate void ApplyParametersAction(AvatarPlayableController @this, int value);
    }
}
