﻿
using System;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class ScrollingTextMenu
    {
        public QMNestedButton menuBase;
        public QMSingleButton actionButton;
        private QMSingleButton upButton;
        private QMSingleButton downButton;
        private GameObject console;
        private GameObject menuTitle;
        public Text menuTitleText;
        private GameObject baseText;
        public Text baseTextText;

        public QMSingleButton menuEntryButton { get; set; }

        public ScrollingTextMenu(
          string parentPath,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Color? buttonColor,
          string titleText,
          string bodyText,
          string buttonText = "",
          Action buttonAction = null,
          string buttonTooltip = "")
        {
            this.menuBase = new QMNestedButton(parentPath, x, y, menuName, "");
            this.menuBase.getMainButton().DestroyMe();
            this.menuBase.getBackButton().setAction((Action)(() => QuickMenuUtils.ShowQuickmenuPage("ShortcutMenu")));
            this.menuEntryButton = new QMSingleButton(parentPath, x, y, menuName, new Action(this.OpenMenu), menuTooltip, buttonColor);
            this.console = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("AvatarStatsMenu/_Console").gameObject, this.menuBase.getBackButton().getGameObject().transform.parent);
            this.menuTitle = this.console.transform.Find("_Header/Text_Overall").gameObject;
            this.menuTitleText = this.menuTitle.GetComponent<Text>();
            this.menuTitleText.color = Color.white;
            this.menuTitleText.text = titleText;
            this.menuTitleText.supportRichText = true;
            this.menuTitleText.rectTransform.anchoredPosition -= new Vector2(100f, 0.0f);
            this.menuTitleText.rectTransform.offsetMax = new Vector2(900f, 0.0f);
            this.console.transform.Find("_Header/Text_Rating").localScale = Vector3.zero;
            this.console.transform.Find("_Header/InfoIcon").localScale = Vector3.zero;
            this.baseText = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, this.console.transform.Find("_StatsConsole/Viewport/Content"));
            this.baseTextText = this.baseText.GetComponent<Text>();
            this.baseTextText.rectTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
            this.baseTextText.fontStyle = FontStyle.Normal;
            this.baseTextText.alignment = TextAnchor.UpperLeft;
            this.baseTextText.color = Color.white;
            this.baseTextText.text = bodyText;
            this.baseTextText.supportRichText = true;
            this.baseTextText.horizontalOverflow = HorizontalWrapMode.Wrap;
            this.baseTextText.fontSize = (int)((double)this.baseText.GetComponent<Text>().fontSize / 1.25);
            this.baseTextText.rectTransform.offsetMax = new Vector2(825f, -10f);
            this.upButton = new QMSingleButton(this.menuBase, 4, 0, "", (Action)(() => this.console.transform.Find("_StatsConsole").GetComponent<ScrollRect>().velocity -= new Vector2(0.0f, 1000f)), "Scrolls the text view up");
            this.upButton.getGameObject().GetComponent<Image>().sprite = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("EmojiMenu/PageUp").GetComponent<Image>().sprite;
            this.downButton = new QMSingleButton(this.menuBase, 4, buttonAction == null ? 2 : 1, "", (Action)(() => this.console.transform.Find("_StatsConsole").GetComponent<ScrollRect>().velocity += new Vector2(0.0f, 1000f)), "Scrolls the text view down");
            this.downButton.getGameObject().GetComponent<Image>().sprite = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("EmojiMenu/PageDown").GetComponent<Image>().sprite;
            if (buttonAction != null)
            {
                this.actionButton = new QMSingleButton(this.menuBase, 2, 2, buttonText, buttonAction, buttonTooltip);
                this.actionButton.getGameObject().GetComponent<RectTransform>().sizeDelta *= new Vector2(4f, 0.75f);
                this.actionButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(200f, -50f);
                this.actionButton.getGameObject().GetComponentInChildren<Text>().fontSize += 2;
            }
            if (buttonAction != null)
                return;
            this.console.GetComponent<RectTransform>().sizeDelta += new Vector2(0.0f, 300f);
            this.console.transform.Find("_StatsConsole").GetComponent<RectTransform>().sizeDelta += new Vector2(0.0f, 250f);
        }

        public ScrollingTextMenu(
          QMNestedButton menuButton,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Color? buttonColor,
          string titleText,
          string bodyText,
          string buttonText = "",
          Action buttonAction = null,
          string buttonTooltip = "")
          : this(menuButton.getMenuName(), x, y, menuName, menuTooltip, buttonColor, titleText, bodyText, buttonText, buttonAction, buttonTooltip)
        {
        }

        public void OpenMenu() => QuickMenuUtils.ShowQuickmenuPage(this.menuBase.getMenuName());
    }
}
