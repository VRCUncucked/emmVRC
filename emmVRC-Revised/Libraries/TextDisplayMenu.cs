﻿
using System;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Libraries
{
    public class TextDisplayMenu
    {
        public string menuName;
        public QMNestedButton menuBase;
        public QMSingleButton actionButton;
        private GameObject menuTitle;
        private GameObject baseText;

        public QMSingleButton menuEntryButton { get; set; }

        public TextDisplayMenu(
          string parentPath,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Color? buttonColor,
          string titleText,
          string bodyText,
          string buttonText = "",
          Action buttonAction = null,
          string buttonTooltip = "")
        {
            this.menuBase = new QMNestedButton(parentPath, x, y, menuName, "");
            this.menuBase.getMainButton().DestroyMe();
            this.menuBase.getBackButton().setAction((Action)(() => QuickMenuUtils.ShowQuickmenuPage("ShortcutMenu")));
            this.menuEntryButton = new QMSingleButton(parentPath, x, y, menuName, new Action(this.OpenMenu), menuTooltip, buttonColor);
            this.menuTitle = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, this.menuBase.getBackButton().getGameObject().transform.parent);
            this.menuTitle.GetComponent<Text>().fontStyle = FontStyle.Normal;
            this.menuTitle.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            this.menuTitle.GetComponent<Text>().color = Color.white;
            this.menuTitle.GetComponent<Text>().text = titleText;
            this.menuTitle.GetComponent<RectTransform>().anchoredPosition += new Vector2(580f, -425f);
            this.baseText = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, this.menuBase.getBackButton().getGameObject().transform.parent);
            this.baseText.GetComponent<Text>().fontStyle = FontStyle.Normal;
            this.baseText.GetComponent<Text>().alignment = TextAnchor.UpperLeft;
            this.baseText.GetComponent<Text>().color = Color.white;
            this.baseText.GetComponent<Text>().text = bodyText;
            this.baseText.GetComponent<Text>().fontSize = (int)((double)this.baseText.GetComponent<Text>().fontSize / 1.25);
            this.baseText.GetComponent<RectTransform>().anchoredPosition += new Vector2(25f, -550f);
            if (buttonAction == null)
                return;
            this.actionButton = new QMSingleButton(this.menuBase, 2, 2, buttonText, buttonAction, buttonTooltip);
            this.actionButton.getGameObject().GetComponent<RectTransform>().sizeDelta *= new Vector2(4f, 1f);
            this.actionButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(200f, 0.0f);
            this.actionButton.getGameObject().GetComponentInChildren<Text>().fontSize += 2;
        }

        public TextDisplayMenu(
          QMNestedButton menuButton,
          int x,
          int y,
          string menuName,
          string menuTooltip,
          Color? buttonColor,
          string titleText,
          string bodyText,
          string buttonText = "",
          Action buttonAction = null,
          string buttonTooltip = "")
          : this(menuButton.getMenuName(), x, y, menuName, menuTooltip, buttonColor, titleText, bodyText, buttonText, buttonAction, buttonTooltip)
        {
        }

        public void OpenMenu()
        {
            this.UpdateMenu();
            QuickMenuUtils.ShowQuickmenuPage(this.menuBase.getMenuName());
        }

        public void UpdateMenu()
        {
        }
    }
}
