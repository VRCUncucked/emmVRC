﻿
using UnhollowerBaseLib;
using UnityEngine;

namespace emmVRC.Libraries
{
    public class TextureUtilities
    {
        public static Texture2D FlipTextureVertically(Texture2D original)
        {
            Texture2D texture2D = original;
            Il2CppStructArray<Color> pixels = texture2D.GetPixels();
            Color[] colorArray = new Color[pixels.Length];
            int width = texture2D.width;
            int height = texture2D.height;
            for (int index1 = 0; index1 < width; ++index1)
            {
                for (int index2 = 0; index2 < height; ++index2)
                    colorArray[index1 + index2 * width] = pixels[index1 + (height - index2 - 1) * width];
            }
            texture2D.SetPixels((Il2CppStructArray<Color>)colorArray);
            texture2D.Apply();
            return texture2D;
        }

        public static Texture2D FlipTextureHorizontally(Texture2D original)
        {
            Texture2D texture2D = original;
            Il2CppStructArray<Color> pixels = texture2D.GetPixels();
            Color[] colorArray = new Color[pixels.Length];
            int width = texture2D.width;
            int height = texture2D.height;
            for (int index1 = 0; index1 < width; ++index1)
            {
                for (int index2 = 0; index2 < height; ++index2)
                    colorArray[index1 * height + index2] = pixels[width - index1 - 1 + index2 * height];
            }
            texture2D.SetPixels((Il2CppStructArray<Color>)colorArray);
            texture2D.Apply();
            return texture2D;
        }
    }
}
