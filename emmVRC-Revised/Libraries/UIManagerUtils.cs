﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnhollowerRuntimeLib.XrefScans;

namespace emmVRC.Libraries
{
    public static class UIManagerUtils
    {
        private static UIManagerUtils.ShowScreenAction ourShowScreenAction;
        private static UIManagerUtils.GetPageAction ourGetPageAction;

        public static void QueueHUDMessage(this VRCUiManager manager, string message) => manager.field_Private_List_1_String_0.Add(message);

        public static UIManagerUtils.ShowScreenAction ShowScreenActionAction
        {
            get
            {
                if (UIManagerUtils.ourShowScreenAction != null)
                    return UIManagerUtils.ourShowScreenAction;
                UIManagerUtils.ourShowScreenAction = (UIManagerUtils.ShowScreenAction)Delegate.CreateDelegate(typeof(UIManagerUtils.ShowScreenAction), (object)VRCUiManager.prop_VRCUiManager_0, ((IEnumerable<MethodInfo>)typeof(VRCUiManager).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it.ReturnType == typeof(VRCUiPage) && it.GetParameters().Length == 1 && it.GetParameters()[0].ParameterType == typeof(VRCUiPage) && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "Screen Not Found - ")))));
                return UIManagerUtils.ourShowScreenAction;
            }
        }

        public static VRCUiPage ShowScreen(this VRCUiManager manager, VRCUiPage page) => UIManagerUtils.ShowScreenActionAction(page);

        public static void ShowScreen(this VRCUiManager manager, string pageName, bool otherThing)
        {
            VRCUiPage page = UIManagerUtils.GetPage(pageName);
            if (!((UnityEngine.Object)page != (UnityEngine.Object)null & otherThing))
                return;
            manager.ShowScreen(page);
        }

        public static UIManagerUtils.GetPageAction GetPage
        {
            get
            {
                if (UIManagerUtils.ourGetPageAction != null)
                    return UIManagerUtils.ourGetPageAction;
                UIManagerUtils.ourGetPageAction = (UIManagerUtils.GetPageAction)Delegate.CreateDelegate(typeof(UIManagerUtils.GetPageAction), (object)VRCUiPopupManager.prop_VRCUiPopupManager_0, ((IEnumerable<MethodInfo>)typeof(VRCUiPopupManager).GetMethods(BindingFlags.Instance | BindingFlags.Public)).Single<MethodInfo>((Func<MethodInfo, bool>)(it => it.GetParameters().Length == 1 && XrefScanner.XrefScan((MethodBase)it).Any<XrefInstance>((Func<XrefInstance, bool>)(jt => jt.Type == XrefType.Global && jt.ReadAsObject()?.ToString() == "Screen Not Found - ")))));
                return UIManagerUtils.ourGetPageAction;
            }
        }

        public delegate VRCUiPage ShowScreenAction(VRCUiPage page);

        public delegate VRCUiPage GetPageAction(string page);
    }
}
