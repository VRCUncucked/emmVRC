﻿
using emmVRC.Libraries;
using emmVRC.Menus;
using Il2CppSystem.IO;
using System;
using UnhollowerBaseLib;
using UnityEngine;
using UnityEngine.Events;
using VRC.Core;
using VRC.SDKBase;

namespace emmVRC.Managers
{
    public class AvatarPermissionManager
    {
        public static PaginatedMenu baseMenu;
        public static PageItem DynamicBonesEnabledToggle;
        public static PageItem ParticleSystemsEnabledToggle;
        public static PageItem ClothEnabledToggle;
        public static PageItem ShadersEnabledToggle;
        public static PageItem AudioSourcesEnabledToggle;
        public static PageItem HandCollidersToggle;
        public static PageItem FeetCollidersToggle;
        public static PageItem HeadBonesToggle;
        public static PageItem ChestBonesToggle;
        public static PageItem HipBonesToggle;
        public static bool UserInteractMenu;
        public static UnityAction originalBackAction;
        public static AvatarPermissions selectedAvatarPermissions;

        public static void Initialize()
        {
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/AvatarPermissions/")))
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/AvatarPermissions/"));
            AvatarPermissionManager.baseMenu = new PaginatedMenu(UserTweaksMenu.UserTweaks, 29304, 102394, "Avatar\nPermissions", "", new UnityEngine.Color?());
            AvatarPermissionManager.baseMenu.menuEntryButton.DestroyMe();
            AvatarPermissionManager.DynamicBonesEnabledToggle = new PageItem("Dynamic Bones", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.DynamicBonesEnabled = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.DynamicBonesEnabled = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Enables Dynamic Bones for the selected avatar");
            AvatarPermissionManager.ParticleSystemsEnabledToggle = new PageItem("Particle\nSystems", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.ParticleSystemsEnabled = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.ParticleSystemsEnabled = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Enables Particle Systems for the selected avatar");
            AvatarPermissionManager.ClothEnabledToggle = new PageItem("Cloth", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.ClothEnabled = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.ClothEnabled = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Enables Cloth for the selected avatar");
            AvatarPermissionManager.ShadersEnabledToggle = new PageItem("Shaders", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.ShadersEnabled = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.ShadersEnabled = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Enables Shaders for the selected avatar");
            AvatarPermissionManager.AudioSourcesEnabledToggle = new PageItem("Audio\nSources", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.AudioSourcesEnabled = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.AudioSourcesEnabled = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Enables Audio Sources for the selected avatar");
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.DynamicBonesEnabledToggle);
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.ParticleSystemsEnabledToggle);
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.ClothEnabledToggle);
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.ShadersEnabledToggle);
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.AudioSourcesEnabledToggle);
            AvatarPermissionManager.baseMenu.pageItems.Add(PageItem.Space);
            AvatarPermissionManager.baseMenu.pageItems.Add(PageItem.Space);
            AvatarPermissionManager.baseMenu.pageItems.Add(PageItem.Space);
            AvatarPermissionManager.baseMenu.pageItems.Add(PageItem.Space);
            AvatarPermissionManager.HandCollidersToggle = new PageItem("Hand\nColliders", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.HandColliders = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.HandColliders = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Select to only use these colliders for Global Dynamic Bone interactions");
            AvatarPermissionManager.FeetCollidersToggle = new PageItem("Feet\nColliders", (System.Action)(() =>
           {
               AvatarPermissionManager.selectedAvatarPermissions.FeetColliders = true;
               AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
               AvatarPermissionManager.ReloadAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         AvatarPermissionManager.selectedAvatarPermissions.FeetColliders = false;
         AvatarPermissionManager.selectedAvatarPermissions.SaveAvatarPermissions();
         AvatarPermissionManager.ReloadAvatars();
     }), "TOGGLE: Select to only use these colliders for Global Dynamic Bone interactions");
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.HandCollidersToggle);
            AvatarPermissionManager.baseMenu.pageItems.Add(AvatarPermissionManager.FeetCollidersToggle);
            AvatarPermissionManager.baseMenu.pageTitles.Add("Avatar Features");
            AvatarPermissionManager.baseMenu.pageTitles.Add("Exclusive Global Dynamic Bone Colliders");
            AvatarPermissionManager.baseMenu.menuBase.getBackButton().getGameObject().GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            AvatarPermissionManager.baseMenu.menuBase.getBackButton().getGameObject().GetComponent<UnityEngine.UI.Button>().onClick.AddListener((UnityAction)(System.Action)(() =>
          {
              if (AvatarPermissionManager.UserInteractMenu)
                  QuickMenuUtils.ShowQuickmenuPage("UserInteractMenu");
              else
                  QuickMenuUtils.ShowQuickmenuPage(PlayerTweaksMenu.baseMenu.getMenuName());
          }));
        }

        public static void ReloadAvatars() => VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();

        public static void OpenMenu(string avatarId, bool inUserInteractMenu = false)
        {
            AvatarPermissionManager.UserInteractMenu = inUserInteractMenu;
            AvatarPermissionManager.selectedAvatarPermissions = AvatarPermissions.GetAvatarPermissions(avatarId);
            AvatarPermissionManager.DynamicBonesEnabledToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.DynamicBonesEnabled);
            AvatarPermissionManager.ParticleSystemsEnabledToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.ParticleSystemsEnabled);
            AvatarPermissionManager.ClothEnabledToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.ClothEnabled);
            AvatarPermissionManager.ShadersEnabledToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.ShadersEnabled);
            AvatarPermissionManager.AudioSourcesEnabledToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.AudioSourcesEnabled);
            AvatarPermissionManager.HandCollidersToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.HandColliders);
            AvatarPermissionManager.FeetCollidersToggle.SetToggleState(AvatarPermissionManager.selectedAvatarPermissions.FeetColliders);
            AvatarPermissionManager.baseMenu.OpenMenu();
        }

        public static void ProcessAvatar(GameObject avatarObject, VRC_AvatarDescriptor avatarDescriptor)
        {
            if ((UnityEngine.Object)avatarObject == (UnityEngine.Object)null || (UnityEngine.Object)avatarDescriptor == (UnityEngine.Object)null || (UnityEngine.Object)avatarDescriptor.GetComponent<PipelineManager>() == (UnityEngine.Object)null)
                return;
            Transform[] transformArray = new Transform[3]
            {
        avatarObject.transform,
        avatarObject.transform.parent.Find("_AvatarMirrorClone"),
        avatarObject.transform.parent.Find("_AvatarShadowClone")
            };
            foreach (Transform transform in transformArray)
            {
                if ((UnityEngine.Object)transform == (UnityEngine.Object)null)
                    break;
                GameObject gameObject = transform.gameObject;
                try
                {
                    AvatarPermissions avatarPermissions = AvatarPermissions.GetAvatarPermissions(avatarDescriptor.GetComponent<PipelineManager>().blueprintId);
                    if (!avatarPermissions.AudioSourcesEnabled)
                    {
                        foreach (AudioSource componentsInChild in gameObject.GetComponentsInChildren<AudioSource>(true))
                        {
                            if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null)
                                UnityEngine.Object.Destroy((UnityEngine.Object)componentsInChild);
                        }
                    }
                    if (!avatarPermissions.ClothEnabled)
                    {
                        foreach (Cloth componentsInChild in gameObject.GetComponentsInChildren<Cloth>(true))
                        {
                            if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null)
                                UnityEngine.Object.Destroy((UnityEngine.Object)componentsInChild);
                        }
                    }
                    if (!avatarPermissions.DynamicBonesEnabled)
                    {
                        foreach (DynamicBone componentsInChild in gameObject.GetComponentsInChildren<DynamicBone>(true))
                        {
                            if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null)
                                UnityEngine.Object.Destroy((UnityEngine.Object)componentsInChild);
                        }
                        foreach (DynamicBoneCollider componentsInChild in gameObject.GetComponentsInChildren<DynamicBoneCollider>(true))
                        {
                            if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null)
                                UnityEngine.Object.Destroy((UnityEngine.Object)componentsInChild);
                        }
                    }
                    if (!avatarPermissions.ParticleSystemsEnabled)
                    {
                        foreach (ParticleSystem componentsInChild in gameObject.GetComponentsInChildren<ParticleSystem>(true))
                        {
                            if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null)
                                componentsInChild.maxParticles = 0;
                        }
                    }
                    if (!avatarPermissions.ShadersEnabled)
                    {
                        foreach (Renderer componentsInChild in gameObject.GetComponentsInChildren<Renderer>(true))
                        {
                            if ((UnityEngine.Object)componentsInChild != (UnityEngine.Object)null)
                            {
                                emmVRCLoader.Logger.LogDebug("Found renderer on " + componentsInChild.gameObject.name);
                                foreach (Material material in (Il2CppArrayBase<Material>)componentsInChild.materials)
                                {
                                    if ((UnityEngine.Object)material != (UnityEngine.Object)null)
                                    {
                                        emmVRCLoader.Logger.LogDebug("Found material " + material.name + " using shader " + material.shader.name + ", overriding with Diffuse...");
                                        material.shader = Shader.Find("Diffuse");
                                    }
                                }
                            }
                        }
                    }
                    emmVRCLoader.Logger.LogDebug("Finished processing avatar permissions");
                }
                catch (Exception ex)
                {
                    emmVRCLoader.Logger.LogError("Error processing avatar: " + ex.ToString());
                }
            }
        }
    }
}
