﻿
using emmVRC.TinyJSON;
using Il2CppSystem.IO;
using System;

namespace emmVRC.Managers
{
    public class AvatarPermissions
    {
        public string AvatarId;
        public bool HandColliders = true;
        public bool FeetColliders;
        public bool HeadBones;
        public bool ChestBones;
        public bool HipBones;
        public bool DynamicBonesEnabled = true;
        public bool ParticleSystemsEnabled = true;
        public bool ClothEnabled = true;
        public bool ShadersEnabled = true;
        public bool AudioSourcesEnabled = true;

        public static AvatarPermissions GetAvatarPermissions(string avatarId)
        {
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/AvatarPermissions/" + avatarId + ".json")))
                return Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/AvatarPermissions/" + avatarId + ".json"))).Make<AvatarPermissions>();
            return new AvatarPermissions() { AvatarId = avatarId };
        }

        public void SaveAvatarPermissions() => File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/AvatarPermissions/" + this.AvatarId + ".json"), Encoder.Encode((object)this));
    }
}
