﻿
using System;
using UnityEngine;

namespace emmVRC.Managers
{
    public class DebugAction
    {
        public KeyCode ActionKey;
        public Action ActionAction;
        public string Name = "";
        public string Description = "";
    }
}
