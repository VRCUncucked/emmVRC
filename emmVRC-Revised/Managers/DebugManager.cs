﻿
using emmVRC.Objects;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace emmVRC.Managers
{
    public class DebugManager
    {
        public static List<DebugAction> DebugActions = new List<DebugAction>();

        public static void Initialize()
        {
            if (Environment.CommandLine.Contains("--emmvrc.debug"))
                Attributes.Debug = true;
            MelonCoroutines.Start(DebugManager.Loop());
        }

        public static IEnumerator Loop()
        {
            while (true)
            {
                if (Attributes.Debug)
                {
                    foreach (DebugAction debugAction in DebugManager.DebugActions)
                    {
                        if (Input.GetKeyDown(debugAction.ActionKey))
                            debugAction.ActionAction();
                    }
                }
                yield return (object)new WaitForFixedUpdate();
            }
        }
    }
}
