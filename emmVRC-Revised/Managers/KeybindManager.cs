﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Menus;
using MelonLoader;
using System.Collections;
using UnityEngine;

namespace emmVRC.Managers
{
    public class KeybindManager
    {
        private static bool keyFlag;

        public static void Initialize() => MelonCoroutines.Start(KeybindManager.Loop());

        private static IEnumerator Loop()
        {
            while (true)
            {
                if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.H))
                {
                    Configuration.JSONConfig.UIVisible = !Configuration.JSONConfig.UIVisible;
                    Configuration.SaveConfig();
                    UIElementsMenu.ToggleHUD.setToggleState(Configuration.JSONConfig.HUDEnabled);
                }
                if (Configuration.JSONConfig.EnableKeybinds)
                {
                    {
                        if ((Input.GetKey((KeyCode)Configuration.JSONConfig.FlightKeybind[1]) || Configuration.JSONConfig.FlightKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.FlightKeybind[0]) && !KeybindManager.keyFlag))
                        {
                            PlayerTweaksMenu.FlightToggle.setToggleState(!Flight.FlightEnabled, true);
                            KeybindManager.keyFlag = true;
                        }
                        if ((Input.GetKey((KeyCode)Configuration.JSONConfig.NoclipKeybind[1]) || Configuration.JSONConfig.NoclipKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.NoclipKeybind[0]) && !KeybindManager.keyFlag))
                        {
                            PlayerTweaksMenu.NoclipToggle.setToggleState(!Flight.NoclipEnabled, true);
                            KeybindManager.keyFlag = true;
                        }
                        if ((Input.GetKey((KeyCode)Configuration.JSONConfig.SpeedKeybind[1]) || Configuration.JSONConfig.SpeedKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.SpeedKeybind[0]) && !KeybindManager.keyFlag))
                        {
                            PlayerTweaksMenu.SpeedToggle.setToggleState(!Speed.SpeedModified, true);
                            KeybindManager.keyFlag = true;
                        }
                    }
                    if ((Input.GetKey((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[1]) || Configuration.JSONConfig.ThirdPersonKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[0]) && !KeybindManager.keyFlag))
                    {
                        if (ThirdPerson.CameraSetup != 2)
                        {
                            ++ThirdPerson.CameraSetup;
                            ThirdPerson.TPCameraBack.transform.position -= ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
                            ThirdPerson.TPCameraFront.transform.position += ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
                            ThirdPerson.zoomOffset = 0.0f;
                            KeybindManager.keyFlag = true;
                        }
                        else
                        {
                            ThirdPerson.CameraSetup = 0;
                            ThirdPerson.TPCameraBack.transform.position -= ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
                            ThirdPerson.TPCameraFront.transform.position += ThirdPerson.TPCameraBack.transform.forward * ThirdPerson.zoomOffset;
                            ThirdPerson.zoomOffset = 0.0f;
                            KeybindManager.keyFlag = true;
                        }
                    }
                    if ((Input.GetKey((KeyCode)Configuration.JSONConfig.GoHomeKeybind[1]) || Configuration.JSONConfig.GoHomeKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.GoHomeKeybind[0]) && !KeybindManager.keyFlag))
                    {
                        ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/GoHomeButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
                        KeybindManager.keyFlag = true;
                    }
                    if ((Input.GetKey((KeyCode)Configuration.JSONConfig.RespawnKeybind[1]) || Configuration.JSONConfig.RespawnKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.RespawnKeybind[0]) && !KeybindManager.keyFlag))
                    {
                        ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/RespawnButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
                        KeybindManager.keyFlag = true;
                    }
                    if (!Input.GetKey((KeyCode)Configuration.JSONConfig.FlightKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.NoclipKeybind[0]) && (!Input.GetKey((KeyCode)Configuration.JSONConfig.SpeedKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[0])) && (!Input.GetKey((KeyCode)Configuration.JSONConfig.GoHomeKeybind[0]) && !Input.GetKey((KeyCode)Configuration.JSONConfig.RespawnKeybind[0]) && KeybindManager.keyFlag))
                        KeybindManager.keyFlag = false;
                }
                yield return (object)new WaitForEndOfFrame();
            }
        }
    }
}
