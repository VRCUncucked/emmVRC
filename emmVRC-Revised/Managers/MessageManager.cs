﻿
using MelonLoader;
using System.Collections;
using System.Collections.Generic;

namespace emmVRC.Managers
{
    public class MessageManager
    {
        private static List<PendingMessage> pendingMessages = new List<PendingMessage>();
        public static bool messageRead = true;

        public static void Initialize() => MelonCoroutines.Start(MessageManager.CheckLoop());

        public static IEnumerator CheckLoop()
        {
            yield return (object)null;
        }
    }
}
