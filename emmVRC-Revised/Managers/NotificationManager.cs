﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Menus;
using emmVRC.Objects;
using emmVRCLoader;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Managers
{
    public class NotificationManager
    {
        private static bool Enabled = true;
        private static int notificationActiveTimer = 0;
        public static QMNestedButton NotificationMenu;
        private static QMSingleButton NotificationButton1;
        private static QMSingleButton NotificationButton2;
        private static QMSingleButton NotificationButton3;
        private static GameObject NotificationIcon;
        internal static List<Notification> Notifications = new List<Notification>();
        private static bool blink = false;

        public static void Initialize()
        {
            GameObject original = GameObject.Find("UserInterface/UnscaledUI/HudContent/Hud/NotificationDotParent/NotificationDot");
            NotificationIcon = UnityEngine.Object.Instantiate<GameObject>(original, original.transform.parent);
            NotificationIcon.name = "emmVRCNotificationIcon";
            NotificationIcon.GetComponent<Image>().sprite = Resources.alertSprite;
            NotificationMenu = new QMNestedButton(!Configuration.JSONConfig.StealthMode ? "ShortcutMenu" : ReportWorldMenu.baseMenu.getMenuName(), Configuration.JSONConfig.NotificationButtonPositionX, Configuration.JSONConfig.NotificationButtonPositionY, "\nemmVRC\nNotifications", "  new emmVRC Notifications are available!");
            NotificationButton1 = new QMSingleButton(NotificationMenu, 1, 0, "Accept", (System.Action)null, "Accept");
            NotificationButton2 = new QMSingleButton(NotificationMenu, 2, 0, "Decline", (System.Action)null, "Decline");
            NotificationButton3 = new QMSingleButton(NotificationMenu, 3, 0, "Block", (System.Action)null, "Block");
            NotificationMenu.getMainButton().setAction((System.Action)(() =>
           {
               QuickMenuUtils.ShowQuickmenuPage(NotificationMenu.getMenuName());
               QuickMenuUtils.GetQuickMenuInstance().SetQuickMenuContext(QuickMenuContextualDisplay.EnumNPublicSealedvaUnNoToUs7vUsNoUnique.Notification, text: Notifications[0].Message);
               if (Notifications[0].Button1Action != null)
               {
                   NotificationButton1.setButtonText(Notifications[0].Button1Text);
                   NotificationButton1.setAction(Notifications[0].Button1Action);
                   NotificationButton1.setActive(true);
               }
               else
                   NotificationButton1.setActive(false);
               if (Notifications[0].Button2Action != null)
               {
                   NotificationButton2.setButtonText(Notifications[0].Button2Text);
                   NotificationButton2.setAction(Notifications[0].Button2Action);
                   NotificationButton2.setActive(true);
               }
               else
                   NotificationButton2.setActive(false);
               if (Notifications[0].Button3Action != null)
               {
                   NotificationButton3.setButtonText(Notifications[0].Button3Text);
                   NotificationButton3.setAction(Notifications[0].Button3Action);
                   NotificationButton3.setActive(true);
               }
               else
                   NotificationButton3.setActive(false);
           }));
            if (Configuration.JSONConfig.TabMode)
                NotificationMenu.getMainButton().getGameObject().transform.localScale = Vector3.zero;
            Loop().NoAwait("emmVRC Notification Manager Thread");
        }

        private static async Task Loop()
        {
            while (Enabled)
            {
                await Task.Delay(1000);
                await emmVRC.AwaitUpdate.Yield();
                if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
                {
                    try
                    {
                        if (Notifications.Count > 0)
                        {
                            if (!Configuration.JSONConfig.StealthMode)
                                NotificationIcon.SetActive(true);
                            else
                                NotificationIcon.SetActive(false);
                            NotificationIcon.GetComponent<Image>().sprite = Notifications[0].Icon;
                            NotificationMenu.getMainButton().setActive(true);
                            QMSingleButton mainButton1 = NotificationMenu.getMainButton();
                            int count;
                            string str1;
                            if (!blink)
                            {
                                str1 = Notifications.Count.ToString() ?? "";
                            }
                            else
                            {
                                count = Notifications.Count;
                                str1 = "<color=#FF69B4>" + count.ToString() + "</color>";
                            }
                            string buttonText = str1 + "\nemmVRC\nNotifications";
                            mainButton1.setButtonText(buttonText);
                            QMSingleButton mainButton2 = NotificationMenu.getMainButton();
                            count = Notifications.Count;
                            string buttonToolTip = count.ToString() + " new emmVRC notifications are available!" + (Notifications[0].Timeout != -1 ? " This notification will expire in " + Notifications[0].Timeout.ToString() + " seconds." : "");
                            mainButton2.setToolTip(buttonToolTip);
                            if (Configuration.JSONConfig.TabMode && (UnityEngine.Object)TabMenu.badge != (UnityEngine.Object)null)
                            {
                                TabMenu.badge.SetActive(true);
                                Text componentInChildren = TabMenu.badge.GetComponentInChildren<Text>();
                                count = Notifications.Count;
                                string str2 = count.ToString() + " NEW";
                                componentInChildren.text = str2;
                            }
                        }
                        else
                        {
                            NotificationIcon.SetActive(false);
                            NotificationMenu.getMainButton().setActive(false);
                            notificationActiveTimer = 0;
                            if (Configuration.JSONConfig.TabMode && (UnityEngine.Object)TabMenu.badge != (UnityEngine.Object)null)
                                TabMenu.badge.SetActive(false);
                        }
                        if (Notifications.Count > 0)
                        {
                            if (Notifications[0].Timeout != -1)
                            {
                                if (Notifications[0].Timeout == notificationActiveTimer)
                                {
                                    Notifications.RemoveAt(0);
                                    notificationActiveTimer = 0;
                                }
                                else
                                    ++notificationActiveTimer;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        emmVRCLoader.Logger.LogError("Notification Manager update loop encountered an exception: " + ex.ToString());
                    }
                }
            }
        }

        public static void AddNotification(
          string text,
          string button1Text,
          System.Action button1Action,
          string button2Text,
          System.Action button2Action,
          string button3Text,
          System.Action button3Action,
          Sprite notificationIcon = null,
          int timeout = -1)
        {
            Notification notification = new Notification()
            {
                Message = text,
                Button1Text = button1Text,
                Button2Text = button2Text,
                Button3Text = button3Text,
                Button1Action = button1Action,
                Button2Action = button2Action,
                Button3Action = button3Action,
                Icon = (UnityEngine.Object)notificationIcon == (UnityEngine.Object)null ? Resources.errorSprite : notificationIcon,
                Timeout = timeout
            };
            Notifications.Add(notification);
        }

        public static void AddNotification(
          string text,
          string button1Text,
          System.Action button1Action,
          string button2Text,
          System.Action button2Action,
          Sprite notificationIcon = null,
          int timeout = -1)
        {
            AddNotification(text, button1Text, button1Action, button2Text, button2Action, "", (System.Action)null, notificationIcon, timeout);
        }

        public static void DismissCurrentNotification()
        {
            if (Notifications.Count > 0)
                Notifications.Remove(Notifications[0]);
            QuickMenuUtils.ShowQuickmenuPage("ShortcutMenu");
            QuickMenuUtils.GetQuickMenuInstance().SetQuickMenuContext(QuickMenuContextualDisplay.EnumNPublicSealedvaUnNoToUs7vUsNoUnique.NoSelection);
        }
    }
}
