﻿
using emmVRC.Network.Objects;

namespace emmVRC.Managers
{
    public class PendingMessage
    {
        public Message message;
        public bool read;
    }
}
