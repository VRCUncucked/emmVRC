﻿
using emmVRC.Libraries;
using emmVRC.Menus;
using Il2CppSystem.IO;
using System;
using UnityEngine;

namespace emmVRC.Managers
{
    public class UserPermissionManager
    {
        public static PaginatedMenu baseMenu;
        public static PageItem GlobalDynamicBonesEnabledToggle;
        public static UserPermissions selectedUserPermissions;

        public static void Initialize()
        {
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/")))
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/"));
            UserPermissionManager.baseMenu = new PaginatedMenu(UserTweaksMenu.UserTweaks, 21304, 142394, "User\nPermissions", "", new Color?());
            UserPermissionManager.baseMenu.menuEntryButton.DestroyMe();
            UserPermissionManager.GlobalDynamicBonesEnabledToggle = new PageItem("Global Dynamic\nBones", (System.Action)(() =>
           {
               UserPermissionManager.selectedUserPermissions.GlobalDynamicBonesEnabled = true;
               UserPermissionManager.selectedUserPermissions.SaveUserPermissions();
               UserPermissionManager.ReloadUsers();
           }), "Disabled", (System.Action)(() =>
     {
         UserPermissionManager.selectedUserPermissions.GlobalDynamicBonesEnabled = false;
         UserPermissionManager.selectedUserPermissions.SaveUserPermissions();
         UserPermissionManager.ReloadUsers();
     }), "TOGGLE: Enables Global Dynamic Bones for the selected user");
            UserPermissionManager.baseMenu.pageItems.Add(UserPermissionManager.GlobalDynamicBonesEnabledToggle);
            UserPermissionManager.baseMenu.pageTitles.Add("User Options");
        }

        private static void ReloadUsers() => VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();

        public static void OpenMenu(string UserId)
        {
            UserPermissionManager.selectedUserPermissions = UserPermissions.GetUserPermissions(UserId);
            UserPermissionManager.GlobalDynamicBonesEnabledToggle.SetToggleState(UserPermissionManager.selectedUserPermissions.GlobalDynamicBonesEnabled);
            UserPermissionManager.baseMenu.OpenMenu();
        }
    }
}
