﻿
using emmVRC.TinyJSON;
using Il2CppSystem.IO;
using System;

namespace emmVRC.Managers
{
    public class UserPermissions
    {
        public string UserId;
        public bool GlobalDynamicBonesEnabled;

        public static UserPermissions GetUserPermissions(string UserId)
        {
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/" + UserId + ".json")))
                return Decoder.Decode(File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/" + UserId + ".json"))).Make<UserPermissions>();
            return new UserPermissions() { UserId = UserId };
        }

        public void SaveUserPermissions() => File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/UserPermissions/" + this.UserId + ".json"), Encoder.Encode((object)this));
    }
}
