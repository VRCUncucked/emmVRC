﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRCLoader;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace emmVRC.Menus
{
    public class ActionMenuFunctions
    {
        public static CustomActionMenu.Page functionsMenu;
        public static CustomActionMenu.Page favouriteEmojisMenu;
        public static CustomActionMenu.Page riskyFunctionsMenu;
        public static List<CustomActionMenu.Button> favouriteEmojiButtons;
        public static CustomActionMenu.Button flightButton;
        public static CustomActionMenu.Button noclipButton;
        public static CustomActionMenu.Button speedButton;
        public static CustomActionMenu.Button espButton;
        public static CustomActionMenu.Page avatarParametersMenu;
        public static CustomActionMenu.Button saveAvatarParameters;
        public static CustomActionMenu.Button clearAvatarParameters;
        private static EmojiMenu emojiMenu;
        private static bool playingEmoji = false;
        private static int currentEmojiPlaying = -1;

        public static IEnumerator Initialize()
        {
            while ((UnityEngine.Object)Resources.onlineSprite == (UnityEngine.Object)null || (UnityEngine.Object)Resources.onlineSprite.texture == (UnityEngine.Object)null)
                yield return (object)new WaitForEndOfFrame();
            ActionMenuFunctions.functionsMenu = new CustomActionMenu.Page(CustomActionMenu.BaseMenu.MainMenu, "emmVRC\nFunctions", Resources.onlineSprite.texture);
            ActionMenuFunctions.riskyFunctionsMenu = new CustomActionMenu.Page(ActionMenuFunctions.functionsMenu, "Risky\nFunctions", Resources.flyTexture);
            ActionMenuFunctions.favouriteEmojisMenu = new CustomActionMenu.Page(ActionMenuFunctions.functionsMenu, "Favorite\nEmojis", Resources.rpSprite.texture);
            ActionMenuFunctions.favouriteEmojiButtons = new List<CustomActionMenu.Button>();
            for (int index = 0; index < 8; ++index)
            {
                int currentEmojiButtonOption = index;
                ActionMenuFunctions.favouriteEmojiButtons.Add(new CustomActionMenu.Button(ActionMenuFunctions.favouriteEmojisMenu, "", (System.Action)(() =>
               {
                   emmVRCLoader.Logger.LogDebug("Trying to spawn Emoji " + Configuration.JSONConfig.FavouritedEmojis[currentEmojiButtonOption].ToString());
                   ActionMenuFunctions.emojiMenu.TriggerEmoji(Configuration.JSONConfig.FavouritedEmojis[currentEmojiButtonOption]);
                   ActionMenuFunctions.playingEmoji = true;
                   ActionMenuFunctions.currentEmojiPlaying = Configuration.JSONConfig.FavouritedEmojis[currentEmojiButtonOption];
                   MelonCoroutines.Start(ActionMenuFunctions.EmojiTimeout());
               })));
            }
            ActionMenuFunctions.flightButton = new CustomActionMenu.Button(ActionMenuFunctions.riskyFunctionsMenu, "Flight:\nOff", (System.Action)(() =>
           {
               PlayerTweaksMenu.FlightToggle.setToggleState(!Flight.FlightEnabled, true);
           }), CustomActionMenu.ToggleOffTexture);
            ActionMenuFunctions.noclipButton = new CustomActionMenu.Button(ActionMenuFunctions.riskyFunctionsMenu, "Noclip:\nOff", (System.Action)(() =>
           {
               PlayerTweaksMenu.NoclipToggle.setToggleState(!Flight.NoclipEnabled, true);
           }), CustomActionMenu.ToggleOffTexture);
            ActionMenuFunctions.speedButton = new CustomActionMenu.Button(ActionMenuFunctions.riskyFunctionsMenu, "Speed:\nOff", (System.Action)(() =>
           {
               PlayerTweaksMenu.SpeedToggle.setToggleState(!Speed.SpeedModified, true);
           }), CustomActionMenu.ToggleOffTexture);
            ActionMenuFunctions.espButton = new CustomActionMenu.Button(ActionMenuFunctions.riskyFunctionsMenu, "ESP:\nOff", (System.Action)(() =>
           {
               PlayerTweaksMenu.ESPToggle.setToggleState(!ESP.ESPEnabled, true);
           }), CustomActionMenu.ToggleOffTexture);
            MelonCoroutines.Start(ActionMenuFunctions.Loop());
        }

        public static IEnumerator Loop()
        {
        label_1:
            do
            {
                yield return (object)new WaitForEndOfFrame();
                if ((UnityEngine.Object)ActionMenuFunctions.emojiMenu == (UnityEngine.Object)null)
                {
                    if ((UnityEngine.Object)((Component)QuickMenu.prop_QuickMenu_0).transform.Find("EmojiMenu").GetComponent<EmojiMenu>() == (UnityEngine.Object)null)
                        yield return (object)new WaitForSeconds(1f);
                    ActionMenuFunctions.emojiMenu = ((Component)QuickMenu.prop_QuickMenu_0).transform.Find("EmojiMenu").GetComponent<EmojiMenu>();
                }
                if ((UnityEngine.Object)ActionMenuFunctions.flightButton.currentPedalOption != (UnityEngine.Object)null)
                {
                    ActionMenuFunctions.flightButton.SetButtonText("Flight:\n" + (Flight.FlightEnabled ? "On" : "Off"));
                    ActionMenuFunctions.flightButton.SetIcon(Flight.FlightEnabled ? Resources.toggleOnTexture : Resources.toggleOffTexture);
                    ActionMenuFunctions.noclipButton.SetButtonText("Noclip:\n" + (Flight.NoclipEnabled ? "On" : "Off"));
                    ActionMenuFunctions.noclipButton.SetIcon(Flight.NoclipEnabled ? Resources.toggleOnTexture : Resources.toggleOffTexture);
                    ActionMenuFunctions.speedButton.SetButtonText("Speed:\n" + (Speed.SpeedModified ? "On" : "Off"));
                    ActionMenuFunctions.speedButton.SetIcon(Speed.SpeedModified ? Resources.toggleOnTexture : Resources.toggleOffTexture);
                    ActionMenuFunctions.espButton.SetButtonText("ESP:\n" + (ESP.ESPEnabled ? "On" : "Off"));
                    ActionMenuFunctions.espButton.SetIcon(ESP.ESPEnabled ? Resources.toggleOnTexture : Resources.toggleOffTexture);
                    ActionMenuFunctions.flightButton.SetEnabled(true);
                    ActionMenuFunctions.noclipButton.SetEnabled(true);
                    ActionMenuFunctions.speedButton.SetEnabled(true);
                    ActionMenuFunctions.espButton.SetEnabled(true);
                }
                if (EmojiFavourites.AvailableEmojis != null && EmojiFavourites.AvailableEmojis.Count != 0)
                {
                    for (int index = 0; index < Configuration.JSONConfig.FavouritedEmojis.Count; ++index)
                        ActionMenuFunctions.favouriteEmojiButtons[index].SetIcon(EmojiFavourites.AvailableEmojis[Configuration.JSONConfig.FavouritedEmojis[index]].GetComponent<ParticleSystemRenderer>().material.mainTexture.Cast<Texture2D>());
                    for (int index = 0; index < 8 - Configuration.JSONConfig.FavouritedEmojis.Count; ++index)
                        ActionMenuFunctions.favouriteEmojiButtons[Configuration.JSONConfig.FavouritedEmojis.Count + index].SetIcon((Texture2D)null);
                }
            }
            while (!ActionMenuFunctions.favouriteEmojiButtons.All<CustomActionMenu.Button>((Func<CustomActionMenu.Button, bool>)(a => (UnityEngine.Object)a.currentPedalOption != (UnityEngine.Object)null)));
            for (int index = 0; index < Configuration.JSONConfig.FavouritedEmojis.Count; ++index)
            {
                if ((UnityEngine.Object)ActionMenuFunctions.favouriteEmojiButtons[index].currentPedalOption != (UnityEngine.Object)null)
                    ActionMenuFunctions.favouriteEmojiButtons[index].SetEnabled(!ActionMenuFunctions.playingEmoji);
            }
            for (int index = 0; index < 8 - Configuration.JSONConfig.FavouritedEmojis.Count; ++index)
            {
                if ((UnityEngine.Object)ActionMenuFunctions.favouriteEmojiButtons[Configuration.JSONConfig.FavouritedEmojis.Count + index].currentPedalOption != (UnityEngine.Object)null)
                    ActionMenuFunctions.favouriteEmojiButtons[Configuration.JSONConfig.FavouritedEmojis.Count + index].SetEnabled(false);
            }
            goto label_1;
        }

        private static IEnumerator EmojiTimeout()
        {
            yield return (object)new WaitForSeconds(2f);
            ActionMenuFunctions.playingEmoji = false;
            ActionMenuFunctions.currentEmojiPlaying = -1;
        }
    }
}
