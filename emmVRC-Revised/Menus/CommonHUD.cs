﻿
using emmVRC.Libraries;
using Il2CppSystem.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using VRC;

namespace emmVRC.Menus
{
    public class CommonHUD
    {
        public static string RenderPlayerList()
        {
            string str = "";
            if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
            {
                int num = 0;
                if ((UnityEngine.Object)PlayerManager.field_Private_Static_PlayerManager_0 != (UnityEngine.Object)null)
                {
                    if (PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0 != null)
                    {
                        try
                        {
                            List<Player>.Enumerator enumerator = PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                Player current = enumerator.Current;
                                if ((UnityEngine.Object)current != (UnityEngine.Object)null && current.field_Private_VRCPlayerApi_0 != null && num != 22)
                                {
                                    str = str + (current.field_Private_VRCPlayerApi_0.isMaster ? "♕ " : "     ") + "<color=#" + ColorConversion.ColorToHex(VRCPlayer.Method_Public_Static_Color_APIUser_0(current.prop_APIUser_0)) + ">" + current.prop_APIUser_0.GetName() + "</color> - " + current._vrcplayer.prop_Int16_0.ToString() + " ms - <color=" + ColorConversion.ColorToHex(current.prop_PlayerNet_0.LerpFramerateColor(), true) + ">" + ((double)current.prop_PlayerNet_0.GetFramerate() != -1.0 ? current.prop_PlayerNet_0.GetFramerate().ToString() ?? "" : "N/A") + "</color> fps\n";
                                    ++num;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exception exception = new Exception();
                        }
                    }
                }
            }
            return str;
        }

        public static string RenderWorldInfo()
        {
            string str = "";
            if (RoomManager.field_Internal_Static_ApiWorld_0 != null && (UnityEngine.Object)VRCPlayer.field_Internal_Static_VRCPlayer_0 != (UnityEngine.Object)null)
                str = str + "<b><color=red>X: " + (Mathf.Floor(VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject.GetComponent<Player>().transform.position.x * 10f) / 10f).ToString() + "</color></b>  " + "<b><color=lime>Y: " + (Mathf.Floor(VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject.GetComponent<Player>().transform.position.y * 10f) / 10f).ToString() + "</color></b>  " + "<b><color=cyan>Z: " + (Mathf.Floor(VRCPlayer.field_Internal_Static_VRCPlayer_0.gameObject.GetComponent<Player>().transform.position.z * 10f) / 10f).ToString() + "</color></b>  ";
            return str + "\n\n";
        }

        public static GameObject[] InitializeHUD(Transform parent)
        {
            GameObject gameObject1 = new GameObject("Background");
            gameObject1.AddComponent<CanvasRenderer>();
            gameObject1.AddComponent<RawImage>();
            gameObject1.GetComponent<RectTransform>().sizeDelta = new Vector2(256f, 768f);
            gameObject1.GetComponent<RectTransform>().position = (Vector3)new Vector2((float)(130 - Screen.width / 2), (float)(Screen.height / 6 - 64));
            gameObject1.transform.SetParent(parent, false);
            gameObject1.GetComponent<Image>().sprite = Resources.HUD_Minimized;
            GameObject gameObject2 = new GameObject("Text");
            gameObject2.AddComponent<CanvasRenderer>();
            gameObject2.transform.SetParent(gameObject1.transform, false);
            Text text = gameObject2.AddComponent<Text>();
            text.font = UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
            text.fontSize = 15;
            text.text = "            emmVRClient  fps: 90";
            gameObject2.GetComponent<RectTransform>().sizeDelta = new Vector2(250f, 768f);
            return new GameObject[2] { gameObject1, gameObject2 };
        }
    }
}
