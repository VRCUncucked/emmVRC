﻿
using emmVRC.Libraries;
using System;
using UnityEngine;

namespace emmVRC.Menus
{
    public class CreditsMenu
    {
        public static PaginatedMenu baseMenu;
        public static ScrollingTextMenu noticeMenu;

        public static void Initialize()
        {
            CreditsMenu.baseMenu = new PaginatedMenu(FunctionsMenu.baseMenu.menuBase, 7628, 12024, "Supporters", "", new Color?());
            CreditsMenu.baseMenu.menuEntryButton.DestroyMe();
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#E91E63>Emilia</color>", (Action)null, "Main developer of emmVRC"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#E91E63>Hordini</color>", (Action)null, "Supporter, community manager, and major cutie"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#E91E63>Brandon</color>", (Action)null, "Network developer"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#E91E63>Janni9009</color>", (Action)null, "Developer and supporter"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#E91E63>Lily</color>", (Action)null, "Developer and supporter"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#E91E63>Supah</color>", (Action)null, "Major supporter and moderator"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#0091FF>rakosi2</color>", (Action)null, "Moderator and Helper in the community"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#C67228>RiskiVR</color>", (Action)null, "Helper in the community"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#71368A>Herp\nDerpinstine</color>", (Action)null, "Major coding help, developer of MelonLoader"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#71368A>knah</color>", (Action)null, "Netcode developer, as well as developer of the Unhollower, the most important part of MelonLoader"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#71368A>Slaynash</color>", (Action)null, "Original developer of VRCModLoader, the entire reason why emmVRC exists today"));
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#71368A>DubyaDude</color>", (Action)null, "Developer of the Ruby Button API"));
            for (int index = 0; index < 5; ++index)
                CreditsMenu.baseMenu.pageItems.Add(PageItem.Space);
            CreditsMenu.noticeMenu = new ScrollingTextMenu(CreditsMenu.baseMenu.menuBase, 10293, 10221, "", "", new Color?(), "Notice to you", "emmVRC started off as a simple proof-of-concept for modding VRChat. It originally only had a primitive Global Dynamic Bones setup, and that was all. But seeing the potential of mods, I set off to test the boundaries of what a simple Mono mod could do.\n\nFast forward an entire year later, and emmVRC is a far bigger project than I ever could have imagined. Every day, we see more and more users joining the Discord, and using the mod. It's amazing to see, and I couldn't have done it without all of you guys.\n\nThank you for supporting emmVRC throughout the year, and helping me keep doing what I do! -Emilia");
            CreditsMenu.noticeMenu.menuEntryButton.DestroyMe();
            CreditsMenu.baseMenu.pageItems.Add(new PageItem("<color=#FF69B4>Notice</color>", (Action)(() => CreditsMenu.noticeMenu.OpenMenu()), "..."));
        }
    }
}
