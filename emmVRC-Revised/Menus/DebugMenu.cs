﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using Il2CppSystem.Collections.Generic;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
    public class DebugMenu
    {
        public static QMNestedButton menuBase;
        private static QMSingleButton testPopupV1OneButtonButton;
        private static QMSingleButton testPopupV1TwoButtonsButton;
        private static QMSingleButton testPopupV2OneButtonButton;
        private static QMSingleButton testPopupV2TwoButtonsButton;
        private static QMSingleButton testInputPopupButton;
        private static QMSingleButton testHUDMessageButton;
        private static QMSingleButton testNotificationButton;
        private static QMNestedButton benchmarkMenu;
        private static QMSingleButton colorChangeBenchmark;
        private static QMSingleButton monoListBenchmark;
        private static QMSingleButton il2cppListBenchmark;
        public static PaginatedMenu debugActionsMenu;

        public static void Initialize()
        {
            DebugMenu.menuBase = new QMNestedButton(FunctionsMenu.baseMenu.menuBase, 10293, 20934, "Debug", "");
            DebugMenu.menuBase.getMainButton().DestroyMe();
            DebugMenu.testPopupV1OneButtonButton = new QMSingleButton(DebugMenu.menuBase, 1, 0, "Test\nSingle\nButton\nPopup", (System.Action)(() =>
           {
               try
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC Debug", "This is a test of the popup with one button.", "Okay", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("Single button popup failed: " + ex.ToString());
                   Managers.NotificationManager.AddNotification("Single button popup failed with an exception. See the console for more details.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
               }
           }), "Test the single-button variant of the standard popup");
            DebugMenu.testPopupV1TwoButtonsButton = new QMSingleButton(DebugMenu.menuBase, 2, 0, "Test\nDouble\nButton\nPopup", (System.Action)(() =>
           {
               try
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC Debug", "This is a test of the popup with two buttons.", "Okay", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()), "Okay again", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("Double button popup failed: " + ex.ToString());
                   Managers.NotificationManager.AddNotification("Double button popup failed with an exception. See the console for more details.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
               }
           }), "Test the double-button variant of the standard popup");
            DebugMenu.testPopupV2OneButtonButton = new QMSingleButton(DebugMenu.menuBase, 3, 0, "Test\nSingle\nButton\nPopup V2", (System.Action)(() =>
           {
               try
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2("emmVRC Debug", "This is a test of the popup V2 with one button.", "Okay", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("Single button popup V2 failed: " + ex.ToString());
                   Managers.NotificationManager.AddNotification("Single button popup V2 failed with an exception. See the console for more details.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
               }
           }), "Test the single-button variant of the standard popup");
            DebugMenu.testPopupV2TwoButtonsButton = new QMSingleButton(DebugMenu.menuBase, 4, 0, "Test\nDouble\nButton\nPopup V2", (System.Action)(() =>
           {
               try
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopupV2("emmVRC Debug", "This is a test of the popup V2 with two buttons.", "Okay", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()), "Okay again", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("Double button popup V2 failed: " + ex.ToString());
                   Managers.NotificationManager.AddNotification("Double button popup V2 failed with an exception. See the console for more details.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
               }
           }), "Test the double-button variant of the standard popup");
            DebugMenu.testInputPopupButton = new QMSingleButton(DebugMenu.menuBase, 1, 1, "Test\nInput\nPopup", (System.Action)(() =>
           {
               try
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter test text here", "", InputField.InputType.Standard, false, "Foobar", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((str, keycodes, txt) => VRCUiManager.prop_VRCUiManager_0.QueueHUDMessage("Text entered: " + str)), (Il2CppSystem.Action)null, "Enter text...", startOnLeft: true);
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("Input popup failed: " + ex.ToString());
                   Managers.NotificationManager.AddNotification("Input popup failed with an exception. See the console for more details.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
               }
           }), "Test the input popup");
            DebugMenu.testHUDMessageButton = new QMSingleButton(DebugMenu.menuBase, 2, 1, "Test\nHUD\nMessage", (System.Action)(() =>
           {
               try
               {
                   VRCUiManager.prop_VRCUiManager_0.QueueHUDMessage("This is a test message");
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("HUD message failed: " + ex.ToString());
                   Managers.NotificationManager.AddNotification("HUD message failed with an exception. See the console for more details.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
               }
           }), "Test HUD message queueing");
            DebugMenu.testNotificationButton = new QMSingleButton(DebugMenu.menuBase, 3, 1, "Test\nNotifications", (System.Action)(() =>
           {
               try
               {
                   Managers.NotificationManager.AddNotification("This is a test notification.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "Also\nDismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), Resources.onlineSprite);
               }
               catch (System.Exception ex)
               {
                   emmVRCLoader.Logger.LogError("Notification manager error: " + ex.ToString());
                   VRCUiManager.prop_VRCUiManager_0.QueueHUDMessage("A notification manager error occured.");
               }
           }), "Add a test notification to the Notification Manager");
            DebugMenu.benchmarkMenu = new QMNestedButton(DebugMenu.menuBase, 4, 1, "Benchmarks", "Test the performance of various sections of emmVRC");
            DebugMenu.colorChangeBenchmark = new QMSingleButton(DebugMenu.benchmarkMenu, 1, 0, "Color\nChanging\nBenchmark", (System.Action)(() =>
           {
               Stopwatch[] stopwatchArray = new Stopwatch[5]
          {
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch()
             };
               Stopwatch stopwatch1 = new Stopwatch();
               string uiColorHex = Configuration.JSONConfig.UIColorHex;
               bool colorChangingEnabled = Configuration.JSONConfig.UIColorChangingEnabled;
               Configuration.JSONConfig.UIColorHex = "#FFFFFF";
               Configuration.JSONConfig.UIColorChangingEnabled = true;
               for (int index = 0; index < 5; ++index)
               {
                   stopwatchArray[index].Start();
                   ColorChanger.ApplyIfApplicable();
                   stopwatchArray[index].Stop();
               }
               Configuration.JSONConfig.UIColorHex = uiColorHex;
               Configuration.JSONConfig.UIColorChangingEnabled = colorChangingEnabled;
               ColorChanger.ApplyIfApplicable();
               long num = 0;
               foreach (Stopwatch stopwatch2 in stopwatchArray)
                   num += stopwatch2.ElapsedMilliseconds;
               Managers.NotificationManager.AddNotification("Average time taken for color change: " + (num / 5L).ToString() + "ms.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.alertSprite);
           }), "Tests the UI color changing, to see the time it consumes CPU");
            DebugMenu.il2cppListBenchmark = new QMSingleButton(DebugMenu.benchmarkMenu, 2, 0, "IL2CPP\nList\nBenchmark", (System.Action)(() =>
           {
               Stopwatch[] stopwatchArray1 = new Stopwatch[5]
          {
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch()
             };
               Stopwatch[] stopwatchArray2 = new Stopwatch[5]
          {
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch()
             };
               var list = new Il2CppSystem.Collections.Generic.List<float>();
               for (int index1 = 0; index1 < 5; ++index1)
               {
                   stopwatchArray1[index1].Start();
                   for (int index2 = 0; index2 < 63; ++index2)
                       list.Add((float)index2 * 0.1f);
                   stopwatchArray1[index1].Stop();
                   stopwatchArray2[index1].Start();
                   foreach (double num in list)
                       ;
                   stopwatchArray2[index1].Stop();
               }
               long num1 = 0;
               foreach (Stopwatch stopwatch in stopwatchArray1)
                   num1 += stopwatch.ElapsedMilliseconds;
               long num2 = num1 / 5L;
               long num3 = 0;
               foreach (Stopwatch stopwatch in stopwatchArray2)
                   num3 += stopwatch.ElapsedMilliseconds;
               long num4 = num3 / 5L;
               Managers.NotificationManager.AddNotification("IL2CPP Average fill time: " + num2.ToString() + "ms, average iterate time: " + num4.ToString() + "ms.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.alertSprite);
           }), "Tests IL2CPP lists for efficiency");
            DebugMenu.monoListBenchmark = new QMSingleButton(DebugMenu.benchmarkMenu, 3, 0, "Mono\nList\nBenchmark", (System.Action)(() =>
           {
               Stopwatch[] stopwatchArray1 = new Stopwatch[5]
          {
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch()
             };
               Stopwatch[] stopwatchArray2 = new Stopwatch[5]
          {
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch(),
          new Stopwatch()
             };
               var floatList = new Il2CppSystem.Collections.Generic.List<float>();
               for (int index1 = 0; index1 < 5; ++index1)
               {
                   stopwatchArray1[index1].Start();
                   for (int index2 = 0; index2 < 63; ++index2)
                       floatList.Add((float)index2 * 0.1f);
                   stopwatchArray1[index1].Stop();
                   stopwatchArray2[index1].Start();
                   foreach (double num in floatList)
                       ;
                   stopwatchArray2[index1].Stop();
               }
               long num1 = 0;
               foreach (Stopwatch stopwatch in stopwatchArray1)
                   num1 += stopwatch.ElapsedMilliseconds;
               long num2 = num1 / 5L;
               long num3 = 0;
               foreach (Stopwatch stopwatch in stopwatchArray2)
                   num3 += stopwatch.ElapsedMilliseconds;
               long num4 = num3 / 5L;
               Managers.NotificationManager.AddNotification("Mono Average fill time: " + num2.ToString() + "ms, average iterate time: " + num4.ToString() + "ms.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.alertSprite);
           }), "Tests Mono lists for efficiency");
            DebugMenu.debugActionsMenu = new PaginatedMenu(DebugMenu.menuBase, 1, 2, "Registered\nDebug\nActions", "Shows all of the registered debug actions registered in this build of emmVRC, if any", new Color?());
        }

        public static void PopulateDebugMenu()
        {
            if (DebugManager.DebugActions.Count <= 0)
                return;
            foreach (DebugAction debugAction in DebugManager.DebugActions)
            {
                string name = debugAction.Name;
                System.Action actionAction = debugAction.ActionAction;
                if (string.IsNullOrWhiteSpace(name))
                    name = "Action " + (DebugMenu.debugActionsMenu.pageItems.Count + 1).ToString();
                DebugMenu.debugActionsMenu.pageItems.Add(new PageItem(name, actionAction, debugAction.Description));
            }
        }
    }
}
