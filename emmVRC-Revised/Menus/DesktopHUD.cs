﻿
using emmVRC.Hacks;
using emmVRC.Network;
using emmVRC.Objects;
using MelonLoader;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VRC;
using VRC.Core;

namespace emmVRC.Menus
{
    public class DesktopHUD
    {
        private static GameObject CanvasObject;
        private static GameObject BackgroundObject;
        private static GameObject TextObject;
        private static GameObject LogoIconContainer;
        private static Image BackgroundImage;
        private static Image emmLogo;
        private static Text TextText;
        private static bool keyFlag;
        public static bool UIExpanded = false;
        public static bool Initialized = false;
        public static bool enabled = true;
        private static CanvasScaler scaler;

        public static IEnumerator Initialize()
        {
            emmVRCLoader.Logger.LogDebug("Initializing Desktop HUD");
            while ((Object)Resources.HUD_Minimized == (Object)null)
                yield return (object)null;
            DesktopHUD.CanvasObject = new GameObject("emmVRCUICanvas");
            Object.DontDestroyOnLoad((Object)DesktopHUD.CanvasObject);
            DesktopHUD.CanvasObject.AddComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            DesktopHUD.CanvasObject.transform.position = Vector3.zero;
            DesktopHUD.scaler = DesktopHUD.CanvasObject.AddComponent<CanvasScaler>();
            DesktopHUD.scaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            DesktopHUD.BackgroundObject = new GameObject("Background");
            DesktopHUD.BackgroundObject.AddComponent<CanvasRenderer>();
            DesktopHUD.BackgroundImage = DesktopHUD.BackgroundObject.AddComponent<Image>();
            DesktopHUD.BackgroundObject.GetComponent<RectTransform>().anchorMin = new Vector2(0.0f, 1f);
            DesktopHUD.BackgroundObject.GetComponent<RectTransform>().anchorMax = new Vector2(0.0f, 1f);
            DesktopHUD.BackgroundObject.GetComponent<RectTransform>().pivot = new Vector2(0.0f, 1f);
            DesktopHUD.BackgroundObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.0f, -5f);
            DesktopHUD.BackgroundObject.GetComponent<RectTransform>().sizeDelta = new Vector2(325f, 768f);
            DesktopHUD.BackgroundObject.transform.SetParent(DesktopHUD.CanvasObject.transform, false);
            DesktopHUD.BackgroundImage.sprite = Resources.HUD_Minimized;
            DesktopHUD.TextObject = new GameObject("Text");
            DesktopHUD.TextObject.AddComponent<CanvasRenderer>();
            DesktopHUD.TextObject.transform.SetParent(DesktopHUD.BackgroundObject.transform, false);
            DesktopHUD.TextText = DesktopHUD.TextObject.AddComponent<Text>();
            DesktopHUD.TextText.font = UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
            DesktopHUD.TextText.fontSize = 15;
            DesktopHUD.TextText.text = "";
            DesktopHUD.TextObject.GetComponent<RectTransform>().sizeDelta = new Vector2(310f, 768f);
            RectTransform component = DesktopHUD.TextObject.GetComponent<RectTransform>();
            component.localPosition = component.localPosition + new Vector3(0.0f, 3f, 0.0f);
            DesktopHUD.LogoIconContainer = new GameObject("emmHUDLogo");
            DesktopHUD.LogoIconContainer.AddComponent<CanvasRenderer>();
            DesktopHUD.LogoIconContainer.transform.SetParent(DesktopHUD.BackgroundObject.transform, false);
            DesktopHUD.emmLogo = DesktopHUD.LogoIconContainer.AddComponent<Image>();
            DesktopHUD.emmLogo.sprite = Resources.emmHUDLogo;
            DesktopHUD.emmLogo.GetComponent<RectTransform>().anchorMin = new Vector2(0.0f, 1f);
            DesktopHUD.emmLogo.GetComponent<RectTransform>().anchorMax = new Vector2(0.0f, 1f);
            DesktopHUD.emmLogo.GetComponent<RectTransform>().pivot = new Vector2(0.0f, 1f);
            DesktopHUD.emmLogo.GetComponent<RectTransform>().anchoredPosition = new Vector2(10f, -2.5f);
            DesktopHUD.emmLogo.GetComponent<RectTransform>().localScale = new Vector3(0.58f, 0.58f, 0.58f);
            DesktopHUD.CanvasObject.SetActive(false);
            DesktopHUD.Initialized = true;
            emmVRCLoader.Logger.LogDebug("Desktop HUD initialized fully.");
            MelonCoroutines.Start(DesktopHUD.Loop());
        }

        private static IEnumerator Loop()
        {
            while (true)
            {
                do
                {
                    do
                    {
                        if (Configuration.JSONConfig.HUDEnabled && DesktopHUD.enabled)
                            DesktopHUD.CanvasObject.SetActive(true);
                        else
                            DesktopHUD.CanvasObject.SetActive(false);
                        yield return (object)new WaitForEndOfFrame();
                        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && (Input.GetKey(KeyCode.E) && !DesktopHUD.keyFlag))
                        {
                            DesktopHUD.UIExpanded = !DesktopHUD.UIExpanded;
                            DesktopHUD.BackgroundImage.sprite = !DesktopHUD.UIExpanded ? Resources.HUD_Minimized : Resources.HUD_Base;
                            DesktopHUD.keyFlag = true;
                        }
                        if ((Input.GetKey((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[1]) || Configuration.JSONConfig.ToggleHUDEnabledKeybind[1] == 0) && (Input.GetKey((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[0]) && !DesktopHUD.keyFlag) && Configuration.JSONConfig.EnableKeybinds)
                        {
                            Configuration.JSONConfig.HUDEnabled = !Configuration.JSONConfig.HUDEnabled;
                            Configuration.SaveConfig();
                            DesktopHUD.keyFlag = true;
                        }
                        if (!Input.GetKey(KeyCode.E) && !Input.GetKey((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[0]) && DesktopHUD.keyFlag)
                            DesktopHUD.keyFlag = false;
                    }
                    while (!(bool)(Object)DesktopHUD.TextText);
                    if (DesktopHUD.UIExpanded)
                    {
                        string str1 = CommonHUD.RenderPlayerList();
                        Text textText = DesktopHUD.TextText;
                        string[] strArray = new string[12]
                        {
              "\n                  <color=#FF69B4>emmVRC</color>                        fps: ",
              Mathf.Floor(1f / Time.deltaTime).ToString(),
              "\n                  press 'CTRL+E' to close\n\n\nUsers in room",
              !((Object) PlayerManager.prop_PlayerManager_0 != (Object) null) || RoomManager.field_Internal_Static_ApiWorldInstance_0 == null ? "" : " (" + PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.Count.ToString() + ")",
              ":\n",
              str1,
              "\n\nPosition in world:\n",
              CommonHUD.RenderWorldInfo(),
              "\n\n",
              Configuration.JSONConfig.emmVRCNetworkEnabled ? (NetworkClient.webToken != null ? "<color=lime>Connected to the\nemmVRC Network</color>" : "<color=red>Not connected to the\nemmVRC Network</color>") : "",
              "\n\n",
              null
                        };
                        string str2;
                        if (!Attributes.Debug)
                            str2 = "";
                        else
                            str2 = "Current frame time: " + FrameTimeCalculator.frameTimes[FrameTimeCalculator.iterator == 0 ? FrameTimeCalculator.frameTimes.Length - 1 : FrameTimeCalculator.iterator - 1].ToString() + "ms\nAverage frame time: " + FrameTimeCalculator.frameTimeAvg.ToString() + "ms\n";
                        strArray[11] = str2;
                        string str3 = string.Concat(strArray);
                        textText.text = str3;
                        if (APIUser.CurrentUser != null && Configuration.JSONConfig.InfoSpoofingEnabled)
                            DesktopHUD.TextText.text = DesktopHUD.TextText.text.Replace(APIUser.CurrentUser.GetName(), NameSpoofGenerator.spoofedName);
                    }
                }
                while (DesktopHUD.UIExpanded);
                DesktopHUD.TextText.text = "\n                  <color=#FF69B4>emmVRC</color>                        fps: " + Mathf.Floor(1f / Time.deltaTime).ToString() + "\n                  press 'CTRL+E' to open";
            }
        }
    }
}
