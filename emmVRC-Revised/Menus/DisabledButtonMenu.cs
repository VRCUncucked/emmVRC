﻿
using emmVRC.Libraries;
using System;
using UnityEngine;

namespace emmVRC.Menus
{
    public class DisabledButtonMenu
    {
        public static QMNestedButton baseMenu;
        private static QMSingleButton emojiButton;
        private static QMSingleButton reportWorld;
        private static QMToggleButton rankToggleButton;

        public static void Initialize()
        {
            DisabledButtonMenu.baseMenu = new QMNestedButton(FunctionsMenu.baseMenu.menuBase, 1920, 1080, "Disabled\nButtons", "Contains buttons from the Quick Menu that were disabled by emmVRC");
            DisabledButtonMenu.baseMenu.getMainButton().DestroyMe();
            DisabledButtonMenu.emojiButton = new QMSingleButton(DisabledButtonMenu.baseMenu, 1, 0, "Emoji", (Action)(() => QuickMenuUtils.ShowQuickmenuPage("EmojiMenu")), "Express Yourself with Emojis");
            DisabledButtonMenu.reportWorld = new QMSingleButton(DisabledButtonMenu.baseMenu, 2, 0, "Report\nWorld", (Action)(() => ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/ReportWorldButton").GetComponent<UnityEngine.UI.Button>().onClick.Invoke()), "Report Issues with this World");
            DisabledButtonMenu.rankToggleButton = new QMToggleButton(DisabledButtonMenu.baseMenu, 3, 0, "Appearing as\nRank", (Action)(() =>
           {
               ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
               DisabledButtonMenu.LoadMenu();
           }), "Appear as\n<color=#2BCE5C>User</color> Rank", (Action)(() =>
     {
         ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors").GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
         DisabledButtonMenu.LoadMenu();
     }), "TOGGLE: (Known or Higher Trust Rank): Display Your Trust Rank as User", defaultPosition: true);
        }

        public static void LoadMenu()
        {
            QuickMenuUtils.ShowQuickmenuPage(DisabledButtonMenu.baseMenu.getMenuName());
            DisabledButtonMenu.emojiButton.setActive(Configuration.JSONConfig.DisableEmojiButton);
            DisabledButtonMenu.reportWorld.setActive(Configuration.JSONConfig.DisableReportWorldButton);
            DisabledButtonMenu.rankToggleButton.setActive(Configuration.JSONConfig.DisableRankToggleButton);
            if (((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors/KNOWN").gameObject.activeSelf)
            {
                if (((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors/KNOWN/ON").gameObject.activeSelf)
                {
                    DisabledButtonMenu.rankToggleButton.setOnText("Appearing as\n<color=#FF7B42>Known User</color>");
                    DisabledButtonMenu.rankToggleButton.setOffText("Appear as\n<color=#2BCE5C>User</color> Rank");
                    DisabledButtonMenu.rankToggleButton.setToolTip("TOGGLE: (Known or Higher Trust Rank): Display Your Trust Rank as User");
                }
                else
                {
                    if (!((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors/KNOWN/OFF").gameObject.activeSelf)
                        return;
                    DisabledButtonMenu.rankToggleButton.setOnText("Appearing as\n<color=#FF7B42>Known User</color>");
                    DisabledButtonMenu.rankToggleButton.setOffText("Appear as\n<color=#2BCE5C>User</color> Rank");
                    DisabledButtonMenu.rankToggleButton.setToolTip("TOGGLE: (Known or Higher Trust Rank): Display Your Actual Trust Rank");
                }
            }
            else if (((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors/TRUSTED").gameObject.activeSelf)
            {
                if (((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors/TRUSTED/ON").gameObject.activeSelf)
                {
                    DisabledButtonMenu.rankToggleButton.setOnText("Appearing as\n<color=#8143E6>Trusted User</color>");
                    DisabledButtonMenu.rankToggleButton.setOffText("Appear as\n<color=#2BCE5C>User</color> Rank");
                    DisabledButtonMenu.rankToggleButton.setToolTip("TOGGLE: (Known or Higher Trust Rank): Display Your Trust Rank as User");
                }
                else
                {
                    if (!((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/Toggle_States_ShowTrustRank_Colors/TRUSTED/OFF").gameObject.activeSelf)
                        return;
                    DisabledButtonMenu.rankToggleButton.setOnText("Appear as\n<color=#8143E6>Trusted User</color>");
                    DisabledButtonMenu.rankToggleButton.setOffText("Appearing as\n<color=#2BCE5C>User</color> Rank");
                    DisabledButtonMenu.rankToggleButton.setToolTip("TOGGLE: (Known or Higher Trust Rank): Display Your Actual Trust Rank");
                }
            }
            else
                DisabledButtonMenu.rankToggleButton.setActive(false);
        }
    }
}
