﻿
using emmVRC.Libraries;
using MelonLoader;
using System.Collections;
using UnityEngine;

namespace emmVRC.Menus
{
    public class FlashlightMenu
    {
        public static QMNestedButton baseMenu;
        public static QMToggleButton toggleFlashlight;
        public static QMToggleButton toggleHeadlight;
        public static ColorPicker setFlashlightLightColor;
        public static Objects.Slider flashlightStrengthSlider;
        public static Color lightColor = Color.white;
        public static float LightStrength = 10f;
        public static GameObject FlashlightObject;
        public static GameObject HeadlightObject;
        public static GameObject CameraBase;

        public static void Initialize()
        {
            FlashlightMenu.baseMenu = new QMNestedButton(WorldTweaksMenu.baseMenu, 3, 1, "Flashlight", "Configure and summon a flashlight you can carry through your current world");
            FlashlightMenu.toggleFlashlight = new QMToggleButton(FlashlightMenu.baseMenu, 1, 0, "Flashlight On", (System.Action)(() =>
           {
               GameObject primitive1 = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
               primitive1.GetComponent<Renderer>().material.color = Configuration.menuColor();
               primitive1.GetComponent<Collider>().isTrigger = true;
               primitive1.transform.localScale = new Vector3(0.05f, 0.125f, 0.05f);
               primitive1.transform.position = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position + new Vector3(0.0f, 1f, 0.0f);
               Rigidbody rigidbody = primitive1.AddComponent<Rigidbody>();
               rigidbody.useGravity = false;
               rigidbody.isKinematic = true;
               primitive1.AddComponent<VRCSDK2.VRC_Pickup>().AutoHold = VRC.SDKBase.VRC_Pickup.AutoHoldMode.Yes;
               GameObject gameObject = new GameObject("LightBase");
               gameObject.transform.SetParent(primitive1.transform);
               gameObject.transform.localPosition = Vector3.zero;
               Light lght = gameObject.AddComponent<Light>();
               lght.color = FlashlightMenu.lightColor;
               gameObject.transform.Rotate(90f, 0.0f, 0.0f);
               GameObject primitive2 = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
               primitive2.GetComponent<Renderer>().material.color = Configuration.menuColor();
               primitive2.transform.SetParent(primitive1.transform);
               primitive2.transform.localPosition = new Vector3(0.0f, -0.75f, 0.0f);
               primitive2.transform.localScale = new Vector3(1.5f, 0.25f, 1.5f);
               primitive2.GetComponent<Collider>().isTrigger = true;
               lght.type = LightType.Spot;
               lght.range = FlashlightMenu.LightStrength;
               primitive1.transform.Rotate(90f, 0.0f, 0.0f);
               FlashlightMenu.FlashlightObject = primitive1;
               Objects.VRC_UdonTrigger.Instantiate(FlashlightMenu.FlashlightObject, "Toggle On/Off", (System.Action)(() => lght.enabled = !lght.enabled));
           }), "Flashlight Off", (System.Action)(() => UnityEngine.Object.Destroy((UnityEngine.Object)FlashlightMenu.FlashlightObject)), "TOGGLE: Turns on and off the flashlight");
            FlashlightMenu.toggleHeadlight = new QMToggleButton(FlashlightMenu.baseMenu, 2, 0, "Headlight On", (System.Action)(() =>
           {
               if ((UnityEngine.Object)FlashlightMenu.CameraBase == (UnityEngine.Object)null)
                   FlashlightMenu.CameraBase = GameObject.Find("Camera (eye)");
               if ((UnityEngine.Object)FlashlightMenu.CameraBase == (UnityEngine.Object)null)
                   FlashlightMenu.CameraBase = GameObject.Find("CenterEyeAnchor");
               if (!((UnityEngine.Object)FlashlightMenu.CameraBase != (UnityEngine.Object)null))
                   return;
               GameObject gameObject1 = new GameObject("Headlight");
               gameObject1.transform.SetParent(FlashlightMenu.CameraBase.transform, false);
               GameObject gameObject2 = new GameObject("LightBase");
               gameObject2.transform.SetParent(gameObject1.transform);
               gameObject2.transform.localPosition = Vector3.zero;
               Light light = gameObject2.AddComponent<Light>();
               light.color = FlashlightMenu.lightColor;
               gameObject2.transform.rotation = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation;
               gameObject2.transform.Rotate(20f, 0.0f, 0.0f);
               light.type = LightType.Spot;
               light.range = FlashlightMenu.LightStrength;
               light.spotAngle *= 2f;
               FlashlightMenu.HeadlightObject = gameObject1;
           }), "Headlight Off", (System.Action)(() => UnityEngine.Object.Destroy((UnityEngine.Object)FlashlightMenu.HeadlightObject)), "TOGGLE: Turns on and off the headlight");
            FlashlightMenu.setFlashlightLightColor = new ColorPicker(FlashlightMenu.baseMenu.getMenuName(), 4, 0, "Light\nColor", "Allows you to set the flashlight color", (System.Action<Color>)(result =>
           {
               FlashlightMenu.lightColor = result;
               if ((UnityEngine.Object)FlashlightMenu.FlashlightObject != (UnityEngine.Object)null)
                   FlashlightMenu.FlashlightObject.GetComponentInChildren<Light>().color = result;
               if ((UnityEngine.Object)FlashlightMenu.HeadlightObject != (UnityEngine.Object)null)
                   FlashlightMenu.HeadlightObject.GetComponentInChildren<Light>().color = result;
               QuickMenuUtils.ShowQuickmenuPage(FlashlightMenu.baseMenu.getMenuName());
           }), (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(FlashlightMenu.baseMenu.getMenuName())), new Color?(FlashlightMenu.lightColor), new Color?(Color.white));
            FlashlightMenu.flashlightStrengthSlider = new Objects.Slider(FlashlightMenu.baseMenu.getMenuName(), 1, 2, (System.Action<float>)(flt =>
           {
               FlashlightMenu.LightStrength = flt;
               if ((UnityEngine.Object)FlashlightMenu.FlashlightObject != (UnityEngine.Object)null)
                   FlashlightMenu.FlashlightObject.GetComponentInChildren<Light>().range = FlashlightMenu.LightStrength;
               if (!((UnityEngine.Object)FlashlightMenu.HeadlightObject != (UnityEngine.Object)null))
                   return;
               FlashlightMenu.HeadlightObject.GetComponentInChildren<Light>().range = FlashlightMenu.LightStrength;
           }), 10f);
            FlashlightMenu.flashlightStrengthSlider.slider.GetComponent<UnityEngine.UI.Slider>().maxValue = 100f;
            FlashlightMenu.flashlightStrengthSlider.slider.GetComponent<UnityEngine.UI.Slider>().minValue = 10f;
            FlashlightMenu.flashlightStrengthSlider.slider.GetComponent<RectTransform>().anchoredPosition += new Vector2(480f, -104f);
            FlashlightMenu.flashlightStrengthSlider.slider.GetComponent<RectTransform>().sizeDelta *= new Vector2(2f, 1f);
            MelonCoroutines.Start(FlashlightMenu.Loop());
        }

        private static IEnumerator Loop()
        {
            while (true)
                yield return (object)new WaitForEndOfFrame();
        }
    }
}
