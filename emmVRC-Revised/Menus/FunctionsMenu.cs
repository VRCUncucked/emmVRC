﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Objects;
using MelonLoader;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
    public class FunctionsMenu
    {
        public static PaginatedMenu baseMenu;
        private static PageItem worldTweaksButton;
        private static PageItem playerTweaksButton;
        private static PageItem instanceHistoryButton;
        private static PageItem disabledButtonsButton;
        private static PageItem programsButton;
        private static PageItem playerHistoryButton;
        private static PageItem settingsButton;
        private static PageItem alarmClockButton;
        private static PageItem debugMenuButton;
        private static PageItem forceQuitButton;
        private static PageItem instantRestartButton;
        private static QMSingleButton PrevTrackButton;
        private static QMSingleButton PlayTrackButton;
        private static QMSingleButton StopTrackButton;
        private static QMSingleButton NextTrackButton;

        public static void Initialize()
        {
            FunctionsMenu.baseMenu = new PaginatedMenu("ShortcutMenu", Configuration.JSONConfig.FunctionsButtonX, Configuration.JSONConfig.FunctionsButtonY, "<color=#FF69B4>emmVRC</color>\nFunctions", "Extra functions that can enhance the user experience or provide practical features", new Color?());
            if (Configuration.JSONConfig.StealthMode)
            {
                FunctionsMenu.baseMenu.menuEntryButton.DestroyMe();
                QMSingleButton qmSingleButton = new QMSingleButton(ReportWorldMenu.baseMenu, 1, 0, "<color=#FF69B4>emmVRC</color>\nFunctions", (Action)(() => FunctionsMenu.baseMenu.OpenMenu()), "Extra functions that can enhance the user experience or provide practical features");
            }
            if (Configuration.JSONConfig.TabMode && !Configuration.JSONConfig.StealthMode)
                FunctionsMenu.baseMenu.menuEntryButton.getGameObject().transform.localScale = Vector3.zero;
            FunctionsMenu.worldTweaksButton = new PageItem("World\nTweaks", (Action)(() => QuickMenuUtils.ShowQuickmenuPage(WorldTweaksMenu.baseMenu.getMenuName())), "Contains tweaks to affect the world around you");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.worldTweaksButton);
            FunctionsMenu.playerTweaksButton = new PageItem("Player\nTweaks", (Action)(() => QuickMenuUtils.ShowQuickmenuPage(PlayerTweaksMenu.baseMenu.getMenuName())), "Contains tweaks to affect your movement, as well as the players around you");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.playerTweaksButton);
            FunctionsMenu.instanceHistoryButton = new PageItem("Instance\nHistory", (Action)(() =>
           {
               InstanceHistoryMenu.baseMenu.OpenMenu();
               InstanceHistoryMenu.LoadMenu();
           }), "Allows you to join an instance you were previously in, so long as you have not been kicked from it");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.instanceHistoryButton);
            FunctionsMenu.disabledButtonsButton = new PageItem("Disabled\nButtons", (Action)(() => DisabledButtonMenu.LoadMenu()), "Contains buttons from the Quick Menu that were disabled by emmVRC");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.disabledButtonsButton);
            FunctionsMenu.programsButton = new PageItem("Programs", (Action)(() => ProgramMenu.baseMenu.OpenMenu()), "Lets you launch external programs from within VRChat.");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.programsButton);
            FunctionsMenu.playerHistoryButton = new PageItem("Player\nHistory", (Action)(() => PlayerHistoryMenu.ShowMenu()), "Allows you to view players who have entered this instance since you joined");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.playerHistoryButton);
            FunctionsMenu.settingsButton = new PageItem("Settings", (Action)(() => SettingsMenu.LoadMenu()), "Access the Settings for emmVRC, including Risky Functions, color changes, etc.");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.settingsButton);
            FunctionsMenu.alarmClockButton = new PageItem("Alarm\nClocks", (Action)(() => QuickMenuUtils.ShowQuickmenuPage(AlarmClock.baseMenu.getMenuName())), "Configure emmVRC's alarm clocks");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.alarmClockButton);
            for (int index = 0; index <= 3; ++index)
                FunctionsMenu.baseMenu.pageItems.Add(PageItem.Space);
            FunctionsMenu.debugMenuButton = new PageItem("Debug", (Action)(() => QuickMenuUtils.ShowQuickmenuPage(DebugMenu.menuBase.getMenuName())), "Contains debug actions to test emmVRC and the modding environment as a whole");
            if (Attributes.Debug)
                FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.debugMenuButton);
            else
                FunctionsMenu.baseMenu.pageItems.Add(PageItem.Space);
            FunctionsMenu.forceQuitButton = new PageItem("Force\nQuit", (Action)(() => DestructiveActions.ForceQuit()), "Quits the game, instantly.");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.forceQuitButton);
            FunctionsMenu.instantRestartButton = new PageItem("Instant\nRestart", (Action)(() => DestructiveActions.ForceRestart()), "Restarts the game, instantly.");
            FunctionsMenu.baseMenu.pageItems.Add(FunctionsMenu.instantRestartButton);
            if (ModCompatibility.MControl)
                return;
            MelonCoroutines.Start(FunctionsMenu.AddMediaKeys());
        }

        private static IEnumerator AddMediaKeys()
        {
            QuickMenuUtils.ResizeQuickMenuCollider();
            FunctionsMenu.PrevTrackButton = new QMSingleButton(FunctionsMenu.baseMenu.menuBase, 1, -2, "", new Action(MediaControl.PrevTrack), "Go to the previous song in your Playlist (click twice)\n or restart the current song");
            while ((UnityEngine.Object)Resources.Media_Nav == (UnityEngine.Object)null)
                yield return (object)null;
            FunctionsMenu.PrevTrackButton.getGameObject().GetComponent<Image>().sprite = Resources.Media_Nav;
            FunctionsMenu.PrevTrackButton.getGameObject().transform.rotation *= Quaternion.Euler(0.0f, 0.0f, 180f);
            FunctionsMenu.PlayTrackButton = new QMSingleButton(FunctionsMenu.baseMenu.menuBase, 2, -2, "", new Action(MediaControl.PlayPause), "Pause or continue listening to the current song");
            while ((UnityEngine.Object)Resources.Media_PlayPause == (UnityEngine.Object)null)
                yield return (object)null;
            FunctionsMenu.PlayTrackButton.getGameObject().GetComponent<Image>().sprite = Resources.Media_PlayPause;
            FunctionsMenu.StopTrackButton = new QMSingleButton(FunctionsMenu.baseMenu.menuBase, 3, -2, "", new Action(MediaControl.Stop), "Stop the current song completely");
            while ((UnityEngine.Object)Resources.Media_Stop == (UnityEngine.Object)null)
                yield return (object)null;
            FunctionsMenu.StopTrackButton.getGameObject().GetComponent<Image>().sprite = Resources.Media_Stop;
            FunctionsMenu.NextTrackButton = new QMSingleButton(FunctionsMenu.baseMenu.menuBase, 4, -2, "", new Action(MediaControl.NextTrack), "Go to the next song in your Playlist");
            FunctionsMenu.NextTrackButton.getGameObject().GetComponent<Image>().sprite = Resources.Media_Nav;
        }
    }
}
