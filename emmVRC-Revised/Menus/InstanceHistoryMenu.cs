﻿
using emmVRC.Libraries;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using emmVRCLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace emmVRC.Menus
{
    public class InstanceHistoryMenu
    {
        public static PaginatedMenu baseMenu;
        private static QMSingleButton clearInstances;
        private static List<SerializedWorld> previousInstances;

        public static void Initialize()
        {
            InstanceHistoryMenu.baseMenu = new PaginatedMenu(FunctionsMenu.baseMenu.menuBase, 201945, 104894, "Instance\nHistory", "", new Color?());
            InstanceHistoryMenu.baseMenu.menuEntryButton.DestroyMe();
            InstanceHistoryMenu.clearInstances = new QMSingleButton(InstanceHistoryMenu.baseMenu.menuBase, 5, 1, "<color=#FFCCBB>Clear\nInstances</color>", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("Instance History", "Are you sure you want to clear the instance history? All previously joined instances will be lost!", "Yes", (System.Action)(() =>
          {
              InstanceHistoryMenu.previousInstances.Clear();
              InstanceHistoryMenu.SaveInstances();
              InstanceHistoryMenu.LoadMenu();
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
          }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()))), "Clears the instance history of all previous instances");
            InstanceHistoryMenu.previousInstances = new List<SerializedWorld>();
            if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm")))
            {
                InstanceHistoryMenu.previousInstances = new List<SerializedWorld>();
                File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"), Encoding.UTF8.GetBytes(TinyJSON.Encoder.Encode((object)InstanceHistoryMenu.previousInstances, EncodeOptions.PrettyPrint)));
            }
            else
            {
                string jsonString = Encoding.UTF8.GetString(File.ReadAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm")));
                try
                {
                    InstanceHistoryMenu.previousInstances = TinyJSON.Decoder.Decode(jsonString).Make<List<SerializedWorld>>();
                    InstanceHistoryMenu.previousInstances.RemoveAll((Predicate<SerializedWorld>)(a => UnixTime.ToDateTime(a.loggedDateTime) < DateTime.Now.AddDays(-3.0)));
                    InstanceHistoryMenu.SaveInstances();
                }
                catch (Exception ex)
                {
                    Exception exception = new Exception();
                    emmVRCLoader.Logger.LogError("Your instance history file is invalid. It will be wiped.");
                    File.Delete(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"));
                    InstanceHistoryMenu.previousInstances = new List<SerializedWorld>();
                }
            }
        }

        public static void LoadMenu()
        {
            InstanceHistoryMenu.baseMenu.pageItems.Clear();
            foreach (SerializedWorld previousInstance in InstanceHistoryMenu.previousInstances)
            {
                SerializedWorld pastInstance = previousInstance;
                List<PageItem> pageItems = InstanceHistoryMenu.baseMenu.pageItems;
                string name = pastInstance.WorldName + "\n" + InstanceIDUtilities.GetInstanceID(pastInstance.WorldTags);
                System.Action action = (System.Action)(() => VRCFlowManager.prop_VRCFlowManager_0.EnterWorld(pastInstance.WorldID, pastInstance.WorldTags));
                string[] strArray = new string[9]
                {
          pastInstance.WorldName,
          pastInstance.WorldTags.Contains("region(jp)") ? " [JP Region]" : (pastInstance.WorldTags.Contains("region(eu)") ? " [EU Region]" : ""),
          " (",
          InstanceHistoryMenu.PrettifyInstanceType(pastInstance.WorldType),
          "), last joined ",
          null,
          null,
          null,
          null
                };
                DateTime dateTime = UnixTime.ToDateTime(pastInstance.loggedDateTime);
                strArray[5] = dateTime.ToShortDateString();
                strArray[6] = " ";
                dateTime = UnixTime.ToDateTime(pastInstance.loggedDateTime);
                strArray[7] = dateTime.ToShortTimeString();
                strArray[8] = "\nSelect to join";
                string tooltip = string.Concat(strArray);
                PageItem pageItem = new PageItem(name, action, tooltip);
                pageItems.Insert(0, pageItem);
            }
        }

        public static void SaveInstances() => File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/instancehistory.emm"), Encoding.UTF8.GetBytes(TinyJSON.Encoder.Encode((object)InstanceHistoryMenu.previousInstances, EncodeOptions.PrettyPrint)));

        public static IEnumerator EnteredWorld()
        {
            while (true)
            {
                if (RoomManager.field_Internal_Static_ApiWorld_0 != null)
                    goto label_3;
                label_1:
                yield return (object)new WaitForEndOfFrame();
                continue;
            label_3:
                if (RoomManager.field_Internal_Static_ApiWorldInstance_0 == null)
                    goto label_1;
                else
                    break;
            }
            try
            {
                SerializedWorld serializedWorld1 = new SerializedWorld()
                {
                    WorldID = RoomManager.field_Internal_Static_ApiWorld_0.id,
                    WorldTags = RoomManager.field_Internal_Static_ApiWorldInstance_0.instanceId,
                    WorldOwner = RoomManager.field_Internal_Static_ApiWorldInstance_0.ownerId == null ? "" : RoomManager.field_Internal_Static_ApiWorldInstance_0.ownerId,
                    WorldType = RoomManager.field_Internal_Static_ApiWorldInstance_0.type.ToString(),
                    WorldName = RoomManager.field_Internal_Static_ApiWorld_0.name,
                    WorldImageURL = RoomManager.field_Internal_Static_ApiWorld_0.thumbnailImageUrl
                };
                SerializedWorld serializedWorld2 = (SerializedWorld)null;
                foreach (SerializedWorld previousInstance in InstanceHistoryMenu.previousInstances)
                {
                    if (previousInstance.WorldID == serializedWorld1.WorldID && InstanceIDUtilities.GetInstanceID(previousInstance.WorldTags) == InstanceIDUtilities.GetInstanceID(serializedWorld1.WorldTags))
                        serializedWorld2 = previousInstance;
                }
                if (serializedWorld2 != null)
                    InstanceHistoryMenu.previousInstances.Remove(serializedWorld2);
                InstanceHistoryMenu.previousInstances.Add(serializedWorld1);
                InstanceHistoryMenu.SaveInstances();
                InstanceHistoryMenu.LoadMenu();
            }
            catch (Exception ex)
            {
                emmVRCLoader.Logger.LogError(ex.ToString());
            }
        }

        public static string PrettifyInstanceType(string original)
        {
            if (original == "FriendsOfGuests")
                return "Friends+";
            if (original == "FriendsOnly")
                return "Friends";
            if (original == "InvitePlus")
                return "Invite+";
            return !(original == "InviteOnly") ? original : "Invite";
        }
    }
}
