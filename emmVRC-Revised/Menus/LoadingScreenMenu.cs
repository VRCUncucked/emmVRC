﻿
using emmVRC.Libraries;
using System;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace emmVRC.Menus
{
    public class LoadingScreenMenu
    {
        public static GameObject functionsButton;

        public static void Initialize()
        {
            LoadingScreenMenu.functionsButton = UnityEngine.Object.Instantiate<Transform>(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup/ButtonMiddle"), QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Popups/LoadingPopup")).gameObject;
            LoadingScreenMenu.functionsButton.GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, (float)sbyte.MinValue);
            LoadingScreenMenu.functionsButton.SetActive(Configuration.JSONConfig.ForceRestartButtonEnabled);
            LoadingScreenMenu.functionsButton.name = "LoadingFunctionsButton";
            LoadingScreenMenu.functionsButton.GetComponentInChildren<Text>().text = "Force Restart";
            LoadingScreenMenu.functionsButton.GetComponent<UnityEngine.UI.Button>().onClick = new UnityEngine.UI.Button.ButtonClickedEvent();
            LoadingScreenMenu.functionsButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(DelegateSupport.ConvertDelegate<UnityAction>((Delegate)(() => DestructiveActions.ForceRestart())));
        }
    }
}
