﻿
using emmVRC.Libraries;
using emmVRC.Objects;
using MelonLoader;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRC.Core;

namespace emmVRC.Menus
{
    public class PlayerHistoryMenu
    {
        public static PaginatedMenu baseMenu;
        private static PageItem toggleHistory;
        private static PageItem toggleJoinLeaveLog;
        public static List<InstancePlayer> currentPlayers;
        public static int Timeout;
        private static List<PageItem> currentInstancePlayers;

        public static void Initialize()
        {
            PlayerHistoryMenu.baseMenu = new PaginatedMenu(FunctionsMenu.baseMenu.menuBase, 10293, 12934, "Player\nHistory", "If you're reading this, hi!", new UnityEngine.Color?());
            PlayerHistoryMenu.baseMenu.menuEntryButton.DestroyMe();
            PlayerHistoryMenu.toggleHistory = new PageItem("Enable", (System.Action)(() =>
           {
               Configuration.JSONConfig.PlayerHistoryEnable = true;
               Configuration.SaveConfig();
           }), "Disable", (System.Action)(() =>
     {
         Configuration.JSONConfig.PlayerHistoryEnable = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Enables or disables the Player History");
            PlayerHistoryMenu.toggleJoinLeaveLog = new PageItem("Log Join\nand Leaves", (System.Action)(() =>
           {
               Configuration.JSONConfig.LogPlayerJoin = true;
               Configuration.SaveConfig();
           }), "Disable", (System.Action)(() =>
     {
         Configuration.JSONConfig.LogPlayerJoin = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Enables the logging of the names of players to the console and the emmVRC log upon joining and leaving. Please disable this before sending support requests!");
            PlayerHistoryMenu.baseMenu.pageItems.Add(PlayerHistoryMenu.toggleHistory);
            PlayerHistoryMenu.baseMenu.pageItems.Add(PlayerHistoryMenu.toggleJoinLeaveLog);
            PlayerHistoryMenu.currentInstancePlayers = new List<PageItem>();
            PlayerHistoryMenu.currentPlayers = new List<InstancePlayer>();
        }

        public static void ShowMenu()
        {
            PlayerHistoryMenu.toggleHistory.SetToggleState(Configuration.JSONConfig.PlayerHistoryEnable);
            PlayerHistoryMenu.toggleJoinLeaveLog.SetToggleState(Configuration.JSONConfig.LogPlayerJoin);
            if (PlayerHistoryMenu.currentInstancePlayers.Count > 0)
            {
                foreach (PageItem currentInstancePlayer in PlayerHistoryMenu.currentInstancePlayers)
                    PlayerHistoryMenu.baseMenu.pageItems.Remove(currentInstancePlayer);
            }
            PlayerHistoryMenu.currentInstancePlayers = new List<PageItem>();
            foreach (InstancePlayer currentPlayer in PlayerHistoryMenu.currentPlayers)
            {
                InstancePlayer plr = currentPlayer;
                PageItem pageItem = new PageItem(plr.Name, (System.Action)(() =>
               {
                   if (PlayerHistoryMenu.Timeout != 0 || !NetworkConfig.Instance.APICallsAllowed)
                       return;
                   APIUser.FetchUser(plr.UserID, (Il2CppSystem.Action<APIUser>)(System.Action<APIUser>)(usr =>
                 {
                     QuickMenu.prop_QuickMenu_0.field_Private_APIUser_0 = usr;
                     QuickMenu.prop_QuickMenu_0.Method_Public_Void_EnumNPublicSealedvaUnWoAvSoSeUsDeSaCuUnique_Boolean_0((QuickMenu.EnumNPublicSealedvaUnWoAvSoSeUsDeSaCuUnique)4, false);
                 }), (Il2CppSystem.Action<string>)(System.Action<string>)(str => emmVRCLoader.Logger.LogError("API returned an error: " + str)));
                   PlayerHistoryMenu.Timeout = 5;
                   MelonCoroutines.Start(PlayerHistoryMenu.WaitForTimeout());
               }), "Joined " + plr.TimeJoinedStamp);
                PlayerHistoryMenu.baseMenu.pageItems.Add(pageItem);
                PlayerHistoryMenu.currentInstancePlayers.Add(pageItem);
            }
            PlayerHistoryMenu.baseMenu.OpenMenu();
        }

        public static IEnumerator WaitForTimeout()
        {
            for (; PlayerHistoryMenu.Timeout != 0; --PlayerHistoryMenu.Timeout)
                yield return (object)new WaitForSeconds(1f);
        }
    }
}
