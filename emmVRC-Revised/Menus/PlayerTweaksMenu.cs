﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using UnityEngine;
using UnityEngine.UI;
using VRC.Animation;

namespace emmVRC.Menus
{
    internal class PlayerTweaksMenu
    {
        internal static QMNestedButton baseMenu;
        internal static GameObject SpeedText;
        internal static QMSingleButton UnloadDynamicBonesButton;
        internal static QMSingleButton SelectCurrentUserButton;
        internal static QMSingleButton ReloadAllAvatars;
        internal static QMSingleButton AvatarOptionsMenu;
        internal static QMSingleButton EnableJumpButton;
        internal static QMSingleButton WaypointMenu;
        internal static QMToggleButton FlightToggle;
        internal static QMToggleButton NoclipToggle;
        internal static QMToggleButton ESPToggle;
        internal static QMToggleButton SpeedToggle;
        internal static QMSingleButton SpeedReset;
        internal static QMSingleButton SpeedMinusButton;
        internal static QMSingleButton SpeedPlusButton;
        internal static Objects.Slider SpeedSlider;

        internal static void Initialize()
        {
            PlayerTweaksMenu.baseMenu = new QMNestedButton(FunctionsMenu.baseMenu.menuBase, 192948, 102394, "Player\nTweaks", "");
            PlayerTweaksMenu.baseMenu.getMainButton().DestroyMe();
            PlayerTweaksMenu.UnloadDynamicBonesButton = new QMSingleButton(PlayerTweaksMenu.baseMenu, 1, 0, "Remove\nLoaded\nDynamic\nBones", (System.Action)(() =>
           {
               foreach (UnityEngine.Object @object in UnityEngine.Object.FindObjectsOfType<DynamicBone>())
                   UnityEngine.Object.Destroy(@object);
               foreach (UnityEngine.Object @object in UnityEngine.Object.FindObjectsOfType<DynamicBoneCollider>())
                   UnityEngine.Object.Destroy(@object);
           }), "Unload all the current dynamic bones in the instance");
            PlayerTweaksMenu.SelectCurrentUserButton = new QMSingleButton(PlayerTweaksMenu.baseMenu, 3, 0, "Select\nCurrent\nUser", (System.Action)(() => QuickMenuUtils.GetQuickMenuInstance().SelectPlayer(VRCPlayer.field_Internal_Static_VRCPlayer_0._player)), "Selects you as the current user");
            PlayerTweaksMenu.AvatarOptionsMenu = new QMSingleButton(PlayerTweaksMenu.baseMenu, 4, 0, "Avatar\nOptions", (System.Action)(() => AvatarPermissionManager.OpenMenu(VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_ApiAvatar_0.id)), "Allows you to configure permissions for this user's avatar, which includes dynamic bone settings");
            PlayerTweaksMenu.EnableJumpButton = new QMSingleButton(PlayerTweaksMenu.baseMenu, 1, 1, "Jumping", (System.Action)(() =>
           {
               VRCPlayer.field_Internal_Static_VRCPlayer_0.prop_VRCPlayerApi_0.SetJumpImpulse(2.8f);
               PlayerTweaksMenu.EnableJumpButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
           }), "Enables jumping for this world. Requires Risky Functions");
            PlayerTweaksMenu.EnableJumpButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            PlayerTweaksMenu.EnableJumpButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, -96f);
            PlayerTweaksMenu.WaypointMenu = new QMSingleButton(PlayerTweaksMenu.baseMenu, 1, 1, "Waypoints", (System.Action)(() => WaypointsMenu.LoadWaypointList()), "Allows you to teleport to pre-defined waypoints. Requires Risky Functions");
            PlayerTweaksMenu.WaypointMenu.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            PlayerTweaksMenu.WaypointMenu.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, 96f);
            PlayerTweaksMenu.FlightToggle = new QMToggleButton(PlayerTweaksMenu.baseMenu, 2, 1, "Flight", (System.Action)(() => Flight.FlightEnabled = true), "Disabled", (System.Action)(() =>
          {
              Flight.FlightEnabled = false;
              if (!Flight.NoclipEnabled)
                  return;
              Flight.NoclipEnabled = false;
              PlayerTweaksMenu.NoclipToggle.setToggleState(false);
              VRCPlayer.field_Internal_Static_VRCPlayer_0.GetComponent<VRCMotionState>().field_Private_CharacterController_0.enabled = true;
          }), "TOGGLE: Enables flight. Requires Risky Functions");
            PlayerTweaksMenu.NoclipToggle = new QMToggleButton(PlayerTweaksMenu.baseMenu, 3, 1, "Noclip", (System.Action)(() =>
           {
               Flight.NoclipEnabled = true;
               if (Flight.FlightEnabled)
                   return;
               Flight.FlightEnabled = true;
               PlayerTweaksMenu.FlightToggle.setToggleState(true);
           }), "Disabled", (System.Action)(() => Flight.NoclipEnabled = false), "TOGGLE: Enables NoClip. Requires Risky Functions");
            PlayerTweaksMenu.ESPToggle = new QMToggleButton(PlayerTweaksMenu.baseMenu, 4, 1, "ESP On", (System.Action)(() => ESP.ESPEnabled = true), "ESP Off", (System.Action)(() => ESP.ESPEnabled = false), "TOGGLE: Enables ESP for all the players in the instance. Requires Risky Functions");
            PlayerTweaksMenu.ReloadAllAvatars = new QMSingleButton(PlayerTweaksMenu.baseMenu, 5, 1, "Reload\nAll\nAvatars", (System.Action)(() => VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars()), "Reloads all the current avatars in the room");
            PlayerTweaksMenu.SpeedToggle = new QMToggleButton(PlayerTweaksMenu.baseMenu, 4, 2, "Speed On", (System.Action)(() =>
           {
               Speed.SpeedModified = true;
               PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().enabled = true;
               PlayerTweaksMenu.SpeedReset.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
               PlayerTweaksMenu.SpeedMinusButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
               PlayerTweaksMenu.SpeedPlusButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
               Speed.Modifier = PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value;
               PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: " + PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value.ToString("N2");
           }), "Speed Off", (System.Action)(() =>
     {
         Speed.SpeedModified = false;
         PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().enabled = false;
         PlayerTweaksMenu.SpeedReset.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
         PlayerTweaksMenu.SpeedMinusButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
         PlayerTweaksMenu.SpeedPlusButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
         PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: Disabled";
     }), "TOGGLE: Enables the Speed modifier. Requires Risky Functions");
            PlayerTweaksMenu.SpeedSlider = new Objects.Slider(PlayerTweaksMenu.baseMenu.getMenuName(), 1, 2, (System.Action<float>)(val =>
           {
               Speed.Modifier = val;
               if (!Speed.SpeedModified)
                   return;
               PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: " + val.ToString("N2");
           }), 0.5f);
            PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().enabled = false;
            PlayerTweaksMenu.SpeedSlider.slider.GetComponent<RectTransform>().anchoredPosition += new Vector2(256f, -104f);
            PlayerTweaksMenu.SpeedSlider.slider.GetComponent<RectTransform>().sizeDelta *= new Vector2(0.85f, 1f);
            PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().maxValue = Configuration.JSONConfig.MaxSpeedIncrease;
            PlayerTweaksMenu.SpeedReset = new QMSingleButton(PlayerTweaksMenu.baseMenu, 3, 2, "Reset", (System.Action)(() =>
           {
               PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value = 1f;
               Speed.Modifier = PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value;
               PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: " + PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value.ToString("N2");
           }), "Resets the speed modifier to the default value");
            PlayerTweaksMenu.SpeedReset.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            PlayerTweaksMenu.SpeedReset.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, 96f);
            PlayerTweaksMenu.SpeedReset.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
            PlayerTweaksMenu.SpeedMinusButton = new QMSingleButton(PlayerTweaksMenu.baseMenu, 1, 2, "-", (System.Action)(() =>
           {
               PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value -= 0.25f;
               Speed.Modifier = PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value;
               PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: " + PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value.ToString("N2");
           }), "Decrease the speed modifier by 0.25");
            PlayerTweaksMenu.SpeedMinusButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(2.0175f, 2.0175f);
            PlayerTweaksMenu.SpeedMinusButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(-96f, -96f);
            PlayerTweaksMenu.SpeedMinusButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
            PlayerTweaksMenu.SpeedPlusButton = new QMSingleButton(PlayerTweaksMenu.baseMenu, 3, 2, "+", (System.Action)(() =>
           {
               PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value += 0.25f;
               Speed.Modifier = PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value;
               PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: " + PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().value.ToString("N2");
           }), "Increase the speed modifier by 0.25");
            PlayerTweaksMenu.SpeedPlusButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(2.0175f, 2.0175f);
            PlayerTweaksMenu.SpeedPlusButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(96f, -96f);
            PlayerTweaksMenu.SpeedPlusButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
            PlayerTweaksMenu.SpeedText = UnityEngine.Object.Instantiate<GameObject>(((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("QuickMenu_NewElements/_InfoBar/EarlyAccessText").gameObject, PlayerTweaksMenu.SpeedMinusButton.getGameObject().transform.parent);
            PlayerTweaksMenu.SpeedText.GetComponent<Text>().fontStyle = FontStyle.Normal;
            PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: Disabled";
            PlayerTweaksMenu.SpeedText.GetComponent<RectTransform>().anchoredPosition = PlayerTweaksMenu.SpeedMinusButton.getGameObject().GetComponent<RectTransform>().anchoredPosition + new Vector2(192f, 192f);

            PlayerTweaksMenu.WaypointMenu.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            PlayerTweaksMenu.FlightToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            PlayerTweaksMenu.NoclipToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            PlayerTweaksMenu.SpeedToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            PlayerTweaksMenu.ESPToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            PlayerTweaksMenu.SpeedSlider.slider.GetComponent<UnityEngine.UI.Slider>().enabled = true;
            PlayerTweaksMenu.SpeedText.GetComponent<Text>().text = "Speed: Disabled";
            PlayerTweaksMenu.EnableJumpButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
        }
    }
}
