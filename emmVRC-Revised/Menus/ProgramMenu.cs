﻿
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using emmVRCLoader;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace emmVRC.Menus
{
    public class ProgramMenu
    {
        public static PaginatedMenu baseMenu;

        public static void Initialize()
        {
            ProgramMenu.baseMenu = new PaginatedMenu(FunctionsMenu.baseMenu.menuBase, 203945, 102894, "Programs", "", new Color?());
            ProgramMenu.baseMenu.menuEntryButton.DestroyMe();
            List<Program> programList = new List<Program>();
            if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/programs.json")))
            {
                programList = new List<Program>();
                programList.Add(new Program()
                {
                    name = "Notepad",
                    programPath = "C:\\Windows\\notepad.exe",
                    toolTip = "Example program: Launch Notepad. See programs.json for usage"
                });
                File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/programs.json"), Encoder.Encode((object)programList, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
            }
            else
            {
                string jsonString = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/programs.json"));
                try
                {
                    programList = Decoder.Decode(jsonString).Make<List<Program>>();
                }
                catch (Exception ex)
                {
                    emmVRCLoader.Logger.LogError("Your program list file is invalid. Please check JSON validity and try again. Error: " + ex.ToString());
                    Managers.NotificationManager.AddNotification("Your program list file is invalid. Please check JSON validity and try again.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
                }
            }
            foreach (Program program1 in programList)
            {
                Program program = program1;
                ProgramMenu.baseMenu.pageItems.Add(new PageItem(program.name, (System.Action)(() =>
               {
                   try
                   {
                       if (program.programPath != "")
                       {
                           Process.Start("C:\\Windows\\system32\\cmd.exe", "/C " + program.programPath);
                       }
                       else
                       {
                           if (!(program.url != ""))
                               return;
                           MelonCoroutines.Start(ProgramMenu.HTTPGet(program.url, program.name));
                       }
                   }
                   catch (Exception ex)
                   {
                       emmVRCLoader.Logger.LogError("Error occured while launching your program: " + ex.ToString());
                   }
               }), program.toolTip));
            }
        }

        public static IEnumerator HTTPGet(string url, string programName)
        {
            UnityWebRequest request = new UnityWebRequest(url, "GET");
            request.Send();
            while (!request.isDone && !request.isNetworkError && !request.isHttpError)
                yield return (object)new WaitForSeconds(0.1f);
            if (request.isNetworkError || request.isHttpError || request.responseCode != 200L)
                VRCUiPopupManager.prop_VRCUiPopupManager_0.ShowAlert("emmVRC", "The HTTP request for " + programName + " returned: " + request.responseCode.ToString(), 1500f);
        }
    }
}
