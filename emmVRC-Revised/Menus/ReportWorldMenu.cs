﻿
using emmVRC.Libraries;
using System;
using UnityEngine;

namespace emmVRC.Menus
{
    public class ReportWorldMenu
    {
        public static QMNestedButton baseMenu;
        public static QMSingleButton ReportWorldButton;
        public static GameObject RealReportWorldButton;
        public static UnityEngine.UI.Button.ButtonClickedEvent baseReportWorldEvent;

        public static void Initialize()
        {
            ReportWorldMenu.baseMenu = new QMNestedButton("ShortcutMenu", 5, 1, "Report\nWorld", "Report Issues with this World");
            ReportWorldMenu.ReportWorldButton = new QMSingleButton(ReportWorldMenu.baseMenu, 5, 1, "Report\nWorld", (Action)(() => ReportWorldMenu.RealReportWorldButton.GetComponentInChildren<UnityEngine.UI.Button>(true).onClick.Invoke()), "Report Issues with this World (this is the real button)");
            ReportWorldMenu.RealReportWorldButton = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu/ReportWorldButton").gameObject;
            ReportWorldMenu.RealReportWorldButton.SetActive(false);
        }
    }
}
