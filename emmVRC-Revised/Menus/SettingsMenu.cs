﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Network;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using MelonLoader;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using VRC.Core;

namespace emmVRC.Menus
{
    public class SettingsMenu
    {
        public static PaginatedMenu baseMenu;
        public static QMSingleButton ResetSettingsButton;
        public static QMSingleButton ExportAvatarListButton;
        private static PageItem VRFlightControls;
        private static PageItem GlobalDynamicBones;
        private static PageItem FriendGlobalDynamicBones;
        private static PageItem EveryoneGlobalDynamicBones;
        private static PageItem UIExpansionKitIntegration;
        private static PageItem TrackingSaving;
        private static PageItem ActionMenuIntegration;
        private static PageItem EmojiFavouriteMenu;
        private static PageItem RiskyFunctions;
        private static PageItem InfoBar;
        private static PageItem Clock;
        private static PageItem MasterIcon;
        private static PageItem LogoButton;
        private static PageItem HUD;
        private static PageItem ChooseHUD;
        private static PageItem MoveVRHUD;
        private static PageItem ForceRestart;
        private static PageItem UnlimitedFPS;
        private static PageItem emmVRCNetwork;
        private static PageItem GlobalChat;
        private static PageItem AvatarFavoriteList;
        private static PageItem AvatarFavoriteJumpToStart;
        private static PageItem SubmitAvatarPedestals;
        private static PageItem FunctionsMenuPosition;
        private static PageItem LogoPosition;
        private static PageItem NotificationPosition;
        private static ButtonConfigurationMenu FunctionsMenuPositionMenu;
        private static ButtonConfigurationMenu LogoPositionMenu;
        private static ButtonConfigurationMenu NotificationPositionMenu;
        private static PageItem TabMode;
        private static PageItem StealthMode;
        private static PageItem UIColorChanging;
        private static PageItem UIColorChangePickerButton;
        private static ColorPicker UIColorChangePicker;
        private static PageItem UIActionMenuColorChanging;
        private static PageItem UIMicIconColorChanging;
        private static PageItem UIMicIconPulse;
        private static PageItem UIExpansionKitColorChanging;
        private static PageItem InfoSpoofing;
        private static PageItem InfoSpooferNamePicker;
        private static PageItem CameraPlus;
        private static PageItem NameplateColorChanging;
        private static PageItem FriendNameplateColorPickerButton;
        private static ColorPicker FriendNameplateColorPicker;
        private static PageItem VisitorNameplateColorPickerButton;
        private static ColorPicker VisitorNameplateColorPicker;
        private static PageItem NewUserNameplateColorPickerButton;
        private static ColorPicker NewUserNameplateColorPicker;
        private static PageItem UserNameplateColorPickerButton;
        private static ColorPicker UserNameplateColorPicker;
        private static PageItem KnownUserNameplateColorPickerButton;
        private static ColorPicker KnownUserNameplateColorPicker;
        private static PageItem TrustedUserNameplateColorPickerButton;
        private static ColorPicker TrustedUserNameplateColorPicker;
        private static PageItem VeteranUserNameplateColorPickerButton;
        private static ColorPicker VeteranUserNameplateColorPicker;
        private static PageItem LegendaryUserNameplateColorPickerButton;
        private static ColorPicker LegendaryUserNameplateColorPicker;
        private static PageItem DisableReportWorld;
        private static PageItem DisableEmoji;
        private static PageItem DisableEmote;
        private static PageItem DisableRankToggle;
        private static PageItem DisableOldInviteButtons;
        private static PageItem DisablePlaylists;
        private static PageItem DisableAvatarStats;
        private static PageItem DisableReportUser;
        private static PageItem MinimalWarnKick;
        private static PageItem DisableOneHandMovement;
        private static PageItem DisableMicTooltip;
        private static PageItem DisableVRCPlusAds;
        private static PageItem DisableVRCPlusQMButtons;
        private static PageItem DisableVRCPlusMenuTabs;
        private static PageItem DisableVRCPlusUserInfo;
        private static PageItem DisableAvatarHotWorlds;
        private static PageItem DisableAvatarRandomWorlds;
        private static PageItem DisableAvatarPersonalList;
        private static PageItem DisableAvatarLegacyList;
        private static PageItem DisableAvatarPublicList;
        private static PageItem DisableAvatarOtherList;
        private static PageItem EnableKeybinds;
        private static PageItem FlightKeybind;
        private static PageItem NoclipKeybind;
        private static PageItem SpeedKeybind;
        private static PageItem ThirdPersonKeybind;
        private static PageItem ToggleHUDEnabledKeybind;
        private static PageItem RespawnKeybind;
        private static PageItem GoHomeKeybind;
        private static bool canToggleNetwork = true;

        public static void Initialize()
        {
            SettingsMenu.baseMenu = new PaginatedMenu(FunctionsMenu.baseMenu.menuBase, 1024, 768, "Settings", "The base menu for the settings menu", new UnityEngine.Color?());
            SettingsMenu.baseMenu.menuEntryButton.DestroyMe();
            SettingsMenu.ResetSettingsButton = new QMSingleButton(SettingsMenu.baseMenu.menuBase, 5, 1, "<color=#FFCCBB>Revert to\ndefaults</color>", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Are you sure you want to revert settings? You will lose your custom colors and custom button positions!", "Yes", (System.Action)(() =>
          {
              Configuration.JSONConfig = new Objects.Config()
              {
                  WelcomeMessageShown = true
              };
              Configuration.SaveConfig();
              ColorChanger.ApplyIfApplicable();
              Nameplates.colorChanged = true;
              ShortcutMenuButtons.Process();
              emmVRCLoader.Logger.Log("emmVRC settings have been reverted.");
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
          }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()))), "Resets all emmVRC Settings to their default values. This requires a restart to fully take effect");
            SettingsMenu.ExportAvatarListButton = new QMSingleButton(SettingsMenu.baseMenu.menuBase, 5, 0, "Export\nAvatar\nList", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Would you like to export your emmVRC Favorite Avatars?", "Yes", (System.Action)(() =>
          {
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
              List<ExportedAvatar> exportedAvatarList = new List<ExportedAvatar>();
              foreach (ApiAvatar loadedAvatar in CustomAvatarFavorites.LoadedAvatars)
                  exportedAvatarList.Add(new ExportedAvatar()
                  {
                      avatar_id = loadedAvatar.id,
                      avatar_name = loadedAvatar.name
                  });
              File.WriteAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/ExportedList.json"), Encoder.Encode((object)exportedAvatarList, EncodeOptions.PrettyPrint | EncodeOptions.NoTypeHints));
              Managers.NotificationManager.AddNotification("Your emmVRC Favorite list has been exported to UserData/emmVRC/ExportedList.json", "Dismiss", Managers.NotificationManager.DismissCurrentNotification, "", null, Resources.alertSprite);
          }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()))), "Exports your emmVRC Avatar Favorites to a json file");
            SettingsMenu.RiskyFunctions = new PageItem("Risky Functions", (System.Action)(() =>
           {
               if (!Configuration.JSONConfig.RiskyFunctionsWarningShown)
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("Risky Functions", "By agreeing, you accept that the use of these functions could be reported to VRChat, and that you will not use them for malicious purposes.\n", "Agree", (System.Action)(() =>
             {
                 VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
                 Configuration.JSONConfig.RiskyFunctionsEnabled = true;
                 Configuration.JSONConfig.RiskyFunctionsWarningShown = true;
                 Configuration.SaveConfig();
                 SettingsMenu.RefreshMenu();
             }), "Decline", (System.Action)(() =>
       {
           VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
           SettingsMenu.RiskyFunctions.SetToggleState(false);
           SettingsMenu.RefreshMenu();
       }));
               }
               else
               {
                   Configuration.JSONConfig.RiskyFunctionsEnabled = true;
                   Configuration.SaveConfig();
                   SettingsMenu.RefreshMenu();
               }
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.RiskyFunctionsEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         if (Flight.FlightEnabled || Flight.NoclipEnabled)
             PlayerTweaksMenu.FlightToggle.setToggleState(false, true);
         if (Speed.SpeedModified)
             PlayerTweaksMenu.SpeedToggle.setToggleState(false, true);
         if (!ESP.ESPEnabled)
             return;
         PlayerTweaksMenu.ESPToggle.setToggleState(false, true);
     }), "TOGGLE: Enables the Risky Functions, which contains functions that shouldn't be used in public worlds. This includes flight, noclip, speed, and teleporting/waypoints");
            SettingsMenu.VRFlightControls = new PageItem("VR Flight\nControls", (System.Action)(() =>
           {
               Configuration.JSONConfig.VRFlightControls = true;
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.VRFlightControls = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Enables the enhanced VR flight controls. May not work on Windows Mixed Reality");
            SettingsMenu.GlobalDynamicBones = new PageItem("Global\nDynamic Bones", (System.Action)(() =>
           {
               if (!ModCompatibility.MultiplayerDynamicBones)
               {
                   Configuration.JSONConfig.GlobalDynamicBonesEnabled = true;
                   Configuration.SaveConfig();
                   SettingsMenu.RefreshMenu();
                   VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
               }
               else
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Global Dynamic Bones are not compatible with MultiplayerDynamicBones. You must remove this mod to use Global Dynamic Bones.", "Dismiss", new System.Action(VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup));
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.GlobalDynamicBonesEnabled = false;
         Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled = false;
         Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         SettingsMenu.LoadMenu();
         VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
     }), "TOGGLE: Enables the Global Dynamic Bones system");
            SettingsMenu.FriendGlobalDynamicBones = new PageItem("Friend Global\nDynamic Bones", (System.Action)(() =>
           {
               Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled = true;
               Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled = false;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               SettingsMenu.LoadMenu();
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         SettingsMenu.LoadMenu();
         VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
     }), "TOGGLE: Enables Global Dynamic Bones for friends. Note that this might cause lag with lots of friends in a room");
            SettingsMenu.EveryoneGlobalDynamicBones = new PageItem("Everybody Global\nDynamic Bones", (System.Action)(() =>
           {
               Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled = true;
               Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled = false;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               SettingsMenu.LoadMenu();
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         SettingsMenu.LoadMenu();
         VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
     }), "TOGGLE: Enables Global Dynamic Bones for everyone. Note that this might cause lag in large instances");
            SettingsMenu.UIExpansionKitIntegration = new PageItem("UI Expansion\nKit Integration", (System.Action)(() =>
           {
               Configuration.JSONConfig.UIExpansionKitIntegration = true;
               Configuration.SaveConfig();
               if ((UnityEngine.Object)ModCompatibility.FlightButton != (UnityEngine.Object)null)
                   ModCompatibility.FlightButton.SetActive(true);
               if ((UnityEngine.Object)ModCompatibility.NoclipButton != (UnityEngine.Object)null)
                   ModCompatibility.NoclipButton.SetActive(true);
               if ((UnityEngine.Object)ModCompatibility.SpeedButton != (UnityEngine.Object)null)
                   ModCompatibility.SpeedButton.SetActive(true);
               if ((UnityEngine.Object)ModCompatibility.ESPButton != (UnityEngine.Object)null)
                   ModCompatibility.ESPButton.SetActive(true);
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIExpansionKitIntegration = false;
         Configuration.SaveConfig();
         if ((UnityEngine.Object)ModCompatibility.FlightButton != (UnityEngine.Object)null)
             ModCompatibility.FlightButton.SetActive(false);
         if ((UnityEngine.Object)ModCompatibility.NoclipButton != (UnityEngine.Object)null)
             ModCompatibility.NoclipButton.SetActive(false);
         if ((UnityEngine.Object)ModCompatibility.SpeedButton != (UnityEngine.Object)null)
             ModCompatibility.SpeedButton.SetActive(false);
         if ((UnityEngine.Object)ModCompatibility.ESPButton != (UnityEngine.Object)null)
             ModCompatibility.ESPButton.SetActive(false);
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Shows the Risky Functions buttons in the UI Expansion Kit menu", ModCompatibility.UIExpansionKit);
            SettingsMenu.TrackingSaving = new PageItem("Calibration\nSaving", (System.Action)(() =>
           {
               Configuration.JSONConfig.TrackingSaving = true;
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.TrackingSaving = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Saves calibration between avatar switches for Full Body Tracking");
            SettingsMenu.ActionMenuIntegration = new PageItem("Action Menu\nIntegration", (System.Action)(() =>
           {
               Configuration.JSONConfig.ActionMenuIntegration = true;
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.ActionMenuIntegration = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Integrates an emmVRC menu into the Action (Radial) Menu, for easy access");
            SettingsMenu.EmojiFavouriteMenu = new PageItem("Emoji\nFavorites", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(EmojiFavourites.baseMenu.getMenuName())), "Configure your Emoji favorites, accessible from the Action Menu");
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.RiskyFunctions);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.GlobalDynamicBones);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.FriendGlobalDynamicBones);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.EveryoneGlobalDynamicBones);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.VRFlightControls);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.ActionMenuIntegration);
            if (!ModCompatibility.FBTSaver)
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.TrackingSaving);
            else
                SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            if (ModCompatibility.UIExpansionKit)
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIExpansionKitIntegration);
            else
                SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.EmojiFavouriteMenu);
            SettingsMenu.InfoBar = new PageItem("Info Bar", (System.Action)(() =>
           {
               Configuration.JSONConfig.InfoBarDisplayEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.InfoBarDisplayEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enable the info bar in the Quick Menu, which shows the current emmVRC version and network compatibility");
            SettingsMenu.Clock = new PageItem("Clock", (System.Action)(() =>
           {
               Configuration.JSONConfig.ClockEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.ClockEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the clock in the Quick Menu, which shows your computer time, and instance time");
            SettingsMenu.MasterIcon = new PageItem("Master Icon", (System.Action)(() =>
           {
               Configuration.JSONConfig.MasterIconEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.MasterIconEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the crown icon above the instance master");
            SettingsMenu.HUD = new PageItem("HUD", (System.Action)(() =>
           {
               Configuration.JSONConfig.HUDEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.HUDEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the HUD, which shows players in the room and instance information");
            SettingsMenu.ChooseHUD = new PageItem("Show Quick Menu HUD\n in desktop", (System.Action)(() =>
           {
               if (!VRHUD.Initialized)
                   MelonCoroutines.Start(VRHUD.Initialize());
               else
                   VRHUD.ToggleHUDButton.setActive(true);
               DesktopHUD.enabled = false;
               VRHUD.enabled = true;
               Configuration.JSONConfig.VRHUDInDesktop = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         if (!DesktopHUD.Initialized)
             MelonCoroutines.Start(DesktopHUD.Initialize());
         if (VRHUD.Initialized)
             VRHUD.ToggleHUDButton.setActive(false);
         VRHUD.enabled = false;
         DesktopHUD.enabled = true;
         Configuration.JSONConfig.VRHUDInDesktop = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Switch between the Quick Menu HUD or normal HUD while in Desktop Mode");
            SettingsMenu.MoveVRHUD = new PageItem("Closer Quick Menu\nHUD", (System.Action)(() =>
           {
               Configuration.JSONConfig.MoveVRHUDIfSpaceFree = true;
               Configuration.SaveConfig();
               VRHUD.RefreshPosition();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.MoveVRHUDIfSpaceFree = false;
         Configuration.SaveConfig();
         VRHUD.RefreshPosition();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Allows the Quick Menu HUD to move inwards by one row if no emmVRC or Vanilla Buttons occupy the space (requires restart)");
            SettingsMenu.LogoButton = new PageItem("Logo Button", (System.Action)(() =>
           {
               Configuration.JSONConfig.LogoButtonEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.LogoButtonEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Enables the emmVRC Logo on your Quick Menu, that takes you to the Discord.");
            SettingsMenu.ForceRestart = new PageItem("Force Restart\non Loading Screen", (System.Action)(() =>
           {
               Configuration.JSONConfig.ForceRestartButtonEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.ForceRestartButtonEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the Force Restart button on the loading screen, to help free you from softlocks");
            SettingsMenu.UnlimitedFPS = new PageItem("Unlimited FPS", (System.Action)(() =>
           {
               Configuration.JSONConfig.UnlimitedFPSEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UnlimitedFPSEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Changes the VRChat FPS limit to 144, in desktop only.");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.InfoBar);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.Clock);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.MasterIcon);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.HUD);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.ChooseHUD);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.MoveVRHUD);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.LogoButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.ForceRestart);
            }
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UnlimitedFPS);
            if (Configuration.JSONConfig.StealthMode)
            {
                for (int index = 0; index < 8; ++index)
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            }
            SettingsMenu.emmVRCNetwork = new PageItem("emmVRC Network\nEnabled", (System.Action)(() =>
           {
               if (SettingsMenu.canToggleNetwork)
               {
                   Configuration.JSONConfig.emmVRCNetworkEnabled = true;
                   Configuration.SaveConfig();
                   SettingsMenu.RefreshMenu();
                   NetworkClient.InitializeClient();
                   MelonCoroutines.Start(emmVRC.loadNetworked());
               }
               else
               {
                   VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You must wait 5 seconds before toggling the network on again.", "Okay", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                   SettingsMenu.emmVRCNetwork.SetToggleState(false);
               }
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.emmVRCNetworkEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         if (NetworkClient.webToken != null)
             HTTPRequest.get(NetworkClient.baseURL + "/api/authentication/logout").NoAwait("Logout");
         NetworkClient.webToken = (string)null;
         SettingsMenu.canToggleNetwork = false;
         MelonCoroutines.Start(SettingsMenu.WaitForDelayNetwork());
     }), "TOGGLE: Enables the emmVRC Network, which provides more functionality, like Global Chat and Messaging");
            SettingsMenu.GlobalChat = new PageItem("Global Chat", (System.Action)(() =>
           {
               Configuration.JSONConfig.GlobalChatEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.GlobalChatEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the fetching and use of the Global Chat using the emmVRC Network", false);
            SettingsMenu.AvatarFavoriteList = new PageItem("emmVRC\nFavorite List", (System.Action)(() =>
           {
               Configuration.JSONConfig.AvatarFavoritesEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.AvatarFavoritesEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the emmVRC Custom Avatar Favorite list, using the emmVRC Network");
            SettingsMenu.AvatarFavoriteJumpToStart = new PageItem("Jump to\nStart", (System.Action)(() =>
           {
               Configuration.JSONConfig.AvatarFavoritesJumpToStart = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.AvatarFavoritesJumpToStart = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: In the emmVRC Favorites, automatically jumps back to the start when switching pages or reloading favorites");
            SettingsMenu.SubmitAvatarPedestals = new PageItem("Submit Public\nAvatar Pedestals", (System.Action)(() =>
           {
               Configuration.JSONConfig.SubmitAvatarPedestals = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.SubmitAvatarPedestals = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Automatically sends public avatar pedestals in the world to the emmVRC Network to be used for search");
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.emmVRCNetwork);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.AvatarFavoriteList);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.AvatarFavoriteJumpToStart);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.SubmitAvatarPedestals);
            for (int index = 0; index < 5; ++index)
                SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.FunctionsMenuPosition = new PageItem("Functions\nMenu\nButton", (System.Action)(() => SettingsMenu.FunctionsMenuPositionMenu.OpenMenu()), "Allows changing the position of the Functions button");
            SettingsMenu.LogoPosition = new PageItem("Logo\nButton", (System.Action)(() => SettingsMenu.LogoPositionMenu.OpenMenu()), "Allows changing the position of the Logo button");
            SettingsMenu.NotificationPosition = new PageItem("Notification\nButton", (System.Action)(() => SettingsMenu.NotificationPositionMenu.OpenMenu()), "Allows changing the position of the Notification button");
            SettingsMenu.FunctionsMenuPositionMenu = new ButtonConfigurationMenu(SettingsMenu.baseMenu.menuBase.getMenuName(), 312312, 654632, "", (System.Action<ButtonConfigurationMenu>)(menu => SettingsMenu.GetUsedMenuSpaces(menu)), (System.Action<Vector2>)(vec =>
          {
              SettingsMenu.LoadMenu();
              QMSingleButton qmSingleButton = new QMSingleButton("ShortcutMenu", 1, 0, "", (System.Action)null, "");
              FunctionsMenu.baseMenu.menuEntryButton.getGameObject().GetComponent<RectTransform>().anchoredPosition = qmSingleButton.getGameObject().GetComponent<RectTransform>().anchoredPosition;
              qmSingleButton.DestroyMe();
              Configuration.JSONConfig.FunctionsButtonX = Mathf.FloorToInt(vec.x);
              Configuration.JSONConfig.FunctionsButtonY = Mathf.FloorToInt(vec.y);
              Configuration.SaveConfig();
              FunctionsMenu.baseMenu.menuEntryButton.setLocation(Mathf.FloorToInt(vec.x), Mathf.FloorToInt(vec.y));
          }), "", "Select the new position for the Functions button");
            SettingsMenu.LogoPositionMenu = new ButtonConfigurationMenu(SettingsMenu.baseMenu.menuBase.getMenuName(), 520394, 296321, "", (System.Action<ButtonConfigurationMenu>)(menu => SettingsMenu.GetUsedMenuSpaces(menu)), (System.Action<Vector2>)(vec =>
          {
              SettingsMenu.LoadMenu();
              QMSingleButton qmSingleButton = new QMSingleButton("ShortcutMenu", 1, 0, "", (System.Action)null, "");
              qmSingleButton.DestroyMe();
              Configuration.JSONConfig.LogoButtonX = Mathf.FloorToInt(vec.x);
              Configuration.JSONConfig.LogoButtonY = Mathf.FloorToInt(vec.y);
              Configuration.SaveConfig();
          }), "", "Select the new position for the Logo button");
            SettingsMenu.NotificationPositionMenu = new ButtonConfigurationMenu(SettingsMenu.baseMenu.menuBase.getMenuName(), 322222, 255423, "", (System.Action<ButtonConfigurationMenu>)(menu => SettingsMenu.GetUsedMenuSpaces(menu)), (System.Action<Vector2>)(vec =>
          {
              SettingsMenu.LoadMenu();
              QMSingleButton qmSingleButton = new QMSingleButton("ShortcutMenu", 1, 0, "", (System.Action)null, "");
              Managers.NotificationManager.NotificationMenu.getMainButton().getGameObject().GetComponent<RectTransform>().anchoredPosition = qmSingleButton.getGameObject().GetComponent<RectTransform>().anchoredPosition;
              qmSingleButton.DestroyMe();
              Configuration.JSONConfig.NotificationButtonPositionX = Mathf.FloorToInt(vec.x);
              Configuration.JSONConfig.NotificationButtonPositionY = Mathf.FloorToInt(vec.y);
              Configuration.SaveConfig();
              Managers.NotificationManager.NotificationMenu.getMainButton().setLocation(Mathf.FloorToInt(vec.x), Mathf.FloorToInt(vec.y));
              Managers.NotificationManager.AddNotification("Your new button position has been saved.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.alertSprite);
          }), "", "Select the new position for the Notification button");
            SettingsMenu.TabMode = new PageItem("Tab Mode", (System.Action)(() =>
           {
               Configuration.JSONConfig.TabMode = true;
               Configuration.SaveConfig();
               if (FunctionsMenu.baseMenu.menuEntryButton != null)
                   FunctionsMenu.baseMenu.menuEntryButton.getGameObject().transform.localScale = Vector3.zero;
               Managers.NotificationManager.NotificationMenu.getMainButton().getGameObject().transform.localScale = Vector3.zero;
               TabMenu.newTab.transform.localScale = Vector3.one;
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.TabMode = false;
         Configuration.SaveConfig();
         if (FunctionsMenu.baseMenu.menuEntryButton != null)
             FunctionsMenu.baseMenu.menuEntryButton.getGameObject().transform.localScale = Vector3.one;
         Managers.NotificationManager.NotificationMenu.getMainButton().getGameObject().transform.localScale = Vector3.one;
         TabMenu.newTab.transform.localScale = Vector3.zero;
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Enables the emmVRC Tab, which replaces the Shortcut Menu buttons with a menu.");
            SettingsMenu.StealthMode = new PageItem("Stealth Mode", (System.Action)(() => VRCUiPopupManager.prop_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "Are you sure you want to enable stealth mode? This will hide emmVRC from the menus, and you must use the Report World button to access emmVRC", "Yes", (System.Action)(() =>
          {
              Configuration.JSONConfig.StealthMode = true;
              Configuration.SaveConfig();
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You must restart your game for this change to apply.\nRestart now?", "Yes", (System.Action)(() => DestructiveActions.ForceRestart()), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
          }), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()))), "Disabled", (System.Action)(() =>
    {
        Configuration.JSONConfig.StealthMode = false;
        Configuration.SaveConfig();
        VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You must restart your game for this change to apply.\nRestart now?", "Yes", (System.Action)(() => DestructiveActions.ForceRestart()), "No", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
    }), "TOGGLE: Enables emmVRC's stealth mode. This disables most of the visual features, in order to blend in with vanilla VRChat. The Functions button is available in the \"Report World\" menu.");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.FunctionsMenuPosition);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.LogoPosition);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.NotificationPosition);
            }
            else
            {
                for (int index = 0; index < 3; ++index)
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            }
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            if (!Configuration.JSONConfig.StealthMode)
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.TabMode);
            else
                SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.StealthMode);
            SettingsMenu.UIColorChanging = new PageItem("UI Color\nChange", (System.Action)(() =>
           {
               Configuration.JSONConfig.UIColorChangingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               ColorChanger.ApplyIfApplicable();
               if (!ModCompatibility.UIExpansionKit)
                   return;
               MelonCoroutines.Start(ModCompatibility.ColorUIExpansionKit());
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIColorChangingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         ColorChanger.ApplyIfApplicable();
         if (!ModCompatibility.UIExpansionKit)
             return;
         MelonCoroutines.Start(ModCompatibility.ColorUIExpansionKit());
     }), "TOGGLE: Enables the color changing module, which affects UI, ESP, and loading");
            SettingsMenu.UIColorChangePicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1001, 1000, "UI Color", "Select the color for the UI", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.UIColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               ColorChanger.ApplyIfApplicable();
               if (!ModCompatibility.UIExpansionKit)
                   return;
               MelonCoroutines.Start(ModCompatibility.ColorUIExpansionKit());
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.UIColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#0EA6AD")));
            SettingsMenu.UIColorChangePickerButton = new PageItem("Select UI\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.UIColorChangePicker.baseMenu.getMenuName())), "Selects the color splashed across the rest of the UI");
            SettingsMenu.UIActionMenuColorChanging = new PageItem("Action Menu\nChange", (System.Action)(() =>
           {
               Configuration.JSONConfig.UIActionMenuColorChangingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               ColorChanger.ApplyIfApplicable();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIActionMenuColorChangingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         ColorChanger.ApplyIfApplicable();
     }), "TOGGLE: Enables the color changing module for the radial menu");
            SettingsMenu.UIMicIconColorChanging = new PageItem("Muted Icon\nColor Change", (System.Action)(() =>
           {
               Configuration.JSONConfig.UIMicIconColorChangingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               ColorChanger.ApplyIfApplicable();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIMicIconColorChangingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         ColorChanger.ApplyIfApplicable();
     }), "TOGGLE: Enables the color changing module for the Muted Microphone icon on the HUD");
            SettingsMenu.UIMicIconPulse = new PageItem("Muted Icon\nPulsing", (System.Action)(() =>
           {
               Configuration.JSONConfig.UIMicIconPulsingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               ColorChanger.ApplyIfApplicable();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIMicIconPulsingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         ColorChanger.ApplyIfApplicable();
     }), "TOGGLE: Enables or disables VRChat's new pulsating effect on the Microphone icon");
            SettingsMenu.UIExpansionKitColorChanging = new PageItem("UI Expansion\nKit Coloring", (System.Action)(() =>
           {
               Configuration.JSONConfig.UIExpansionKitColorChangingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               VRCUiManager.prop_VRCUiManager_0.QueueHUDMessage("You must restart VRChat for your changes to apply");
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.UIExpansionKitColorChangingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         VRCUiManager.prop_VRCUiManager_0.QueueHUDMessage("You must restart VRChat for your changes to apply");
     }), "TOGGLE: Enables changing the color of UIExpansionKit's menu. Requires a restart to apply");
            SettingsMenu.InfoSpoofing = new PageItem("Local\nInfo Spoofing", (System.Action)(() =>
           {
               Configuration.JSONConfig.InfoSpoofingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.InfoSpoofingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         foreach (Text componentsInChild in QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentsInChildren<Text>())
         {
             if (componentsInChild.text.Contains(NameSpoofGenerator.spoofedName))
                 componentsInChild.text = componentsInChild.text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
         }
         foreach (Text componentsInChild in ((Component)QuickMenuUtils.GetQuickMenuInstance()).gameObject.GetComponentsInChildren<Text>())
         {
             if (componentsInChild.text.Contains(NameSpoofGenerator.spoofedName))
                 componentsInChild.text = componentsInChild.text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
         }
         if (!VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text.Contains(NameSpoofGenerator.spoofedName))
             return;
         VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text = VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
     }), "TOGGLE: Enables local info spoofing, which can protect your identity in screenshots, recordings, and streams");
            SettingsMenu.InfoSpooferNamePicker = new PageItem("Set spoofed\nName", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Enter your spoof name (or none for random)", "", InputField.InputType.Standard, false, "Accept", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((name, keyk, tx) =>
          {
              if (Configuration.JSONConfig.InfoSpoofingEnabled)
              {
                  foreach (Text componentsInChild in QuickMenuUtils.GetVRCUiMInstance().menuContent().GetComponentsInChildren<Text>())
                  {
                      if (componentsInChild.text.Contains(NameSpoofGenerator.spoofedName))
                          componentsInChild.text = componentsInChild.text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
                  }
                  foreach (Text componentsInChild in ((Component)QuickMenuUtils.GetQuickMenuInstance()).gameObject.GetComponentsInChildren<Text>())
                  {
                      if (componentsInChild.text.Contains(NameSpoofGenerator.spoofedName))
                          componentsInChild.text = componentsInChild.text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
                  }
                  if (VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text.Contains(NameSpoofGenerator.spoofedName))
                      VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text = VRCPlayer.field_Internal_Static_VRCPlayer_0.GetNameplateText().text.Replace(NameSpoofGenerator.spoofedName, APIUser.CurrentUser.GetName());
              }
              Configuration.JSONConfig.InfoSpoofingName = name;
              Configuration.SaveConfig();
              SettingsMenu.RefreshMenu();
          }), (Il2CppSystem.Action)null, "Enter spoof name....")), "Allows you to change your spoofed name to one that never changes");
            SettingsMenu.CameraPlus = new PageItem("Camera\nPlus", (System.Action)(() =>
           {
               Configuration.JSONConfig.CameraPlus = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               Hacks.CameraPlus.SetCameraPlus();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.CameraPlus = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         Hacks.CameraPlus.SetCameraPlus();
     }), "TOGGLE: Enables CameraPlus, which adds additional buttons and functions to the VRChat camera");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIColorChanging);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIColorChangePickerButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIActionMenuColorChanging);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIMicIconColorChanging);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIMicIconPulse);
                if (ModCompatibility.UIExpansionKit)
                    SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UIExpansionKitColorChanging);
                else
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.InfoSpoofing);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.InfoSpooferNamePicker);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.CameraPlus);
            }
            SettingsMenu.FriendNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1000, "Friend Nameplate Color", "Select the color for Friend Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.FriendNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.FriendNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#FFFF00")));
            SettingsMenu.VisitorNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1001, "Visitor Nameplate Color", "Select the color for Visitor Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.VisitorNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.VisitorNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#CCCCCC")));
            SettingsMenu.NewUserNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1002, "New User Nameplate Color", "Select the color for New User Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.NewUserNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.NewUserNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#1778FF")));
            SettingsMenu.UserNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1003, "User Nameplate Color", "Select the color for User Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.UserNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.UserNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#2BCE5C")));
            SettingsMenu.KnownUserNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1004, "Known User Nameplate Color", "Select the color for Known User Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.KnownUserNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.KnownUserNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#FF7B42")));
            SettingsMenu.TrustedUserNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1005, "Trusted User Nameplate Color", "Select the color for Trusted User Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.TrustedUserNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#8143E6")));
            SettingsMenu.VeteranUserNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1006, "Veteran User Nameplate Color (OGTrustRanks)", "Select the color for Veteran User Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.VeteranUserNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#ABCDEE")));
            SettingsMenu.LegendaryUserNameplateColorPicker = new ColorPicker(SettingsMenu.baseMenu.menuBase.getMenuName(), 1000, 1007, "Legendary User Nameplate Color (OGTrustRanks)", "Select the color for Legendary User Nameplate colors", (System.Action<UnityEngine.Color>)(newColor =>
           {
               Configuration.JSONConfig.LegendaryUserNamePlateColorHex = ColorConversion.ColorToHex(newColor, true);
               Configuration.SaveConfig();
               SettingsMenu.LoadMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), (System.Action)(() => SettingsMenu.LoadMenu()), new UnityEngine.Color?(ColorConversion.HexToColor(Configuration.JSONConfig.TrustedUserNamePlateColorHex)), new UnityEngine.Color?(ColorConversion.HexToColor("#FF69B4")));
            SettingsMenu.NameplateColorChanging = new PageItem("Nameplate\nColor Changing", (System.Action)(() =>
           {
               Configuration.JSONConfig.NameplateColorChangingEnabled = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               Nameplates.colorChanged = true;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.NameplateColorChangingEnabled = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         Nameplates.colorChanged = true;
         VRCPlayer.field_Internal_Static_VRCPlayer_0.ReloadAllAvatars();
     }), "TOGGLE: Enables the nameplate color changing module, which changes the colors of the various trust ranks in nameplates and the UI");
            SettingsMenu.FriendNameplateColorPickerButton = new PageItem("Friend\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.FriendNameplateColorPicker.baseMenu.getMenuName())), "Select the color for Friend Nameplate colors");
            SettingsMenu.VisitorNameplateColorPickerButton = new PageItem("Visitor\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.VisitorNameplateColorPicker.baseMenu.getMenuName())), "Select the color for Visitor Nameplate colors");
            SettingsMenu.NewUserNameplateColorPickerButton = new PageItem("New User\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.NewUserNameplateColorPicker.baseMenu.getMenuName())), "Select the color for New User Nameplate colors");
            SettingsMenu.UserNameplateColorPickerButton = new PageItem("User\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.UserNameplateColorPicker.baseMenu.getMenuName())), "Select the color for User Nameplate colors");
            SettingsMenu.KnownUserNameplateColorPickerButton = new PageItem("Known User\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.KnownUserNameplateColorPicker.baseMenu.getMenuName())), "Select the color for Known User Nameplate colors");
            SettingsMenu.TrustedUserNameplateColorPickerButton = new PageItem("Trusted User\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.TrustedUserNameplateColorPicker.baseMenu.getMenuName())), "Select the color for Trusted User Nameplate colors");
            SettingsMenu.VeteranUserNameplateColorPickerButton = new PageItem("Veteran User\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.VeteranUserNameplateColorPicker.baseMenu.getMenuName())), "Select the color for Veteran User Nameplate colors (via OGTrustRanks)");
            SettingsMenu.LegendaryUserNameplateColorPickerButton = new PageItem("Legendary User\nNameplate\nColor", (System.Action)(() => QuickMenuUtils.ShowQuickmenuPage(SettingsMenu.LegendaryUserNameplateColorPicker.baseMenu.getMenuName())), "Select the color for Legendary User Nameplate colors (via OGTrustRanks)");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.NameplateColorChanging);
                if (!ModCompatibility.OGTrustRank)
                {
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
                }
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.FriendNameplateColorPickerButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.VisitorNameplateColorPickerButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.NewUserNameplateColorPickerButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.UserNameplateColorPickerButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.KnownUserNameplateColorPickerButton);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.TrustedUserNameplateColorPickerButton);
                if (ModCompatibility.OGTrustRank)
                {
                    SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.VeteranUserNameplateColorPickerButton);
                    SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.LegendaryUserNameplateColorPickerButton);
                }
            }
            SettingsMenu.DisableReportWorld = new PageItem("Disable\nReport World", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableReportWorldButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableReportWorldButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the 'Report World' button in the Quick Menu. Its functionality can be found in the Worlds menu, and the Disabled Buttons menu");
            SettingsMenu.DisableEmoji = new PageItem("Disable\nEmoji", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableEmojiButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableEmojiButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the 'Emoji' button in the Quick Menu. Its functionality can be found in the Disabled Buttons menu, as well as the new Radial menu");
            SettingsMenu.DisableEmote = new PageItem("Disable\nEmote", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableEmoteButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableEmoteButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the 'Emote' button in the Quick Menu. Its functionality can be found in the new Radial menu");
            SettingsMenu.DisableRankToggle = new PageItem("Disable\nRank Toggle", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableRankToggleButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableRankToggleButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the 'Rank Toggle' switch in the Quick Menu. Its functionality can be found in the Disabled Buttons menu");
            SettingsMenu.DisableOldInviteButtons = new PageItem("Disable Old\nInvite Buttons", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableOldInviteButtons = true;
               Configuration.SaveConfig();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableOldInviteButtons = false;
         Configuration.SaveConfig();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the old invite buttons from pre-build 1046 versions of VRChat");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableReportWorld);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableEmoji);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableEmote);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableRankToggle);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableOldInviteButtons);
                for (int index = 0; index < 4; ++index)
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            }
            SettingsMenu.DisableReportUser = new PageItem("Disable\nReport User", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableReportUserButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               UserInteractMenuButtons.Initialize();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableReportUserButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         UserInteractMenuButtons.Initialize();
     }), "TOGGLE: Disables the 'Report User' button in the User Interact Menu. Its functionality can be found in the Social menu");
            SettingsMenu.DisablePlaylists = new PageItem("Disable\nPlaylists", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisablePlaylistsButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               UserInteractMenuButtons.Initialize();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisablePlaylistsButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         UserInteractMenuButtons.Initialize();
     }), "TOGGLE: Disables the 'Playlists' button in the User Interact Menu. Its functionality can be found in the Social menu");
            SettingsMenu.DisableAvatarStats = new PageItem("Disable\nAvatar Stats", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarStatsButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               UserInteractMenuButtons.Initialize();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarStatsButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         UserInteractMenuButtons.Initialize();
     }), "TOGGLE: Disables the 'Avatar Stats' button in the User Interact Menu.");
            SettingsMenu.MinimalWarnKick = new PageItem("Minimal Warn\nKick Button", (System.Action)(() =>
           {
               Configuration.JSONConfig.MinimalWarnKickButton = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               UserInteractMenuButtons.Initialize();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.MinimalWarnKickButton = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         UserInteractMenuButtons.Initialize();
     }), "TOGGLE: Combines the Warn and Kick buttons into one space, to make room for more buttons");
            SettingsMenu.DisableOneHandMovement = new PageItem("Disable\nHand Movement", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableOneHandMovement = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               ActionMenuTweaks.Apply();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableOneHandMovement = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         ActionMenuTweaks.Apply();
     }), "TOGGLE: Disables the one-handed movement indicator when one of your VR Controllers has lost tracking");
            SettingsMenu.DisableMicTooltip = new PageItem("Disable\nMic Tooltip", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableMicTooltip = true;
               Configuration.SaveConfig();
               UnscaledUITweaks.Process();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableMicTooltip = false;
         Configuration.SaveConfig();
         UnscaledUITweaks.Process();
     }), "TOGGLE: Disables the \"Hold V\" tooltip above the microphone indicator in the UI");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisablePlaylists);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableReportUser);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarStats);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.MinimalWarnKick);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableOneHandMovement);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableMicTooltip);
                for (int index = 0; index < 3; ++index)
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            }
            SettingsMenu.DisableVRCPlusAds = new PageItem("Disable VRC+\nQuick Menu Ads", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableVRCPlusAds = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableVRCPlusAds = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the VRChat Plus adverts in the Quick Menu");
            SettingsMenu.DisableVRCPlusQMButtons = new PageItem("Disable VRC+\nQuick Menu Buttons", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableVRCPlusQMButtons = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
               MelonCoroutines.Start(ShortcutMenuButtons.Process());
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableVRCPlusQMButtons = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
         MelonCoroutines.Start(ShortcutMenuButtons.Process());
     }), "TOGGLE: Disables the VRChat Plus buttons in the Quick Menu");
            SettingsMenu.DisableVRCPlusMenuTabs = new PageItem("Disable VRC+\nMenu Tabs", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableVRCPlusMenuTabs = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableVRCPlusMenuTabs = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the VRChat Plus menu tabs, and adjusts the rest of the tabs to fit again");
            SettingsMenu.DisableVRCPlusUserInfo = new PageItem("Disable VRC+\nUser Info", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableVRCPlusUserInfo = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableVRCPlusUserInfo = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the VRChat Plus User Info additions");
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableVRCPlusAds);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableVRCPlusQMButtons);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableVRCPlusMenuTabs);
                SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableVRCPlusUserInfo);
                for (int index = 0; index < 5; ++index)
                    SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            }
            SettingsMenu.DisableAvatarHotWorlds = new PageItem("Disable Hot Avatar\nWorld List", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarHotWorlds = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarHotWorlds = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the 'Hot Avatar Worlds' list in your Avatars menu");
            SettingsMenu.DisableAvatarRandomWorlds = new PageItem("Disable Random\nAvatar World List", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarRandomWorlds = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarRandomWorlds = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the 'Random Avatar Worlds' list in your Avatars menu");
            SettingsMenu.DisableAvatarPersonalList = new PageItem("Disable Personal\nAvatar List", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarPersonal = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarPersonal = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the 'Personal Avatars' list in your Avatars menu");
            SettingsMenu.DisableAvatarLegacyList = new PageItem("Disable Legacy\nAvatar List", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarLegacy = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarLegacy = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the 'Legacy Avatars' list in your Avatars menu");
            SettingsMenu.DisableAvatarPublicList = new PageItem("Disable Public\nAvatar List", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarPublic = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarPublic = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the 'Public Avatars' list in your Avatars menu");
            SettingsMenu.DisableAvatarOtherList = new PageItem("Disable Other\nAvatar List", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarOther = true;
               Configuration.SaveConfig();
               SettingsMenu.RefreshMenu();
           }), "Enabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarOther = false;
         Configuration.SaveConfig();
         SettingsMenu.RefreshMenu();
     }), "TOGGLE: Disables the 'Other' list in your Avatars menu");
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarHotWorlds);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarRandomWorlds);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarPersonalList);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarLegacyList);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarPublicList);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.DisableAvatarOtherList);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.EnableKeybinds = new PageItem("Enable\nKeybinds", (System.Action)(() =>
           {
               Configuration.JSONConfig.EnableKeybinds = true;
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.EnableKeybinds = false;
         Configuration.SaveConfig();
     }), "TOGGLE: Enables or disables the desktop-mode keybinds for emmVRC");
            SettingsMenu.FlightKeybind = new PageItem("Flight\nKeybind:\nLeftCTRL + F", (System.Action)(() => KeybindChanger.Show("Please press a keybind for Flight:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.FlightKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.FlightKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for flying (Default is LeftCTRL + F");
            SettingsMenu.NoclipKeybind = new PageItem("Noclip\nKeybind:\nLeftCTRL + M", (System.Action)(() => KeybindChanger.Show("Please press a keybind for Noclip:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.NoclipKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.NoclipKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for noclip (Default is LeftCTRL + M");
            SettingsMenu.SpeedKeybind = new PageItem("Speed\nKeybind:\nLeftCTRL + G", (System.Action)(() => KeybindChanger.Show("Please press a keybind for Speed:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.SpeedKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.SpeedKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for speed (Default is LeftCTRL + G");
            SettingsMenu.ThirdPersonKeybind = new PageItem("Third\nPerson\nKeybind:\nLeftCTRL + T", (System.Action)(() => KeybindChanger.Show("Please press a keybind for Third Person:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.ThirdPersonKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.ThirdPersonKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for third person (Default is LeftCTRL + T");
            SettingsMenu.ToggleHUDEnabledKeybind = new PageItem("Toggle HUD\nEnabled\nKeybind:\nLeftCTRL + J", (System.Action)(() => KeybindChanger.Show("Please press a keybind for toggling the HUD:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.ToggleHUDEnabledKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.ToggleHUDEnabledKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for toggling the HUD on and off (Default is LeftCTRL + J");
            SettingsMenu.RespawnKeybind = new PageItem("Respawn\nKeybind:\nLeftCTRL + Y", (System.Action)(() => KeybindChanger.Show("Please press a keybind for respawning:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.RespawnKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.RespawnKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for respawning (Default is LeftCTRL + Y");
            SettingsMenu.GoHomeKeybind = new PageItem("Go Home\nKeybind:\nLeftCTRL + U", (System.Action)(() => KeybindChanger.Show("Please press a keybind for going home:", (System.Action<KeyCode, KeyCode>)((mainKey, modifier) =>
          {
              Configuration.JSONConfig.GoHomeKeybind[0] = (int)mainKey;
              Configuration.JSONConfig.GoHomeKeybind[1] = (int)modifier;
              Configuration.SaveConfig();
              SettingsMenu.LoadMenu();
          }), (System.Action)(() => SettingsMenu.LoadMenu()))), "Change the keybind for going to your home world (Default is LeftCTRL + U");
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.EnableKeybinds);
            SettingsMenu.baseMenu.pageItems.Add(PageItem.Space);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.FlightKeybind);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.NoclipKeybind);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.SpeedKeybind);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.ThirdPersonKeybind);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.ToggleHUDEnabledKeybind);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.RespawnKeybind);
            SettingsMenu.baseMenu.pageItems.Add(SettingsMenu.GoHomeKeybind);
            SettingsMenu.baseMenu.pageTitles.Add("Core Features" + (Configuration.JSONConfig.StealthMode ? " (Stealth Mode Enabled)" : ""));
            SettingsMenu.baseMenu.pageTitles.Add("Visual Features" + (Configuration.JSONConfig.StealthMode ? " (Stealth Mode Enabled)" : ""));
            SettingsMenu.baseMenu.pageTitles.Add("Network Features" + (Configuration.JSONConfig.StealthMode ? " (Stealth Mode Enabled)" : ""));
            SettingsMenu.baseMenu.pageTitles.Add("Button Positions" + (Configuration.JSONConfig.StealthMode ? " (Stealth Mode Enabled)" : ""));
            if (!Configuration.JSONConfig.StealthMode)
            {
                SettingsMenu.baseMenu.pageTitles.Add("UI Changing");
                SettingsMenu.baseMenu.pageTitles.Add("Nameplate Color Changing");
                SettingsMenu.baseMenu.pageTitles.Add("Disable Quick Menu Buttons");
                SettingsMenu.baseMenu.pageTitles.Add("Disable Other Features");
                SettingsMenu.baseMenu.pageTitles.Add("Disable VRChat Plus Buttons");
            }
            SettingsMenu.baseMenu.pageTitles.Add("Disable Avatar Menu Lists" + (Configuration.JSONConfig.StealthMode ? " (Stealth Mode Enabled)" : ""));
            SettingsMenu.baseMenu.pageTitles.Add("Keybinds" + (Configuration.JSONConfig.StealthMode ? " (Stealth Mode Enabled)" : ""));
        }

        public static void RefreshMenu()
        {
            try
            {
                SettingsMenu.RiskyFunctions.SetToggleState(Configuration.JSONConfig.RiskyFunctionsEnabled);
                SettingsMenu.VRFlightControls.SetToggleState(Configuration.JSONConfig.VRFlightControls);
                SettingsMenu.GlobalDynamicBones.SetToggleState(Configuration.JSONConfig.GlobalDynamicBonesEnabled);
                SettingsMenu.FriendGlobalDynamicBones.SetToggleState(Configuration.JSONConfig.FriendGlobalDynamicBonesEnabled);
                SettingsMenu.EveryoneGlobalDynamicBones.SetToggleState(Configuration.JSONConfig.EveryoneGlobalDynamicBonesEnabled);
                SettingsMenu.emmVRCNetwork.SetToggleState(Configuration.JSONConfig.emmVRCNetworkEnabled);
                SettingsMenu.GlobalChat.SetToggleState(Configuration.JSONConfig.GlobalChatEnabled);
                SettingsMenu.AvatarFavoriteList.SetToggleState(Configuration.JSONConfig.AvatarFavoritesEnabled);
                SettingsMenu.AvatarFavoriteJumpToStart.SetToggleState(Configuration.JSONConfig.AvatarFavoritesJumpToStart);
                SettingsMenu.SubmitAvatarPedestals.SetToggleState(Configuration.JSONConfig.SubmitAvatarPedestals);
                SettingsMenu.UIExpansionKitIntegration.SetToggleState(Configuration.JSONConfig.UIExpansionKitIntegration);
                SettingsMenu.TrackingSaving.SetToggleState(Configuration.JSONConfig.TrackingSaving);
                SettingsMenu.InfoBar.SetToggleState(Configuration.JSONConfig.InfoBarDisplayEnabled);
                SettingsMenu.Clock.SetToggleState(Configuration.JSONConfig.ClockEnabled);
                SettingsMenu.MasterIcon.SetToggleState(Configuration.JSONConfig.MasterIconEnabled);
                SettingsMenu.HUD.SetToggleState(Configuration.JSONConfig.HUDEnabled);
                SettingsMenu.ChooseHUD.SetToggleState(Configuration.JSONConfig.VRHUDInDesktop);
                SettingsMenu.MoveVRHUD.SetToggleState(Configuration.JSONConfig.MoveVRHUDIfSpaceFree);
                SettingsMenu.LogoButton.SetToggleState(Configuration.JSONConfig.LogoButtonEnabled);
                SettingsMenu.ForceRestart.SetToggleState(Configuration.JSONConfig.ForceRestartButtonEnabled);
                SettingsMenu.UnlimitedFPS.SetToggleState(Configuration.JSONConfig.UnlimitedFPSEnabled);
                if (!Configuration.JSONConfig.StealthMode)
                {
                    SettingsMenu.LogoPosition.Active = !Configuration.JSONConfig.TabMode;
                    SettingsMenu.FunctionsMenuPosition.Active = !Configuration.JSONConfig.TabMode;
                    SettingsMenu.NotificationPosition.Active = !Configuration.JSONConfig.TabMode;
                }
                SettingsMenu.TabMode.SetToggleState(Configuration.JSONConfig.TabMode);
                SettingsMenu.StealthMode.SetToggleState(Configuration.JSONConfig.StealthMode);
                SettingsMenu.UIColorChanging.SetToggleState(Configuration.JSONConfig.UIColorChangingEnabled);
                SettingsMenu.UIActionMenuColorChanging.SetToggleState(Configuration.JSONConfig.UIActionMenuColorChangingEnabled);
                SettingsMenu.UIMicIconColorChanging.SetToggleState(Configuration.JSONConfig.UIMicIconColorChangingEnabled);
                SettingsMenu.UIMicIconPulse.SetToggleState(Configuration.JSONConfig.UIMicIconPulsingEnabled);
                SettingsMenu.UIExpansionKitColorChanging.SetToggleState(Configuration.JSONConfig.UIExpansionKitColorChangingEnabled);
                SettingsMenu.InfoSpoofing.SetToggleState(Configuration.JSONConfig.InfoSpoofingEnabled);
                SettingsMenu.InfoSpooferNamePicker.Name = "Set\nSpoofed\nName";
                SettingsMenu.CameraPlus.SetToggleState(Configuration.JSONConfig.CameraPlus);
                SettingsMenu.NameplateColorChanging.SetToggleState(Configuration.JSONConfig.NameplateColorChangingEnabled);
                SettingsMenu.DisableReportWorld.SetToggleState(Configuration.JSONConfig.DisableReportWorldButton);
                SettingsMenu.DisableEmoji.SetToggleState(Configuration.JSONConfig.DisableEmojiButton);
                SettingsMenu.DisableEmote.SetToggleState(Configuration.JSONConfig.DisableEmoteButton);
                SettingsMenu.DisableRankToggle.SetToggleState(Configuration.JSONConfig.DisableRankToggleButton);
                SettingsMenu.DisableOldInviteButtons.SetToggleState(Configuration.JSONConfig.DisableOldInviteButtons);
                SettingsMenu.DisablePlaylists.SetToggleState(Configuration.JSONConfig.DisablePlaylistsButton);
                SettingsMenu.DisableAvatarStats.SetToggleState(Configuration.JSONConfig.DisableAvatarStatsButton);
                SettingsMenu.DisableReportUser.SetToggleState(Configuration.JSONConfig.DisableReportUserButton);
                SettingsMenu.MinimalWarnKick.SetToggleState(Configuration.JSONConfig.MinimalWarnKickButton);
                SettingsMenu.DisableOneHandMovement.SetToggleState(Configuration.JSONConfig.DisableOneHandMovement);
                SettingsMenu.DisableMicTooltip.SetToggleState(Configuration.JSONConfig.DisableMicTooltip);
                SettingsMenu.DisableVRCPlusAds.SetToggleState(Configuration.JSONConfig.DisableVRCPlusAds);
                SettingsMenu.DisableVRCPlusQMButtons.SetToggleState(Configuration.JSONConfig.DisableVRCPlusQMButtons);
                SettingsMenu.DisableVRCPlusMenuTabs.SetToggleState(Configuration.JSONConfig.DisableVRCPlusMenuTabs);
                SettingsMenu.DisableVRCPlusUserInfo.SetToggleState(Configuration.JSONConfig.DisableVRCPlusUserInfo);
                SettingsMenu.DisableAvatarHotWorlds.SetToggleState(Configuration.JSONConfig.DisableAvatarHotWorlds);
                SettingsMenu.DisableAvatarRandomWorlds.SetToggleState(Configuration.JSONConfig.DisableAvatarRandomWorlds);
                SettingsMenu.DisableAvatarPersonalList.SetToggleState(Configuration.JSONConfig.DisableAvatarPersonal);
                SettingsMenu.DisableAvatarLegacyList.SetToggleState(Configuration.JSONConfig.DisableAvatarLegacy);
                SettingsMenu.DisableAvatarPublicList.SetToggleState(Configuration.JSONConfig.DisableAvatarPublic);
                SettingsMenu.DisableAvatarOtherList.SetToggleState(Configuration.JSONConfig.DisableAvatarOther);
                SettingsMenu.EnableKeybinds.SetToggleState(Configuration.JSONConfig.EnableKeybinds);
                SettingsMenu.FlightKeybind.Name = "Flight:\n" + (Configuration.JSONConfig.FlightKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.FlightKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.FlightKeybind[0]);
                SettingsMenu.NoclipKeybind.Name = "Noclip:\n" + (Configuration.JSONConfig.NoclipKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.NoclipKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.NoclipKeybind[0]);
                SettingsMenu.SpeedKeybind.Name = "Speed:\n" + (Configuration.JSONConfig.SpeedKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.SpeedKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.SpeedKeybind[0]);
                SettingsMenu.ThirdPersonKeybind.Name = "Third\nPerson:\n" + (Configuration.JSONConfig.ThirdPersonKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.ThirdPersonKeybind[0]);
                SettingsMenu.ToggleHUDEnabledKeybind.Name = "Toggle HUD\nEnabled:\n" + (Configuration.JSONConfig.ToggleHUDEnabledKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.ToggleHUDEnabledKeybind[0]);
                SettingsMenu.RespawnKeybind.Name = "Respawn:\n" + (Configuration.JSONConfig.RespawnKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.RespawnKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.RespawnKeybind[0]);
                SettingsMenu.GoHomeKeybind.Name = "Go Home:\n" + (Configuration.JSONConfig.GoHomeKeybind[1] != 0 ? KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.GoHomeKeybind[1]) + "+" : "") + KeyCodeConversion.Stringify((KeyCode)Configuration.JSONConfig.GoHomeKeybind[0]);
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("Error: " + ex.ToString());
            }
        }

        public static void GetUsedMenuSpaces(ButtonConfigurationMenu menu)
        {
            List<KeyValuePair<string, Vector2>> newDisabledButtons = new List<KeyValuePair<string, Vector2>>()
      {
        new KeyValuePair<string, Vector2>("Worlds", new Vector2(1f, 0.0f)),
        new KeyValuePair<string, Vector2>("Avatar", new Vector2(2f, 0.0f)),
        new KeyValuePair<string, Vector2>("Social", new Vector2(3f, 0.0f)),
        new KeyValuePair<string, Vector2>("Safety", new Vector2(4f, 0.0f)),
        new KeyValuePair<string, Vector2>("Go Home", new Vector2(1f, 1f)),
        new KeyValuePair<string, Vector2>("Respawn", new Vector2(2f, 1f)),
        new KeyValuePair<string, Vector2>("Seated\nStanding\nPlay", new Vector2(3f, 1f)),
        new KeyValuePair<string, Vector2>("Settings", new Vector2(4f, 1f)),
        new KeyValuePair<string, Vector2>("UI\nElements", new Vector2(1f, 2f)),
        new KeyValuePair<string, Vector2>("Camera", new Vector2(2f, 2f))
      };
            if (!Configuration.JSONConfig.DisableEmoteButton)
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("Emote", new Vector2(3f, 2f)));
            if (!Configuration.JSONConfig.DisableEmojiButton)
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("Emoji", new Vector2(4f, 2f)));
            if (!Configuration.JSONConfig.DisableRankToggleButton)
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("Toggle\nRank", new Vector2(5f, 2f)));
            if (!Configuration.JSONConfig.DisableReportWorldButton)
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("Report\nWorld", new Vector2(5f, 1f)));
            if (!menu.menuTitle.Contains("Functions"))
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("<color=#FF69B4>emmVRC</color>\nFunctions", new Vector2((float)Configuration.JSONConfig.FunctionsButtonX, (float)Configuration.JSONConfig.FunctionsButtonY)));
            if (!menu.menuTitle.Contains("Logo") && Configuration.JSONConfig.LogoButtonEnabled)
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("<color=#FF69B4>emmVRC</color>\nLogo", new Vector2((float)Configuration.JSONConfig.LogoButtonX, (float)Configuration.JSONConfig.LogoButtonY)));
            if (!menu.menuTitle.Contains("Notification"))
                newDisabledButtons.Add(new KeyValuePair<string, Vector2>("<color=#FF69B4>emmVRC</color>\nNotification", new Vector2((float)Configuration.JSONConfig.NotificationButtonPositionX, (float)Configuration.JSONConfig.NotificationButtonPositionY)));
            menu.ChangeDisabledButtons(newDisabledButtons);
        }

        public static void LoadMenu()
        {
            SettingsMenu.RefreshMenu();
            SettingsMenu.baseMenu.OpenMenu();
        }

        public static IEnumerator WaitForDelayNetwork()
        {
            yield return (object)new WaitForSeconds(5f);
            SettingsMenu.canToggleNetwork = true;
        }
    }
}
