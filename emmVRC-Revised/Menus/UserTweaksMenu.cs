﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects;
using VRC.Core;

namespace emmVRC.Menus
{
    public class UserTweaksMenu
    {
        public static QMNestedButton UserTweaks;
        public static QMSingleButton AvatarPermissions;
        public static QMSingleButton UserPermissions;
        public static QMSingleButton PlayerNotesButton;
        public static QMSingleButton TeleportButton;
        public static QMSingleButton SendMessageButton;
        public static QMSingleButton FavoriteAvatarButton;
        public static QMSingleButton BlockUserButton;

        public static void Initialize()
        {
            UserTweaksMenu.UserTweaks = new QMNestedButton("UserInteractMenu", Configuration.JSONConfig.PlayerActionsButtonX, Configuration.JSONConfig.PlayerActionsButtonY, "<color=#FF69B4>emmVRC</color>\nActions", "Provides functions and tweaks with the selected user");
            UserTweaksMenu.AvatarPermissions = new QMSingleButton(UserTweaksMenu.UserTweaks, 1, 0, "Avatar\nOptions", (System.Action)(() => AvatarPermissionManager.OpenMenu(QuickMenuUtils.GetQuickMenuInstance().field_Private_Player_0._vrcplayer.prop_ApiAvatar_0.id, true)), "Allows you to configure permissions for this user's avatar, which includes dynamic bone settings");
            UserTweaksMenu.UserPermissions = new QMSingleButton(UserTweaksMenu.UserTweaks, 2, 0, "User\nOptions", (System.Action)(() => UserPermissionManager.OpenMenu(QuickMenuUtils.GetQuickMenuInstance().field_Private_APIUser_0.id)), "Allows you to configure permissions for this user, which includes enabling Global Dynamic Bones");
            UserTweaksMenu.PlayerNotesButton = new QMSingleButton(UserTweaksMenu.UserTweaks, 3, 0, "Player\nNotes", (System.Action)(() => PlayerNotes.LoadNote(QuickMenuUtils.GetQuickMenuInstance().field_Private_APIUser_0.id, QuickMenuUtils.GetQuickMenuInstance().field_Private_APIUser_0.GetName())), "Allows you to write a note for the specified user");
            UserTweaksMenu.TeleportButton = new QMSingleButton(UserTweaksMenu.UserTweaks, 4, 0, "Teleport\nto player", (System.Action)(() =>
           {
               if (!Configuration.JSONConfig.RiskyFunctionsEnabled)
                   return;
               VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position = QuickMenuUtils.GetQuickMenuInstance().field_Private_Player_0._vrcplayer.transform.position;
           }), "Teleports to the selected player. Requires risky functions");
            UserTweaksMenu.FavoriteAvatarButton = new QMSingleButton(UserTweaksMenu.UserTweaks, 1, 1, "Favorite\nAvatar", (System.Action)(() =>
           {
               {
                   bool flag = false;
                   if (QuickMenuUtils.GetQuickMenuInstance().field_Private_Player_0.prop_ApiAvatar_0.releaseStatus == "public")
                   {
                       foreach (ApiModel loadedAvatar in CustomAvatarFavorites.LoadedAvatars)
                       {
                           if (loadedAvatar.id == QuickMenuUtils.GetQuickMenuInstance().field_Private_Player_0.prop_ApiAvatar_0.id)
                               flag = true;
                       }
                       if (flag)
                           VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "You already have this avatar favorited", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
                       else
                           CustomAvatarFavorites.FavoriteAvatar(QuickMenuUtils.GetQuickMenuInstance().field_Private_Player_0.prop_ApiAvatar_0).NoAwait("FavoriteAvatar");
                   }
                   else
                       VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "This avatar is not public, or the user does not have cloning turned on.", "Dismiss", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup()));
               }
           }), "Allows you to favorite this user's avatar, if the avatar is public and cloning is on");
        }

        public static void SetRiskyFuncsAllowed(bool status) => UserTweaksMenu.TeleportButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = status;
    }
}
