﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Network;
using emmVRC.Objects;
using MelonLoader;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using VRC;
using VRC.Core;

namespace emmVRC.Menus
{
    public class VRHUD
    {
        private static Transform ShortcutMenu;
        private static GameObject BackgroundObject;
        private static GameObject TextObject;
        private static GameObject LogoIconContainer;
        private static Image BackgroundImage;
        private static Image emmLogo;
        private static Text TextText;
        public static bool enabled = true;
        public static bool Initialized;
        public static QMSingleButton ToggleHUDButton;

        public static IEnumerator Initialize()
        {
            emmVRCLoader.Logger.Log("[emmVRC] Initializing Quickmenu HUD");
            while ((UnityEngine.Object)Resources.HUD_Base == (UnityEngine.Object)null)
                yield return (object)null;
            VRHUD.ShortcutMenu = ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find("ShortcutMenu");
            VRHUD.BackgroundObject = new GameObject("Background");
            VRHUD.BackgroundObject.transform.SetParent(VRHUD.ShortcutMenu, false);
            VRHUD.BackgroundObject.AddComponent<CanvasRenderer>();
            VRHUD.BackgroundImage = VRHUD.BackgroundObject.AddComponent<Image>();
            if (Configuration.JSONConfig.MoveVRHUDIfSpaceFree && Configuration.JSONConfig.DisableRankToggleButton && Configuration.JSONConfig.DisableReportWorldButton && (Configuration.JSONConfig.FunctionsButtonX != 5 || Configuration.JSONConfig.TabMode))
            {
                if (!Configuration.JSONConfig.LogoButtonEnabled || Configuration.JSONConfig.LogoButtonX != 5 || Configuration.JSONConfig.TabMode)
                {
                    RectTransform component = VRHUD.BackgroundObject.GetComponent<RectTransform>();
                    component.sizeDelta = new Vector2(900f, 1920f);
                    component.anchoredPosition = new Vector2(1290f, 290f);
                }
                else
                {
                    RectTransform component = VRHUD.BackgroundObject.GetComponent<RectTransform>();
                    component.sizeDelta = new Vector2(900f, 1920f);
                    component.anchoredPosition = new Vector2(1745f, 290f);
                }
            }
            else
            {
                RectTransform component = VRHUD.BackgroundObject.GetComponent<RectTransform>();
                component.sizeDelta = new Vector2(900f, 1920f);
                component.anchoredPosition = new Vector2(1745f, 290f);
            }
            VRHUD.BackgroundImage.sprite = Resources.HUD_Base;
            VRHUD.TextObject = new GameObject("Text");
            VRHUD.TextObject.transform.SetParent(VRHUD.BackgroundObject.transform, false);
            VRHUD.TextObject.AddComponent<CanvasRenderer>();
            VRHUD.TextText = VRHUD.TextObject.AddComponent<Text>();
            VRHUD.TextObject.GetComponent<RectTransform>().sizeDelta = new Vector2(860f, 1920f);
            RectTransform component1 = VRHUD.TextObject.GetComponent<RectTransform>();
            component1.localPosition = component1.localPosition - new Vector3(0.0f, 10f, 0.0f);
            VRHUD.TextText.font = UnityEngine.Resources.GetBuiltinResource<Font>("Arial.ttf");
            VRHUD.TextText.fontSize = 40;
            VRHUD.TextText.text = "";
            VRHUD.ToggleHUDButton = new QMSingleButton("ShortcutMenu", 0, 0, "", (System.Action)(() => VRHUD.enabled = !VRHUD.enabled), "Toggles the Quick Menu HUD", new UnityEngine.Color?(UnityEngine.Color.grey));
            GameObject gameObject = VRHUD.ToggleHUDButton.getGameObject();
            RectTransform component2 = gameObject.GetComponent<RectTransform>();
            component2.sizeDelta = new Vector2(168f, 168f);
            component2.anchoredPosition = new Vector2(1590f, 60f);
            gameObject.GetComponentInChildren<Image>().sprite = Resources.onlineSprite;
            VRHUD.LogoIconContainer = new GameObject("emmHUDLogo");
            VRHUD.LogoIconContainer.AddComponent<CanvasRenderer>();
            VRHUD.LogoIconContainer.transform.SetParent(VRHUD.BackgroundObject.transform, false);
            VRHUD.emmLogo = VRHUD.LogoIconContainer.AddComponent<Image>();
            VRHUD.emmLogo.sprite = Resources.emmHUDLogo;
            VRHUD.emmLogo.GetComponent<RectTransform>().localPosition = (Vector3)new Vector2(-365f, 880f);
            VRHUD.emmLogo.GetComponent<RectTransform>().localScale = new Vector3(1.3f, 1.3f, 1.3f);
            VRHUD.Initialized = true;
            MelonCoroutines.Start(VRHUD.Loop());
        }

        public static void RefreshPosition()
        {
            if (Configuration.JSONConfig.MoveVRHUDIfSpaceFree && Configuration.JSONConfig.DisableRankToggleButton && (Configuration.JSONConfig.DisableReportWorldButton && Configuration.JSONConfig.FunctionsButtonX != 5))
            {
                if (!Configuration.JSONConfig.LogoButtonEnabled || Configuration.JSONConfig.LogoButtonX != 5)
                    VRHUD.BackgroundObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(1290f, 290f);
                else
                    VRHUD.BackgroundObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(1745f, 290f);
            }
            else
                VRHUD.BackgroundObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(1745f, 290f);
        }

        private static IEnumerator Loop()
        {
            while (true)
            {
                do
                {
                    do
                    {
                        if (Configuration.JSONConfig.HUDEnabled && VRHUD.enabled && !VRHUD.BackgroundObject.activeSelf)
                            VRHUD.BackgroundObject.SetActive(true);
                        else if (!Configuration.JSONConfig.HUDEnabled && VRHUD.BackgroundObject.activeSelf || !VRHUD.enabled && VRHUD.BackgroundObject.activeSelf)
                            VRHUD.BackgroundObject.SetActive(false);
                        yield return (object)new WaitForEndOfFrame();
                    }
                    while (!(bool)(UnityEngine.Object)VRHUD.TextText);
                    string str1 = CommonHUD.RenderPlayerList();
                    Text textText = VRHUD.TextText;
                    string[] strArray = new string[12]
                    {
            "\n            <color=#FF69B4>emmVRC</color> v",
            Attributes.Version,
            "\n\n\nUsers in room",
            !((UnityEngine.Object) PlayerManager.prop_PlayerManager_0 != (UnityEngine.Object) null) || RoomManager.field_Internal_Static_ApiWorldInstance_0 == null ? "" : " (" + PlayerManager.field_Private_Static_PlayerManager_0.field_Private_List_1_Player_0.Count.ToString() + ")",
            ":\n",
            str1,
            "\n\nPosition in world:\n",
            CommonHUD.RenderWorldInfo(),
            "\n\n",
            Configuration.JSONConfig.emmVRCNetworkEnabled ? (NetworkClient.webToken != null ? "<color=lime>Connected to the\nemmVRC Network</color>" : "<color=red>Not connected to the\nemmVRC Network</color>") : "",
            "\n\n",
            null
                    };
                    string str2;
                    if (!Attributes.Debug)
                        str2 = "";
                    else
                        str2 = "Current frame time: " + FrameTimeCalculator.frameTimes[FrameTimeCalculator.iterator == 0 ? FrameTimeCalculator.frameTimes.Length - 1 : FrameTimeCalculator.iterator - 1].ToString() + "ms\nAverage frame time: " + FrameTimeCalculator.frameTimeAvg.ToString() + "ms\n";
                    strArray[11] = str2;
                    string str3 = string.Concat(strArray);
                    textText.text = str3;
                }
                while (APIUser.CurrentUser == null || !Configuration.JSONConfig.InfoSpoofingEnabled);
                VRHUD.TextText.text = VRHUD.TextText.text.Replace(APIUser.CurrentUser.GetName(), NameSpoofGenerator.spoofedName);
            }
        }
    }
}
