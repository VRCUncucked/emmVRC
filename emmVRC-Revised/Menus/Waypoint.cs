﻿
namespace emmVRC.Menus
{
    public class Waypoint
    {
        public string Name = "";
        public float x;
        public float y;
        public float z;
        public float rx;
        public float ry;
        public float rz;
        public float rw;
    }
}
