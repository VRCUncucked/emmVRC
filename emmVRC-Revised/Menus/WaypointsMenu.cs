﻿
using emmVRC.Libraries;
using emmVRC.TinyJSON;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace emmVRC.Menus
{
    public class WaypointsMenu
    {
        public static QMNestedButton baseMenu;
        private static QMSingleButton waypoint1Button;
        private static QMSingleButton waypoint2Button;
        private static QMSingleButton waypoint3Button;
        private static QMSingleButton waypoint4Button;
        private static QMSingleButton waypoint5Button;
        private static QMSingleButton waypoint6Button;
        private static QMSingleButton waypoint7Button;
        private static QMSingleButton waypoint8Button;
        public static QMNestedButton selectedWaypointMenu;
        private static QMSingleButton teleportButton;
        private static QMSingleButton renameButton;
        private static QMSingleButton setLocationButton;
        private static QMSingleButton removeButton;
        private static int selectedWaypoint = 0;
        private static string currentWorldID = "";
        private static List<Waypoint> worldWaypoints = new List<Waypoint>();

        public static void Initialize()
        {
            WaypointsMenu.baseMenu = new QMNestedButton(PlayerTweaksMenu.baseMenu, 19293, 10234, "Waypoints", "");
            WaypointsMenu.baseMenu.getMainButton().DestroyMe();
            WaypointsMenu.waypoint1Button = new QMSingleButton(WaypointsMenu.baseMenu, 1, 0, "Waypoint\n1", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 0;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint2Button = new QMSingleButton(WaypointsMenu.baseMenu, 2, 0, "Waypoint\n2", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 1;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint3Button = new QMSingleButton(WaypointsMenu.baseMenu, 3, 0, "Waypoint\n3", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 2;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint4Button = new QMSingleButton(WaypointsMenu.baseMenu, 4, 0, "Waypoint\n4", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 3;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint5Button = new QMSingleButton(WaypointsMenu.baseMenu, 1, 1, "Waypoint\n5", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 4;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint6Button = new QMSingleButton(WaypointsMenu.baseMenu, 2, 1, "Waypoint\n6", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 5;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint7Button = new QMSingleButton(WaypointsMenu.baseMenu, 3, 1, "Waypoint\n7", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 6;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.waypoint8Button = new QMSingleButton(WaypointsMenu.baseMenu, 4, 1, "Waypoint\n8", (System.Action)(() =>
           {
               WaypointsMenu.selectedWaypoint = 7;
               WaypointsMenu.LoadWaypointOptions();
           }), "View options for this waypoint");
            WaypointsMenu.selectedWaypointMenu = new QMNestedButton(WaypointsMenu.baseMenu, 1024, 768, "Selected Waypoint", "");
            WaypointsMenu.selectedWaypointMenu.getMainButton().DestroyMe();
            WaypointsMenu.teleportButton = new QMSingleButton(WaypointsMenu.selectedWaypointMenu, 1, 0, "Teleport", (System.Action)(() =>
           {
               VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position = new Vector3(WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].x, WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].y, WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].z);
               VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation = new Quaternion(WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].rx, WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].ry, WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].rz, WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].rw);
           }), "Teleport to this waypoint");
            WaypointsMenu.renameButton = new QMSingleButton(WaypointsMenu.selectedWaypointMenu, 2, 0, "Rename", (System.Action)(() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Type a name (or none for default)", "", InputField.InputType.Standard, false, "Accept", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((name, keyk, tx) =>
          {
              WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].Name = name;
              WaypointsMenu.SaveWaypoints();
          }), (Il2CppSystem.Action)null, "Enter name...")), "Rename this waypoint (currently unnamed)");
            WaypointsMenu.setLocationButton = new QMSingleButton(WaypointsMenu.selectedWaypointMenu, 3, 0, "Set\nLocation", (System.Action)(() =>
           {
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].x = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.x;
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].y = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.y;
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].z = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.position.z;
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].rx = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.x;
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].ry = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.y;
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].rz = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.z;
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].rw = VRCPlayer.field_Internal_Static_VRCPlayer_0.transform.rotation.w;
               WaypointsMenu.SaveWaypoints();
               WaypointsMenu.teleportButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
               WaypointsMenu.renameButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
               WaypointsMenu.removeButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
           }), "Set the waypoint location");
            WaypointsMenu.removeButton = new QMSingleButton(WaypointsMenu.selectedWaypointMenu, 4, 0, "Remove", (System.Action)(() =>
           {
               WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint] = new Waypoint();
               WaypointsMenu.SaveWaypoints();
               WaypointsMenu.teleportButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
               WaypointsMenu.renameButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
           }), "Remove this waypoint");
        }

        public static IEnumerator LoadWorld()
        {
            while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                yield return (object)new WaitForEndOfFrame();
            WaypointsMenu.currentWorldID = RoomManager.field_Internal_Static_ApiWorld_0.id;
            if (!Directory.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/Waypoints")))
                Directory.CreateDirectory(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/Waypoints"));
            if (File.Exists(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/Waypoints/" + WaypointsMenu.currentWorldID + ".json")))
                WaypointsMenu.worldWaypoints = Decoder.Decode(File.ReadAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/Waypoints/" + WaypointsMenu.currentWorldID + ".json"))).Make<List<Waypoint>>();
            else
                WaypointsMenu.worldWaypoints = new List<Waypoint>()
        {
          new Waypoint(),
          new Waypoint(),
          new Waypoint(),
          new Waypoint(),
          new Waypoint(),
          new Waypoint(),
          new Waypoint(),
          new Waypoint()
        };
        }

        public static void LoadWaypointList()
        {
            WaypointsMenu.waypoint1Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[0].Name) ? "Waypoint\n1" : WaypointsMenu.worldWaypoints[0].Name);
            WaypointsMenu.waypoint2Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[1].Name) ? "Waypoint\n2" : WaypointsMenu.worldWaypoints[1].Name);
            WaypointsMenu.waypoint3Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[2].Name) ? "Waypoint\n3" : WaypointsMenu.worldWaypoints[2].Name);
            WaypointsMenu.waypoint4Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[3].Name) ? "Waypoint\n4" : WaypointsMenu.worldWaypoints[3].Name);
            WaypointsMenu.waypoint5Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[4].Name) ? "Waypoint\n5" : WaypointsMenu.worldWaypoints[4].Name);
            WaypointsMenu.waypoint6Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[5].Name) ? "Waypoint\n6" : WaypointsMenu.worldWaypoints[5].Name);
            WaypointsMenu.waypoint7Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[6].Name) ? "Waypoint\n7" : WaypointsMenu.worldWaypoints[6].Name);
            WaypointsMenu.waypoint8Button.setButtonText(string.IsNullOrWhiteSpace(WaypointsMenu.worldWaypoints[7].Name) ? "Waypoint\n8" : WaypointsMenu.worldWaypoints[7].Name);
            QuickMenuUtils.ShowQuickmenuPage(WaypointsMenu.baseMenu.getMenuName());
        }

        private static void LoadWaypointOptions()
        {
            if ((double)WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].x == 0.0 && (double)WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].y == 0.0 && (double)WaypointsMenu.worldWaypoints[WaypointsMenu.selectedWaypoint].z == 0.0)
            {
                WaypointsMenu.teleportButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                WaypointsMenu.renameButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
            }
            else
            {
                WaypointsMenu.teleportButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
                WaypointsMenu.renameButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
                WaypointsMenu.teleportButton.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            }
            QuickMenuUtils.ShowQuickmenuPage(WaypointsMenu.selectedWaypointMenu.getMenuName());
        }

        private static void SaveWaypoints() => File.WriteAllText(Path.Combine(System.Environment.CurrentDirectory, "UserData/emmVRC/Waypoints/" + WaypointsMenu.currentWorldID + ".json"), Encoder.Encode((object)WaypointsMenu.worldWaypoints));
    }
}
