﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using UnityEngine;

namespace emmVRC.Menus
{
    public class WorldTweaksMenu
    {
        public static QMNestedButton baseMenu;
        public static QMSingleButton OptimizeMirrorsButton;
        public static QMSingleButton BeautifyMirrorsButton;
        public static QMSingleButton RevertMirrorsButton;
        public static QMSingleButton OptimizePedestalsButton;
        public static QMSingleButton RevertPedestalsButton;
        public static QMSingleButton ReloadWorldButton;
        public static QMToggleButton PortalBlockToggle;
        public static QMSingleButton DeletePortalsButton;
        public static QMToggleButton ChairBlockToggle;
        public static QMToggleButton ItemESPToggle;
        public static QMToggleButton TriggerESPToggle;
        public static QMToggleButton VideoPlayerToggle;
        public static QMToggleButton VRCPickupToggle;
        public static QMToggleButton PickupObjectToggle;
        public static QMToggleButton AvatarPedestalToggle;

        public static void Initialize()
        {
            WorldTweaksMenu.baseMenu = new QMNestedButton(FunctionsMenu.baseMenu.menuBase, 192384, 129302, "World\nTweaks", "");
            WorldTweaksMenu.baseMenu.getMainButton().DestroyMe();
            WorldTweaksMenu.OptimizeMirrorsButton = new QMSingleButton(WorldTweaksMenu.baseMenu, 1, 0, "Optimize", (System.Action)(() => MirrorTweaks.Optimize()), "Sets the mirrors around you to only reflect the bare necessities, for optimization");
            WorldTweaksMenu.OptimizeMirrorsButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            WorldTweaksMenu.OptimizeMirrorsButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, 96f);
            WorldTweaksMenu.BeautifyMirrorsButton = new QMSingleButton(WorldTweaksMenu.baseMenu, 1, 0, "Beautify", (System.Action)(() => MirrorTweaks.Beautify()), "Sets the mirrors around you to reflect absolutely everything");
            WorldTweaksMenu.BeautifyMirrorsButton.getGameObject().GetComponent<RectTransform>().sizeDelta /= new Vector2(1f, 2.0175f);
            WorldTweaksMenu.BeautifyMirrorsButton.getGameObject().GetComponent<RectTransform>().anchoredPosition += new Vector2(0.0f, -96f);
            WorldTweaksMenu.RevertMirrorsButton = new QMSingleButton(WorldTweaksMenu.baseMenu, 2, 0, "Revert\nMirrors", (System.Action)(() => MirrorTweaks.Revert()), "Reverts the mirrors in the world to their default reflections");
            WorldTweaksMenu.ReloadWorldButton = new QMSingleButton(WorldTweaksMenu.baseMenu, 3, 0, "Reload\nWorld", (System.Action)(() => VRCFlowManager.prop_VRCFlowManager_0.EnterWorld(RoomManager.field_Internal_Static_ApiWorld_0.id, RoomManager.field_Internal_Static_ApiWorldInstance_0.instanceId)), "Loads the current instance again");
            WorldTweaksMenu.PortalBlockToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 4, 0, "Portals On", (System.Action)(() =>
           {
               Configuration.JSONConfig.PortalBlockingEnable = false;
               Configuration.SaveConfig();
           }), "Portals Off", (System.Action)(() =>
     {
         Configuration.JSONConfig.PortalBlockingEnable = true;
         Configuration.SaveConfig();
     }), "TOGGLE: Enables or disables portals in the current world", defaultPosition: true);
            WorldTweaksMenu.PortalBlockToggle.setToggleState(!Configuration.JSONConfig.PortalBlockingEnable);
            WorldTweaksMenu.ChairBlockToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 4, 1, "Chairs On", (System.Action)(() =>
           {
               Configuration.JSONConfig.ChairBlockingEnable = false;
               Configuration.SaveConfig();
           }), "Chairs Off", (System.Action)(() =>
     {
         Configuration.JSONConfig.ChairBlockingEnable = true;
         Configuration.SaveConfig();
     }), "TOGGLE: Enables or disables chairs in the current world", defaultPosition: true);
            WorldTweaksMenu.ChairBlockToggle.setToggleState(!Configuration.JSONConfig.ChairBlockingEnable);
            WorldTweaksMenu.ItemESPToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 1, 1, "Item ESP", (System.Action)(() => WorldFunctions.ToggleItemESP(true)), "Disabled", (System.Action)(() => WorldFunctions.ToggleItemESP(false)), "TOGGLE: An Outline around all Pickup-able items in the world");
            WorldTweaksMenu.TriggerESPToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 2, 1, "Trigger ESP", (System.Action)(() => WorldFunctions.ToggleTriggerESP(true)), "Disabled", (System.Action)(() => WorldFunctions.ToggleTriggerESP(false)), "TOGGLE: An Outline around all triggers in the world\n(Some may not show due to area triggers blocking anything being behide larger area triggers)");
            WorldTweaksMenu.VRCPickupToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 1, 2, "Enable\nItem Pickup", (System.Action)(() =>
           {
               ComponentToggle.pickupable = true;
               ComponentToggle.Toggle();
           }), "Disabled", (System.Action)(() =>
     {
         ComponentToggle.pickupable = false;
         ComponentToggle.Toggle();
     }), "TOGGLE: Keep Objects visible, but disable you being able to pick them up");
            WorldTweaksMenu.VRCPickupToggle.setToggleState(true);
            WorldTweaksMenu.PickupObjectToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 2, 2, "Show\nPickup Objects", (System.Action)(() =>
           {
               ComponentToggle.pickup_object = true;
               ComponentToggle.Toggle();
           }), "Disabled", (System.Action)(() =>
     {
         ComponentToggle.pickup_object = false;
         ComponentToggle.Toggle();
     }), "TOGGLE: Hide all pickup objects in the world");
            WorldTweaksMenu.PickupObjectToggle.setToggleState(true);
            WorldTweaksMenu.VideoPlayerToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 3, 2, "Show\nVideo Players", (System.Action)(() =>
           {
               ComponentToggle.videoplayers = true;
               ComponentToggle.Toggle();
           }), "Disabled", (System.Action)(() =>
     {
         ComponentToggle.videoplayers = false;
         ComponentToggle.Toggle();
     }), "TOGGLE: Video Players");
            WorldTweaksMenu.VideoPlayerToggle.setToggleState(true);
            WorldTweaksMenu.AvatarPedestalToggle = new QMToggleButton(WorldTweaksMenu.baseMenu, 4, 2, "Avatar\nPedestals", (System.Action)(() =>
           {
               Configuration.JSONConfig.DisableAvatarPedestals = false;
               PedestalTweaks.Revert();
               Configuration.SaveConfig();
           }), "Disabled", (System.Action)(() =>
     {
         Configuration.JSONConfig.DisableAvatarPedestals = true;
         PedestalTweaks.Disable();
         Configuration.SaveConfig();
     }), "Toggles all the avatar pedestals in the world.");
            WorldTweaksMenu.AvatarPedestalToggle.setToggleState(!Configuration.JSONConfig.DisableAvatarPedestals);
        }

        public static void DisableOnSceneLoad()
        {
            WorldTweaksMenu.ItemESPToggle.setToggleState(false, true);
            WorldTweaksMenu.TriggerESPToggle.setToggleState(false, true);
        }

        internal static void SetRiskyFuncsAllowed(bool state)
        {
            if (state)
            {
                if (!ComponentToggle.pickupable)
                {
                    ComponentToggle.pickupable = true;
                    WorldTweaksMenu.VRCPickupToggle.setToggleState(true);
                }
                if (!ComponentToggle.pickup_object)
                {
                    ComponentToggle.pickup_object = true;
                    WorldTweaksMenu.PickupObjectToggle.setToggleState(true);
                }
                WorldTweaksMenu.ItemESPToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
                WorldTweaksMenu.TriggerESPToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
                WorldTweaksMenu.VRCPickupToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
                WorldTweaksMenu.PickupObjectToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = true;
            }
            else
            {
                WorldTweaksMenu.ItemESPToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                WorldTweaksMenu.TriggerESPToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                WorldTweaksMenu.VRCPickupToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
                WorldTweaksMenu.PickupObjectToggle.getGameObject().GetComponent<UnityEngine.UI.Button>().enabled = false;
            }
        }
    }
}
