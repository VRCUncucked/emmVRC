﻿
using System;
using System.IO;
using System.Text;

namespace emmVRC.Network
{
    public static class Authentication
    {
        private static string path = Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/");
        private static string extension = ".ema";

        public static bool Exists(string userID) => File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/" + userID + Authentication.extension));

        public static string ReadTokenFile(string userID) => !File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/" + userID + Authentication.extension)) ? "" : File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/" + userID + Authentication.extension));

        public static void DeleteTokenFile(string userID)
        {
            if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/" + userID + Authentication.extension)))
                return;
            File.Delete(Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/" + userID + Authentication.extension));
        }

        public static void CreateTokenFile(string userID, string data)
        {
            string path2 = userID + Authentication.extension;
            File.WriteAllBytes(Path.Combine(Authentication.path, path2), Encoding.UTF8.GetBytes(data));
        }
    }
}
