﻿
using emmVRCLoader;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace emmVRC.Network
{
    public class HTTPRequest
    {
        public static Task<string> get(string url) => HTTPRequest.request(HttpMethod.Get, url);

        public static Task<string> post(string url, object obj) => HTTPRequest.request(HttpMethod.Post, url, obj);

        public static Task<string> put(string url, object obj) => HTTPRequest.request(HttpMethod.Put, url, obj);

        public static Task<string> patch(string url, object obj) => HTTPRequest.request(new HttpMethod("PATCH"), url, obj);

        public static Task<string> delete(string url, object obj) => HTTPRequest.request(HttpMethod.Delete, url, obj);

        private static async Task<string> request(HttpMethod method, string url, object obj = null)
        {
            try
            {
                emmVRCLoader.Logger.LogDebug($"request: {method} -> {url}, {obj?.ToString()}");
                HttpRequestMessage request = new HttpRequestMessage(method, url);
                if (obj != null)
                {
                    string content = TinyJSON.Encoder.Encode(obj);
                    request.Content = (HttpContent)new StringContent(content, Encoding.UTF8, "application/json");
                }
                using (HttpResponseMessage responseMessage = await NetworkClient.httpClient.SendAsync(request))
                {
                    if (responseMessage.IsSuccessStatusCode) {
                        string result = await responseMessage.Content.ReadAsStringAsync();
                        emmVRCLoader.Logger.LogDebug($"response: {result}");
                        return result;
                    }
                    emmVRCLoader.Logger.LogDebug(responseMessage.ReasonPhrase);
                    return responseMessage.ReasonPhrase;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
