﻿
using emmVRC.TinyJSON;

namespace emmVRC.Network
{
    public class HTTPResponse
    {
        public static Variant Serialize(string httpContent) => Decoder.Decode(httpContent);

        public static string Deserialize(Variant obj) => Encoder.Encode((object)obj);
    }
}
