﻿
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Objects;
using emmVRC.TinyJSON;
using MelonLoader;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using VRC.Core;

namespace emmVRC.Network
{
    public static class NetworkClient
    {
        private const string BaseAddress = "https://api.emmvrc.com";
        private static int Port = 443;
        public const string configURL = "https://dl.emmvrc.com";
        private static string LoginKey;
        private static string _webToken;
        private static bool userIDTried = false;
        private static bool keyFileTried = false;
        private static bool passwordTried = false;
        public static int retries = 0;
        private static string message = "To those interested; this class is very much temporary. The entire network is going to be rewritten at some point soon.";

        public static string baseURL => "https://api.emmvrc.com:" + NetworkClient.Port.ToString();

        public static string webToken
        {
            get => NetworkClient._webToken;
            set
            {
                NetworkClient._webToken = value;
                NetworkClient.httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", NetworkClient._webToken);
            }
        }

        public static HttpClient httpClient { get; set; }

        public static void InitializeClient()
        {
            emmVRCLoader.Logger.Log("Init client");
            NetworkClient.retries = 0;
            NetworkClient.httpClient = new HttpClient();
            NetworkClient.httpClient.DefaultRequestHeaders.Accept.Clear();
            NetworkClient.httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            NetworkClient.httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("emmVRC/1.0 (Client; emmVRCClient/" + Attributes.Version + ", Headset; " + (XRDevice.isPresent ? XRDevice.model : "None") + ")");
            NetworkClient.fetchConfig().NoAwait("fetchConfig");
            NetworkClient.login().NoAwait("login");
        }

        public static T DestroyClient<T>(Func<T> callback = null)
        {
            NetworkClient.httpClient = (HttpClient)null;
            NetworkClient.webToken = (string)null;
            return callback();
        }

        private static async Task fetchConfig()
        {
            while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                await emmVRC.AwaitUpdate.Yield();
            await NetworkClient.getConfigAsync();
        }

        private static async Task login(string password = "")
        {
            while (RoomManager.field_Internal_Static_ApiWorld_0 == null)
                await emmVRC.AwaitUpdate.Yield();
            await NetworkClient.sendRequest(password);
        }

        public static void PromptLogin() => NetworkClient.OpenPasswordPrompt();

        private static void OpenPasswordPrompt() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("To login, please enter your pin", "", InputField.InputType.Standard, true, "Login", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((password, keyk, tx) =>
       {
           NetworkClient.passwordTried = false;
           NetworkClient.login(password).NoAwait("login");
       }), (Il2CppSystem.Action)null, "Enter pin....");

        private static void RequestNewPin() => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Please enter your new pin", "", InputField.InputType.Standard, true, "Set Pin", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((pin, keyk, tx) => VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowInputPopup("Please confirm your new pin", "", InputField.InputType.Standard, true, "Set Pin", (System.Action<string, Il2CppSystem.Collections.Generic.List<KeyCode>, Text>)((pin2, keyk2, tx2) =>
      {
          if (pin == pin2)
          {
              NetworkClient.login(pin2).NoAwait("login");
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.HideCurrentPopup();
          }
          else
              VRCUiPopupManager.field_Private_Static_VRCUiPopupManager_0.ShowStandardPopup("emmVRC", "The pins you entered did not match. Please try again.", "Okay", (System.Action)(() => NetworkClient.RequestNewPin()));
      }), (Il2CppSystem.Action)null, "Enter pin....", false)), (Il2CppSystem.Action)null, "Enter pin....", false);

        private static async Task getConfigAsync()
        {
            try
            {
                NetworkConfig.Instance = Decoder.Decode(await HTTPRequest.get("https://dl.emmvrc.com/configuration.php")).Make<NetworkConfig>();
                await emmVRC.AwaitUpdate.Yield();
                if (NetworkConfig.Instance.MessageID == -1 || Configuration.JSONConfig.LastSeenStartupMessage == NetworkConfig.Instance.MessageID)
                    return;
                Managers.NotificationManager.AddNotification(NetworkConfig.Instance.StartupMessage, "Dismiss", (System.Action)(() =>
               {
                   Configuration.JSONConfig.LastSeenStartupMessage = NetworkConfig.Instance.MessageID;
                   Configuration.SaveConfig();
                   Managers.NotificationManager.DismissCurrentNotification();
               }), "", (System.Action)null, Resources.alertSprite);
            }
            catch (System.Exception ex)
            {
                emmVRCLoader.Logger.LogError("Client configuration could not be fetched from emmVRC. Assuming default values. Error: " + ex?.ToString());
                NetworkConfig.Instance = new NetworkConfig();
            }
        }

        private static string ApiUserId = "usr_82d37ea6-ca95-400b-a975-e08bddb3cab4";
        private static string ApiUserName = "The Dot~";

        private static async Task sendRequest(string password = "")
        {
            if (NetworkConfig.Instance.DeleteAndDisableAuthFile)
                Network.Authentication.DeleteTokenFile(ApiUserId);
            if (string.IsNullOrWhiteSpace(password) && !NetworkClient.userIDTried)
            {
                NetworkClient.LoginKey = ApiUserId;
                NetworkClient.userIDTried = true;
            }
            else
                NetworkClient.LoginKey = !string.IsNullOrWhiteSpace(password) || !NetworkClient.userIDTried || !Network.Authentication.Exists(ApiUserId) ? password : Network.Authentication.ReadTokenFile(ApiUserId);
            string createFile = "0";
            if (!Network.Authentication.Exists(ApiUserId))
                createFile = "1";
            string response = "undefined";
            string str = "";
            System.Random random = new System.Random();
            int num = random.Next(5, 7);
            for (int index = 0; index < num; ++index)
                str += "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"[random.Next("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".Length)].ToString();
            try
            {
                response = await HTTPRequest.post(NetworkClient.baseURL + "/api/authentication/login", (object)new Dictionary<string, string>()
                {
                    ["username"] = ApiUserId,
                    ["name"] = ApiUserName,
                    [nameof(password)] = NetworkClient.LoginKey,
                    ["loginKey"] = createFile,
                    ["tagIdentifier"] = str
                });
                TinyJSON.Variant result = HTTPResponse.Serialize(response);
                NetworkClient.webToken = (string)result["token"];
                await emmVRC.AwaitUpdate.Yield();
                if ((bool)result["reset"])
                    Managers.NotificationManager.AddNotification("You need to set a pin to protect your emmVRC account.", "Set\nPin", (System.Action)(() =>
                   {
                       Managers.NotificationManager.DismissCurrentNotification();
                       NetworkClient.RequestNewPin();
                   }), "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), Resources.alertSprite);
                if (createFile == "1")
                {
                    if (NetworkClient.LoginKey != ApiUserId)
                    {
                        try
                        {
                            Network.Authentication.CreateTokenFile(ApiUserId, (string)result["loginKey"]);
                        }
                        catch (System.Exception ex)
                        {
                            emmVRCLoader.Logger.LogError(ex.ToString());
                        }
                        NetworkClient.LoginKey = (string)result["loginKey"];
                    }
                }
                NetworkClient.userIDTried = false;
                NetworkClient.keyFileTried = false;
                NetworkClient.passwordTried = false;
                result = (TinyJSON.Variant)null;
            }
            catch (System.Exception ex)
            {
                AwaitProvider.YieldAwaitable yieldAwaitable = emmVRC.AwaitUpdate.Yield();
                await yieldAwaitable;
                if (response.ToLower().Contains("unauthorized"))
                {
                    if (response.ToLower().Contains("banned"))
                    {
                        Managers.NotificationManager.AddNotification("You cannot connect to the emmVRC Network because you are banned.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
                    }
                    else
                    {
                        if (NetworkClient.keyFileTried && Network.Authentication.Exists(ApiUserId))
                            Network.Authentication.DeleteTokenFile(ApiUserId);
                        if (NetworkClient.userIDTried && !NetworkClient.keyFileTried)
                        {
                            NetworkClient.keyFileTried = true;
                            await NetworkClient.sendRequest();
                        }
                        else if (NetworkClient.userIDTried && NetworkClient.keyFileTried && (password != "" && !NetworkClient.passwordTried) && (!NetworkConfig.Instance.DisableAuthFile && !NetworkConfig.Instance.DeleteAndDisableAuthFile))
                        {
                            NetworkClient.passwordTried = true;
                            await NetworkClient.sendRequest(password);
                        }
                        else
                        {
                            yieldAwaitable = emmVRC.AwaitUpdate.Yield();
                            await yieldAwaitable;
                            Managers.NotificationManager.AddNotification("You need to log in to the emmVRC Network. Please log in or enter a pin to create one. If you have forgotten your pin, or are experiencing issues, please contact us in the emmVRC Discord.", "Login", (System.Action)(() =>
                           {
                               Managers.NotificationManager.DismissCurrentNotification();
                               NetworkClient.PromptLogin();
                           }), "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), Resources.alertSprite);
                        }
                    }
                }
                else if (response.ToLower().Contains("forbidden"))
                    Managers.NotificationManager.AddNotification("You have tried to log in too many times. Please try again later.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.errorSprite);
                else if (NetworkClient.retries <= NetworkConfig.Instance.NetworkAutoRetries)
                {
                    ++NetworkClient.retries;
                    await NetworkClient.sendRequest(password);
                }
                else
                    Managers.NotificationManager.AddNotification("The emmVRC Network is currently unavailable. Please try again later.", "Reconnect", (System.Action)(() =>
                   {
                       NetworkClient.retries = 0;
                       NetworkClient.InitializeClient();
                       MelonCoroutines.Start(emmVRC.loadNetworked());
                       Managers.NotificationManager.DismissCurrentNotification();
                   }), "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), Resources.errorSprite);
            }
            createFile = (string)null;
            response = (string)null;
        }
    }
}
