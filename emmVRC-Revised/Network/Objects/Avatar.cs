﻿
using emmVRC.Objects;
using Il2CppSystem.Collections.Generic;
using System;
using System.Text;
using VRC.Core;

namespace emmVRC.Network.Objects
{
    public class Avatar : SerializedObject
    {
        public string avatar_name = "";
        public string avatar_id = "";
        public string avatar_asset_url = "";
        public string avatar_thumbnail_image_url = "";
        public string avatar_author_id = "";
        public string avatar_category = "";
        public string avatar_author_name = "";
        public int avatar_public = 1;
        public int avatar_supported_platforms = 3;

        public Avatar()
        {
        }

        public Avatar(ApiAvatar vrcAvatar)
        {
            this.avatar_id = vrcAvatar.id;
            this.avatar_name = vrcAvatar.name;
            this.avatar_asset_url = vrcAvatar.assetUrl;
            this.avatar_author_id = vrcAvatar.authorId;
            this.avatar_author_name = vrcAvatar.authorName;
            this.avatar_category = "";
            this.avatar_thumbnail_image_url = vrcAvatar.thumbnailImageUrl;
            this.avatar_supported_platforms = (int)vrcAvatar.supportedPlatforms;
            this.avatar_public = vrcAvatar.releaseStatus == "private" ? 0 : (vrcAvatar.releaseStatus == "public" ? 1 : (int)byte.MaxValue);
        }

        public List<string> avatar_tags
        {
            get
            {
                List<string> list = new List<string>();
                list.Add("avatar");
                return list;
            }
        }

        public ApiAvatar apiAvatar()
        {
            if (this.avatar_public != 2)
            {
                ApiAvatar apiAvatar = new ApiAvatar();
                apiAvatar.name = this.avatar_name;
                apiAvatar.id = this.avatar_id;
                apiAvatar.assetUrl = this.avatar_asset_url;
                apiAvatar.thumbnailImageUrl = this.avatar_thumbnail_image_url;
                apiAvatar.authorId = this.avatar_author_id;
                apiAvatar.authorName = this.avatar_author_name;
                apiAvatar.description = this.avatar_name;
                apiAvatar.releaseStatus = NetworkConfig.Instance.DisableAvatarChecks ? "public" : (this.avatar_public == 0 ? "private" : (this.avatar_public == 1 ? "public" : "unavailable"));
                apiAvatar.unityVersion = "2018.4.20f1";
                apiAvatar.version = 1;
                apiAvatar.apiVersion = 1;
                apiAvatar.Endpoint = "avatars";
                apiAvatar.Populated = false;
                apiAvatar.assetVersion = new AssetVersion("2018.4.20f1", 0);
                apiAvatar.tags = this.avatar_tags;
                apiAvatar.supportedPlatforms = (ApiModel.SupportedPlatforms)this.avatar_supported_platforms;
                return apiAvatar;
            }
            ApiAvatar apiAvatar1 = new ApiAvatar();
            apiAvatar1.releaseStatus = "unavailable";
            apiAvatar1.name = this.avatar_name;
            apiAvatar1.id = "null";
            apiAvatar1.assetUrl = "";
            apiAvatar1.thumbnailImageUrl = "http://img.thetrueyoshifan.com/AvatarUnavailable.png";
            apiAvatar1.version = 0;
            apiAvatar1.apiVersion = 0;
            apiAvatar1.Endpoint = "avatars";
            apiAvatar1.Populated = false;
            apiAvatar1.assetVersion = new AssetVersion("2018.4.20f1", 0);
            return apiAvatar1;
        }
    }
}
