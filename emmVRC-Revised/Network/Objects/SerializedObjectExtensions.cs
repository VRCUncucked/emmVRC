﻿
using emmVRC.TinyJSON;

namespace emmVRC.Network.Objects
{
    public static class SerializedObjectExtensions
    {
        public static string Encode(this SerializedObject @object) => Encoder.Encode((object)@object);

        public static Variant Decode(string str) => Decoder.Decode(str);
    }
}
