﻿
using System.Collections.Generic;

namespace emmVRC.Objects
{
    public class Config
    {
        public string LastVersion = "0.0.0";
        public bool RiskyFunctionsEnabled;
        public bool GlobalDynamicBonesEnabled = true;
        public bool FriendGlobalDynamicBonesEnabled;
        public bool EveryoneGlobalDynamicBonesEnabled;
        public bool emmVRCNetworkEnabled = true;
        public bool GlobalChatEnabled = true;
        public bool VRFlightControls = true;
        public bool UIExpansionKitIntegration = true;
        public bool UnlimitedFPSEnabled;
        public bool InfoBarDisplayEnabled = true;
        public bool ClockEnabled = true;
        public bool AvatarFavoritesEnabled = true;
        public bool AvatarFavoritesJumpToStart = true;
        public bool SubmitAvatarPedestals = true;
        public bool MasterIconEnabled = true;
        public bool LogoButtonEnabled = true;
        public bool HUDEnabled = true;
        public bool VRHUDInDesktop;
        public bool MoveVRHUDIfSpaceFree;
        public bool ForceRestartButtonEnabled = true;
        public bool PortalBlockingEnable;
        public bool ChairBlockingEnable;
        public bool PlayerHistoryEnable;
        public bool LogPlayerJoin;
        public bool TrackingSaving = true;
        public bool ActionMenuIntegration = true;
        public bool MigrationButtonVisible = true;
        public bool DisableMicTooltip;
        public bool CameraPlus = true;
        public int FavoriteRenderLimit = 50;
        public int SearchRenderLimit = 100;
        public int CustomFOV = 60;
        public int FPSLimit = 144;
        public int LastSeenStartupMessage = -1;
        public bool TabMode;
        public int FunctionsButtonX;
        public int FunctionsButtonY = 1;
        public int LogoButtonX;
        public int LogoButtonY = -1;
        public int UserInteractButtonX = 4;
        public int UserInteractButtonY = 2;
        public int NotificationButtonPositionX;
        public int NotificationButtonPositionY;
        public int PlayerActionsButtonX = 1;
        public int PlayerActionsButtonY = 3;
        public bool StealthMode;
        public List<int> FavouritedEmojis = new List<int>();
        public bool PersistentAlarm;
        public uint AlarmTime;
        public bool PersistentInstanceAlarm;
        public uint InstanceAlarmTime;
        public bool DisableAvatarPedestals;
        public bool DisableReportWorldButton;
        public bool DisableEmojiButton;
        public bool DisableEmoteButton;
        public bool DisableRankToggleButton;
        public bool DisableOldInviteButtons;
        public bool DisablePlaylistsButton;
        public bool DisableAvatarStatsButton;
        public bool DisableReportUserButton;
        public bool MinimalWarnKickButton;
        public bool DisableOneHandMovement;
        public bool DisableVRCPlusAds;
        public bool DisableVRCPlusQMButtons;
        public bool DisableVRCPlusMenuTabs;
        public bool DisableVRCPlusUserInfo;
        public bool DisableAvatarHotWorlds;
        public bool DisableAvatarRandomWorlds;
        public bool DisableAvatarPersonal;
        public bool DisableAvatarLegacy;
        public bool DisableAvatarPublic;
        public bool DisableAvatarOther;
        public bool UIColorChangingEnabled;
        public string UIColorHex = "#0EA6AD";
        public bool UIActionMenuColorChangingEnabled = true;
        public bool UIExpansionKitColorChangingEnabled = true;
        public bool UIMicIconColorChangingEnabled;
        public bool UIMicIconPulsingEnabled = true;
        public bool NameplateColorChangingEnabled;
        public string FriendNamePlateColorHex = "#FFFF00";
        public string VisitorNamePlateColorHex = "#CCCCCC";
        public string NewUserNamePlateColorHex = "#1778FF";
        public string UserNamePlateColorHex = "#2BCE5C";
        public string KnownUserNamePlateColorHex = "#FF7B42";
        public string TrustedUserNamePlateColorHex = "#8143E6";
        public string VeteranUserNamePlateColorHex = "#ABCDEE";
        public string LegendaryUserNamePlateColorHex = "#FF69B4";
        public bool InfoSpoofingEnabled;
        public string InfoSpoofingName = "";
        public int SortingMode;
        public bool SortingInverse;
        public bool WelcomeMessageShown;
        public bool RiskyFunctionsWarningShown;
        public bool NameplatesVisible = true;
        public bool UIVisible = true;
        public float UIVolume;
        public bool UIVolumeMute;
        public float WorldVolume;
        public bool WorldVolumeMute;
        public float VoiceVolume;
        public bool VoiceVolumeMute;
        public float AvatarVolume;
        public bool AvatarVolumeMute;
        public float MaxSpeedIncrease = 5f;
        public bool EnableKeybinds = true;
        public int[] FlightKeybind = new int[2] { 102, 306 };
        public int[] NoclipKeybind = new int[2] { 109, 306 };
        public int[] SpeedKeybind = new int[2] { 103, 306 };
        public int[] ThirdPersonKeybind = new int[2] { 116, 306 };
        public int[] ToggleHUDEnabledKeybind = new int[2]
        {
      106,
      306
        };
        public int[] RespawnKeybind = new int[2] { 121, 306 };
        public int[] GoHomeKeybind = new int[2] { 117, 306 };
    }
}
