﻿
namespace emmVRC.Objects
{
    public class NetworkConfig
    {
        public int MessageUpdateRate = 60;
        public bool DisableAuthFile;
        public bool DeleteAndDisableAuthFile;
        public bool DisableAvatarChecks;
        public bool APICallsAllowed = true;
        public bool MessagingAllowed = true;
        public int NetworkAutoRetries;
        public bool AvatarPedestalScansAllowed = true;
        public string StartupMessage = "";
        public int MessageID = -1;
        public static NetworkConfig Instance = new NetworkConfig();
    }
}
