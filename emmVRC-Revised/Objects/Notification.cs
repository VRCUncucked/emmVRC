﻿
using System;
using System.Collections.Generic;
using UnityEngine;

namespace emmVRC.Objects
{
    public class Notification
    {
        public string Message;
        public string Button1Text;
        public string Button2Text;
        public string Button3Text;
        public Action Button1Action;
        public Action Button2Action;
        public Action Button3Action;
        public Sprite Icon;
        public int Timeout = -1;
        public List<object> PersistentObjects = new List<object>();
    }
}
