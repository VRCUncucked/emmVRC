﻿
using VRC.Core;

namespace emmVRC.Objects
{
    public class SerializedAvatar
    {
        public string avatar_name = "";
        public string avatar_id = "";
        public string avatar_asset_url = "";
        public string avatar_thumbnail_image_url = "";
        public string avatar_author_id = "";
        public ApiModel.SupportedPlatforms avatar_supported_platforms = ApiModel.SupportedPlatforms.All;
    }
}
