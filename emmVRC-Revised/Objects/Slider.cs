﻿
using emmVRC.Libraries;
using System;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;

namespace emmVRC.Objects
{
    public class Slider
    {
        public QMSingleButton basePosition;
        public GameObject slider;

        public Slider(string parentPath, int x, int y, Action<float> evt, float defaultValue = 0.0f)
        {
            this.basePosition = new QMSingleButton(parentPath, x, y, "", (Action)null, "");
            this.basePosition.setActive(false);
            this.slider = UnityEngine.Object.Instantiate<Transform>(QuickMenuUtils.GetVRCUiMInstance().menuContent().transform.Find("Screens/Settings/AudioDevicePanel/VolumeSlider"), ((Component)QuickMenuUtils.GetQuickMenuInstance()).transform.Find(parentPath)).gameObject;
            try
            {
                this.slider.transform.Find("Fill Area/Label").gameObject.SetActive(false);
            }
            catch (Exception ex)
            {
                Exception exception = new Exception();
            }
            this.slider.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            this.slider.transform.localPosition = this.basePosition.getGameObject().transform.localPosition;
            this.slider.GetComponentInChildren<RectTransform>().anchorMin += new Vector2(0.06f, 0.0f);
            this.slider.GetComponentInChildren<RectTransform>().anchorMax += new Vector2(0.1f, 0.0f);
            this.slider.GetComponentInChildren<UnityEngine.UI.Slider>().onValueChanged = new UnityEngine.UI.Slider.SliderEvent();
            this.slider.GetComponentInChildren<UnityEngine.UI.Slider>().value = defaultValue;
            this.slider.GetComponentInChildren<UnityEngine.UI.Slider>().onValueChanged.AddListener(DelegateSupport.ConvertDelegate<UnityAction<float>>((Delegate)evt));
        }
    }
}
