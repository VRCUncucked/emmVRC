﻿
using System;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;

namespace emmVRC.Objects
{
    public class emmVRCPanel : MonoBehaviour
    {
        public TextMesh activeUsers;
        public TextMesh totalUsers;
        public TextMesh globalChat;
        public bool updateRequested;
        public static bool classInjected;

        public emmVRCPanel(IntPtr ptr)
          : base(ptr)
        {
        }

        public static emmVRCPanel Instantiate(GameObject parent)
        {
            if (!emmVRCPanel.classInjected)
            {
                ClassInjector.RegisterTypeInIl2Cpp<emmVRCPanel>();
                emmVRCPanel.classInjected = true;
            }
            return parent.AddComponent<emmVRCPanel>();
        }

        public void Start()
        {
            MeshFilter meshFilter = this.gameObject.AddComponent<MeshFilter>();
            Mesh mesh1 = new Mesh();
            Mesh mesh2 = mesh1;
            meshFilter.mesh = mesh2;
            Vector3[] vector3Array1 = new Vector3[4]
            {
        Vector3.zero,
        new Vector3(1.65f, 0.0f, 0.0f),
        new Vector3(0.0f, 1f, 0.0f),
        new Vector3(1.65f, 1f, 0.0f)
            };
            mesh1.vertices = (Il2CppStructArray<Vector3>)vector3Array1;
            int[] numArray = new int[6] { 0, 2, 1, 2, 3, 1 };
            mesh1.triangles = (Il2CppStructArray<int>)numArray;
            Vector3[] vector3Array2 = new Vector3[4]
            {
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward,
        -Vector3.forward
            };
            mesh1.normals = (Il2CppStructArray<Vector3>)vector3Array2;
            Vector2[] vector2Array = new Vector2[4]
            {
        new Vector2(0.0f, 0.0f),
        new Vector2(1f, 0.0f),
        new Vector2(0.0f, 1f),
        new Vector2(1f, 1f)
            };
            mesh1.uv = (Il2CppStructArray<Vector2>)vector2Array;
            MeshRenderer meshRenderer = this.gameObject.AddComponent<MeshRenderer>();
            meshRenderer.material = new Material(Shader.Find("Standard"));
            meshRenderer.material.SetTexture("_MainTex", (Texture)Resources.panelTexture);
            GameObject gameObject1 = UnityEngine.Object.Instantiate<GameObject>(new GameObject(), this.gameObject.transform);
            gameObject1.transform.localPosition = new Vector3(0.325f, 0.65f, 0.0f);
            gameObject1.transform.localScale = new Vector3(0.05f, 0.05f, 0.25f);
            this.activeUsers = gameObject1.AddComponent<TextMesh>();
            this.activeUsers.anchor = TextAnchor.UpperCenter;
            this.activeUsers.color = Color.black;
            GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(new GameObject(), this.gameObject.transform);
            gameObject2.transform.localPosition = new Vector3(1.3f, 0.65f, 0.0f);
            gameObject2.transform.localScale = new Vector3(0.05f, 0.05f, 0.25f);
            this.totalUsers = gameObject2.AddComponent<TextMesh>();
            this.totalUsers.anchor = TextAnchor.UpperCenter;
            this.totalUsers.color = Color.black;
            GameObject gameObject3 = UnityEngine.Object.Instantiate<GameObject>(new GameObject(), this.gameObject.transform);
            gameObject3.transform.localPosition = new Vector3(0.0125f, 0.325f, 0.0f);
            gameObject3.transform.localScale = new Vector3(0.03f, 0.03f, 0.25f);
            this.globalChat = gameObject3.AddComponent<TextMesh>();
            this.globalChat.color = Color.black;
        }
    }
}
