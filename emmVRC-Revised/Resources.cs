﻿
using emmVRCLoader;
using System;
using System.Collections;
using System.IO;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Networking;

namespace emmVRC
{
    public static class Resources
    {
        private static AssetBundle emmVRCBundle;
        private static AssetBundle udonBundle;
        public static string resourcePath = Path.Combine(Environment.CurrentDirectory, "UserData/emmVRC/Resources");
        public static string dependenciesPath = Path.Combine(Environment.CurrentDirectory, "Dependencies");
        public static Cubemap blankGradient;
        public static Sprite offlineSprite;
        public static Sprite onlineSprite;
        public static Sprite owonlineSprite;
        public static Sprite alertSprite;
        public static Sprite errorSprite;
        public static Sprite messageSprite;
        public static Sprite alarmSprite;
        public static Sprite rpSprite;
        public static Sprite crownSprite;
        public static Sprite Media_Nav;
        public static Sprite Media_PlayPause;
        public static Sprite Media_Stop;
        public static Sprite HUD_Base;
        public static Sprite HUD_Minimized;
        public static Sprite emmHUDLogo;
        public static Sprite TabIcon;
        public static Sprite authorSprite;
        public static Sprite lensOn;
        public static Sprite lensOff;
        public static Sprite zoomIn;
        public static Sprite zoomOut;
        public static AudioClip customLoadingMusic;
        public static Texture2D panelTexture;
        public static Texture2D saveTexture;
        public static Texture2D deleteTexture;
        public static Texture2D flyTexture;
        public static Texture2D toggleOnTexture;
        public static Texture2D toggleOffTexture;
        public static AudioClip alarmTone;

        private static Sprite LoadSprite(string sprite)
        {
            Sprite sprite1 = Resources.emmVRCBundle.LoadAsset(sprite, Il2CppType.Of<Sprite>()).Cast<Sprite>();
            sprite1.hideFlags |= HideFlags.DontUnloadUnusedAsset;
            return sprite1;
        }

        private static Texture2D LoadTexture(string texture)
        {
            Texture2D texture2D = Resources.emmVRCBundle.LoadAsset(texture, Il2CppType.Of<Texture2D>()).Cast<Texture2D>();
            texture2D.hideFlags |= HideFlags.DontUnloadUnusedAsset;
            return texture2D;
        }

        private static Cubemap LoadCubemap(string cubemap)
        {
            Cubemap cubemap1 = Resources.emmVRCBundle.LoadAsset(cubemap, Il2CppType.Of<Cubemap>()).Cast<Cubemap>();
            cubemap1.hideFlags |= HideFlags.DontUnloadUnusedAsset;
            return cubemap1;
        }

        private static AudioClip LoadAudioClip(string audioclip)
        {
            AudioClip audioClip = Resources.emmVRCBundle.LoadAsset(audioclip, Il2CppType.Of<AudioClip>()).Cast<AudioClip>();
            audioClip.hideFlags |= HideFlags.DontUnloadUnusedAsset;
            return audioClip;
        }

        public static IEnumerator LoadResources()
        {
            if (Environment.CommandLine.Contains("--emmvrc.assetdev") && Environment.CommandLine.Contains("--emmvrc.devmode"))
            {
                Resources.emmVRCBundle = AssetBundle.LoadFromFile(Path.Combine(Resources.dependenciesPath, "Normal.emm"));
            }
            else
            {
                UnityWebRequest assetBundleRequest = !Environment.CommandLine.Contains("--emmvrc.anniversarymode") ? (!Environment.CommandLine.Contains("--emmvrc.pridemode") ? (!Environment.CommandLine.Contains("--emmvrc.normalmode") ? (!Environment.CommandLine.Contains("--emmvrc.halloweenmode") ? (!Environment.CommandLine.Contains("--emmvrc.xmasmode") ? (!Environment.CommandLine.Contains("--emmvrc.beemode") ? UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/emmVRCResources.emm") : UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/Seasonals/Bee.emm")) : UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/Seasonals/Xmas.emm")) : UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/Seasonals/Halloween.emm")) : UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/Seasonals/Normal.emm")) : UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/Seasonals/Pride.emm")) : UnityWebRequest.Get("https://dl.emmvrc.com/downloads/emmvrcresources/Seasonals/Anniversary.emm");
                assetBundleRequest.SendWebRequest();
                while (!assetBundleRequest.isDone && !assetBundleRequest.isHttpError)
                    yield return (object)new WaitForSeconds(0.1f);
                if (assetBundleRequest.isHttpError)
                {
                    try
                    {
                        Resources.emmVRCBundle = AssetBundle.LoadFromFile(Path.Combine(Resources.dependenciesPath, "Resources.emm"));
                    }
                    catch (Exception ex)
                    {
                        emmVRCLoader.Logger.LogError("emmVRC could not load resources. Many UI elements and features will be broken.");
                    }
                }
                else
                {
                    File.WriteAllBytes(Path.Combine(Resources.dependenciesPath, "Resources.emm"), (byte[])(Il2CppArrayBase<byte>)assetBundleRequest.downloadHandler.data);
                    AssetBundleCreateRequest dlBundle = AssetBundle.LoadFromMemoryAsync(assetBundleRequest.downloadHandler.data);
                    while (!dlBundle.isDone)
                        yield return (object)new WaitForSeconds(0.1f);
                    Resources.emmVRCBundle = dlBundle.assetBundle;
                    dlBundle = (AssetBundleCreateRequest)null;
                }
                assetBundleRequest = (UnityWebRequest)null;
            }
            Resources.offlineSprite = Resources.LoadSprite("Offline.png");
            Resources.onlineSprite = Resources.LoadSprite("Online.png");
            Resources.owonlineSprite = Resources.LoadSprite("OwOnline.png");
            Resources.alertSprite = Resources.LoadSprite("Alert.png");
            Resources.errorSprite = Resources.LoadSprite("Error.png");
            Resources.messageSprite = Resources.LoadSprite("Message.png");
            Resources.alarmSprite = Resources.LoadSprite("Alarm.png");
            Resources.rpSprite = Resources.LoadSprite("RP.png");
            Resources.lensOn = Resources.LoadSprite("LensOn.png");
            Resources.lensOff = Resources.LoadSprite("LensOff.png");
            Resources.zoomIn = Resources.LoadSprite("ZoomIn.png");
            Resources.zoomOut = Resources.LoadSprite("ZoomOut.png");
            Resources.crownSprite = Resources.LoadSprite("Crown.png");
            Resources.authorSprite = Resources.LoadSprite("Author.png");
            Resources.Media_Nav = Resources.LoadSprite("Media_NAV.png");
            Resources.Media_PlayPause = Resources.LoadSprite("Media_PLAY_PAUSE.png");
            Resources.Media_Stop = Resources.LoadSprite("Media_STOP.png");
            Resources.HUD_Base = Resources.LoadSprite("UIMaximized.png");
            Resources.HUD_Minimized = Resources.LoadSprite("UIMinimized.png");
            Resources.emmHUDLogo = Resources.LoadSprite("emmSimplifedLogo.png");
            Resources.TabIcon = Resources.LoadSprite("TabIcon.png");
            Resources.panelTexture = Resources.LoadTexture("Panel.png");
            Resources.saveTexture = Resources.LoadTexture("Save.png");
            Resources.deleteTexture = Resources.LoadTexture("Delete.png");
            Resources.flyTexture = Resources.LoadTexture("Fly.png");
            Resources.toggleOnTexture = Resources.LoadTexture("E_GUI_Toggle_ON.png");
            Resources.toggleOffTexture = Resources.LoadTexture("E_GUI_Toggle_OFF.png");
            Resources.blankGradient = Resources.LoadCubemap("Gradient.png");
            Resources.alarmTone = Resources.LoadAudioClip("AlarmSound.ogg");
        }
    }
}
