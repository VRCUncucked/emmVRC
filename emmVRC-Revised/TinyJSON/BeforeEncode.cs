﻿
using System;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [AttributeUsage(AttributeTargets.Method)]
    [Obfuscation(Exclude = true)]
    public class BeforeEncode : Attribute
    {
    }
}
