﻿
using System;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true)]
    [Obfuscation(Exclude = true)]
    public class DecodeAlias : Attribute
    {
        public string[] Names { get; private set; }

        public DecodeAlias(params string[] names) => this.Names = names;

        public bool Contains(string name) => Array.IndexOf<string>(this.Names, name) > -1;
    }
}
