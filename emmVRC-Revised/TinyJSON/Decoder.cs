﻿
using System;
using System.IO;
using System.Text;

namespace emmVRC.TinyJSON
{
    public sealed class Decoder : IDisposable
    {
        private const string whiteSpace = " \t\n\r";
        private const string wordBreak = " \t\n\r{}[],:\"";
        private StringReader json;

        private Decoder(string jsonString) => this.json = new StringReader(jsonString);

        public static Variant Decode(string jsonString)
        {
            using (Decoder decoder = new Decoder(jsonString))
                return decoder.DecodeValue();
        }

        public void Dispose()
        {
            this.json.Dispose();
            this.json = (StringReader)null;
        }

        private ProxyObject DecodeObject()
        {
            ProxyObject proxyObject = new ProxyObject();
            this.json.Read();
            while (true)
            {
                Decoder.Token nextToken;
                do
                {
                    nextToken = this.NextToken;
                    if (nextToken != Decoder.Token.None)
                    {
                        if (nextToken == Decoder.Token.CloseBrace)
                            goto label_5;
                    }
                    else
                        goto label_4;
                }
                while (nextToken == Decoder.Token.Comma);
                string key = (string)this.DecodeString();
                if (key != null)
                {
                    if (this.NextToken == Decoder.Token.Colon)
                    {
                        this.json.Read();
                        proxyObject.Add(key, this.DecodeValue());
                    }
                    else
                        goto label_9;
                }
                else
                    goto label_7;
            }
        label_4:
            return (ProxyObject)null;
        label_5:
            return proxyObject;
        label_7:
            return (ProxyObject)null;
        label_9:
            return (ProxyObject)null;
        }

        private ProxyArray DecodeArray()
        {
            ProxyArray proxyArray = new ProxyArray();
            this.json.Read();
            bool flag = true;
            while (flag)
            {
                Decoder.Token nextToken = this.NextToken;
                switch (nextToken)
                {
                    case Decoder.Token.None:
                        return (ProxyArray)null;
                    case Decoder.Token.CloseBracket:
                        flag = false;
                        continue;
                    case Decoder.Token.Comma:
                        continue;
                    default:
                        proxyArray.Add(this.DecodeByToken(nextToken));
                        continue;
                }
            }
            return proxyArray;
        }

        private Variant DecodeValue() => this.DecodeByToken(this.NextToken);

        private Variant DecodeByToken(Decoder.Token token)
        {
            switch (token)
            {
                case Decoder.Token.OpenBrace:
                    return (Variant)this.DecodeObject();
                case Decoder.Token.OpenBracket:
                    return (Variant)this.DecodeArray();
                case Decoder.Token.String:
                    return this.DecodeString();
                case Decoder.Token.Number:
                    return this.DecodeNumber();
                case Decoder.Token.True:
                    return (Variant)new ProxyBoolean(true);
                case Decoder.Token.False:
                    return (Variant)new ProxyBoolean(false);
                case Decoder.Token.Null:
                    return (Variant)null;
                default:
                    return (Variant)null;
            }
        }

        private Variant DecodeString()
        {
            StringBuilder stringBuilder1 = new StringBuilder();
            this.json.Read();
            bool flag = true;
            while (flag)
            {
                if (this.json.Peek() == -1)
                    break;
                char nextChar1 = this.NextChar;
                switch (nextChar1)
                {
                    case '"':
                        flag = false;
                        continue;
                    case '\\':
                        if (this.json.Peek() == -1)
                        {
                            flag = false;
                            continue;
                        }
                        char nextChar2 = this.NextChar;
                        switch (nextChar2)
                        {
                            case '"':
                            case '/':
                            case '\\':
                                stringBuilder1.Append(nextChar2);
                                continue;
                            case 'b':
                                stringBuilder1.Append('\b');
                                continue;
                            case 'f':
                                stringBuilder1.Append('\f');
                                continue;
                            case 'n':
                                stringBuilder1.Append('\n');
                                continue;
                            case 'r':
                                stringBuilder1.Append('\r');
                                continue;
                            case 't':
                                stringBuilder1.Append('\t');
                                continue;
                            case 'u':
                                StringBuilder stringBuilder2 = new StringBuilder();
                                for (int index = 0; index < 4; ++index)
                                    stringBuilder2.Append(this.NextChar);
                                stringBuilder1.Append((char)Convert.ToInt32(stringBuilder2.ToString(), 16));
                                continue;
                            default:
                                continue;
                        }
                    default:
                        stringBuilder1.Append(nextChar1);
                        continue;
                }
            }
            return (Variant)new ProxyString(stringBuilder1.ToString());
        }

        private Variant DecodeNumber() => (Variant)new ProxyNumber((IConvertible)this.NextWord);

        private void ConsumeWhiteSpace()
        {
            while (" \t\n\r".IndexOf(this.PeekChar) != -1)
            {
                this.json.Read();
                if (this.json.Peek() == -1)
                    break;
            }
        }

        private char PeekChar
        {
            get
            {
                int num = this.json.Peek();
                return num != -1 ? Convert.ToChar(num) : char.MinValue;
            }
        }

        private char NextChar => Convert.ToChar(this.json.Read());

        private string NextWord
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder();
                while (" \t\n\r{}[],:\"".IndexOf(this.PeekChar) == -1)
                {
                    stringBuilder.Append(this.NextChar);
                    if (this.json.Peek() == -1)
                        break;
                }
                return stringBuilder.ToString();
            }
        }

        private Decoder.Token NextToken
        {
            get
            {
                this.ConsumeWhiteSpace();
                if (this.json.Peek() == -1)
                    return Decoder.Token.None;
                switch (this.PeekChar)
                {
                    case '"':
                        return Decoder.Token.String;
                    case ',':
                        this.json.Read();
                        return Decoder.Token.Comma;
                    case '-':
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        return Decoder.Token.Number;
                    case ':':
                        return Decoder.Token.Colon;
                    case '[':
                        return Decoder.Token.OpenBracket;
                    case ']':
                        this.json.Read();
                        return Decoder.Token.CloseBracket;
                    case '{':
                        return Decoder.Token.OpenBrace;
                    case '}':
                        this.json.Read();
                        return Decoder.Token.CloseBrace;
                    default:
                        string nextWord = this.NextWord;
                        if (nextWord == "false")
                            return Decoder.Token.False;
                        if (nextWord == "true")
                            return Decoder.Token.True;
                        return nextWord == "null" ? Decoder.Token.Null : Decoder.Token.None;
                }
            }
        }

        private enum Token
        {
            None,
            OpenBrace,
            CloseBrace,
            OpenBracket,
            CloseBracket,
            Colon,
            Comma,
            String,
            Number,
            True,
            False,
            Null,
        }
    }
}
