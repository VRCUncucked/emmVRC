﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public sealed class Encoder
    {
        private static readonly Type includeAttrType = typeof(Include);
        private static readonly Type excludeAttrType = typeof(Exclude);
        private static readonly Type typeHintAttrType = typeof(TypeHint);
        private readonly StringBuilder builder;
        private readonly EncodeOptions options;
        private int indent;

        private Encoder(EncodeOptions options)
        {
            this.options = options;
            this.builder = new StringBuilder();
            this.indent = 0;
        }

        public static string Encode(object obj) => Encoder.Encode(obj, EncodeOptions.None);

        public static string Encode(object obj, EncodeOptions options)
        {
            Encoder encoder = new Encoder(options);
            encoder.EncodeValue(obj, false);
            return encoder.builder.ToString();
        }

        private bool PrettyPrintEnabled => (this.options & EncodeOptions.PrettyPrint) == EncodeOptions.PrettyPrint;

        private bool TypeHintsEnabled => (this.options & EncodeOptions.NoTypeHints) != EncodeOptions.NoTypeHints;

        private bool IncludePublicPropertiesEnabled => (this.options & EncodeOptions.IncludePublicProperties) == EncodeOptions.IncludePublicProperties;

        private bool EnforceHierarchyOrderEnabled => (this.options & EncodeOptions.EnforceHierarchyOrder) == EncodeOptions.EnforceHierarchyOrder;

        private void EncodeValue(object value, bool forceTypeHint)
        {
            switch (value)
            {
                case null:
                    this.builder.Append("null");
                    break;
                case string _:
                    this.EncodeString((string)value);
                    break;
                case ProxyString _:
                    this.EncodeString(((Variant)value).ToString((IFormatProvider)CultureInfo.InvariantCulture));
                    break;
                case char _:
                    this.EncodeString(value.ToString());
                    break;
                case bool flag:
                    this.builder.Append(flag ? "true" : "false");
                    break;
                case Enum _:
                    this.EncodeString(value.ToString());
                    break;
                case Array _:
                    this.EncodeArray((Array)value, forceTypeHint);
                    break;
                case IList _:
                    this.EncodeList((IList)value, forceTypeHint);
                    break;
                case IDictionary _:
                    this.EncodeDictionary((IDictionary)value, forceTypeHint);
                    break;
                case Guid _:
                    this.EncodeString(value.ToString());
                    break;
                case ProxyArray _:
                    this.EncodeProxyArray((ProxyArray)value);
                    break;
                case ProxyObject _:
                    this.EncodeProxyObject((ProxyObject)value);
                    break;
                case float _:
                case double _:
                case int _:
                case uint _:
                case long _:
                case sbyte _:
                case byte _:
                case short _:
                case ushort _:
                case ulong _:
                case Decimal _:
                case ProxyBoolean _:
                case ProxyNumber _:
                    this.builder.Append(Convert.ToString(value, (IFormatProvider)CultureInfo.InvariantCulture));
                    break;
                default:
                    this.EncodeObject(value, forceTypeHint);
                    break;
            }
        }

        private IEnumerable<FieldInfo> GetFieldsForType(Type type)
        {
            if (!this.EnforceHierarchyOrderEnabled)
                return (IEnumerable<FieldInfo>)type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            Stack<Type> typeStack = new Stack<Type>();
            for (; type != (Type)null; type = type.BaseType)
                typeStack.Push(type);
            List<FieldInfo> fieldInfoList = new List<FieldInfo>();
            while (typeStack.Count > 0)
                fieldInfoList.AddRange((IEnumerable<FieldInfo>)typeStack.Pop().GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
            return (IEnumerable<FieldInfo>)fieldInfoList;
        }

        private IEnumerable<PropertyInfo> GetPropertiesForType(Type type)
        {
            if (!this.EnforceHierarchyOrderEnabled)
                return (IEnumerable<PropertyInfo>)type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            Stack<Type> typeStack = new Stack<Type>();
            for (; type != (Type)null; type = type.BaseType)
                typeStack.Push(type);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            while (typeStack.Count > 0)
                propertyInfoList.AddRange((IEnumerable<PropertyInfo>)typeStack.Pop().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));
            return (IEnumerable<PropertyInfo>)propertyInfoList;
        }

        private void EncodeObject(object value, bool forceTypeHint)
        {
            Type type = value.GetType();
            this.AppendOpenBrace();
            forceTypeHint = forceTypeHint || this.TypeHintsEnabled;
            bool propertiesEnabled = this.IncludePublicPropertiesEnabled;
            bool firstItem = !forceTypeHint;
            if (forceTypeHint)
            {
                if (this.PrettyPrintEnabled)
                    this.AppendIndent();
                this.EncodeString("@type");
                this.AppendColon();
                this.EncodeString(type.FullName);
                firstItem = false;
            }
            foreach (FieldInfo fieldInfo in this.GetFieldsForType(type))
            {
                bool forceTypeHint1 = false;
                bool flag = fieldInfo.IsPublic;
                foreach (object customAttribute in fieldInfo.GetCustomAttributes(true))
                {
                    if (Encoder.excludeAttrType.IsInstanceOfType(customAttribute))
                        flag = false;
                    if (Encoder.includeAttrType.IsInstanceOfType(customAttribute))
                        flag = true;
                    if (Encoder.typeHintAttrType.IsInstanceOfType(customAttribute))
                        forceTypeHint1 = true;
                }
                if (flag)
                {
                    this.AppendComma(firstItem);
                    this.EncodeString(fieldInfo.Name);
                    this.AppendColon();
                    this.EncodeValue(fieldInfo.GetValue(value), forceTypeHint1);
                    firstItem = false;
                }
            }
            foreach (PropertyInfo propertyInfo in this.GetPropertiesForType(type))
            {
                if (propertyInfo.CanRead)
                {
                    bool forceTypeHint1 = false;
                    bool flag = propertiesEnabled;
                    foreach (object customAttribute in propertyInfo.GetCustomAttributes(true))
                    {
                        if (Encoder.excludeAttrType.IsInstanceOfType(customAttribute))
                            flag = false;
                        if (Encoder.includeAttrType.IsInstanceOfType(customAttribute))
                            flag = true;
                        if (Encoder.typeHintAttrType.IsInstanceOfType(customAttribute))
                            forceTypeHint1 = true;
                    }
                    if (flag)
                    {
                        this.AppendComma(firstItem);
                        this.EncodeString(propertyInfo.Name);
                        this.AppendColon();
                        this.EncodeValue(propertyInfo.GetValue(value, (object[])null), forceTypeHint1);
                        firstItem = false;
                    }
                }
            }
            this.AppendCloseBrace();
        }

        private void EncodeProxyArray(ProxyArray value)
        {
            if (value.Count == 0)
            {
                this.builder.Append("[]");
            }
            else
            {
                this.AppendOpenBracket();
                bool firstItem = true;
                foreach (Variant variant in (IEnumerable<Variant>)value)
                {
                    this.AppendComma(firstItem);
                    this.EncodeValue((object)variant, false);
                    firstItem = false;
                }
                this.AppendCloseBracket();
            }
        }

        private void EncodeProxyObject(ProxyObject value)
        {
            if (value.Count == 0)
            {
                this.builder.Append("{}");
            }
            else
            {
                this.AppendOpenBrace();
                bool firstItem = true;
                foreach (string key in value.Keys)
                {
                    this.AppendComma(firstItem);
                    this.EncodeString(key);
                    this.AppendColon();
                    this.EncodeValue((object)value[key], false);
                    firstItem = false;
                }
                this.AppendCloseBrace();
            }
        }

        private void EncodeDictionary(IDictionary value, bool forceTypeHint)
        {
            if (value.Count == 0)
            {
                this.builder.Append("{}");
            }
            else
            {
                this.AppendOpenBrace();
                bool firstItem = true;
                foreach (object key in (IEnumerable)value.Keys)
                {
                    this.AppendComma(firstItem);
                    this.EncodeString(key.ToString());
                    this.AppendColon();
                    this.EncodeValue(value[key], forceTypeHint);
                    firstItem = false;
                }
                this.AppendCloseBrace();
            }
        }

        private void EncodeList(IList value, bool forceTypeHint)
        {
            if (value.Count == 0)
            {
                this.builder.Append("[]");
            }
            else
            {
                this.AppendOpenBracket();
                bool firstItem = true;
                foreach (object obj in (IEnumerable)value)
                {
                    this.AppendComma(firstItem);
                    this.EncodeValue(obj, forceTypeHint);
                    firstItem = false;
                }
                this.AppendCloseBracket();
            }
        }

        private void EncodeArray(Array value, bool forceTypeHint)
        {
            if (value.Rank == 1)
            {
                this.EncodeList((IList)value, forceTypeHint);
            }
            else
            {
                int[] indices = new int[value.Rank];
                this.EncodeArrayRank(value, 0, indices, forceTypeHint);
            }
        }

        private void EncodeArrayRank(Array value, int rank, int[] indices, bool forceTypeHint)
        {
            this.AppendOpenBracket();
            int lowerBound = value.GetLowerBound(rank);
            int upperBound = value.GetUpperBound(rank);
            if (rank == value.Rank - 1)
            {
                for (int index = lowerBound; index <= upperBound; ++index)
                {
                    indices[rank] = index;
                    this.AppendComma(index == lowerBound);
                    this.EncodeValue(value.GetValue(indices), forceTypeHint);
                }
            }
            else
            {
                for (int index = lowerBound; index <= upperBound; ++index)
                {
                    indices[rank] = index;
                    this.AppendComma(index == lowerBound);
                    this.EncodeArrayRank(value, rank + 1, indices, forceTypeHint);
                }
            }
            this.AppendCloseBracket();
        }

        private void EncodeString(string value)
        {
            this.builder.Append('"');
            foreach (char ch in value.ToCharArray())
            {
                switch (ch)
                {
                    case '\b':
                        this.builder.Append("\\b");
                        break;
                    case '\t':
                        this.builder.Append("\\t");
                        break;
                    case '\n':
                        this.builder.Append("\\n");
                        break;
                    case '\f':
                        this.builder.Append("\\f");
                        break;
                    case '\r':
                        this.builder.Append("\\r");
                        break;
                    case '"':
                        this.builder.Append("\\\"");
                        break;
                    case '\\':
                        this.builder.Append("\\\\");
                        break;
                    default:
                        int int32 = Convert.ToInt32(ch);
                        if (int32 >= 32 && int32 <= 126)
                        {
                            this.builder.Append(ch);
                            break;
                        }
                        this.builder.Append("\\u" + Convert.ToString(int32, 16).PadLeft(4, '0'));
                        break;
                }
            }
            this.builder.Append('"');
        }

        private void AppendIndent()
        {
            for (int index = 0; index < this.indent; ++index)
                this.builder.Append('\t');
        }

        private void AppendOpenBrace()
        {
            this.builder.Append('{');
            if (!this.PrettyPrintEnabled)
                return;
            this.builder.Append('\n');
            ++this.indent;
        }

        private void AppendCloseBrace()
        {
            if (this.PrettyPrintEnabled)
            {
                this.builder.Append('\n');
                --this.indent;
                this.AppendIndent();
            }
            this.builder.Append('}');
        }

        private void AppendOpenBracket()
        {
            this.builder.Append('[');
            if (!this.PrettyPrintEnabled)
                return;
            this.builder.Append('\n');
            ++this.indent;
        }

        private void AppendCloseBracket()
        {
            if (this.PrettyPrintEnabled)
            {
                this.builder.Append('\n');
                --this.indent;
                this.AppendIndent();
            }
            this.builder.Append(']');
        }

        private void AppendComma(bool firstItem)
        {
            if (!firstItem)
            {
                this.builder.Append(',');
                if (this.PrettyPrintEnabled)
                    this.builder.Append('\n');
            }
            if (!this.PrettyPrintEnabled)
                return;
            this.AppendIndent();
        }

        private void AppendColon()
        {
            this.builder.Append(':');
            if (!this.PrettyPrintEnabled)
                return;
            this.builder.Append(' ');
        }
    }
}
