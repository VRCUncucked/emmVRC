﻿
using System;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    [Obfuscation(Exclude = true)]
    public class Exclude : Attribute
    {
    }
}
