﻿
using System;
using System.Collections.Generic;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public static class Extensions
    {
        public static bool AnyOfType<TSource>(this IEnumerable<TSource> source, Type expectedType)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (expectedType == (Type)null)
                throw new ArgumentNullException(nameof(expectedType));
            foreach (TSource source1 in source)
            {
                if (expectedType.IsInstanceOfType((object)source1))
                    return true;
            }
            return false;
        }
    }
}
