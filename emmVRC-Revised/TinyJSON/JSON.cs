﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    public static class JSON
    {
        private static readonly Type includeAttrType = typeof(Include);
        private static readonly Type excludeAttrType = typeof(Exclude);
        private static readonly Type decodeAliasAttrType = typeof(DecodeAlias);
        private static readonly Dictionary<string, Type> typeCache = new Dictionary<string, Type>();
        private const BindingFlags instanceBindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        private const BindingFlags staticBindingFlags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
        private static readonly MethodInfo decodeTypeMethod = typeof(JSON).GetMethod("DecodeType", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        private static readonly MethodInfo decodeListMethod = typeof(JSON).GetMethod("DecodeList", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        private static readonly MethodInfo decodeDictionaryMethod = typeof(JSON).GetMethod("DecodeDictionary", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        private static readonly MethodInfo decodeArrayMethod = typeof(JSON).GetMethod("DecodeArray", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
        private static readonly MethodInfo decodeMultiRankArrayMethod = typeof(JSON).GetMethod("DecodeMultiRankArray", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

        public static Variant Load(string json) => json != null ? Decoder.Decode(json) : throw new ArgumentNullException(nameof(json));

        public static string Dump(object data) => JSON.Dump(data, EncodeOptions.None);

        public static string Dump(object data, EncodeOptions options)
        {
            if (data != null)
            {
                Type type = data.GetType();
                if (!type.IsEnum && !type.IsPrimitive && !type.IsArray)
                {
                    foreach (MethodInfo method in type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                    {
                        if (((IEnumerable<object>)method.GetCustomAttributes(false)).AnyOfType<object>(typeof(BeforeEncode)) && method.GetParameters().Length == 0)
                            method.Invoke(data, (object[])null);
                    }
                }
            }
            return Encoder.Encode(data, options);
        }

        public static void MakeInto<T>(Variant data, out T item) => item = JSON.DecodeType<T>(data);

        private static Type FindType(string fullName)
        {
            if (fullName == null)
                return (Type)null;
            Type type1;
            if (JSON.typeCache.TryGetValue(fullName, out type1))
                return type1;
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type type2 = assembly.GetType(fullName);
                if (type2 != (Type)null)
                {
                    JSON.typeCache.Add(fullName, type2);
                    return type2;
                }
            }
            return (Type)null;
        }

        private static T DecodeType<T>(Variant data)
        {
            if (data == null)
                return default(T);
            Type type1 = typeof(T);
            if (type1.IsEnum)
                return (T)Enum.Parse(type1, data.ToString((IFormatProvider)CultureInfo.InvariantCulture));
            if (type1.IsPrimitive || type1 == typeof(string) || type1 == typeof(Decimal))
                return (T)Convert.ChangeType((object)data, type1);
            if (type1 == typeof(Guid))
                return (T)(object)new Guid(data.ToString(CultureInfo.InvariantCulture));
            if (type1.IsArray)
            {
                if (type1.GetArrayRank() == 1)
                    return (T)JSON.decodeArrayMethod.MakeGenericMethod(type1.GetElementType()).Invoke((object)null, new object[1]
                    {
            (object) data
                    });
                if (!(data is ProxyArray proxyArray2))
                    throw new DecodeException("Variant is expected to be a ProxyArray here, but it is not.");
                int[] rankLengths = new int[type1.GetArrayRank()];
                if (!proxyArray2.CanBeMultiRankArray(rankLengths))
                    throw new DecodeException("Error decoding multidimensional array; JSON data doesn't seem fit this structure.");
                Type elementType = type1.GetElementType();
                Array array = !(elementType == (Type)null) ? Array.CreateInstance(elementType, rankLengths) : throw new DecodeException("Array element type is expected to be not null, but it is.");
                MethodInfo methodInfo = JSON.decodeMultiRankArrayMethod.MakeGenericMethod(elementType);
                try
                {
                    methodInfo.Invoke((object)null, new object[4]
                    {
            (object) proxyArray2,
            (object) array,
            (object) 1,
            (object) rankLengths
                    });
                }
                catch (Exception ex)
                {
                    throw new DecodeException("Error decoding multidimensional array. Did you try to decode into an array of incompatible rank or element type?", ex);
                }
                return (T)Convert.ChangeType((object)array, typeof(T));
            }
            if (typeof(IList).IsAssignableFrom(type1))
                return (T)JSON.decodeListMethod.MakeGenericMethod(type1.GetGenericArguments()).Invoke((object)null, new object[1]
                {
          (object) data
                });
            if (typeof(IDictionary).IsAssignableFrom(type1))
                return (T)JSON.decodeDictionaryMethod.MakeGenericMethod(type1.GetGenericArguments()).Invoke((object)null, new object[1]
                {
          (object) data
                });
            string fullName = data is ProxyObject proxyObject ? proxyObject.TypeHint : throw new InvalidCastException("ProxyObject expected when decoding into '" + type1.FullName + "'.");
            T obj1;
            if (fullName != null && fullName != type1.FullName)
            {
                Type type2 = JSON.FindType(fullName);
                if (type2 == (Type)null)
                    throw new TypeLoadException("Could not load type '" + fullName + "'.");
                obj1 = type1.IsAssignableFrom(type2) ? (T)Activator.CreateInstance(type2) : throw new InvalidCastException("Cannot assign type '" + fullName + "' to type '" + type1.FullName + "'.");
                type1 = type2;
            }
            else
                obj1 = Activator.CreateInstance<T>();
            foreach (KeyValuePair<string, Variant> keyValuePair in (IEnumerable<KeyValuePair<string, Variant>>)data)
            {
                FieldInfo fieldInfo = type1.GetField(keyValuePair.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (fieldInfo == (FieldInfo)null)
                {
                    foreach (FieldInfo field in type1.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                    {
                        foreach (object customAttribute in field.GetCustomAttributes(true))
                        {
                            if (JSON.decodeAliasAttrType.IsInstanceOfType(customAttribute) && ((DecodeAlias)customAttribute).Contains(keyValuePair.Key))
                            {
                                fieldInfo = field;
                                break;
                            }
                        }
                    }
                }
                if (fieldInfo != (FieldInfo)null)
                {
                    bool flag = fieldInfo.IsPublic;
                    foreach (object customAttribute in fieldInfo.GetCustomAttributes(true))
                    {
                        if (JSON.excludeAttrType.IsInstanceOfType(customAttribute))
                            flag = false;
                        if (JSON.includeAttrType.IsInstanceOfType(customAttribute))
                            flag = true;
                    }
                    if (flag)
                    {
                        MethodInfo methodInfo = JSON.decodeTypeMethod.MakeGenericMethod(fieldInfo.FieldType);
                        if (type1.IsValueType)
                        {
                            object obj2 = (object)obj1;
                            fieldInfo.SetValue(obj2, methodInfo.Invoke((object)null, new object[1]
                            {
                (object) keyValuePair.Value
                            }));
                            obj1 = (T)obj2;
                        }
                        else
                            fieldInfo.SetValue((object)obj1, methodInfo.Invoke((object)null, new object[1]
                            {
                (object) keyValuePair.Value
                            }));
                    }
                }
                PropertyInfo propertyInfo = type1.GetProperty(keyValuePair.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (propertyInfo == (PropertyInfo)null)
                {
                    foreach (PropertyInfo property in type1.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                    {
                        foreach (object customAttribute in property.GetCustomAttributes(false))
                        {
                            if (JSON.decodeAliasAttrType.IsInstanceOfType(customAttribute) && ((DecodeAlias)customAttribute).Contains(keyValuePair.Key))
                            {
                                propertyInfo = property;
                                break;
                            }
                        }
                    }
                }
                if (propertyInfo != (PropertyInfo)null && propertyInfo.CanWrite && ((IEnumerable<object>)propertyInfo.GetCustomAttributes(false)).AnyOfType<object>(JSON.includeAttrType))
                {
                    MethodInfo methodInfo = JSON.decodeTypeMethod.MakeGenericMethod(propertyInfo.PropertyType);
                    if (type1.IsValueType)
                    {
                        object obj2 = (object)obj1;
                        propertyInfo.SetValue(obj2, methodInfo.Invoke((object)null, new object[1]
                        {
              (object) keyValuePair.Value
                        }), (object[])null);
                        obj1 = (T)obj2;
                    }
                    else
                        propertyInfo.SetValue((object)obj1, methodInfo.Invoke((object)null, new object[1]
                        {
              (object) keyValuePair.Value
                        }), (object[])null);
                }
            }
            foreach (MethodInfo method in type1.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (((IEnumerable<object>)method.GetCustomAttributes(false)).AnyOfType<object>(typeof(AfterDecode)))
                {
                    method.Invoke(obj1, method.GetParameters().Length == 0 ? null : new object[] { data });
                }
            }
            return obj1;
        }

        private static List<T> DecodeList<T>(Variant data)
        {
            List<T> objList = new List<T>();
            if (!(data is ProxyArray proxyArray))
                throw new DecodeException("Variant is expected to be a ProxyArray here, but it is not.");
            foreach (Variant data1 in (IEnumerable<Variant>)proxyArray)
                objList.Add(JSON.DecodeType<T>(data1));
            return objList;
        }

        private static Dictionary<TKey, TValue> DecodeDictionary<TKey, TValue>(Variant data)
        {
            Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();
            Type type = typeof(TKey);
            if (!(data is ProxyObject proxyObject))
                throw new DecodeException("Variant is expected to be a ProxyObject here, but it is not.");
            foreach (KeyValuePair<string, Variant> keyValuePair in (IEnumerable<KeyValuePair<string, Variant>>)proxyObject)
            {
                TKey key = (TKey)(type.IsEnum ? Enum.Parse(type, keyValuePair.Key) : Convert.ChangeType((object)keyValuePair.Key, type));
                TValue obj = JSON.DecodeType<TValue>(keyValuePair.Value);
                dictionary.Add(key, obj);
            }
            return dictionary;
        }

        private static T[] DecodeArray<T>(Variant data)
        {
            T[] objArray = data is ProxyArray proxyArray ? new T[proxyArray.Count] : throw new DecodeException("Variant is expected to be a ProxyArray here, but it is not.");
            int num = 0;
            foreach (Variant data1 in (IEnumerable<Variant>)proxyArray)
                objArray[num++] = JSON.DecodeType<T>(data1);
            return objArray;
        }

        private static void DecodeMultiRankArray<T>(
          ProxyArray arrayData,
          Array array,
          int arrayRank,
          int[] indices)
        {
            int count = arrayData.Count;
            for (int index = 0; index < count; ++index)
            {
                indices[arrayRank - 1] = index;
                if (arrayRank < array.Rank)
                    JSON.DecodeMultiRankArray<T>(arrayData[index] as ProxyArray, array, arrayRank + 1, indices);
                else
                    array.SetValue((object)JSON.DecodeType<T>(arrayData[index]), indices);
            }
        }

        public static void SupportTypeForAOT<T>()
        {
            JSON.DecodeType<T>((Variant)null);
            JSON.DecodeList<T>((Variant)null);
            JSON.DecodeArray<T>((Variant)null);
            JSON.DecodeDictionary<short, T>((Variant)null);
            JSON.DecodeDictionary<ushort, T>((Variant)null);
            JSON.DecodeDictionary<int, T>((Variant)null);
            JSON.DecodeDictionary<uint, T>((Variant)null);
            JSON.DecodeDictionary<long, T>((Variant)null);
            JSON.DecodeDictionary<ulong, T>((Variant)null);
            JSON.DecodeDictionary<float, T>((Variant)null);
            JSON.DecodeDictionary<double, T>((Variant)null);
            JSON.DecodeDictionary<Decimal, T>((Variant)null);
            JSON.DecodeDictionary<bool, T>((Variant)null);
            JSON.DecodeDictionary<string, T>((Variant)null);
        }

        private static void SupportValueTypesForAOT()
        {
            JSON.SupportTypeForAOT<short>();
            JSON.SupportTypeForAOT<ushort>();
            JSON.SupportTypeForAOT<int>();
            JSON.SupportTypeForAOT<uint>();
            JSON.SupportTypeForAOT<long>();
            JSON.SupportTypeForAOT<ulong>();
            JSON.SupportTypeForAOT<float>();
            JSON.SupportTypeForAOT<double>();
            JSON.SupportTypeForAOT<Decimal>();
            JSON.SupportTypeForAOT<bool>();
            JSON.SupportTypeForAOT<string>();
        }
    }
}
