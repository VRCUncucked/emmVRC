﻿
using System;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obsolete("Use the AfterDecode attribute instead.")]
    [Obfuscation(Exclude = true)]
    public sealed class Load : AfterDecode
    {
    }
}
