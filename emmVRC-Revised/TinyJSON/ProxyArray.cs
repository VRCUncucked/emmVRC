﻿
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public sealed class ProxyArray : Variant, IEnumerable<Variant>, IEnumerable
    {
        private readonly List<Variant> list;

        public ProxyArray() => this.list = new List<Variant>();

        IEnumerator<Variant> IEnumerable<Variant>.GetEnumerator() => (IEnumerator<Variant>)this.list.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => (IEnumerator)this.list.GetEnumerator();

        public void Add(Variant item) => this.list.Add(item);

        public override Variant this[int index]
        {
            get => this.list[index];
            set => this.list[index] = value;
        }

        public int Count => this.list.Count;

        internal bool CanBeMultiRankArray(int[] rankLengths) => this.CanBeMultiRankArray(0, rankLengths);

        private bool CanBeMultiRankArray(int rank, int[] rankLengths)
        {
            int count1 = this.list.Count;
            rankLengths[rank] = count1;
            if (rank == rankLengths.Length - 1)
                return true;
            if (!(this.list[0] is ProxyArray proxyArray1))
                return false;
            int count2 = proxyArray1.Count;
            for (int index = 1; index < count1; ++index)
            {
                if (!(this.list[index] is ProxyArray proxyArray3) || proxyArray3.Count != count2 || !proxyArray3.CanBeMultiRankArray(rank + 1, rankLengths))
                    return false;
            }
            return true;
        }
    }
}
