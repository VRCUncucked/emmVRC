﻿
using System;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public sealed class ProxyBoolean : Variant
    {
        private readonly bool value;

        public ProxyBoolean(bool value) => this.value = value;

        public override bool ToBoolean(IFormatProvider provider) => this.value;

        public override string ToString(IFormatProvider provider) => !this.value ? "false" : "true";
    }
}
