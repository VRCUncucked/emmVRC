﻿
using System;
using System.Globalization;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public sealed class ProxyNumber : Variant
    {
        private static readonly char[] floatingPointCharacters = new char[2]
        {
      '.',
      'e'
        };
        private readonly IConvertible value;

        public ProxyNumber(IConvertible value) => this.value = value is string str ? ProxyNumber.Parse(str) : value;

        private static IConvertible Parse(string value)
        {
            if (value.IndexOfAny(ProxyNumber.floatingPointCharacters) == -1)
            {
                if (value[0] == '-')
                {
                    long result;
                    if (long.TryParse(value, NumberStyles.Float, (IFormatProvider)NumberFormatInfo.InvariantInfo, out result))
                        return (IConvertible)result;
                }
                else
                {
                    ulong result;
                    if (ulong.TryParse(value, NumberStyles.Float, (IFormatProvider)NumberFormatInfo.InvariantInfo, out result))
                        return (IConvertible)result;
                }
            }
            Decimal result1;
            double result2;
            double result3;
            return Decimal.TryParse(value, NumberStyles.Float, (IFormatProvider)NumberFormatInfo.InvariantInfo, out result1) ? (result1 == 0M && double.TryParse(value, NumberStyles.Float, (IFormatProvider)NumberFormatInfo.InvariantInfo, out result2) && Math.Abs(result2) > double.Epsilon ? (IConvertible)result2 : (IConvertible)result1) : (double.TryParse(value, NumberStyles.Float, (IFormatProvider)NumberFormatInfo.InvariantInfo, out result3) ? (IConvertible)result3 : (IConvertible)0);
        }

        public override bool ToBoolean(IFormatProvider provider) => this.value.ToBoolean(provider);

        public override byte ToByte(IFormatProvider provider) => this.value.ToByte(provider);

        public override char ToChar(IFormatProvider provider) => this.value.ToChar(provider);

        public override Decimal ToDecimal(IFormatProvider provider) => this.value.ToDecimal(provider);

        public override double ToDouble(IFormatProvider provider) => this.value.ToDouble(provider);

        public override short ToInt16(IFormatProvider provider) => this.value.ToInt16(provider);

        public override int ToInt32(IFormatProvider provider) => this.value.ToInt32(provider);

        public override long ToInt64(IFormatProvider provider) => this.value.ToInt64(provider);

        public override sbyte ToSByte(IFormatProvider provider) => this.value.ToSByte(provider);

        public override float ToSingle(IFormatProvider provider) => this.value.ToSingle(provider);

        public override string ToString(IFormatProvider provider) => this.value.ToString(provider);

        public override ushort ToUInt16(IFormatProvider provider) => this.value.ToUInt16(provider);

        public override uint ToUInt32(IFormatProvider provider) => this.value.ToUInt32(provider);

        public override ulong ToUInt64(IFormatProvider provider) => this.value.ToUInt64(provider);
    }
}
