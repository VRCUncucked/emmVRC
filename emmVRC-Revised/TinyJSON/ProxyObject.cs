﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public sealed class ProxyObject : Variant, IEnumerable<KeyValuePair<string, Variant>>, IEnumerable
    {
        public const string TypeHintKey = "@type";
        private readonly Dictionary<string, Variant> dict;

        public ProxyObject() => this.dict = new Dictionary<string, Variant>();

        IEnumerator<KeyValuePair<string, Variant>> IEnumerable<KeyValuePair<string, Variant>>.GetEnumerator() => (IEnumerator<KeyValuePair<string, Variant>>)this.dict.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => (IEnumerator)this.dict.GetEnumerator();

        public void Add(string key, Variant item) => this.dict.Add(key, item);

        public bool TryGetValue(string key, out Variant item) => this.dict.TryGetValue(key, out item);

        public string TypeHint
        {
            get
            {
                Variant variant;
                return this.TryGetValue("@type", out variant) ? variant.ToString((IFormatProvider)CultureInfo.InvariantCulture) : (string)null;
            }
        }

        public override Variant this[string key]
        {
            get => this.dict[key];
            set => this.dict[key] = value;
        }

        public int Count => this.dict.Count;

        public Dictionary<string, Variant>.KeyCollection Keys => this.dict.Keys;

        public Dictionary<string, Variant>.ValueCollection Values => this.dict.Values;
    }
}
