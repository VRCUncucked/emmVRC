﻿
using System;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obsolete("Use the Exclude attribute instead.")]
    [Obfuscation(Exclude = true)]
    public sealed class Skip : Exclude
    {
    }
}
