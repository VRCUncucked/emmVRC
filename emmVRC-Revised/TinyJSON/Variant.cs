﻿
using System;
using System.Globalization;
using System.Reflection;

namespace emmVRC.TinyJSON
{
    [Obfuscation(Exclude = true)]
    public abstract class Variant : IConvertible
    {
        protected static readonly IFormatProvider FormatProvider = (IFormatProvider)new NumberFormatInfo();

        public void Make<T>(out T item) => JSON.MakeInto<T>(this, out item);

        public T Make<T>()
        {
            T obj;
            JSON.MakeInto<T>(this, out obj);
            return obj;
        }

        public string ToJSON() => JSON.Dump((object)this);

        public virtual TypeCode GetTypeCode() => TypeCode.Object;

        public virtual object ToType(Type conversionType, IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to " + conversionType.Name);

        public virtual DateTime ToDateTime(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to DateTime");

        public virtual bool ToBoolean(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Boolean");

        public virtual byte ToByte(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Byte");

        public virtual char ToChar(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Char");

        public virtual Decimal ToDecimal(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Decimal");

        public virtual double ToDouble(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Double");

        public virtual short ToInt16(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Int16");

        public virtual int ToInt32(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Int32");

        public virtual long ToInt64(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Int64");

        public virtual sbyte ToSByte(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to SByte");

        public virtual float ToSingle(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to Single");

        public virtual string ToString(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to String");

        public virtual ushort ToUInt16(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to UInt16");

        public virtual uint ToUInt32(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to UInt32");

        public virtual ulong ToUInt64(IFormatProvider provider) => throw new InvalidCastException("Cannot convert " + this.GetType()?.ToString() + " to UInt64");

        public override string ToString() => this.ToString(Variant.FormatProvider);

        public virtual Variant this[string key]
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }

        public virtual Variant this[int index]
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }

        public static implicit operator bool(Variant variant) => variant.ToBoolean(Variant.FormatProvider);

        public static implicit operator float(Variant variant) => variant.ToSingle(Variant.FormatProvider);

        public static implicit operator double(Variant variant) => variant.ToDouble(Variant.FormatProvider);

        public static implicit operator ushort(Variant variant) => variant.ToUInt16(Variant.FormatProvider);

        public static implicit operator short(Variant variant) => variant.ToInt16(Variant.FormatProvider);

        public static implicit operator uint(Variant variant) => variant.ToUInt32(Variant.FormatProvider);

        public static implicit operator int(Variant variant) => variant.ToInt32(Variant.FormatProvider);

        public static implicit operator ulong(Variant variant) => variant.ToUInt64(Variant.FormatProvider);

        public static implicit operator long(Variant variant) => variant.ToInt64(Variant.FormatProvider);

        public static implicit operator Decimal(Variant variant) => variant.ToDecimal(Variant.FormatProvider);

        public static implicit operator string(Variant variant) => variant.ToString(Variant.FormatProvider);

        public static implicit operator Guid(Variant variant) => new Guid(variant.ToString(Variant.FormatProvider));
    }
}
