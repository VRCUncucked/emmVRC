﻿
using emmVRC.Hacks;
using emmVRC.Libraries;
using emmVRC.Managers;
using emmVRC.Menus;
using emmVRC.Network;
using emmVRCLoader;
using MelonLoader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Windows.Forms;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.XR;
using VRC.Core;

namespace emmVRC
{
    public static class emmVRC
    {
        public static bool Initialized = false;
        public static readonly AwaitProvider AwaitUpdate = new AwaitProvider();

        private static void OnApplicationStart()
        {
            if (System.Environment.CommandLine.Contains("--emmvrc.anniversarymode"))
            {
                emmVRCLoader.Logger.Log("Hello world!");
                emmVRCLoader.Logger.Log("This is the beginning of a new beginning!");
                emmVRCLoader.Logger.Log("Wait... have I said that before?");
            }
            Configuration.Initialize();
            if (System.Environment.CommandLine.Contains("--emmvrc.stealthmode"))
                Configuration.JSONConfig.StealthMode = true;
            if (!(typeof(MelonMod).GetMethod("VRChat_OnUiManagerInit") == (System.Reflection.MethodInfo)null))
                return;
            MelonCoroutines.Start(emmVRC.GetAssembly());
        }

        private static IEnumerator GetAssembly()
        {
            System.Reflection.Assembly assemblyCSharp;
            while (true)
            {
                assemblyCSharp = ((IEnumerable<System.Reflection.Assembly>)System.AppDomain.CurrentDomain.GetAssemblies()).FirstOrDefault<System.Reflection.Assembly>((Func<System.Reflection.Assembly, bool>)(assembly => assembly.GetName().Name == "Assembly-CSharp"));
                if (assemblyCSharp == (System.Reflection.Assembly)null)
                    yield return (object)null;
                else
                    break;
            }
            MelonCoroutines.Start(emmVRC.WaitForUiManagerInit(assemblyCSharp));
        }

        private static IEnumerator WaitForUiManagerInit(System.Reflection.Assembly assemblyCSharp)
        {
            System.Type vrcUiManager = assemblyCSharp.GetType("VRCUiManager");
            System.Reflection.PropertyInfo uiManagerSingleton = ((IEnumerable<System.Reflection.PropertyInfo>)vrcUiManager.GetProperties()).First<System.Reflection.PropertyInfo>((Func<System.Reflection.PropertyInfo, bool>)(pi => pi.PropertyType == vrcUiManager));
            while (uiManagerSingleton.GetValue((object)null) == null)
                yield return (object)null;
            emmVRC.OnUIManagerInit();
        }

        public static void OnUIManagerInit()
        {
            emmVRCLoader.Logger.LogDebug("UI manager initialized");
            int num1 = 0;
            foreach (MonoBehaviour component in GameObject.Find("_Application/ApplicationSetup").GetComponents<MonoBehaviour>())
            {
                if (!((UnityEngine.Object)component.TryCast<Transform>() != (UnityEngine.Object)null) && !((UnityEngine.Object)component.TryCast<ApiCache>() != (UnityEngine.Object)null))
                {
                    emmVRCLoader.Logger.LogDebug(component.GetType().Name);
                    bool flag = false;
                    foreach (Il2CppSystem.Reflection.FieldInfo field in (Il2CppArrayBase<Il2CppSystem.Reflection.FieldInfo>)component.GetIl2CppType().GetFields())
                    {
                        if (!flag && field.FieldType == Il2CppType.Of<int>())
                        {
                            num1 = field.GetValue((Il2CppSystem.Object)component).Unbox<int>();
                            flag = true;
                        }
                    }
                }
            }
            emmVRCLoader.Logger.Log("VRChat build is: " + num1.ToString());
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            string str1 = (string)typeof(MelonLoader.BuildInfo).GetField("Version").GetValue((object)null);
            string str2 = (string)typeof(emmVRCLoader.BuildInfo).GetField("Version").GetValue((object)null);
            if (((IEnumerable<string>)Objects.Attributes.IncompatibleMelonLoaderVersions).Contains<string>(str1))
            {
                emmVRCLoader.Logger.LogError("You are using an incompatible version of MelonLoader: v" + str1 + ". Please install v" + Objects.Attributes.TargetMelonLoaderVersion + " or newer, via the instructions in our Discord under the #how-to channel. emmVRC will not start.");
                int num2 = (int)MessageBox.Show("You are using an incompatible version of MelonLoader: v" + str1 + ". Please install v" + Objects.Attributes.TargetMelonLoaderVersion + " or newer, via the instructions in our Discord under the #how-to channel. emmVRC will not start.", nameof(emmVRC), MessageBoxButtons.OK, MessageBoxIcon.Hand);
                stopwatch.Stop();
            }
            else if (((IEnumerable<string>)Objects.Attributes.IncompatibleemmVRCLoaderVersions).Contains<string>(str2))
            {
                try
                {
                    stopwatch.Stop();
                    WebClient webClient = new WebClient();
                    string str3 = "";
                    foreach (string file in System.IO.Directory.GetFiles(Il2CppSystem.IO.Path.Combine(System.Environment.CurrentDirectory, "Mods")))
                    {
                        if (file.Contains(nameof(emmVRC)))
                        {
                            str3 = file;
                            System.IO.File.Delete(file);
                        }
                    }
                    webClient.DownloadFile("https://dl.emmvrc.com/downloads/emmVRCLoader.dll", str3 != "" ? str3 : Il2CppSystem.IO.Path.Combine(System.Environment.CurrentDirectory, "Mods/emmVRCLoader.dll"));
                    int num2 = (int)MessageBox.Show("The newest emmVRCLoader has been downloaded to your Mods folder. To use emmVRC, restart your game. If the problem persists, remove any current emmVRCLoader files, and download the latest from #loader-updates in the emmVRC Discord.", nameof(emmVRC), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (System.Exception ex)
                {
                    stopwatch.Stop();
                    emmVRCLoader.Logger.LogError("Attempt to download the new loader failed. You must download the latest from https://dl.emmvrc.com/downloads/emmVRCLoader.dll manually.");
                    emmVRCLoader.Logger.LogError("Error: " + ex.ToString());
                    int num2 = (int)MessageBox.Show("You are using an incompatible version of emmVRCLoader: v" + str2 + ". Please install v" + Objects.Attributes.TargetemmVRCLoaderVersion + " or greater, from the #loader-updates channel in the emmVRC Discord. emmVRC cannot start.", nameof(emmVRC), MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            else if (num1 < Objects.Attributes.LastTestedBuildNumber)
            {
                stopwatch.Stop();
                emmVRCLoader.Logger.LogError("You are using an older version of VRChat than supported by emmVRC: " + num1.ToString() + ". Please update VRChat through Steam or Oculus to build " + Objects.Attributes.LastTestedBuildNumber.ToString() + ". emmVRC will not start.");
                int num2 = (int)MessageBox.Show("You are using an older version of VRChat than supported by emmVRC: " + num1.ToString() + ". Please update VRChat through Steam or Oculus to build " + Objects.Attributes.LastTestedBuildNumber.ToString() + ". emmVRC will not start.", nameof(emmVRC), MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                emmVRCLoader.Logger.LogDebug("[NOTICE] emmVRC Debug Mode is intended for development or troubleshooting purposes. Using it in regular play can result in unexpected lag or other issues. If you are seeing this and are not sure what to do, check your launch options for `--emmvrc.debug`.");
                emmVRCLoader.Logger.Log("emmVRC module: " + System.BitConverter.ToString(MD5.Create().ComputeHash((System.IO.Stream)System.IO.File.OpenRead(Il2CppSystem.IO.Path.Combine(System.Environment.CurrentDirectory, "Dependencies/emmVRC.dll")))).Replace("-", "").ToLower());
                if (System.Environment.CommandLine.Contains("--emmvrc.devmode"))
                    emmVRCLoader.Logger.Log("emmVRC is currently running with developer mode enabled. This is not intended for casual use, and should be removed from your launch options.");
                if (Configuration.JSONConfig.emmVRCNetworkEnabled)
                {
                    emmVRCLoader.Logger.Log("Initializing network...");
                    try
                    {
                        NetworkClient.InitializeClient();
                    }
                    catch (System.Exception ex)
                    {
                        emmVRCLoader.Logger.LogError("Error occured while initializing network: " + ex.ToString());
                    }
                }
                emmVRCLoader.Logger.LogDebug("Initializing debug manager");
                DebugManager.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing custom menu music...");
                    MelonCoroutines.Start(CustomMenuMusic.Initialize());
                }
                emmVRCLoader.Logger.LogDebug("Initializing resources...");
                MelonCoroutines.Start(Resources.LoadResources());
                emmVRCLoader.Logger.LogDebug("Initializing UI Elements tweaks...");
                MelonCoroutines.Start(UIElementsMenu.Initialize());
                emmVRCLoader.Logger.LogDebug("Initializing mod compatibility...");
                ModCompatibility.Initialize();
                if (Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Stealth Mode report world menu tweaks...");
                    ReportWorldMenu.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing Functions menu...");
                FunctionsMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing user interact menu...");
                UserTweaksMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing World Tweaks menu...");
                WorldTweaksMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Player Tweaks menu...");
                PlayerTweaksMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Disabled Buttons menu...");
                DisabledButtonMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Programs menu...");
                ProgramMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Credits menu...");
                CreditsMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Instance History menu...");
                InstanceHistoryMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Player History menu...");
                PlayerHistoryMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Settings menu...");
                SettingsMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Debug menu...");
                DebugMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Unscaled UI Tweaks...");
                UnscaledUITweaks.Process();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Social Menu Functions menu...");
                    SocialMenuFunctions.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing User List Refresh button...");
                    UserListRefresh.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing World Functions menu...");
                    WorldFunctions.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Player Notes module...");
                    PlayerNotes.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing World Notes module...");
                    WorldNotes.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Loading Screen Functions menu...");
                    LoadingScreenMenu.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing Keybind Changing module...");
                KeybindChanger.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Keybind manager...");
                KeybindManager.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Notification manager...");
                Managers.NotificationManager.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Processing Shortcut Menu tweaks...");
                    MelonCoroutines.Start(ShortcutMenuButtons.Process());
                }
                emmVRCLoader.Logger.LogDebug("Initializing User Interact Menu tweaks...");
                UserInteractMenuButtons.Initialize();
                emmVRCLoader.Logger.LogDebug("Patching logout function...");
                LogoutPatch.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Quick Menu Clock module...");
                    InfoBarClock.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Quick Menu Status module...");
                    InfoBarStatus.Initialize();
                }
                if (!System.Environment.CurrentDirectory.Contains("vrchat-vrchat"))
                {
                    emmVRCLoader.Logger.LogDebug("Initializing FBT saving module...");
                    FBTSaving.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing Flight and Noclip modules...");
                Flight.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Speed module...");
                Speed.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing ESP module...");
                ESP.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Waypoint menu...");
                WaypointsMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Avatar Menu tweaks...");
                AvatarMenu.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Info Spoofing and Hiding module...");
                    InfoSpoofing.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing Nameplate Color Changing module...");
                Nameplates.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing Flashlight and Headlight module...");
                FlashlightMenu.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing FOV changing module...");
                FOV.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing FPS Cap changing module...");
                FPS.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Action Menu tweaks...");
                    ActionMenuTweaks.Apply();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Volume Slider tweaks...");
                    Volume.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing message system...");
                MessageManager.Initialize();
                if (!ModCompatibility.VRCMinus)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Main Menu tweaks...");
                    MainMenuTweaks.Initialize();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Action Menu module...");
                    MelonCoroutines.Start(ActionMenuFunctions.Initialize());
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    if (Configuration.JSONConfig.VRHUDInDesktop || XRDevice.isPresent)
                        MelonCoroutines.Start(VRHUD.Initialize());
                    else
                        MelonCoroutines.Start(DesktopHUD.Initialize());
                }
                emmVRCLoader.Logger.LogDebug("Initializing Third Person module...");
                ThirdPerson.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Master Icon module...");
                    MasterCrown.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing Avatar Favorite module...");
                CustomAvatarFavorites.Initialize();
                if (Configuration.JSONConfig.emmVRCNetworkEnabled)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing network...");
                    MelonCoroutines.Start(emmVRC.loadNetworked());
                }
                emmVRCLoader.Logger.LogDebug("Processing OnSceneLoaded events...");
                SceneManager.add_sceneLoaded((UnityAction<Scene, LoadSceneMode>)(System.Action<Scene, LoadSceneMode>)((asa, asd) => emmVRC.OnSceneLoaded(asa, asd)));
                emmVRCLoader.Logger.LogDebug("Initializing Avatar Permissions module...");
                AvatarPermissionManager.Initialize();
                emmVRCLoader.Logger.LogDebug("Initializing User Permissions module...");
                UserPermissionManager.Initialize();
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing UI Color Changing module...");
                    ColorChanger.ApplyIfApplicable();
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing Tab Menu...");
                    MelonCoroutines.Start(TabMenu.Initialize());
                }
                if (!Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing User Info tweaks...");
                    UserInfoTweaks.Initialize();
                }
                emmVRCLoader.Logger.LogDebug("Initializing Emoji Favourites system...");
                MelonCoroutines.Start(EmojiFavourites.Initialize());
                emmVRCLoader.Logger.LogDebug("Initializing alarm clock module...");
                MelonCoroutines.Start(AlarmClock.Initialize());
                emmVRCLoader.Logger.LogDebug("Initializing hooks...");
                Hooking.Initialize();
                if (!Configuration.JSONConfig.StealthMode && !ModCompatibility.CameraPlus)
                {
                    emmVRCLoader.Logger.LogDebug("Initializing CameraPlus...");
                    MelonCoroutines.Start(CameraPlus.Initialize());
                }
                stopwatch.Stop();
                emmVRCLoader.Logger.Log("Initialization is successful in " + stopwatch.Elapsed.ToString("ss\\.f", (System.IFormatProvider)null) + "s. Welcome to emmVRC!");
                emmVRCLoader.Logger.Log("You are running version " + Objects.Attributes.Version);
                if (Configuration.JSONConfig.StealthMode)
                {
                    emmVRCLoader.Logger.Log("You have emmVRC's Stealth Mode enabled. To access the functions menu, press the \"Report World\" button. Most visual functions of emmVRC have been disabled.");
                    if (System.Environment.CommandLine.Contains("--emmvrc.stealthmode"))
                        emmVRCLoader.Logger.Log("To disable stealth mode, remove '--emmvrc.stealthmode' from your launch options. Then, disable \"Stealth Mode\" in the emmVRC Settings, and restart the game.");
                    else
                        emmVRCLoader.Logger.Log("To disable stealth mode, disable \"Stealth Mode\" in the emmVRC Settings, and restart the game.");
                }
                if (Configuration.JSONConfig.FunctionsButtonX == 5 && Configuration.JSONConfig.FunctionsButtonY == 2 && !Configuration.JSONConfig.DisableRankToggleButton)
                {
                    QMSingleButton qmSingleButton = new QMSingleButton("ShortcutMenu", 1, 0, "", (System.Action)null, "");
                    FunctionsMenu.baseMenu.menuEntryButton.getGameObject().GetComponent<RectTransform>().anchoredPosition = qmSingleButton.getGameObject().GetComponent<RectTransform>().anchoredPosition;
                    qmSingleButton.DestroyMe();
                    Configuration.JSONConfig.FunctionsButtonX = 0;
                    Configuration.JSONConfig.FunctionsButtonY = 1;
                    Configuration.SaveConfig();
                    FunctionsMenu.baseMenu.menuEntryButton.setLocation(Mathf.FloorToInt(0.0f), Mathf.FloorToInt(1f));
                }
                if (Configuration.JSONConfig.LogoButtonX == 5 && Configuration.JSONConfig.LogoButtonY == -1 && !Configuration.JSONConfig.DisableVRCPlusQMButtons)
                {
                    Configuration.JSONConfig.LogoButtonEnabled = false;
                    Configuration.SaveConfig();
                    MelonCoroutines.Start(ShortcutMenuButtons.Process());
                }
                emmVRC.Initialized = true;
                DebugMenu.PopulateDebugMenu();
            }
        }

        public static IEnumerator loadNetworked()
        {
            while (NetworkClient.webToken == null)
                yield return (object)new WaitForSeconds(1.5f);
            CustomAvatarFavorites.PopulateList().NoAwait("PopulateList");
        }

        public static void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            MirrorTweaks.FetchMirrors();
            PedestalTweaks.FetchPedestals();
            PlayerTweaksMenu.SpeedToggle.setToggleState(false, true);
            PlayerTweaksMenu.FlightToggle.setToggleState(false, true);
            PlayerTweaksMenu.NoclipToggle.setToggleState(false, true);
            PlayerTweaksMenu.ESPToggle.setToggleState(false, true);
            FlashlightMenu.toggleFlashlight.setToggleState(false);
            FlashlightMenu.toggleHeadlight.setToggleState(false, true);
            MelonCoroutines.Start(WaypointsMenu.LoadWorld());
            MelonCoroutines.Start(InstanceHistoryMenu.EnteredWorld());
            if ((UnityEngine.Object)InfoBarClock.clockText != (UnityEngine.Object)null && !Configuration.JSONConfig.StealthMode)
                InfoBarClock.instanceTime = 0U;
            if (!Configuration.JSONConfig.StealthMode)
                MelonCoroutines.Start(CustomWorldObjects.OnRoomEnter());
            if (Configuration.JSONConfig.LastVersion != Objects.Attributes.Version)
            {
                Configuration.JSONConfig.LastVersion = Objects.Attributes.Version;
                Configuration.SaveConfig();
            }
            if (ModCompatibility.MultiplayerDynamicBones && Configuration.JSONConfig.GlobalDynamicBonesEnabled)
            {
                Configuration.JSONConfig.GlobalDynamicBonesEnabled = false;
                Configuration.SaveConfig();
                Managers.NotificationManager.AddNotification("You are currently using MultiplayerDynamicBones. emmVRC's Global Dynamic Bones have been disabled, as only one can be used at a time.", "Dismiss", new System.Action(Managers.NotificationManager.DismissCurrentNotification), "", (System.Action)null, Resources.alertSprite);
            }
            PlayerHistoryMenu.currentPlayers = new List<InstancePlayer>();
            CustomAvatarFavorites.CheckForAvatarPedestals().NoAwait();
            ComponentToggle.OnLevelLoad();
            ComponentToggle.Toggle();
            WorldTweaksMenu.DisableOnSceneLoad();
        }

        public static void OnUpdate()
        {
            if (Objects.Attributes.Debug)
                FrameTimeCalculator.Update();
            emmVRC.AwaitUpdate.Flush();
            if (!emmVRC.Initialized)
                return;
            if ((UnityEngine.Object)Resources.onlineSprite != (UnityEngine.Object)null && !Configuration.JSONConfig.WelcomeMessageShown)
            {
                Managers.NotificationManager.AddNotification("Welcome to the new emmVRC! For updates regarding the client, teasers for new features, and bug reports and support, join the Discord!", "Open\nDiscord", (System.Action)(() =>
               {
                   Process.Start("https://discord.gg/SpZSH5Z");
                   Managers.NotificationManager.DismissCurrentNotification();
               }), "Dismiss", (System.Action)(() => Managers.NotificationManager.DismissCurrentNotification()), Resources.alertSprite);
                Configuration.JSONConfig.WelcomeMessageShown = true;
                Configuration.SaveConfig();
            }
            CustomAvatarFavorites.OnUpdate();
        }

        public static void OnApplicationQuit()
        {
            if (!emmVRC.Initialized || NetworkClient.webToken == null)
                return;
            HTTPRequest.get(NetworkClient.baseURL + "/api/authentication/logout").NoAwait("Logout");
        }
    }
}
