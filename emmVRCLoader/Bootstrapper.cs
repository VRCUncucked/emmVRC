﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace emmVRCLoader
{
    public static class Bootstrapper
    {
        private static bool freshlyLoaded;

        public static ModController mod { get; set; }

        public static void Start()
        {
            Logger.Init();
            if ((Environment.CommandLine.Contains("--noemmvrc") || !UpdateManager.ShouldLoadLib()) && !Environment.CommandLine.Contains("--emmvrc.devmode"))
                return;
            try
            {
                UpdateManager.Check();
            }
            catch (Exception ex)
            {
                Logger.Log("[emmVRCLoader] Error occured while trying to update emmVRC: " + ex.ToString());
            }
            if (UpdateManager.downloadedLib == null)
            {
                Logger.Log("[emmVRCLoader] Could not load emmVRC. Sorry...");
            }
            else
            {
                try
                {
                    byte[] downloadedLib = UpdateManager.downloadedLib;
                    UpdateManager.downloadedLib = (byte[])null;
                    foreach (Type loadableType in Bootstrapper.GetLoadableTypes(Assembly.Load(downloadedLib)))
                    {
                        if ("emmVRC".Equals(loadableType.Name))
                        {
                            try
                            {
                                Bootstrapper.mod = new ModController();
                                Bootstrapper.mod.Create(loadableType);
                                Bootstrapper.OnApplicationStart();
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError("[emmVRCLoader] emmVRC failed! " + ex?.ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError("[emmVRCLoader] Something has gone wrong... " + ex.ToString());
                }
            }
        }

        public static void Update()
        {
            if (Bootstrapper.freshlyLoaded)
            {
                Bootstrapper.freshlyLoaded = false;
                Bootstrapper.mod.OnLevelWasInitialized(0);
            }
            Bootstrapper.mod.OnUpdate();
        }

        public static void LateUpdate() => Bootstrapper.mod.OnLateUpdate();

        public static void FixedUpdate() => Bootstrapper.mod.OnFixedUpdate();

        public static void OnGUI() => Bootstrapper.mod.OnGUI();

        public static void OnApplicationStart()
        {
            Logger.Log("[emmVRCLoader] OnApplicationStart called");
            Bootstrapper.mod.OnApplicationStart();
        }

        public static void OnApplicationQuit()
        {
            Logger.Log("[emmVRCLoader] OnApplicationQuit called");
            Bootstrapper.mod.OnApplicationQuit();
        }

        public static void OnLevelWasLoaded(int level)
        {
            Logger.Log("[emmVRCLoader] OnLevelWasLoaded called (" + level.ToString() + ")");
            Bootstrapper.freshlyLoaded = true;
            Bootstrapper.mod.OnLevelWasLoaded(level);
        }

        public static void OnUIManagerInit()
        {
            Logger.Log("[emmVRCLoader] OnUIManagerInit called");
            Bootstrapper.mod.OnUIManagerInit();
        }

        public static IEnumerable<Type> GetLoadableTypes(Assembly assembly)
        {
            if (assembly == (Assembly)null)
                throw new ArgumentNullException(nameof(assembly));
            try
            {
                return (IEnumerable<Type>)assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                Logger.LogError("[emmVRCLoader] An error occured while getting types from assembly " + assembly.GetName().Name + ". Returning types from error.\n" + ex?.ToString());
                return ((IEnumerable<Type>)ex.Types).Where<Type>((Func<Type, bool>)(t => t != (Type)null));
            }
        }
    }
}
