﻿
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;

namespace emmVRCLoader
{
    public class LoaderCheck
    {
        public static string Check() => Convert.ToBase64String(MD5.Create().ComputeHash(File.ReadAllBytes(Path.Combine(Environment.CurrentDirectory, "Mods/", Process.GetCurrentProcess().MainModule.FileName))));
    }
}
