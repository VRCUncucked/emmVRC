﻿
using MelonLoader;
using System;
using System.IO;

namespace emmVRCLoader
{
    public static class Logger
    {
        private static bool consoleEnabled = true;
        private static StreamWriter log;
        private static string fileprefix = "emmVRC_";
        private static int errorCount = 0;

        internal static void Init()
        {
        }

        internal static void Stop()
        {
            if (Logger.log == null)
                return;
            Logger.log.Close();
        }

        internal static void CleanOld(DirectoryInfo logDirInfo)
        {
        }

        internal static string GetTimestamp() => DateTime.Now.ToString("HH:mm:ss.fff");

        public static void Log(string s) => MelonLogger.Msg(s);

        public static void Log(string s, params object[] args) => MelonLogger.Msg(string.Format(s, args));

        public static void LogError(string s)
        {
            int foregroundColor = (int)Console.ForegroundColor;
            if (Logger.errorCount < (int)byte.MaxValue)
            {
                MelonLogger.Error(s);
                ++Logger.errorCount;
            }
            if (Logger.errorCount != (int)byte.MaxValue)
                return;
            MelonLogger.Error("The error limit has been reached. Please report this issue to Emilia!");
            ++Logger.errorCount;
        }

        public static void LogError(string s, params object[] args)
        {
            int foregroundColor = (int)Console.ForegroundColor;
            if (Logger.errorCount < (int)byte.MaxValue)
            {
                Logger.GetTimestamp();
                MelonLogger.Error(string.Format(s, args));
                ++Logger.errorCount;
            }
            if (Logger.errorCount != (int)byte.MaxValue)
                return;
            MelonLogger.Error("The error limit has been reached. Please report this issue to Emilia!");
            ++Logger.errorCount;
        }

        public static void LogDebug(string s)
        {
            if (!Environment.CommandLine.Contains("--emmvrc.debug"))
                return;
            MelonLogger.Msg("[Debug] " + s);
        }

        public static void LogDebug(string s, params object[] args)
        {
            if (!Environment.CommandLine.Contains("--emmvrc.debug"))
                return;
            MelonLogger.Msg("[Debug] " + string.Format(s, args));
        }
    }
}
