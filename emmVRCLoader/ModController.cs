﻿
using System;
using System.Reflection;

namespace emmVRCLoader
{
    [Obfuscation(Exclude = true)]
    public class ModController
    {
        private MethodInfo onApplicationStartMethod;
        private MethodInfo onApplicationQuitMethod;
        private MethodInfo onLevelWasLoadedMethod;
        private MethodInfo onLevelWasInitializedMethod;
        private MethodInfo onUpdateMethod;
        private MethodInfo onFixedUpdateMethod;
        private MethodInfo onLateUpdateMethod;
        private MethodInfo onGUIMethod;
        private MethodInfo onModSettingsApplied;
        private MethodInfo onUIManagerInit;

        public void Create(Type mod)
        {
            foreach (MethodInfo method in mod.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (method.Name.Equals("OnApplicationStart") && method.GetParameters().Length == 0)
                    this.onApplicationStartMethod = method;
                if (method.Name.Equals("OnApplicationQuit") && method.GetParameters().Length == 0)
                    this.onApplicationQuitMethod = method;
                if (method.Name.Equals("OnLevelWasLoaded") && method.GetParameters().Length == 1 && method.GetParameters()[0].ParameterType == typeof(int))
                    this.onLevelWasLoadedMethod = method;
                if (method.Name.Equals("OnLevelWasInitialized") && method.GetParameters().Length == 1 && method.GetParameters()[0].ParameterType == typeof(int))
                    this.onLevelWasInitializedMethod = method;
                if (method.Name.Equals("OnUpdate") && method.GetParameters().Length == 0)
                    this.onUpdateMethod = method;
                if (method.Name.Equals("OnFixedUpdate") && method.GetParameters().Length == 0)
                    this.onFixedUpdateMethod = method;
                if (method.Name.Equals("OnLateUpdate") && method.GetParameters().Length == 0)
                    this.onLateUpdateMethod = method;
                if (method.Name.Equals("OnGUI") && method.GetParameters().Length == 0)
                    this.onGUIMethod = method;
                if (method.Name.Equals("OnModSettingsApplied") && method.GetParameters().Length == 0)
                    this.onModSettingsApplied = method;
                if (method.Name.Equals("OnUIManagerInit") && method.GetParameters().Length == 0)
                    this.onUIManagerInit = method;
            }
        }

        public virtual void OnApplicationStart() => this.onApplicationStartMethod?.Invoke((object)null, new object[0]);

        public virtual void OnApplicationQuit() => this.onApplicationQuitMethod?.Invoke((object)null, new object[0]);

        public virtual void OnLevelWasLoaded(int level) => this.onLevelWasLoadedMethod?.Invoke((object)null, new object[1]
        {
      (object) level
        });

        public virtual void OnLevelWasInitialized(int level) => this.onLevelWasInitializedMethod?.Invoke((object)null, new object[1]
        {
      (object) level
        });

        public virtual void OnUpdate() => this.onUpdateMethod?.Invoke((object)null, new object[0]);

        public virtual void OnFixedUpdate() => this.onFixedUpdateMethod?.Invoke((object)null, new object[0]);

        public virtual void OnLateUpdate() => this.onLateUpdateMethod?.Invoke((object)null, new object[0]);

        public virtual void OnGUI() => this.onGUIMethod?.Invoke((object)null, new object[0]);

        public virtual void OnModSettingsApplied() => this.onModSettingsApplied?.Invoke((object)null, new object[0]);

        public virtual void OnUIManagerInit() => this.onUIManagerInit?.Invoke((object)null, new object[0]);
    }
}
