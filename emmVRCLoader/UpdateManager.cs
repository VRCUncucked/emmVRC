﻿
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace emmVRCLoader
{
    public class UpdateManager
    {
        private static string WebsiteURL = "https://dl.emmvrc.com/update.php";
        public static bool LoaderNeedsUpdate = false;
        public static bool HasCheckedLoader = false;
        public static bool HasCheckedUpdate = false;
        public static bool HasCheckedLoadLib = false;
        public static bool LoadLibCheck = true;
        internal static byte[] downloadedLib;

        public static void Check()
        {
            if (UpdateManager.HasCheckedUpdate)
                return;
                                                                                    Logger.Log(Environment.CurrentDirectory);
            UpdateManager.downloadedLib = System.IO.File.ReadAllBytes(Path.Combine(Environment.CurrentDirectory, "Dependencies/emmVRC.dll"));
            UpdateManager.HasCheckedUpdate = true;
        }

        public static void LoaderCheck() => UpdateManager.HasCheckedLoader = true;

        public static void DownloadLib()
        {
            try
            {
                if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "Dependencies")))
                    Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "Dependencies"));
                HttpWebResponse response = (HttpWebResponse)WebRequest.Create(UpdateManager.WebsiteURL + "?libdownload").GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Logger.LogError("[emmVRCLoader] Error occured while fetching emmVRC: " + response.StatusDescription);
                }
                else
                {
                    UpdateManager.downloadedLib = Convert.FromBase64String(new StreamReader(response.GetResponseStream()).ReadToEnd());
                    System.IO.File.WriteAllBytes(Path.Combine(Environment.CurrentDirectory, "Dependencies/emmVRC.dll"), UpdateManager.downloadedLib);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }

        public static bool IsLibLatest()
        {
            if (Environment.CommandLine.Contains("--emmvrc.devmode"))
                return true;
            if (!System.IO.File.Exists(Path.Combine(Environment.CurrentDirectory, "Dependencies/emmVRC.dll")))
                return false;
            try
            {
                string lower = BitConverter.ToString(MD5.Create().ComputeHash((Stream)System.IO.File.OpenRead(Path.Combine(Environment.CurrentDirectory, "Dependencies/emmVRC.dll")))).Replace("-", "").ToLower();
                HttpWebResponse response = (HttpWebResponse)WebRequest.Create(UpdateManager.WebsiteURL + "?lib=" + lower).GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                    return new StreamReader(response.GetResponseStream()).ReadToEnd() == "false";
            }
            catch (Exception ex)
            {
                Exception exception = new Exception();
                return true;
            }
            return false;
        }

        public static bool ShouldLoadLib()
        {
            if (!UpdateManager.HasCheckedLoadLib)
            {
                try
                {
                    Logger.Log("[emmVRCLoader] Checking if emmVRC can Load...");
                    HttpWebResponse response = (HttpWebResponse)WebRequest.Create(UpdateManager.WebsiteURL + "?shouldload").GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        if (new StreamReader(response.GetResponseStream()).ReadToEnd() != "true")
                        {
                            Logger.LogError("[emmVRCLoader] emmVRC can't be loaded...");
                            UpdateManager.LoadLibCheck = false;
                        }
                        else
                            Logger.Log("[emmVRCLoader] emmVRC can be loaded!");
                    }
                }
                catch (Exception ex)
                {
                    Exception exception = new Exception();
                    Logger.LogError("emmVRC could not be accessed. Loading from disk, if possible...");
                }
            }
            return UpdateManager.LoadLibCheck;
        }
    }
}
