﻿
using MelonLoader;
using System.Reflection;

[assembly: MelonInfo(typeof(emmVRCLoader.emmVRCLoader), emmVRCLoader.BuildInfo.Name, emmVRCLoader.BuildInfo.Version, emmVRCLoader.BuildInfo.Author, emmVRCLoader.BuildInfo.DownloadLink)]
[assembly: MelonGame("VRChat", "VRChat")]

namespace emmVRCLoader
{
    [Obfuscation(Exclude = true)]
    public static class BuildInfo
    {
        public const string Name = "emmVRCLoader";
        public const string Author = "The emmVRC Team";
        public const string Company = "emmVRC";
        public const string Version = "1.4.0";
        public const string DownloadLink = "https://dl.emmvrc.com/downloads/emmVRCLoader.dll";
    }

    [Obfuscation(Exclude = true)]
    public class emmVRCLoader : MelonMod
    {
        private bool is_pressed;

        public override void OnApplicationStart() => Bootstrapper.Start();

        public override void OnLevelWasLoaded(int level)
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnLevelWasLoaded(level);
        }

        public override void OnLevelWasInitialized(int level)
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnLevelWasInitialized(level);
        }

        public override void OnUpdate()
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnUpdate();
        }

        public override void OnFixedUpdate()
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnFixedUpdate();
        }

        public override void OnLateUpdate()
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnLateUpdate();
        }

        public override void OnGUI()
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnGUI();
        }

        public override void OnApplicationQuit()
        {
            if (Bootstrapper.mod == null)
                return;
            Bootstrapper.mod.OnApplicationQuit();
        }
    }
}
